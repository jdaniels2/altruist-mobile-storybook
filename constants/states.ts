export const states = [
  {
    id: 'AL',
    name: 'Alabama',
    code: 'AL',
  },
  {
    id: 'AK',
    name: 'Alaska',
    code: 'AK',
  },
  {
    id: 'AZ',
    name: 'Arizona',
    code: 'AZ',
  },
  {
    id: 'AR',
    name: 'Arkansas',
    code: 'AR',
  },
  {
    id: 'CA',
    name: 'California',
    code: 'CA',
  },
  {
    id: 'CO',
    name: 'Colorado',
    code: 'CO',
  },
  {
    id: 'CT',
    name: 'Connecticut',
    code: 'CT',
  },
  {
    id: 'DE',
    name: 'Delaware',
    code: 'DE',
  },
  {
    id: 'FL',
    name: 'Florida',
    code: 'FL',
  },
  {
    id: 'GA',
    name: 'Georgia',
    code: 'GA',
  },
  {
    id: 'HI',
    name: 'Hawaii',
    code: 'HI',
  },
  {
    id: 'ID',
    name: 'Idaho',
    code: 'ID',
  },
  {
    id: 'IL',
    name: 'Illinois',
    code: 'IL',
  },
  {
    id: 'IN',
    name: 'Indiana',
    code: 'IN',
  },
  {
    id: 'IA',
    name: 'Iowa',
    code: 'IA',
  },
  {
    id: 'KS',
    name: 'Kansas',
    code: 'KS',
  },
  {
    id: 'KY',
    name: 'Kentucky',
    code: 'KY',
  },
  {
    id: 'LA',
    name: 'Louisiana',
    code: 'LA',
  },
  {
    id: 'ME',
    name: 'Maine',
    code: 'ME',
  },
  {
    id: 'MD',
    name: 'Maryland',
    code: 'MD',
  },
  {
    id: 'MA',
    name: 'Massachusetts',
    code: 'MA',
  },
  {
    id: 'MI',
    name: 'Michigan',
    code: 'MI',
  },
  {
    id: 'MN',
    name: 'Minnesota',
    code: 'MN',
  },
  {
    id: 'MS',
    name: 'Mississippi',
    code: 'MS',
  },
  {
    id: 'MO',
    name: 'Missouri',
    code: 'MO',
  },
  {
    id: 'MT',
    name: 'Montana',
    code: 'MT',
  },
  {
    id: 'NE',
    name: 'Nebraska',
    code: 'NE',
  },
  {
    id: 'NV',
    name: 'Nevada',
    code: 'NV',
  },
  {
    id: 'NH',
    name: 'New Hampshire',
    code: 'NH',
  },
  {
    id: 'NJ',
    name: 'New Jersey',
    code: 'NJ',
  },
  {
    id: 'NM',
    name: 'New Mexico',
    code: 'NM',
  },
  {
    id: 'NY',
    name: 'New York',
    code: 'NY',
  },
  {
    id: 'NC',
    name: 'North Carolina',
    code: 'NC',
  },
  {
    id: 'ND',
    name: 'North Dakota',
    code: 'ND',
  },
  {
    id: 'OH',
    name: 'Ohio',
    code: 'OH',
  },
  {
    id: 'OK',
    name: 'Oklahoma',
    code: 'OK',
  },
  {
    id: 'OR',
    name: 'Oregon',
    code: 'OR',
  },
  {
    id: 'PA',
    name: 'Pennsylvania',
    code: 'PA',
  },
  {
    id: 'RI',
    name: 'Rhode Island',
    code: 'RI',
  },
  {
    id: 'SC',
    name: 'South Carolina',
    code: 'SC',
  },
  {
    id: 'SD',
    name: 'South Dakota',
    code: 'SD',
  },
  {
    id: 'TN',
    name: 'Tennessee',
    code: 'TN',
  },
  {
    id: 'TX',
    name: 'Texas',
    code: 'TX',
  },
  {
    id: 'UT',
    name: 'Utah',
    code: 'UT',
  },
  {
    id: 'VT',
    name: 'Vermont',
    code: 'VT',
  },
  {
    id: 'VA',
    name: 'Virginia',
    code: 'VA',
  },
  {
    id: 'WA',
    name: 'Washington',
    code: 'WA',
  },
  {
    id: 'WV',
    name: 'West Virginia',
    code: 'WV',
  },
  {
    id: 'WI',
    name: 'Wisconsin',
    code: 'WI',
  },
  {
    id: 'WY',
    name: 'Wyoming',
    code: 'WY',
  },
];

export const statesAC = [
  {
    id: 'AL',
    name: 'Alabama',
    code: 'AL',
  },
  {
    id: 'AK',
    name: 'Alaska',
    code: 'AK',
  },
  {
    id: 'AZ',
    name: 'Arizona',
    code: 'AZ',
  },
  {
    id: 'AR',
    name: 'Arkansas',
    code: 'AR',
  },
  {
    id: 'CA',
    name: 'California',
    code: 'CA',
  },
  {
    id: 'CO',
    name: 'Colorado',
    code: 'CO',
  },
  {
    id: 'CT',
    name: 'Connecticut',
    code: 'CT',
  },
  {
    id: 'DE',
    name: 'Delaware',
    code: 'DE',
  },
  {
    id: 'DC',
    name: 'District of Columbia',
    code: 'DC',
  },
  {
    id: 'FL',
    name: 'Florida',
    code: 'FL',
  },
  {
    id: 'GA',
    name: 'Georgia',
    code: 'GA',
  },
  {
    id: 'HI',
    name: 'Hawaii',
    code: 'HI',
  },
  {
    id: 'ID',
    name: 'Idaho',
    code: 'ID',
  },
  {
    id: 'IL',
    name: 'Illinois',
    code: 'IL',
  },
  {
    id: 'IN',
    name: 'Indiana',
    code: 'IN',
  },
  {
    id: 'IA',
    name: 'Iowa',
    code: 'IA',
  },
  {
    id: 'KS',
    name: 'Kansas',
    code: 'KS',
  },
  {
    id: 'KY',
    name: 'Kentucky',
    code: 'KY',
  },
  {
    id: 'LA',
    name: 'Louisiana',
    code: 'LA',
  },
  {
    id: 'ME',
    name: 'Maine',
    code: 'ME',
  },
  {
    id: 'MD',
    name: 'Maryland',
    code: 'MD',
  },
  {
    id: 'MA',
    name: 'Massachusetts',
    code: 'MA',
  },
  {
    id: 'MI',
    name: 'Michigan',
    code: 'MI',
  },
  {
    id: 'MN',
    name: 'Minnesota',
    code: 'MN',
  },
  {
    id: 'MS',
    name: 'Mississippi',
    code: 'MS',
  },
  {
    id: 'MO',
    name: 'Missouri',
    code: 'MO',
  },
  {
    id: 'MT',
    name: 'Montana',
    code: 'MT',
  },
  {
    id: 'NE',
    name: 'Nebraska',
    code: 'NE',
  },
  {
    id: 'NV',
    name: 'Nevada',
    code: 'NV',
  },
  {
    id: 'NH',
    name: 'New Hampshire',
    code: 'NH',
  },
  {
    id: 'NJ',
    name: 'New Jersey',
    code: 'NJ',
  },
  {
    id: 'NM',
    name: 'New Mexico',
    code: 'NM',
  },
  {
    id: 'NY',
    name: 'New York',
    code: 'NY',
  },
  {
    id: 'NC',
    name: 'North Carolina',
    code: 'NC',
  },
  {
    id: 'ND',
    name: 'North Dakota',
    code: 'ND',
  },
  {
    id: 'OH',
    name: 'Ohio',
    code: 'OH',
  },
  {
    id: 'OK',
    name: 'Oklahoma',
    code: 'OK',
  },
  {
    id: 'OR',
    name: 'Oregon',
    code: 'OR',
  },
  {
    id: 'PA',
    name: 'Pennsylvania',
    code: 'PA',
  },
  {
    id: 'RI',
    name: 'Rhode Island',
    code: 'RI',
  },
  {
    id: 'SC',
    name: 'South Carolina',
    code: 'SC',
  },
  {
    id: 'SD',
    name: 'South Dakota',
    code: 'SD',
  },
  {
    id: 'TN',
    name: 'Tennessee',
    code: 'TN',
  },
  {
    id: 'TX',
    name: 'Texas',
    code: 'TX',
  },
  {
    id: 'UT',
    name: 'Utah',
    code: 'UT',
  },
  {
    id: 'VT',
    name: 'Vermont',
    code: 'VT',
  },
  {
    id: 'VA',
    name: 'Virginia',
    code: 'VA',
  },
  {
    id: 'WA',
    name: 'Washington',
    code: 'WA',
  },
  {
    id: 'WV',
    name: 'West Virginia',
    code: 'WV',
  },
  {
    id: 'WI',
    name: 'Wisconsin',
    code: 'WI',
  },
  {
    id: 'WY',
    name: 'Wyoming',
    code: 'WY',
  },
];

export const statesDictionary = states.reduce(
  (prev, curr) => {
    return { ...prev, [curr.id]: curr };
  },
  {} as {
    [key: string]: {
      id: string;
      name: string;
      code: string;
    };
  },
);
