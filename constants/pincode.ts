// Length of the pincode the user has to enter
export const pinLength = 4;

//  After 3 failed tries - redirect user on Sign In Screen
export const maxAttempts = 3;

export const errorColor = 'Orange';
export const titleColor = 'Black';

export const PinStatus = Object.freeze({
  create: 'create',
  confirm: 'confirm',
  enter: 'enter',
  success: 'success',
  fail: 'fail',
});

export type PinStatusType = keyof typeof PinStatus;
