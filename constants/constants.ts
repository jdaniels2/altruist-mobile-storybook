import { Platform, StatusBarStyle } from 'react-native';

import countries from '~/constants/countries';
import {
  INDIVIDUAL_CASH_MANAGEMENT,
  JOINT_CASH_MANAGEMENT,
  JOINT_TENANTS_WROS,
  SINGLE_MEMBER_LLC,
  SOLE_PROPRIETORSHIP,
} from '~/constants/financialAccounts';
import { states, statesAC } from '~/constants/states';
import { PrefferedContact } from '~/constants/trustedContact';
import { theme } from '~/theme/Theme';
import {
  ACATTransferType,
  STATE_INITIAL,
  FundingAccount,
  ApexVerificationStatus,
  PlaidAuthenticationStatus,
  ContributionReason,
  SourceOfFunds,
} from '~/types/graphql';

export const TIMEOUT = 10 * 60 * 1000; // timeout after 10 minutes of inactivity

export const __TEST__ = process.env.JEST_WORKER_ID !== undefined;
export const TOKEN_KEY = 'x-xsrf-token';

export const STATUS_BAR_STYLE: StatusBarStyle = Platform.select({
  android: 'light-content',
  default: 'dark-content',
});
export const STATUS_BAR_BACKGROUND = Platform.select({
  ios: theme.colors['Light Gray'],
  default: '#000000',
});
export const HEADER_SCROLL_OFFSSET = Platform.OS === 'ios' ? 32 : 16;
export const HOME_HEADER_SCROLL_OFFSSET = 96;
export const TOUCHABLE_HIT_SLOP = { top: 15, bottom: 15, left: 15, right: 15 };

export const PHONE_MASK = ['(', 3, ') ', 3, '-', 4];
export const SSN_MASK = [3, '-', 2, '-', 4];
export const TIN_MASK = [2, ' - ', 7];
export const MFA_PHONE_MASK = '(999) 999-9999';

export const CITY_MAX_CHARACTERS = 20;

export const ALTRUIST_SUPPORT_EMAIL = 'support@altruist.com';
export const ALTRUIST_AGREEMENT = 'https://altruist.com/legal/';
export const CUSTODIAL_AGREEMENT = 'https://altruist.com/clearing-custody/';
export const ALTRUIST_LANDING_AGREEMENT = 'https://altruist.com/clearing-custody/';
export const FPSL_AGREEMENT = 'https://altruist.com/legal/';
export const FINRA_URL = 'https://www.finra.org';
export const SIPC_URL = 'https://www.sipc.org';
export const TERMS_OF_USE_URL = 'https://altruist.com/terms';
export const PRIVACY_POLICY_URL = 'https://altruist.com/legal';
export const HCAPTCHA_URL = 'https://www.hcaptcha.com/';
export const HCAPTCHA_PRIVACY_POLICY_URL = 'https://www.hcaptcha.com/privacy';
export const HCAPTCHA_TERMS_OF_SERVICE_URL = 'https://www.hcaptcha.com/terms';
export const SOLO_401K_AGREEMENT =
  'https://altruist.com/m/Adoption-Agreement-for-Equity-Trust-Company-Solo-401k-2023.04.03.pdf';
export const USER_LEAP_EVENT = 'present-mobile-csat-survey';
export const FINGERPRINT_ENDPOINT_URL = 'https://trusted-device.altruist.com';
export const ALTRUIST_BRANCH_URL = 'https://altruistclient.app.link';
export const DISCLOSURE_LINK = 'https://altruist.com/legal/cash-management';

export const maritalStatusList = [
  {
    value: 'Single',

    label: 'Single',
  },

  {
    value: 'Divorced',

    label: 'Divorced',
  },

  {
    value: 'Married',

    label: 'Married',
  },

  {
    value: 'Widowed',

    label: 'Widowed',
  },

  {
    value: 'Partner',

    label: 'Partner',
  },
];

export const maritalStatusListAC = [
  {
    value: 'Single',
    label: 'Single',
  },
  {
    value: 'Married',
    label: 'Married',
  },
];

export const yesNoList = [
  {
    value: true,
    label: 'Yes',
  },
  {
    value: false,
    label: 'No',
  },
];

export const incomeList = [
  {
    value: '100000',
    label: '$0 to $100,000',
  },

  {
    value: '250000',
    label: '$100,001 to $250,000',
  },

  {
    value: '500000',
    label: '$250,001 to $500,000',
  },

  {
    value: '1000000',
    label: '$500,000 +',
  },
];

export const riskLevelList = [
  {
    value: 'LOW',
    label: 'Low',
  },
  {
    value: 'MODERATE',
    label: 'Moderate',
  },
  {
    value: 'SPECULATION',
    label: 'Speculation',
  },
  {
    value: 'HIGH',
    label: 'High',
  },
];

export const employmentType = [
  {
    value: 'EMPLOYED',
    label: 'Employed',
  },
  {
    value: 'UNEMPLOYED',
    label: 'Unemployed',
  },
  {
    value: 'SELF_EMPLOYED',
    label: 'Self-employed',
  },
  {
    value: 'RETIRED',
    label: 'Retired',
  },
  {
    value: 'STUDENT',
    label: 'Student',
  },
];

export const contactTypes = [
  {
    value: PrefferedContact.Email,
    label: 'Email',
  },
  {
    value: PrefferedContact.Phone,
    label: 'Phone',
  },
];

export const visaTypes = [
  {
    value: 'E1',
    label: 'E1',
  },
  {
    value: 'E2',
    label: 'E2',
  },
  {
    value: 'E3',
    label: 'E3',
  },
  {
    value: 'F1',
    label: 'F1',
  },
  {
    value: 'G4',
    label: 'G4',
  },
  {
    value: 'H1B',
    label: 'H1B',
  },
  {
    value: 'L1',
    label: 'L1',
  },
  {
    value: 'O1',
    label: 'O1',
  },
  {
    value: 'TN1',
    label: 'TN1',
  },
  {
    value: 'OTHER',
    label: 'Other',
  },
];

export const typeOfTrustList = [
  {
    value: 'REVOCABLE',
    label: 'Revocable',
  },
  {
    value: 'IRREVOCABLE',
    label: 'Irrevocable',
  },
];

export const typeOfTrustId = [
  {
    value: 'SSN',
    label: 'SSN',
  },
  {
    value: 'EIN',
    label: 'EIN',
  },
];

export const countryPickerItems = countries.map((country) => ({
  key: country.id,
  label: country.name,
  value: country.code3,
}));

export const statePickerItems = states.map((state) => ({
  key: state.id,
  label: state.name,
  value: state.code,
}));

export const statePickerItemsAC = statesAC.map((state) => ({
  key: state.id,
  label: state.name,
  value: state.code,
}));

export const ugmaStatePickerItemsAC = statesAC
  .filter((state) => state.id === 'VT')
  .map((state) => ({
    key: state.id,
    label: state.name,
    value: state.code,
  }));

export const contributionReasons = {
  regular: {
    label: 'Regular',
    value: ContributionReason['REGULAR'],
  },
  employer: {
    label: 'Employer',
    value: ContributionReason['EMPLOYER'],
  },
  employee: {
    label: 'Employee',
    value: ContributionReason['EMPLOYEE'],
  },
  rollover: {
    label: 'Rollover - 60 day',
    value: ContributionReason['ROLLOVER_60_DAY'],
  },
};

export const screenNames: Dict<string> = {
  Home: 'Overview Tab',
  Settings: 'Settings',
  PersonalForm: 'Personal',
  AddressForm: 'Address',
  Accounts: 'Accounts Tab',
  SettingsAccounts: 'Altruist Accounts',
  BanksSelectAccount: 'Banks Select Account',
  Beneficiaries: 'Beneficiaries',
  BeneficiaryDetails: 'Beneficiary Details',
  Instructions: 'Transfer Instructions',
  Check: 'Check Deposit',
  Wire: 'Wire Deposit',
  DocumentAccounts: 'Document List',
  Documents: 'Documents',
  ChangePassword: 'Change Password',
  ForgotPassword: 'Forgot Password',
  Legal: 'Legal',
  ChangePin: 'Change PIN',
  AccountDetails: 'Account Details',
  MigrationNotice: 'Migration Notice',
  MigrationExpectation: 'Migration Expectation',
  Greeting: 'Greeting',
  Preloader: 'Preloader',
  AccountOpening: 'Finish Account',
  MigrationFlow: 'Migration Flow',
  GetStarted: 'Get Started',
  WelcomeBack: 'Welcome Back',
  SignIn: 'Log In',
  PincodeSignIn: 'Enter PIN',
  BiometricSignIn: 'Log In',
  CreatePin: 'Create PIN',
  // RequestNotifications: 'Notifications Tutorial',
  EnableFaceId: 'Face ID Tutorial',
  NoConnection: 'No Connection',
  Transfers: 'Transfers Tab',
  Activity: 'Activity Tab',
};

export const ACCOUNT_STATUS = {
  PENDING: 'PENDING',
  REJECTED: 'REJECTED',
  RESTRICTED: 'RESTRICTED',
  ACTIVE: 'ACTIVE',
  CLOSED: 'CLOSED',
  CLOSE_PENDING: 'CLOSE_PENDING',
  NEEDS_ACTION: 'NEEDS_ACTION',
};

export const stateAbbreviationOptions = Object.keys(STATE_INITIAL).map((a) => ({
  value: a,
  label: a,
  key: a,
}));

export const STATEMENT_TYPE: any = {
  STATEMENT: 'Statement',
  TRADE: 'Trade',
  TAX: 'Statement',
  TAX_1099_AND_1099_CORRECTIONS: 'Tax',
  TAX_1099_R: 'Tax',
  TAX_1042_S: 'Tax',
  TAX_5498_FMV: 'Tax',
  TAX_5498_ESA: 'Tax',
  TAX_10990: 'Tax',
  ACCOUNT_OPENING: 'Account Application',
  BUSINESS: 'Business',
  IMA: 'IMA',
  DEATH_CERTIFICATE: 'Death Certificate',
  SIMPLE: 'Simple',
  FINRA: 'Finra',
  SOLO_401: 'Solo 401(k)',
  TRUST_147C: 'Trust',
};

export const ACAT_TYPE_LABEL: { [key in ACATTransferType]?: string } = {
  FULL_TRANSFER: 'Full',
  PARTIAL_TRANSFER_DELIVERER: 'Partial',
  PARTIAL_TRANSFER_RECEIVER: 'Partial',
};

export enum Month {
  January,
  February,
  March,
  April,
  May,
  June,
  July,
  August,
  September,
  October,
  November,
  December,
}

export const MMA_DOCUMENT_STATUS: any = {
  AUTHORIZATIONS_MMA_APPROVED: 'Approved',
  AUTHORIZATIONS_MMA_REVOKED: 'Revoked',
  AUTHORIZATIONS_MMA_REPLACED: 'Replaced',
};

export const colorMap = {
  stocks: theme.colors['Green'],
  bonds: theme.colors['80Green'],
  commodities: theme.colors['30Black'],
  cash: theme.colors['60Black'],
  funds_cash: theme.colors['60Green'],
  equity: theme.colors['Green'],
  fd: theme.colors['30Black'],
  fixedincome: theme.colors['80Green'],
  funds: theme.colors['30Black'],
  other: theme.colors['40Black'],
};

export const whitelist = {
  ira: 'ira',
  etf: 'etf',
  reit: 'reit',
  ach: 'ach',
  vti: 'vti',
  inv: 'inv',
  fdic: 'fdic',
  sipc: 'sipc',
  ida12: 'ida12',
  jtwros: 'jtwros',
  us: 'us',
  usd: 'usd',
  fx: 'fx',
  nav: 'nav',
  sec: 'sec',
  finra: 'finra',
  cd: 'cd',
  pfd: 'pfd',
  wros: 'wros',
  acats: 'acats',
  uit: 'uit',
  adr: 'adr',
  etn: 'etn',
  msci: 'msci',
  spdr: 'spdr',
  'spdr®': 'spdr®',
  gnma: 'gnma',
  esg: 'esg',
  cvs: 'cvs',
  's&p': 's&p',
  'etf™': 'etf™',
  smallcap: 'smallcap',
  ftse: 'ftse',
  tc: 'tc',
  acwi: 'acwi',
  nnrf: 'nnrf',
  utma: 'utma',
  ugma: 'ugma',
};

export const appID = '1498498855';
export const corpAccounts = [
  'S CORPORATION',
  'C CORPORATION',
  'LLC S CORPORATION',
  'LLC C CORPORATION',
  'LLC PARTNERSHIP',
  'PARTNERSHIP',
  'NON PROFIT',
];

export const solo401kAccounts = ['SOLO 401K', 'ROTH SOLO 401K'];

export const jointAccounts = [
  JOINT_TENANTS_WROS,
  'JOINT COMMUNITY PROPERTY',
  'JOINT TENANTS IN COMMON',
  'JOINT TENANTS BY THE ENTIRETY',
  JOINT_CASH_MANAGEMENT,
];

export const newJointAccounts = [
  'JOINT COMMUNITY PROPERTY',
  'JOINT TENANTS IN COMMON',
  'JOINT TENANTS BY THE ENTIRETY',
];

export const accountsWithConstraints = [
  'JOINT COMMUNITY PROPERTY',
  'JOINT TENANTS BY THE ENTIRETY',
];

export const csCorpRolesFormat = {
  AUTHORIZED_SIGNER: 'Authorized Signer',
  CONTROL_PERSON: 'Control Person',
  SECRETARY: 'Secretary',
  ENTITY_OFFICER: 'Entity Officer',
  AUTHORIZED_OFFICER: 'Authorized Officer',
  BENEFICIAL_OWNER: 'Beneficial Owner',
  PRINCIPAL_APPROVER: 'Principal Approver',
  REGISTERED_REPRESENTATIVE_APPROVER: 'Registered Representative Approver',
};

export const SINGLE_BUSINESS_ACCOUNTS = [SOLE_PROPRIETORSHIP, SINGLE_MEMBER_LLC];

export const SKIP_BENEFICIARIES_ACCOUNT_TYPES = [
  ...corpAccounts,
  'TRUST',
  'UTMA',
  'UGMA',
  ...SINGLE_BUSINESS_ACCOUNTS,
  'RETIREMENT TRUST',
];

export const isRemovedPlaid = (authenticationStatus: PlaidAuthenticationStatus) =>
  authenticationStatus === PlaidAuthenticationStatus.FAILED;

export const isPendingPlaid = (authenticationStatus: PlaidAuthenticationStatus) =>
  authenticationStatus === PlaidAuthenticationStatus.PENDING;

export const isPendingPlaidAutoMicrodeposit = (authenticationStatus: PlaidAuthenticationStatus) =>
  authenticationStatus === PlaidAuthenticationStatus.PENDING_VERIFICATION;

export const isPendingPlaidManualMicrodeposit = (authenticationStatus: PlaidAuthenticationStatus) =>
  authenticationStatus === PlaidAuthenticationStatus.PENDING_MANUAL;

export const isRemovedBankClearing = (verificationStatus: ApexVerificationStatus) =>
  verificationStatus === ApexVerificationStatus.CANCELED ||
  verificationStatus === ApexVerificationStatus.REJECTED;

export const isPendingBankClearing = (verificationStatus: ApexVerificationStatus) =>
  verificationStatus === ApexVerificationStatus.PENDING;

export const isDisconnectedFundingAccount = ({
  verificationStatus,
  authenticationStatus,
}: FundingAccount) =>
  verificationStatus === ApexVerificationStatus.CANCELED ||
  verificationStatus === ApexVerificationStatus.REJECTED ||
  authenticationStatus === PlaidAuthenticationStatus.FAILED;

export const purposeOfAccountList = [
  {
    value: 'INVESTING_WITH_FREQUENT_WITHDRAWALS',
    label: 'Investing with frequent withdrawals',
  },
  {
    value: 'INVESTING_WITH_OCCASIONAL_WITHDRAWALS',
    label: 'Investing with occasional withdrawals',
  },
  {
    value: 'RETIREMENT',
    label: 'Retirement',
  },
  {
    value: 'TUITION_SAVINGS',
    label: 'Tuition savings',
  },
  {
    value: 'ESTATE_TAX_PLANNING',
    label: 'Estate, tax planning',
  },
];

export const purposeOfAccountRetirementTrust = [
  {
    value: 'INVESTING_WITH_FREQUENT_WITHDRAWALS',
    label: 'Investing with frequent withdrawals',
  },
  {
    value: 'INVESTING_WITH_OCCASIONAL_WITHDRAWALS',
    label: 'Investing with occasional withdrawals',
  },
  {
    value: 'RETIREMENT',
    label: 'Retirement',
  },
];

export const purposeOfAccountCorpList = [
  {
    value: 'INVESTING_WITH_FREQUENT_WITHDRAWALS',
    label: 'Investing with frequent withdrawals',
  },
  {
    value: 'INVESTING_WITH_OCCASIONAL_WITHDRAWALS',
    label: 'Investing with occasional withdrawals',
  },
];

export const csCorpAccountNatureFormat = {
  ADMINISTRATIVE_AND_SUPPORT_SERVICES: 'Administration and Support Services',
  AGRICULTURE_FORESTRY_FISHING: 'Agriculture',
  AGRICULTURE_FORESTRY_FISHING_AND_HUNTING: 'Agriculture and Hunting',
  ARTS_ENTERTAINMENT_AND_RECREATION: 'Arts, Entertainment, and Recreation',
  CONSTRUCTION: 'Construction',
  DATA_PROCESSING_HOSTING_AND_RELATED_SERVICES: 'Date Processing Hosting and Related Services',
  EDUCATIONAL_SERVICES: 'Educational Services',
  FINANCE_AND_INSURANCE: 'Finance and Insurance',
  FINANCE_INSURANCE_REAL_ESTATE: 'Finance Insurance Real Estate',
  HEALTH_CARE_AND_SOCIAL_ASSISTANCE: 'Health Care and Social Assistance',
  HOTELS_ACCOMMODATIONS: 'Hotel Accomodations',
  MANAGEMENT_OF_COMPANIES_AND_ENTERPRISES: 'Management of Companies and Enterprises',
  MANUFACTURING: 'Manufacturing',
  MINING: 'Mining',
  MINING_QUARRYING_AND_OIL_AND_GAS_EXTRACTION: 'Mining Quarrying and Oil and Gas Extraction',
  PERSONAL_AND_LAUNDRY_SERVICES: 'Personal and Laundry Services',
  POSTAL_SERVICE_COURIERS_PACKAGE_DELIVERY: 'Postal Service Couriers and Package Delivery',
  PRINTING_AND_RELATED_SUPPORT_ACTIVITIES: 'Printing and Related Support Activities',
  PROFESSIONAL_SCIENTIFIC_AND_TECHNICAL_SERVICES: 'Professional Scientific and Technical Services',
  PUBLIC_ADMINISTRATION: 'Public Administration',
  PUBLISHING_INDUSTRIES: 'Publishing Industries',
  REAL_ESTATE_AND_RENTAL_AND_LEASING: 'Real Estate and Rental Leasing',
  RELIGIOUS_GRANTMAKING_CIVIC_PROFESSIONAL_AND_SIMILAR_ORGS:
    'Religious GrantMaking Civic Professional',
  REPAIR_AND_MAINTENANCE: 'Repair and Maintenance',
  RETAIL_TRADE: 'Retail Trade',
  SERVICES: 'Services',
  TELECOMMUNICATIONS: 'Telecommunications',
  TRANSPORTATION: 'Transporation',
  TRANSPORTATION_COMMUNICATIONS_ELECTRIC_GAS_SANITARY_SERVICES:
    'Transport Communications Electric Gas Sanitary Services',
  UTILITIES: 'Utilities',
  WAREHOUSING_AND_STORAGE: 'Warehouse and Storage',
  WASTE_MANAGEMENT_AND_REMEDIATION_SERVICES: 'Waste Management and Remediation Services',
  WHOLESALE_TRADE: 'Wholesale Trade',
};

export const purposeOfTrustAccountList = [
  { value: 'INVESTING_OF_TRUST_ASSETS', label: 'Investing of trust assets' },
  { value: 'DISTRIBUTION_OF_ESTATE', label: 'Distribution of estate' },
];

export const purposeOfAccountMinorIra = [
  {
    value: 'RETIREMENT',
    label: 'Retirement',
  },
];

export const simpleIraPlanType = [
  {
    value: '5304',
    label: 'SIMPLE 5304',
  },

  {
    value: '5305',
    label: 'SIMPLE 5305',
  },
];

export const occupationList = [
  {
    value: 'Unemployed',
    label: 'Unemployed',
  },

  {
    value: 'Retired',
    label: 'Retired',
  },

  {
    value: 'Student',
    label: 'Student',
  },

  {
    value: 'Homemaker',
    label: 'Homemaker',
  },

  {
    value: 'Self-Employed',
    label: 'Self-Employed',
  },

  {
    value: 'Accounting / Auditing',
    label: 'Accounting, auditing',
  },

  {
    value: 'Administrative / Clerical',
    label: 'Administrative, clerical',
  },

  {
    value: 'Advertising / Marketing',
    label: 'Advertising, marketing',
  },

  {
    value: 'Aerospace / Aviation / Defense',
    label: 'Aerospace, aviation, defense',
  },

  {
    value: 'Artist / Entertainer / Writer',
    label: 'Artist, entertainer, writer',
  },

  {
    value: 'Attorney / Law / Paralegal',
    label: 'Attorney, law, paralegal',
  },

  {
    value: 'Automotive',
    label: 'Automotive',
  },

  {
    value: 'Civil Service',
    label: 'Civil service',
  },

  {
    value: 'Construction / Manufacturing',
    label: 'Construction, manufacturing',
  },

  {
    value: 'Consultant',
    label: 'Consultant',
  },

  {
    value: 'Customer Service / Call Center',
    label: 'Customer service, call center',
  },

  {
    value: 'Design',
    label: 'Design',
  },

  {
    value: 'Education / Training',
    label: 'Education, training',
  },

  {
    value: 'Engineer',
    label: 'Engineer',
  },

  {
    value: 'Executive',
    label: 'Executive',
  },

  {
    value: 'Farming / Ranching / Agriculture',
    label: 'Farming, ranching, agriculture',
  },

  {
    value: 'Financial Service',
    label: 'Financial service',
  },

  {
    value: 'Hair Stylist / Beautician / Esthetician',
    label: 'Hair stylist, beautician, esthetician',
  },

  {
    value: 'Home Services / Housekeeping / Landscaping',
    label: 'Home services, housekeeping, landscaping',
  },

  {
    value: 'Human Resources',
    label: 'Human resources',
  },

  {
    value: 'Information Technology / Systems / Data',
    label: 'Information technology, systems, data',
  },

  {
    value: 'Insurance',
    label: 'Insurance',
  },

  {
    value: 'Law Enforcement / Security',
    label: 'Law enforcement, security',
  },

  {
    value: 'Medical / Veterinarian / Therapy',
    label: 'Medical, veterinarian, therapy',
  },

  {
    value: 'Military',
    label: 'Military',
  },

  {
    value: 'Non-Profit',
    label: 'Non-profit',
  },

  {
    value: 'Politics',
    label: 'Politics',
  },

  {
    value: 'Real Estate',
    label: 'Real estate',
  },

  {
    value: 'Restaurant / Food Services',
    label: 'Restaurant, food services',
  },

  {
    value: 'Retail / Cashier',
    label: 'Retail, cashier',
  },

  {
    value: 'Sales',
    label: 'Sales',
  },

  {
    value: 'Scientist',
    label: 'Scientist',
  },

  {
    value: 'Transportation',
    label: 'Transportation',
  },

  {
    value: 'Utilities',
    label: 'Utilities',
  },
];

const sourceOfFundsBusiness = [
  {
    value: SourceOfFunds['BUSINESS_INCOME_PROFIT'],
    label: 'Business income/profit',
  },
  {
    value: SourceOfFunds['BUSINESS_CAPITAL'],
    label: 'Business capital',
  },
  {
    value: SourceOfFunds['DONATION_GIFT_INHERITANCE'],
    label: 'Donation/gift/inheritance',
  },
  {
    value: SourceOfFunds['GRANT'],
    label: 'Grant',
  },
];

export const sourceOfFundsList = [
  {
    value: SourceOfFunds['GIFT_INHERITANCE'],
    label: 'Gift/inheritance',
  },
  {
    value: SourceOfFunds['EMPLOYMENT_INCOME'],
    label: 'Employment income',
  },
  {
    value: SourceOfFunds['ALIMONY_CHILD_SUPPORT'],
    label: 'Alimony/child support',
  },
  {
    value: SourceOfFunds['INSURANCE_LEGAL_SETTLEMENT'],
    label: 'Insurance/legal settlement',
  },
  {
    value: SourceOfFunds['UNEMPLOYMENT_DISABILITY'],
    label: 'Unemployment/disability',
  },
  {
    value: SourceOfFunds['SAVINGS_INVESTMENTS'],
    label: 'Savings/investments',
  },
];

export const sourceOfFundsCorpList = [
  {
    value: SourceOfFunds['SAVINGS_INVESTMENTS'],
    label: 'Savings/investments',
  },
  ...sourceOfFundsBusiness,
];

export const sourceOfFundsMinorIraAccounts = [
  {
    value: SourceOfFunds['EMPLOYMENT_INCOME'],
    label: 'Employment income',
  },
];

export const sourceOfFundsTrustList = [
  {
    value: SourceOfFunds['SAVINGS_INVESTMENTS'],
    label: 'Savings/investments',
  },
  ...sourceOfFundsBusiness,
];

export const unemployedStatuses = ['STUDENT', 'RETIRED', 'UNEMPLOYED'];
export const unemployedOccupations = ['Unemployed', 'Retired', 'Student', 'Homemaker'];

export const occupationsRequireIndustry = ['Self-Employed', 'Consultant', 'Executive'];

export const occupationListFiltered = occupationList.filter(
  ({ value }) => !occupationsRequireIndustry.includes(value),
);

export const equityTrustAgreementAccountTypes = [
  'SOLO 401K',
  'ROTH SOLO 401K',
  'IRA',
  'ROTH IRA',
  'ROLLOVER IRA',
  'SEP IRA',
  'SIMPLE IRA',
  'TRADITIONAL BENEFICIARY IRA',
  'ROTH BENEFICIARY IRA',
];

export const LOGIN_ERRORS = {
  invalid_token_missing_org_id: 'invalid_token_missing_org_id',
  THIRD_ATTEMPT: 'Attempts remaining: 2',
  LAST_ATTEMPT: 'Attempts remaining: 1',
  LOCKED_ACCOUNT: 'locked account',
  INVALID_MFA_TOKEN: 'Invalid MFA Verification Code',
  LOCKED_ACCOUNT_EXPLICIT: 'locked account explicit',
  LOGIN_FAILED: 'login failed',
  ADVISOR_LOGIN: 'ADVISOR LOGIN',
  BAD_CREDENTIALS: 'bad credentials',
};

export const DEEPLINKS: any = {
  SIGN_UP: 'SignUp',
  FINISH_ACCOUNT_OPENING: 'Finish your application',
  SECRETARY_INVITE: 'Secretary invite',
  MMA_REQUEST: 'MMA Request',
  ACAT_REQUEST: 'ACAT Request',
  VERIFY_BANK: 'Verify Bank',
  VIEW_TRANSFERS: 'View Transfers',
  VIEW_MMA: 'View MMA',
  VIEW_DOCUMENTS: 'View Documents',
  VIEW_AGREEMENTS_ON_CLIENT_PORTAL: 'View Agreements on Client Portal',
};

export const OPERATION_BLACKLIST = {
  loginWithCredentials: true,
  loginWithMfa: true,
  changePassword: true,
  mfaTokenRequest: true,
  mfaTokenRequestWithCredentials: true,
  tokenRequest: true,
};

export const CASH_SYMBOL = 'CASH_BALANCE';

export const keyboardAvoidBehavior = Platform.select({
  ios: 'padding',
  android: undefined,
}) as 'height' | 'padding' | 'position' | undefined;

export const retirementTrustPlanTypeOptions = [
  {
    value: '401(k)',
    label: '401(k)',
  },
  {
    value: 'Profit Sharing',
    label: 'Profit Sharing',
  },
  {
    value: 'Defined Benefit/Pension',
    label: 'Defined Benefit/Pension',
  },
  {
    value: '403(b)7',
    label: '403(b)7',
  },
  {
    value: '457(b)',
    label: '457(b)',
  },
];

export const accountAssetOwnershipOptions = [
  {
    value: 'SINGLE_PARTICIPANT',
    label: 'Single Participant',
  },
  {
    value: 'MULTIPLE_PARTICIPANTS',
    label: 'Multiple Participants',
  },
];

export const highYieldSavingAccountTypes = {
  [INDIVIDUAL_CASH_MANAGEMENT]: true,
  [JOINT_CASH_MANAGEMENT]: true,
};

export const REVIEW_LOCATION: any = {
  DASHBOARD: 'Dashboard',
  LOGOUT: 'Logout',
  TRANSFERS: 'Transfers',
};

export const altruistBlue = '#174455';
export const iosStoreURL = 'itms-apps://apps.apple.com/US/app/id1498498855';
export const androidStoreURL =
  'https://play.google.com/store/apps/details?id=com.altruist.mobileapp&hl=en_US&gl=US';
