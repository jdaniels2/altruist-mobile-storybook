import { Platform } from 'react-native';

export const AktivGroteskRegular = Platform.select({
  ios: 'Aktiv Grotesk',
  android: 'AktivGrotesk-Regular',
});
export const AktivGroteskMedium = 'AktivGrotesk-Medium';
export const BegumMedium = 'Begum-Medium';
