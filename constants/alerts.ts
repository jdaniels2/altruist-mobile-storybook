type AlertOption = {
  title?: string;
  description?: string;
  cancelText?: string;
  confirmText?: string;
  onConfirmPress?: Function;
  onCancelPress?: Function;
};

const alerts = {
  internetConnection: (alertOptions?: AlertOption) => ({
    title: 'There is a problem loading data',
    description: 'Check your internet and try again.',
    cancelText: 'OK',
    ...alertOptions,
  }),
  unknownError: {
    title: `We've encountered a problem`,
    description: 'Try again.',
    cancelText: 'OK',
  },
  investorProfileChanged: {
    title: 'Success',
    description: 'Your Investor Profile has been successfully changed.',
    confirmText: 'Ok',
  },
  getDataFailed: (type = 'data', alertOptions?: AlertOption) => ({
    title: 'Get Data Failed',
    description: `Sorry, we ran into a problem while getting ${type}.`,
    cancelText: 'OK',
    ...alertOptions,
  }),
  mfaFailed: {
    title: 'Multi-factor authentication failed',
    description: `Please try again.`,
    cancelText: 'OK',
  },
  noPhoneNumber: {
    title: 'There is not a default phone number set for MFA',
    description: 'Please add phone number and try again.',
    cancelText: 'OK',
  },
  wrongPhoneNumber: {
    title: 'Phone number is wrong',
    description: 'Please add a new one and try again.',
    cancelText: 'OK',
  },
  loginFailed: {
    title: 'Incorrect email or password',
    description: `Please try again.`,
    cancelText: 'OK',
  },
  thirdLoginAttempt: {
    title: 'Incorrect email or password',
    description: `You have 2 more attempts before your account is locked.`,
    cancelText: 'OK',
  },
  lastLoginAttempt: {
    title: 'Incorrect email or password',
    description: `You have 1 more attempt before your account is locked.`,
    cancelText: 'OK',
  },
  accountLocked: (alertOptions?: AlertOption) => ({
    title: 'Account Locked',
    description:
      'Your account has been locked because of too many incorrect attempts. Please reset your password to regain access.',
    confirmText: 'Reset Password',
    ...alertOptions,
  }),
  advisorLogin: {
    title: 'Are you an Advisor?',
    description: `The Altruist mobile app is currently not supported for Advisors. Please log in to your account on a desktop device.`,
    cancelText: 'OK',
  },
  updateFailed: (type: string, alertOptions?: AlertOption) => ({
    title: `Sorry we can’t update ${type}`,
    description: 'Try later.',
    cancelText: 'Cancel',
    ...alertOptions,
  }),
  createFailed: (type: string, alertOptions?: AlertOption) => ({
    title: `Sorry we can’t create ${type}`,
    description: 'Try later.',
    cancelText: 'Cancel',
    ...alertOptions,
  }),
  deleteFailed: (type: string, alertOptions?: AlertOption) => ({
    title: `Sorry we can’t delete ${type}`,
    description: 'Try later.',
    cancelText: 'Cancel',
    ...alertOptions,
  }),
  resetPassword: (email: string, alertOptions?: AlertOption) => ({
    title: `Please check your email`,
    description: `We sent a link to reset your password to ${email} to unlock your account.`,
    cancelText: 'OK',
    confirmText: 'Contact',
    ...alertOptions,
  }),
};

export default alerts;
