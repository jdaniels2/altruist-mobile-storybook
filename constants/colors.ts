export const mainBackgroundColor = '#C8C8C8';
export const whiteColor = '#FFFFFF';
export const blackTextColor = '#171A1C';
//gradient start and end color for charts background
export const gradientStart = 'rgba(0, 186, 139, 0.3)';
export const gradientEnd = 'rgba(0, 186, 139, 0.0)';
