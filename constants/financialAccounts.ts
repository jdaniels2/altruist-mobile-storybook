import { AccountType } from '~/types/model';

export const SOLE_PROPRIETORSHIP = 'SOLE PROPRIETORSHIP';
export const SINGLE_MEMBER_LLC = 'SINGLE MEMBER LLC';
export const NON_PROFIT = 'NON PROFIT';
export const JOINT_TENANTS_WROS = 'JOINT TENANTS WROS';
export const JOINT_TENANTS_BY_THE_ENTIRETY = 'JOINT TENANTS BY THE ENTIRETY';
export const JOINT_TENANTS_IN_COMMON = 'JOINT TENANTS IN COMMON';
export const JOINT_COMMUNITY_PROPERTY = 'JOINT COMMUNITY PROPERTY';
export const MINOR_IRA = 'MINOR IRA';
export const MINOR_ROTH_IRA = 'MINOR ROTH IRA';
export const UTMA = 'UTMA';
export const UGMA = 'UGMA';
export const INDIVIDUAL_CASH_MANAGEMENT = 'INDIVIDUAL CASH MANAGEMENT';
export const JOINT_CASH_MANAGEMENT = 'JOINT CASH MANAGEMENT';

export const isMinorIraAccountTypes = (accountType?: string) =>
  [MINOR_IRA, MINOR_ROTH_IRA].includes(accountType || '');

export const isMinorAccountTypes = (accountType?: string) =>
  [UTMA, UGMA].includes(accountType || '');

export const ACCOUNT_TYPE_INFOS = {
  'TRADITIONAL BENEFICIARY IRA': { retirement: true, displayName: 'Beneficiary IRA' },
  'INDIVIDUAL ROTH 401K': { retirement: true, displayName: 'Individual Roth 401(k)' },
  'INDIVIDUAL 401K': { retirement: true, displayName: 'Individual 401(k)' },
  'MINOR ROTH IRA': { retirement: true, displayName: 'Minor Roth IRA' },
  'MINOR IRA': { retirement: true, displayName: 'Minor IRA' },
  'ROLLOVER IRA': { retirement: true, displayName: 'Rollover IRA' },
  'SIMPLE IRA': { retirement: true, displayName: 'SIMPLE IRA' },
  'ROTH BENEFICIARY IRA': { retirement: true, displayName: 'Beneficiary Roth IRA' },
  'ROTH 401K': { retirement: true, displayName: 'Roth 401(k)' },
  'ROTH IRA': { retirement: true, displayName: 'Roth IRA' },
  'SEP IRA': { retirement: true, displayName: 'SEP IRA' },
  '401K': { retirement: true, displayName: '401(k)' },
  'SOLO 401K': { retirement: true, displayName: 'Solo 401(k)' },
  'ROTH SOLO 401K': { retirement: true, displayName: 'Roth Solo 401(k)' },
  IRA: { retirement: true, displayName: 'IRA' },
  'COVERDELL ESA': { retirement: true, displayName: 'Coverdell ESA' },
  ESTATE: { retirement: false, displayName: 'Estate' },
  GUARDIAN: { retirement: true, displayName: 'Guardian' },
  PARTNERSHIP: { retirement: false, displayName: 'Partnership' },
  'LLC PARTNERSHIP': { retirement: false, displayName: 'LLC Partnership' },
  'LIMITED LIABILITY PARTNERSHIP': { retirement: false, displayName: 'LLC Partnership' },
  'PROFIT SHARING PLAN': { retirement: true, displayName: 'Profit-Sharing Plan' },
  [SOLE_PROPRIETORSHIP]: { retirement: false, displayName: 'Sole Proprietorship' },
  'TENANTS BY THE ENTIRETY': { retirement: true, displayName: 'Tenants by Entirety' },
  TX: { retirement: true, displayName: 'TX Account' },
  'UNINCORPORATED ASSOCIATION': { retirement: false, displayName: 'Unincorporated Association' },
  'NON-PROFIT ORGANIZATION': { retirement: false, displayName: 'Non-Profit Organziation' },
  'COMMUNITY PROPERTY': { retirement: false, displayName: 'Community Property' },
  [JOINT_TENANTS_WROS]: { retirement: false, displayName: 'Joint (JTWROS)' },
  [JOINT_COMMUNITY_PROPERTY]: { retirement: false, displayName: 'Joint (JCP)' },
  [JOINT_TENANTS_IN_COMMON]: { retirement: false, displayName: 'Joint (JTIC)' },
  [JOINT_TENANTS_BY_THE_ENTIRETY]: { retirement: false, displayName: 'Joint (JTBE)' },
  'RETIREMENT TRUST': { retirement: false, displayName: 'Retirement Trust' },
  'LIMITED LIABILITY': { retirement: false, displayName: 'Limited Liability' },
  'TENANTS IN COMMON': { retirement: false, displayName: 'Tenants In Common' },
  CORPORATION: { retirement: false, displayName: 'Corporation' },
  'TAX EXEMPT': { retirement: false, displayName: 'Tax Exempt' },
  FUNDING: { retirement: false, displayName: 'Funding' },
  TRUST: { retirement: false, displayName: 'Trust' },
  INDIVIDUAL: { retirement: false, displayName: 'Individual' },
  UNKNOWN: { retirement: false, displayName: 'Unknown' },
  CHECKING: { retirement: false, displayName: 'Checking' },
  HOUSE: { retirement: false, displayName: 'House' },
  SAVINGS: { retirement: false, displayName: 'Savings' },
  [SINGLE_MEMBER_LLC]: { retirement: false, displayName: 'Single-Member LLC' },
  'SINGLE-MEMBER LLC': { retirement: false, displayName: 'Single-Member LLC' },
  [NON_PROFIT]: { retirement: false, displayName: 'Non-Profit Organization' },

  /* Plaid */

  'CREDIT CARD': { retirement: false, displayName: 'Credit Card' },
  PAYPAL: { retirement: false, displayName: 'Paypal' },
  'CASH MANAGEMENT': { retirement: false, displayName: 'Cash Management' },
  [INDIVIDUAL_CASH_MANAGEMENT]: { retirement: false, displayName: 'Individual Cash' },
  [JOINT_CASH_MANAGEMENT]: { retirement: false, displayName: 'Joint Cash' },
  CD: { retirement: false, displayName: 'CD' },
  HSA: { retirement: false, displayName: 'HSA' },
  'MONEY MARKET': { retirement: false, displayName: 'Money Market' },
  PREPAID: { retirement: false, displayName: 'Prepaid' },
  AUTO: { retirement: false, displayName: 'Auto' },
  COMMERCIAL: { retirement: false, displayName: 'Commercial' },
  CONSTRUCTION: { retirement: false, displayName: 'Construction' },
  CONSUMER: { retirement: false, displayName: 'Consumer' },
  'HOME EQUITY': { retirement: false, displayName: 'Home Equity' },
  LOAN: { retirement: false, displayName: 'Loan' },
  MORTGAGE: { retirement: false, displayName: 'Mortgage' },
  OVERDRAFT: { retirement: false, displayName: 'Overdraft' },
  'LINE OF CREDIT': { retirement: false, displayName: 'Line of Credit' },
  STUDENT: { retirement: false, displayName: 'Student' },
  '401A': { retirement: true, displayName: '401a' },
  '403B': { retirement: true, displayName: '403b' },
  '457B': { retirement: true, displayName: '457b' },
  '529': { retirement: false, displayName: '529' },
  BROKERAGE: { retirement: false, displayName: 'Brokerage' },
  'CASH ISA': { retirement: false, displayName: 'Cash ISA' },
  'EDUCATION SAVINGS ACCOUNT': { retirement: false, displayName: 'Education Savings Account' },
  'FIXED ANNUITY': { retirement: false, displayName: 'Fixed Annuity' },
  GIC: { retirement: false, displayName: 'GIC' },
  'HEALTH REIMBURSEMENT ARRANGEMENT': {
    retirement: false,
    displayName: 'Health Reimbursement Arrangement',
  },
  ISA: { retirement: true, displayName: 'ISA' },
  KEOGH: { retirement: true, displayName: 'KEOGH' },
  LIF: { retirement: true, displayName: 'LIF' },
  LIRA: { retirement: true, displayName: 'LIRA' },
  LRIF: { retirement: true, displayName: 'LRIF' },
  LRSP: { retirement: true, displayName: 'LRSP' },
  'MUTUAL FUND': { retirement: false, displayName: 'Mutual Fund' },
  'NON-TAXABLE BROKERAGE ACCOUNT': {
    retirement: false,
    displayName: 'Non-Taxable Brokerage Account',
  },
  PENSION: { retirement: true, displayName: 'Pension' },
  PRIF: { retirement: true, displayName: 'PRIF' },
  QSHR: { retirement: false, displayName: 'QSHR' },
  RDSP: { retirement: false, displayName: 'RDSP' },
  RESP: { retirement: false, displayName: 'RESP' },
  RETIREMENT: { retirement: true, displayName: 'Retirement' },
  RLIF: { retirement: false, displayName: 'RLIF' },
  ROTH: { retirement: true, displayName: 'ROTH' },
  RRIF: { retirement: true, displayName: 'RRIF' },
  RRSP: { retirement: true, displayName: 'RRSP' },
  SARSEP: { retirement: true, displayName: 'SARSEP' },
  SIPP: { retirement: true, displayName: 'SIPP' },
  'STOCK PLAN': { retirement: false, displayName: 'Stock Plan' },
  TFSA: { retirement: true, displayName: 'TFSA' },
  'THRIFT SAVINGS PLAN': { retirement: false, displayName: 'Thrift Savings Plan' },
  UGMA: { retirement: false, displayName: 'UGMA' },
  UTMA: { retirement: false, displayName: 'UTMA' },
  'VARIABLE ANNUITY': { retirement: false, displayName: 'Variable Annuity' },
  OTHER: { retirement: false, displayName: 'Other' },

  /* Schwab */

  '403B7': { retirement: true, displayName: '403B7' },
  'CHARITABLE GIFT': { retirement: false, displayName: 'Charitable Gift' },
  CUSTODIAL: { retirement: false, displayName: 'Custodial' },
  'MONEY PURCHASE PLAN': { retirement: false, displayName: 'Money Purchase Plan' },
  'SAR SEP': { retirement: true, displayName: 'Sar Sep' },

  /* Added by Apex team (House Account) */

  'C CORPORATION': { retirement: false, displayName: 'C Corporation' },
  'S CORPORATION': { retirement: false, displayName: 'S Corporation' },
  'LIMITED LIABILITY C': { retirement: false, displayName: 'LLC C Corporation' },
  'LIMITED LIABILITY S': { retirement: false, displayName: 'LLC S Corporation' },

  'LLC C CORPORATION': { retirement: false, displayName: 'LLC C Corporation' },
  'LLC S CORPORATION': { retirement: false, displayName: 'LLC S Corporation' },
};
export type AccountTypeInfo = keyof typeof ACCOUNT_TYPE_INFOS;

const ACCOUNT_TYPES = Object.keys(ACCOUNT_TYPE_INFOS) as AccountType[];

export const RETIREMENT_ACCOUNT_TYPES = new Set(
  ACCOUNT_TYPES.filter((x) => ACCOUNT_TYPE_INFOS[x].retirement),
);

export const CASH_MANAGEMENT_ACCOUNT_TYPES = new Set([
  INDIVIDUAL_CASH_MANAGEMENT,
  JOINT_CASH_MANAGEMENT,
]);
