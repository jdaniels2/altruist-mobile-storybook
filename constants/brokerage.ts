export const Institutions = {
  DriveWealth: 'DRIVEWEALTH',
  ApexCustodian: 'APEX_CUSTODIAN',
  Brokerage: 'BROKERAGE',
  TdAmeritrade: 'TD Ameritrade',
  Ameritrade: 'AMERITRADE',
  Custodian: 'CUSTODIAN',
};
