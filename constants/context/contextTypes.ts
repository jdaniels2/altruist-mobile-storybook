import { Dispatch, SetStateAction, Ref } from 'react';

import { BaseFieldType } from '~/components/molecules/flows/fields';
import { useAccountOpeningFlow } from '~/pages/client/home/components/accountOpeningFlow/logic';
import { BeneficiaryDraftType } from '~/pages/client/home/components/accountOpeningFlow/screens/beneficiaryFlow/fields';
import { BeneficiaryStateType } from '~/pages/client/home/components/accountOpeningFlow/screens/beneficiaryFlow/logic';
import { useChildFlow } from '~/pages/client/home/components/accountOpeningFlow/screens/childFlow/logic';
import { useCorporationState } from '~/pages/client/home/components/accountOpeningFlow/screens/corporationFlow/CorpDetail/logic';
import { useCorporationCorpMemberAuthOfficerSecretaryState } from '~/pages/client/home/components/accountOpeningFlow/screens/corporationFlow/CorpMemberAuthOfficerSecretary/logic';
import { useCorporationBeneficialOwner } from '~/pages/client/home/components/accountOpeningFlow/screens/corporationFlow/CorpMemberBeneficialOwner/logic';
import { useAuthorizedSignerFlow } from '~/pages/client/home/components/accountOpeningFlow/screens/corporationFlow/authorizedSignerFlow/logic';
import { useControlPersonState } from '~/pages/client/home/components/accountOpeningFlow/screens/corporationFlow/controlPersonFlow/logic';
import { useEmployerFlow } from '~/pages/client/home/components/accountOpeningFlow/screens/employerFlow/logic';
import { useJointMoreDetailsFlow } from '~/pages/client/home/components/accountOpeningFlow/screens/jointUserFlow/JointUserMoreDetailsFlow/logic';
import { useJointUserFlow } from '~/pages/client/home/components/accountOpeningFlow/screens/jointUserFlow/logic';
import { useMainUserFlow } from '~/pages/client/home/components/accountOpeningFlow/screens/mainUserFlow/logic';
import { useRetirementTrustDetailsFlow } from '~/pages/client/home/components/accountOpeningFlow/screens/retirementTrustDetailsFlow/logic';
import { useSingleBusinessFlow } from '~/pages/client/home/components/accountOpeningFlow/screens/singleBusinessFlow/logic';
import { useTrustDetailsFlow } from '~/pages/client/home/components/accountOpeningFlow/screens/trustDetailsFlow/logic';
import { useNewTransferFlow } from '~/pages/client/transfers/newTransferFlow/logic';
import { MMA, StateOutput, User2 } from '~/types/graphql';

export type FlowValues = {
  selectedMMA: MMA | undefined;
  approvedMMA: MMA | undefined;
  loading: boolean;
  enableCashJournals: boolean;
  close: () => void;
  onDone: () => void;
  setApprovedMMA: Dispatch<SetStateAction<MMA | undefined>>;
};

export type ResendValues = { contextEmail: string | undefined; setContextEmail: Function };
export type dashboardNextStepCardRef = Ref<any>;

export type AccountOpeningFlowState = ReturnType<typeof useAccountOpeningFlow>;
export type MainUserFlowState = ReturnType<typeof useMainUserFlow>;
export type SingleBusinessFlowState = ReturnType<typeof useSingleBusinessFlow>;
export type BeneficiaryFlowStateType = BeneficiaryStateType & {
  account: StateOutput;
  user: User2;
  draft: BeneficiaryDraftType;
  prevStep: BaseFieldType | undefined;
  close: () => void;
  fromSettings?: boolean;
};
export type NewTransferFlowState = ReturnType<typeof useNewTransferFlow> & {
  progress: number;
  close: () => void;
  canAddFundingAccount: boolean | undefined;
  getNextRoute: () => any;
};
export type TrustDetailsFlowState = ReturnType<typeof useTrustDetailsFlow> & {
  fromSettings?: boolean;
};
export type JointUserFlowState = ReturnType<typeof useJointUserFlow>;
export type JointUserDetailsFlowState = ReturnType<typeof useJointMoreDetailsFlow>;
export type ChildStateType = ReturnType<typeof useChildFlow>;
export type CorporationDetailType = ReturnType<typeof useCorporationState>;
export type AuthorizedSignerType = ReturnType<typeof useAuthorizedSignerFlow>;
export type CorpMemberAuthOfficerType = ReturnType<
  typeof useCorporationCorpMemberAuthOfficerSecretaryState
>;
export type ControlPersonType = ReturnType<typeof useControlPersonState>;
export type EmployerStateType = ReturnType<typeof useEmployerFlow>;
export type RetirementTrustStateType = ReturnType<typeof useRetirementTrustDetailsFlow>;
export type CorpMemberBeneficialOwnerType = ReturnType<typeof useCorporationBeneficialOwner>;
