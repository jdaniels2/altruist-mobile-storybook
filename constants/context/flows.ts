import { Context, createContext } from 'react';

import {
  AccountOpeningFlowState,
  MainUserFlowState,
  BeneficiaryFlowStateType,
  NewTransferFlowState,
  TrustDetailsFlowState,
  JointUserFlowState,
  ChildStateType,
  CorporationDetailType,
  AuthorizedSignerType,
  CorpMemberAuthOfficerType,
  EmployerStateType,
  ControlPersonType,
  CorpMemberBeneficialOwnerType,
  JointUserDetailsFlowState,
  SingleBusinessFlowState,
  RetirementTrustStateType,
} from '~/constants/context/contextTypes';
import { multiAccountFlowContexts } from '~/constants/context/multiAccountContext/multiAccountFlows';

export const flowContexts = {
  AccountOpeningContext: createContext<AccountOpeningFlowState | undefined>(undefined),
  NewTransferContext: createContext<NewTransferFlowState | undefined>(undefined),
  BeneficiaryContext: createContext<BeneficiaryFlowStateType | undefined>(undefined),
  MainUserContext: createContext<MainUserFlowState | undefined>(undefined),
  SingleBusinessContext: createContext<SingleBusinessFlowState | undefined>(undefined),
  TrustDetailsContext: createContext<TrustDetailsFlowState | undefined>(undefined),
  JointUserContext: createContext<JointUserFlowState | undefined>(undefined),
  JointUserMoreDetailsFlow: createContext<JointUserDetailsFlowState | undefined>(undefined),
  ChildContext: createContext<ChildStateType | undefined>(undefined),
  CorporationDetailContext: createContext<CorporationDetailType | undefined>(undefined),
  AuthorizedSignerContext: createContext<AuthorizedSignerType | undefined>(undefined),
  CorpMemberBeneficialOwnerContext: createContext<CorpMemberBeneficialOwnerType | undefined>(
    undefined,
  ),
  CorpMemberAuthOfficerContext: createContext<CorpMemberAuthOfficerType | undefined>(undefined),
  ControlPersonContext: createContext<ControlPersonType | undefined>(undefined),
  EmployerContext: createContext<EmployerStateType | undefined>(undefined),
  RetirementTrustContext: createContext<RetirementTrustStateType | undefined>(undefined),
  ...multiAccountFlowContexts,
};

type FlowContextsType = typeof flowContexts;
export type flowContextType = keyof FlowContextsType;
export type FlowContext<K extends flowContextType> = FlowContextsType[K] extends Context<infer C>
  ? C
  : never;
