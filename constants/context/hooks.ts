import { useContext } from 'react';

import { AccountOpeningFlowState } from '~/constants/context/contextTypes';
import { flowContexts } from '~/constants/context/flows';

export const useAccountOpeningContext = (): AccountOpeningFlowState =>
  useContext(flowContexts.AccountOpeningContext)! ?? {};
