import { createContext } from 'react';

import {
  FlowValues,
  ResendValues,
  dashboardNextStepCardRef,
} from '~/constants/context/contextTypes';

export const RootNavigatorContext = createContext<dashboardNextStepCardRef | undefined>(undefined);
export const MMAContext = createContext<FlowValues>({
  selectedMMA: undefined,
  approvedMMA: undefined,
  enableCashJournals: false,
  setApprovedMMA: () => {},
  loading: false,
  close: () => {},
  onDone: () => {},
});
export const ResendCorpApplicationToSecretaryContext = createContext<ResendValues | undefined>(
  undefined,
);
