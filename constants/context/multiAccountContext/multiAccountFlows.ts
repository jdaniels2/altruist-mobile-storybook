import { createContext } from 'react';

import {
  MultiAccountOpeningIndividualFlowState,
  MultiAccountOpeningFlowState,
} from '~/constants/context/multiAccountContext/multiAccountContextTypes';

export const multiAccountFlowContexts = {
  MultiAccountOpeningContext: createContext<MultiAccountOpeningFlowState | undefined>(undefined),
  IndividualContext: createContext<MultiAccountOpeningIndividualFlowState | undefined>(undefined),
};

export type flowContextType = keyof typeof multiAccountFlowContexts;
