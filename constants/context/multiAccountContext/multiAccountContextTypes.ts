import { useIndividualFlow } from '~/pages/client/home/components/multiAccountOpeningFlow/accounts/Individual/logic';
import useMultiAccountOpeningLogic from '~/pages/client/home/components/multiAccountOpeningFlow/multiAccountOpeningLogic';

export type MultiAccountOpeningIndividualFlowState = ReturnType<typeof useIndividualFlow>;
export type MultiAccountOpeningFlowState = ReturnType<typeof useMultiAccountOpeningLogic>;
