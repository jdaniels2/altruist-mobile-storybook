import moment from 'moment';

import { DateType } from '~/utils/date';

export type FilterOptions = {
  label: string;
  dateFrom: (date?: DateType) => moment.Moment;
};

/** @deprecated Use `filterOptions2` instead. */
export const filterOptions: FilterOptions[] = [
  { label: '5D', dateFrom: (date?: DateType) => moment(date).subtract(5, 'days') },
  { label: '1M', dateFrom: (date?: DateType) => moment(date).subtract(1, 'month') },
  { label: '6M', dateFrom: (date?: DateType) => moment(date).subtract(6, 'month') },
  { label: 'YTD', dateFrom: (date?: DateType) => moment(date).startOf('year') },
  { label: '1Y', dateFrom: (date?: DateType) => moment(date).subtract(1, 'year') },
  { label: 'MAX', dateFrom: () => moment(0) },
];

export type FilterOptions2 = {
  label: string;
  dateFrom: (date?: DateType) => string;
};

export const filterOptions2: FilterOptions2[] = filterOptions.map((a) => ({
  ...a,
  dateFrom(date) {
    return a.dateFrom(date).toISOString();
  },
}));
