export const AsyncStorageKeys = {
  // per session
  pinAttempts: 'pinAttempts',

  // per user
  biometricsEnabled: 'faceIdEnabled',
  timeWentInactive: 'timeWentInactive',
  notFirstLaunch: 'notFirstLogin',
  firstMfaSetup: 'true',
  lastLogin: 'lastLogin',
  fcmToken: 'fcmToken',
  storageVersion: 'storageVersion',
  showSurvey: 'true',
  defaultMfaDevice: 'defaultMfaDevice',
  hasJustLoggedOut: 'hasJustLoggedOut',
  mfaSetupRequired: 'mfaSetupRequired',
  authMethod: 'Credentials',
  signUpDeeplink: 'signUpDeeplink',
  reviewSplitTest: 'reviewSplitTest',
  hyacInvitationAdvisor: 'hyacInvitationAdvisor',
  hasDismissedLinkCard: 'hasDismissedLinkedCard',
  mandatoryVersion: 'mandatoryVersion',
  skippedAppUpdate: 'skippedAppUpdate',
};
