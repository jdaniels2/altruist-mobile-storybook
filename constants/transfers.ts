import moment from 'moment';

import {
  DistributionReason,
  IraContributionType,
  IraDistributionReason,
  TransactionDirection,
} from '~/types/graphql';
import { TransactionType, TRANSACTION_TYPES, ScheduledTransactionFrequency } from '~/types/model';
import { capitalize } from '~/utils/formatter';

export const DAILY_TRANSACTION_LIMIT = 100 * 2500;

export const FREQUENCY_TYPE_OPTIONS: { value: ScheduledTransactionFrequency; label: string }[] = [
  { value: 'ONCE', label: 'Once' },
  { value: 'WEEKLY', label: 'Weekly' },
  { value: 'BIWEEKLY', label: 'Every two weeks' },
  { value: 'MONTHLY', label: 'Monthly' },
  { value: 'QUARTERLY', label: 'Quarterly' },
];

export const IRA_CONTRIBUTION_TYPES_LIST = [
  { label: 'Current', value: 'CURRENT_YEAR' },
  { label: 'Prior', value: 'PRIOR_YEAR' },
] as { value: IraContributionType; label: string }[];

export const IRA_DISTRIBUTION_REASON_LABELS: { [key in IraDistributionReason]: string } = {
  NORMAL: 'Normal',
  DISABILITY: 'Disability',
  PREMATURE: 'Premature',
  DEATH: 'Death',
  EXCESS_CONTRIBUTION_REMOVAL_BEFORE_TAX_DEADLINE: 'Excess Contribution Removal (Current Year)',
  EXCESS_CONTRIBUTION_REMOVAL_AFTER_TAX_DEADLINE: 'Excess Contribution Removal (Prior Year)',
  ROLLOVER_TO_QUALIFIED_PLAN: 'Rollover to qualified plan',
  ROLLOVER_TO_IRA: 'Rollover to IRA',
  TRANSFER: 'Transfer',
  RECHARACTERIZATION_PRIOR_YEAR: 'Recharacterization prior year',
  RECHARACTERIZATION_CURRENT_YEAR: 'Recharacterization current year',
  MANAGEMENT_FEE: 'Management fee',
  PREMATURE_SIMPLE_IRA_LESS_THAN_2_YEARS: 'Premature SIMPLE IRA less than 2 years',
  NORMAL_ROTH_IRA_GREATER_THAN_5_YEARS: 'Normal ROTH IRA greater than 5 years',
  CONVERSION: 'Conversion',
};

export const TAX_WITHHOLDING_LIST = [
  { label: 'Yes', value: true },
  { label: 'No', value: false },
];

export const TRANSACTION_DIRECTION_OPTIONS = [
  {
    value: 'DEPOSIT',
    label: 'Deposit',
  },
  {
    value: 'WITHDRAWAL',
    label: 'Withdrawal',
  },
] as { value: TransactionDirection; label: string }[];

export const TRANSACTION_TYPE_DEFAULT_FILTER_INDEX = 0;

export const TRANSACTION_TYPE_FILTER_OPTIONS = [
  { value: null, label: 'All Transactions' },
  ...TRANSACTION_TYPES.map((type: TransactionType) => ({
    value: type as TransactionType,
    label:
      type === 'ACAT' ? type : type === 'tax_withholding' ? 'Tax Withholding' : capitalize(type),
  })),
];

export const TRANSACTION_RANGE_FILTER_DEFAULT_INDEX = 2;

export const TRANSACTION_RANGE_FILTER_RESET_INDEX = 0;

export const TRANSACTION_RANGE_FILTER_OPTIONS = [
  {
    value: moment(0).format(), // unix epoch time
    label: 'All Time',
  },
  {
    value: moment().subtract(5, 'days').format(),
    label: '5 Days',
  },
  {
    value: moment().subtract(30, 'days').format(),
    label: '30 Days',
  },
  {
    value: moment().subtract(6, 'months').format(),
    label: '6 Months',
  },
  {
    value: moment().subtract(1, 'years').format(),
    label: '1 Year',
  },
  {
    value: moment().subtract(5, 'years').format(),
    label: '5 Years',
  },
  {
    value: moment().subtract(10, 'years').format(),
    label: '10 Years',
  },
];

export const WITHHOLD_DISTRIBUTION_REASONS = [
  'NORMAL',
  'DISABILITY',
  'PREMATURE',
  'DEATH',
  'EXCESS_CONTRIBUTION_REMOVAL_BEFORE_TAX_DEADLINE',
  'EXCESS_CONTRIBUTION_REMOVAL_AFTER_TAX_DEADLINE',
  'PREMATURE_SIMPLE_IRA_LESS_THAN_2_YEARS',
  'NORMAL_ROTH_IRA_GREATER_THAN_5_YEARS',
] as IraDistributionReason[];

export const RECURRING_DISTRIBUTION_REASONS = [
  'EXCESS_CONTRIBUTION_REMOVAL_BEFORE_TAX_DEADLINE',
  'EXCESS_CONTRIBUTION_REMOVAL_AFTER_TAX_DEADLINE',
  'ROLLOVER_TO_QUALIFIED_PLAN',
  'ROLLOVER_TO_IRA',
  'RECHARACTERIZATION_PRIOR_YEAR',
  'RECHARACTERIZATION_CURRENT_YEAR',
] as DistributionReason[];

export const PRIOR_CONTRIBUTION_MIN_DATE = new Date(`${new Date().getFullYear()}-01-01`);

export const PRIOR_CONTRIBUTION_MAX_DATE = new Date(`${new Date().getFullYear()}-04-15`);
