import { StyleSheet, ViewStyle } from 'react-native';

import { theme } from '~/theme/Theme';

type Style = {
  headerWithBorder: ViewStyle;
  headerWithoutBorder: ViewStyle;
};

const headerWithoutBorder = {
  backgroundColor: theme.colors['Light Gray'],
  borderBottomWidth: 0,
  shadowColor: 'transparent',
  elevation: 0,
  shadowOpacity: 0, // hide the shadow for ios
};

export const navigationStyles = StyleSheet.create<Style>({
  headerWithBorder: {
    ...headerWithoutBorder,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors['20Black'],
  },
  headerWithoutBorder,
});
