/* eslint-disable */
/* * * * * * * * * * * * * * * * * * * * * * * 
    THIS FILE IS GENERATED, DO NOT EDIT!
    (use `npm run graphql:types` to update)          
 * * * * * * * * * * * * * * * * * * * * * * */

export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateOnly: any;
  DateTime: any;
  SafeInt: any;
  Upload: any;
};

export type ACATByIdWhereInput = {
  readonly acatId: Scalars['ID'];
};

export type ACATInput = {
  readonly carryingAccountNumber?: InputMaybe<Scalars['String']>;
  readonly carryingDtcNo?: InputMaybe<Scalars['String']>;
  readonly partial?: InputMaybe<ReadonlyArray<InputMaybe<ACATPartialInput>>>;
  readonly subType?: InputMaybe<ACATSubType>;
};

export type ACATPartialInput = {
  readonly qty?: InputMaybe<Scalars['String']>;
  readonly ticker?: InputMaybe<Scalars['String']>;
};

export type ACATPartialType = {
  readonly __typename?: 'ACATPartialType';
  readonly qty?: Maybe<Scalars['String']>;
  readonly ticker?: Maybe<Scalars['String']>;
};

export enum ACATStatus {
  ACCEPTED = 'ACCEPTED',
  APPROVAL_EXPIRED = 'APPROVAL_EXPIRED',
  ASSETS_UNDER_REVIEW = 'ASSETS_UNDER_REVIEW',
  CANCELLED = 'CANCELLED',
  COMPLETE = 'COMPLETE',
  ERROR = 'ERROR',
  FAILED = 'FAILED',
  IN_PROGRESS = 'IN_PROGRESS',
  NEEDS_RESUBMISSION = 'NEEDS_RESUBMISSION',
  PENDING = 'PENDING',
  PENDING_APPROVAL = 'PENDING_APPROVAL',
  PENDING_SUBMITTED = 'PENDING_SUBMITTED',
  REJECT = 'REJECT',
  SETTLE_CLOSE = 'SETTLE_CLOSE',
  SETTLE_PREP = 'SETTLE_PREP',
  SUBMITTED = 'SUBMITTED',
  UNDEFINED = 'UNDEFINED',
  UNDER_REVIEW = 'UNDER_REVIEW'
}

export enum ACATSubType {
  FULL = 'FULL',
  PARTIAL = 'PARTIAL'
}

export type ACATTransaction = {
  readonly __typename?: 'ACATTransaction';
  readonly acatPositions?: Maybe<ReadonlyArray<Maybe<AcatPositions>>>;
  readonly acatType: ACATTransferType;
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly carryingAccountNumber?: Maybe<Scalars['String']>;
  readonly carryingDtcNumber?: Maybe<Scalars['String']>;
  readonly createdDate: Scalars['String'];
  readonly description?: Maybe<Scalars['String']>;
  readonly direction?: Maybe<Scalars['String']>;
  readonly errorReason?: Maybe<Scalars['String']>;
  readonly financialAccountId: Scalars['ID'];
  readonly financialAccountName?: Maybe<Scalars['String']>;
  readonly financialInstitutionName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly status: ACATStatus;
  readonly type?: Maybe<Scalars['String']>;
  readonly uiStatus?: Maybe<UIACATStatus>;
  readonly updatedDate: Scalars['String'];
};

export type ACATTransferStatusLog = {
  readonly __typename?: 'ACATTransferStatusLog';
  readonly acatStatus: ACATStatus;
  readonly acatTransferStatusLogUuid: Scalars['ID'];
  readonly acatTransferUuid: Scalars['ID'];
  readonly clientName?: Maybe<Scalars['String']>;
  readonly createdDate: Scalars['String'];
  readonly partnerErrorCode?: Maybe<Scalars['String']>;
  readonly partnerMessage?: Maybe<Scalars['String']>;
  readonly userUuid: Scalars['ID'];
};

export enum ACATTransferType {
  FAIL_REVERSAL_BROKER_TO_BROKER_ONLY = 'FAIL_REVERSAL_BROKER_TO_BROKER_ONLY',
  FULL_TRANSFER = 'FULL_TRANSFER',
  MUTUAL_FUND_CLEANUP = 'MUTUAL_FUND_CLEANUP',
  PARTIAL_TRANSFER_DELIVERER = 'PARTIAL_TRANSFER_DELIVERER',
  PARTIAL_TRANSFER_RECEIVER = 'PARTIAL_TRANSFER_RECEIVER',
  POSITION_TRANSFER_FUND_FIRM_TO_MUTUAL_FUND_COMPANY_ONLY = 'POSITION_TRANSFER_FUND_FIRM_TO_MUTUAL_FUND_COMPANY_ONLY',
  RECLAIM = 'RECLAIM',
  RESIDUAL_CREDIT = 'RESIDUAL_CREDIT',
  UNDEFINED = 'UNDEFINED'
}

export type ACATType = {
  readonly __typename?: 'ACATType';
  readonly carryingAccountNumber?: Maybe<Scalars['String']>;
  readonly carryingDtcName?: Maybe<Scalars['String']>;
  readonly carryingDtcNo?: Maybe<Scalars['String']>;
  readonly partial?: Maybe<ReadonlyArray<Maybe<ACATPartialType>>>;
  readonly subType?: Maybe<ACATSubType>;
};

export type ACHInput = {
  readonly accountNumber?: InputMaybe<Scalars['String']>;
  readonly depositAmount?: InputMaybe<Scalars['String']>;
  readonly iraContribution?: InputMaybe<IraContributionType>;
  readonly routingNumber?: InputMaybe<Scalars['String']>;
  readonly type?: InputMaybe<Scalars['String']>;
};

export type ACHType = {
  readonly __typename?: 'ACHType';
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly depositAmount?: Maybe<Scalars['String']>;
  readonly iraContribution?: Maybe<IraContributionType>;
  readonly routingNumber?: Maybe<Scalars['String']>;
  readonly type?: Maybe<Scalars['String']>;
};

export type ADVFormsInput = {
  readonly formAdvPart1: Scalars['String'];
  readonly formAdvPart2A: Scalars['String'];
  readonly formAdvPart2B?: InputMaybe<Scalars['String']>;
  readonly formAdvPart3?: InputMaybe<Scalars['String']>;
};

export type ADVFormsResponse = {
  readonly __typename?: 'ADVFormsResponse';
  readonly formAdvPart1?: Maybe<Scalars['String']>;
  readonly formAdvPart2A?: Maybe<Scalars['String']>;
  readonly formAdvPart2B?: Maybe<Scalars['String']>;
  readonly formAdvPart3?: Maybe<Scalars['String']>;
};

export enum ADVISOR_STATUS {
  APPROVED = 'APPROVED',
  COMPLETE = 'COMPLETE',
  DRAFT = 'DRAFT',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED'
}

export type AcatDraft = {
  readonly __typename?: 'AcatDraft';
  readonly draft: AcatDraftData;
  readonly id: Scalars['ID'];
};

export type AcatDraftData = {
  readonly __typename?: 'AcatDraftData';
  readonly acatType: ACATTransferType;
  readonly accountNumber: Scalars['String'];
  readonly clientApprovalStatus: AcatDraftStatus;
  readonly participantNumber: Scalars['String'];
};

export type AcatDraftInput = {
  readonly accountStateId: Scalars['ID'];
  readonly draft: AcatDraftInputData;
};

export type AcatDraftInputData = {
  readonly acatType: ACATTransferType;
  readonly accountNumber: Scalars['String'];
  readonly participantNumber: Scalars['String'];
};

export enum AcatDraftStatus {
  CLIENT_REVIEW_APPROVED = 'CLIENT_REVIEW_APPROVED',
  CLIENT_REVIEW_REJECTED = 'CLIENT_REVIEW_REJECTED',
  PENDING_CLIENT_REVIEW = 'PENDING_CLIENT_REVIEW'
}

export type AcatDraftStatusChangeInput = {
  readonly id: Scalars['ID'];
};

export type AcatDraftUpdateInput = {
  readonly draft: AcatDraftInputData;
  readonly id: Scalars['ID'];
};

export type AcatDraftsGroupedByAccountState = {
  readonly __typename?: 'AcatDraftsGroupedByAccountState';
  readonly accountStates: ReadonlyArray<Maybe<AccountStatesWithAcatDrafts>>;
};

export type AcatPositions = {
  readonly __typename?: 'AcatPositions';
  readonly amount?: Maybe<Scalars['Float']>;
  readonly cusip?: Maybe<Scalars['String']>;
  readonly quantity?: Maybe<Scalars['Float']>;
  readonly ticker?: Maybe<Scalars['String']>;
};

export type AccessRequestResponse = {
  readonly __typename?: 'AccessRequestResponse';
  readonly success?: Maybe<Scalars['Boolean']>;
};

export type Account = {
  readonly __typename?: 'Account';
  readonly acats?: Maybe<ReadonlyArray<Maybe<ACATTransaction>>>;
  readonly accountNumber?: Maybe<Scalars['ID']>;
  readonly brokerageAccountId?: Maybe<Scalars['ID']>;
  readonly journals?: Maybe<ReadonlyArray<Maybe<CashJournalTransaction>>>;
  readonly transactions: ReadonlyArray<Transaction>;
};

export type AccountACATSWhereInput = {
  readonly financialAccountId: Scalars['ID'];
};

export type AccountActivity = {
  readonly expectedWithdrawalFrequency?: InputMaybe<ExpectedWithdrawalFrequency>;
  readonly primaryEntityAccountActivity?: InputMaybe<PrimaryEntityAccountActivity>;
};

export type AccountAddress = {
  readonly __typename?: 'AccountAddress';
  readonly city?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly street?: Maybe<Scalars['String']>;
  readonly type?: Maybe<Scalars['String']>;
  readonly zip?: Maybe<Scalars['String']>;
};

export type AccountAddressInput = {
  readonly city: Scalars['String'];
  readonly state: Scalars['String'];
  readonly street: Scalars['String'];
  readonly zip: Scalars['String'];
};

export type AccountAllocation = {
  readonly __typename?: 'AccountAllocation';
  readonly allocation: Scalars['Float'];
  readonly canUnassign: Scalars['Boolean'];
  readonly excluded?: Maybe<Scalars['Boolean']>;
  readonly financialAccountId: Scalars['ID'];
  readonly id?: Maybe<Scalars['ID']>;
};

export type AccountAllocation2 = {
  readonly __typename?: 'AccountAllocation2';
  readonly allocation?: Maybe<Scalars['Float']>;
  readonly excluded?: Maybe<Scalars['Boolean']>;
  readonly financialAccountId: Scalars['ID'];
};

export type AccountAllocation2Input = {
  readonly allocation?: InputMaybe<Scalars['Float']>;
  readonly excluded?: InputMaybe<Scalars['Boolean']>;
  readonly financialAccountId: Scalars['ID'];
};

export type AccountAllocationInput = {
  readonly allocation: Scalars['Float'];
  readonly canUnassign: Scalars['Boolean'];
  readonly excluded?: InputMaybe<Scalars['Boolean']>;
  readonly financialAccountId: Scalars['ID'];
  readonly id?: InputMaybe<Scalars['ID']>;
};

export enum AccountAnalysisStatus {
  FAILED = 'FAILED',
  REQUIRED = 'REQUIRED',
  SUCCESS = 'SUCCESS'
}

export type AccountBeneficiariesByClientsWhere = {
  readonly userIds: ReadonlyArray<Scalars['ID']>;
};

export type AccountBeneficiariesByUser = {
  readonly __typename?: 'AccountBeneficiariesByUser';
  readonly beneficiaries: ReadonlyArray<Beneficiary>;
  readonly userId: Scalars['ID'];
};

export type AccountBeneficiariesWhere = {
  readonly accountId: Scalars['ID'];
};

export type AccountBeneficiaryByIdWhere = {
  readonly accountId: Scalars['ID'];
  readonly id: Scalars['ID'];
};

export type AccountCashSummary = {
  readonly __typename?: 'AccountCashSummary';
  /** @deprecated Not Used */
  readonly availableForTrade: Scalars['Float'];
  readonly availableForWithdrawal: Scalars['Float'];
  /** @deprecated Not Used */
  readonly balance: Scalars['Float'];
  /** @deprecated Not Used */
  readonly financialAccountId: Scalars['ID'];
  readonly pendingBuyOrdersNotional?: Maybe<Scalars['Float']>;
  readonly pendingSellOrdersNotional?: Maybe<Scalars['Float']>;
  readonly pendingWithdrawals?: Maybe<Scalars['Float']>;
  readonly unsettledCash?: Maybe<Scalars['Float']>;
};

export enum AccountClassification {
  BROKERAGE = 'BROKERAGE',
  CUSTODIAN = 'CUSTODIAN',
  EXTERNAL = 'EXTERNAL',
  FUNDING = 'FUNDING',
  HOUSE = 'HOUSE'
}

export enum AccountCreationStatus {
  ACTIVE = 'ACTIVE',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED',
  RESTRICTED = 'RESTRICTED'
}

export type AccountDetails = {
  readonly __typename?: 'AccountDetails';
  readonly accountCashSummary?: Maybe<AccountCashSummary>;
  /** @deprecated Not Used */
  readonly accountCategory?: Maybe<Scalars['String']>;
  readonly accountClassification?: Maybe<Scalars['String']>;
  readonly accountNickname?: Maybe<Scalars['String']>;
  readonly accountNumber?: Maybe<Scalars['String']>;
  /** @deprecated Not Used */
  readonly accountType?: Maybe<Scalars['String']>;
  readonly balance?: Maybe<Scalars['Float']>;
  readonly category?: Maybe<Scalars['String']>;
  readonly externalConnection?: Maybe<ExternalConnection>;
  readonly financialAccountId?: Maybe<Scalars['ID']>;
  readonly financialAccountName: Scalars['String'];
  readonly financialInstitution?: Maybe<Scalars['String']>;
  /** @deprecated Not Used */
  readonly inceptionDate?: Maybe<Scalars['String']>;
  readonly institutionName?: Maybe<Scalars['String']>;
  readonly lastUpdatedDate?: Maybe<Scalars['String']>;
  readonly lastUpdatedDays?: Maybe<Scalars['Float']>;
  readonly managedAccount?: Maybe<Scalars['Boolean']>;
  readonly mask?: Maybe<Scalars['String']>;
  readonly ownerFirstName?: Maybe<Scalars['String']>;
  readonly ownerLastName?: Maybe<Scalars['String']>;
  readonly ownerState?: Maybe<Scalars['String']>;
  readonly realTimeBalance?: Maybe<Scalars['Float']>;
  readonly status?: Maybe<Scalars['String']>;
};

export type AccountDisclosures = {
  readonly companyTickerSymbols?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['String']>>>;
  readonly entityOrAssociatesHaveNegativeNews?: InputMaybe<Scalars['Boolean']>;
  readonly hasRelatedAccounts?: InputMaybe<Scalars['Boolean']>;
  readonly isAffiliatedExchangeOrFINRA?: InputMaybe<Scalars['Boolean']>;
  readonly isControlPerson?: InputMaybe<Scalars['Boolean']>;
  readonly isExemptLegalCustomer?: InputMaybe<Scalars['Boolean']>;
  readonly isForeignBank?: InputMaybe<Scalars['Boolean']>;
  readonly isMaintainedForForeignFinancialInstitution?: InputMaybe<Scalars['Boolean']>;
  readonly isPabAccount?: InputMaybe<Scalars['Boolean']>;
  readonly isPoliticallyExposed?: InputMaybe<Scalars['Boolean']>;
  readonly memberFirmName?: InputMaybe<Scalars['String']>;
};

export enum AccountDocumentType {
  BUSINESS = 'BUSINESS',
  DEATH_CERTIFICATE = 'DEATH_CERTIFICATE',
  FINRA = 'FINRA',
  IMA = 'IMA',
  NON_PROFIT = 'NON_PROFIT',
  RETIREMENT_TRUST = 'RETIREMENT_TRUST',
  SIMPLE = 'SIMPLE',
  SOLO_401 = 'SOLO_401',
  TRUST_147C = 'TRUST_147C'
}

export type AccountForInternalTransfer = {
  readonly __typename?: 'AccountForInternalTransfer';
  readonly accountNumber: Scalars['String'];
  readonly accountType: Scalars['String'];
  readonly financialAccountId: Scalars['ID'];
  readonly financialAccountName: Scalars['String'];
  readonly hasMMAWithBrokerageAccount: Scalars['Boolean'];
  readonly owners: ReadonlyArray<StatusOwnerDetails>;
  readonly status: AccountCreationStatus;
};

export type AccountFunded = {
  readonly __typename?: 'AccountFunded';
  readonly financialAccountId?: Maybe<Scalars['ID']>;
  readonly fundedDay?: Maybe<Scalars['DateOnly']>;
};

export type AccountInfo = {
  readonly __typename?: 'AccountInfo';
  readonly accountNumberWithCustodian?: Maybe<Scalars['String']>;
  readonly accountStatus?: Maybe<Scalars['String']>;
  readonly bifrostInceptionDate?: Maybe<Scalars['String']>;
  readonly custodianInceptionDate?: Maybe<Scalars['String']>;
  readonly fundingDate?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly lastReconciliation?: Maybe<Scalars['String']>;
};

export type AccountInfoInput = {
  readonly financialAccountIds: ReadonlyArray<Scalars['ID']>;
};

export type AccountInformation = {
  readonly businessAddress: AddressDetails;
  readonly businessPhoneNumber: Scalars['String'];
  readonly name: Scalars['String'];
  readonly stateOfOrigin: Scalars['String'];
  readonly taxId: Scalars['String'];
  readonly type: AccountInformationType;
};

export enum AccountInformationType {
  C_CORPORATION = 'C_CORPORATION',
  LIMITED_LIABILITY_COMPANY_C_CORPORATION = 'LIMITED_LIABILITY_COMPANY_C_CORPORATION',
  LIMITED_LIABILITY_COMPANY_PARTNERSHIP = 'LIMITED_LIABILITY_COMPANY_PARTNERSHIP',
  LIMITED_LIABILITY_COMPANY_S_CORPORATION = 'LIMITED_LIABILITY_COMPANY_S_CORPORATION',
  PARTNERSHIP = 'PARTNERSHIP',
  S_CORPORATION = 'S_CORPORATION'
}

export type AccountInput = {
  readonly accountType: Scalars['String'];
  readonly altruistRepcode?: InputMaybe<Scalars['String']>;
  readonly backupWithholding?: InputMaybe<Scalars['Boolean']>;
  readonly beneficiaries?: InputMaybe<ReadonlyArray<BeneficiaryItemInput>>;
  readonly corporationDetails?: InputMaybe<CorporationDetailsInput>;
  readonly decedentId?: InputMaybe<Scalars['ID']>;
  readonly decedentName?: InputMaybe<Scalars['String']>;
  readonly documents?: InputMaybe<ReadonlyArray<AccountStateDocumentInput>>;
  readonly finraAffiliations?: InputMaybe<ReadonlyArray<AccountStateFinraAffiliationInput>>;
  readonly minorUserId?: InputMaybe<Scalars['ID']>;
  readonly planDetails?: InputMaybe<AccountStatePlanDetailsInput>;
  readonly planName?: InputMaybe<Scalars['String']>;
  readonly planTaxID?: InputMaybe<Scalars['String']>;
  readonly planType?: InputMaybe<Scalars['String']>;
  readonly purposeOfAccount?: InputMaybe<PurposeOfAccount>;
  readonly retirement?: InputMaybe<Scalars['Boolean']>;
  readonly solo401kDetails?: InputMaybe<Solo401kDetailsInput>;
  readonly sourceOfFunds?: InputMaybe<SourceOfFunds>;
  readonly stateOfFormation?: InputMaybe<Scalars['String']>;
  readonly trustDetails?: InputMaybe<TrustInput>;
};

export type AccountManagement = {
  readonly __typename?: 'AccountManagement';
  readonly addresses?: Maybe<ReadonlyArray<AccountAddress>>;
  readonly email?: Maybe<Scalars['String']>;
  readonly jointOwnerInfo?: Maybe<JointOwnerInfo>;
  readonly phoneNumber?: Maybe<Scalars['String']>;
};

export type AccountManagementAddressInput = {
  readonly address?: InputMaybe<AccountAddressInput>;
  readonly financialAccountId: Scalars['String'];
};

export type AccountManagementEmailInput = {
  readonly email?: InputMaybe<Scalars['String']>;
  readonly financialAccountId: Scalars['String'];
};

export type AccountManagementInput = {
  readonly address: AccountAddressInput;
  readonly email: Scalars['String'];
  readonly financialAccountId: Scalars['String'];
  readonly phoneNumber?: InputMaybe<Scalars['String']>;
};

export type AccountManagementOutput = {
  readonly __typename?: 'AccountManagementOutput';
  readonly address?: Maybe<SuccessOutput>;
  readonly email?: Maybe<SuccessOutput>;
  readonly phoneNumber?: Maybe<SuccessOutput>;
};

export type AccountManagementPhoneNumberInput = {
  readonly financialAccountId: Scalars['String'];
  readonly phoneNumber?: InputMaybe<Scalars['String']>;
};

export type AccountName = {
  readonly __typename?: 'AccountName';
  readonly financialAccountId: Scalars['ID'];
  readonly financialAccountName?: Maybe<Scalars['String']>;
  readonly originalAccountName?: Maybe<Scalars['String']>;
};

export enum AccountNature {
  ADMINISTRATIVE_AND_SUPPORT_SERVICES = 'ADMINISTRATIVE_AND_SUPPORT_SERVICES',
  /** @deprecated Remove field after Altruist Clearing migration. Requires BE DTO cleanup first. */
  AGRICULTURE_FORESTRY_FISHING = 'AGRICULTURE_FORESTRY_FISHING',
  AGRICULTURE_FORESTRY_FISHING_AND_HUNTING = 'AGRICULTURE_FORESTRY_FISHING_AND_HUNTING',
  ARTS_ENTERTAINMENT_AND_RECREATION = 'ARTS_ENTERTAINMENT_AND_RECREATION',
  CONSTRUCTION = 'CONSTRUCTION',
  DATA_PROCESSING_HOSTING_AND_RELATED_SERVICES = 'DATA_PROCESSING_HOSTING_AND_RELATED_SERVICES',
  EDUCATIONAL_SERVICES = 'EDUCATIONAL_SERVICES',
  FINANCE_AND_INSURANCE = 'FINANCE_AND_INSURANCE',
  /** @deprecated Remove field after Altruist Clearing migration. Requires BE DTO cleanup first. */
  FINANCE_INSURANCE_REAL_ESTATE = 'FINANCE_INSURANCE_REAL_ESTATE',
  HEALTH_CARE_AND_SOCIAL_ASSISTANCE = 'HEALTH_CARE_AND_SOCIAL_ASSISTANCE',
  HOTELS_ACCOMMODATIONS = 'HOTELS_ACCOMMODATIONS',
  MANAGEMENT_OF_COMPANIES_AND_ENTERPRISES = 'MANAGEMENT_OF_COMPANIES_AND_ENTERPRISES',
  MANUFACTURING = 'MANUFACTURING',
  /** @deprecated Remove field after Altruist Clearing migration. Requires BE DTO cleanup first. */
  MINING = 'MINING',
  MINING_QUARRYING_AND_OIL_AND_GAS_EXTRACTION = 'MINING_QUARRYING_AND_OIL_AND_GAS_EXTRACTION',
  PERSONAL_AND_LAUNDRY_SERVICES = 'PERSONAL_AND_LAUNDRY_SERVICES',
  POSTAL_SERVICE_COURIERS_PACKAGE_DELIVERY = 'POSTAL_SERVICE_COURIERS_PACKAGE_DELIVERY',
  PRINTING_AND_RELATED_SUPPORT_ACTIVITIES = 'PRINTING_AND_RELATED_SUPPORT_ACTIVITIES',
  PROFESSIONAL_SCIENTIFIC_AND_TECHNICAL_SERVICES = 'PROFESSIONAL_SCIENTIFIC_AND_TECHNICAL_SERVICES',
  /** @deprecated Remove field after Altruist Clearing migration. Requires BE DTO cleanup first. */
  PUBLIC_ADMINISTRATION = 'PUBLIC_ADMINISTRATION',
  PUBLISHING_INDUSTRIES = 'PUBLISHING_INDUSTRIES',
  REAL_ESTATE_AND_RENTAL_AND_LEASING = 'REAL_ESTATE_AND_RENTAL_AND_LEASING',
  RELIGIOUS_GRANTMAKING_CIVIC_PROFESSIONAL_AND_SIMILAR_ORGS = 'RELIGIOUS_GRANTMAKING_CIVIC_PROFESSIONAL_AND_SIMILAR_ORGS',
  REPAIR_AND_MAINTENANCE = 'REPAIR_AND_MAINTENANCE',
  RETAIL_TRADE = 'RETAIL_TRADE',
  /** @deprecated Remove field after Altruist Clearing migration. Requires BE DTO cleanup first. */
  SERVICES = 'SERVICES',
  TELECOMMUNICATIONS = 'TELECOMMUNICATIONS',
  TRANSPORTATION = 'TRANSPORTATION',
  /** @deprecated Remove field after Altruist Clearing migration. Requires BE DTO cleanup first. */
  TRANSPORTATION_COMMUNICATIONS_ELECTRIC_GAS_SANITARY_SERVICES = 'TRANSPORTATION_COMMUNICATIONS_ELECTRIC_GAS_SANITARY_SERVICES',
  UTILITIES = 'UTILITIES',
  WAREHOUSING_AND_STORAGE = 'WAREHOUSING_AND_STORAGE',
  WASTE_MANAGEMENT_AND_REMEDIATION_SERVICES = 'WASTE_MANAGEMENT_AND_REMEDIATION_SERVICES',
  /** @deprecated Remove field after Altruist Clearing migration. Requires BE DTO cleanup first. */
  WHOLESALE_TRADE = 'WHOLESALE_TRADE'
}

export type AccountOwner = {
  readonly __typename?: 'AccountOwner';
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly accountType: Scalars['String'];
  readonly ownerFirstName: Scalars['String'];
  readonly ownerLastName: Scalars['String'];
};

export type AccountScheduleAssignment = ScheduleAssignment & {
  readonly __typename?: 'AccountScheduleAssignment';
  readonly endDate?: Maybe<Scalars['String']>;
  readonly financialAccountId?: Maybe<Scalars['ID']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
  readonly scheduleId?: Maybe<Scalars['ID']>;
  readonly scheduleName?: Maybe<Scalars['String']>;
  readonly startDate?: Maybe<Scalars['DateOnly']>;
};

export type AccountStateCreationStatus = {
  readonly __typename?: 'AccountStateCreationStatus';
  readonly accountStateId: Scalars['ID'];
  readonly status?: Maybe<AccountStatusOutput>;
};

export type AccountStateDocument = {
  readonly __typename?: 'AccountStateDocument';
  readonly documentId: Scalars['ID'];
  readonly documentType: AccountDocumentType;
  readonly userId?: Maybe<Scalars['ID']>;
};

export type AccountStateDocumentInput = {
  readonly documentId: Scalars['ID'];
  readonly documentType: AccountDocumentType;
  readonly userId?: InputMaybe<Scalars['ID']>;
};

export type AccountStateFinraAffiliation = {
  readonly __typename?: 'AccountStateFinraAffiliation';
  readonly employerDuplicateStatementEnrolled?: Maybe<Scalars['Boolean']>;
  readonly employerEnrolledMailingAddress?: Maybe<EmployerEnrolledMailingAddress>;
  readonly firmName?: Maybe<Scalars['String']>;
  readonly userId: Scalars['ID'];
};

export type AccountStateFinraAffiliationInput = {
  readonly employerDuplicateStatementEnrolled?: InputMaybe<Scalars['Boolean']>;
  readonly employerEnrolledMailingAddress?: InputMaybe<EmployerEnrolledMailingAddressInput>;
  readonly firmName?: InputMaybe<Scalars['String']>;
  readonly userId: Scalars['ID'];
};

export type AccountStateIdsInput = {
  readonly accountStateIds: ReadonlyArray<Scalars['ID']>;
};

export type AccountStatePlanDetails = {
  readonly __typename?: 'AccountStatePlanDetails';
  readonly participantUserId?: Maybe<Scalars['ID']>;
  readonly planDetailsId?: Maybe<Scalars['ID']>;
};

export type AccountStatePlanDetailsInput = {
  readonly participantUserId?: InputMaybe<Scalars['ID']>;
  readonly planDetailsId?: InputMaybe<Scalars['ID']>;
};

export type AccountStatesCreationStatusWhere = {
  readonly accountStateIds: ReadonlyArray<Scalars['ID']>;
};

export type AccountStatesGroupedByMultiAccountDraftId = {
  readonly __typename?: 'AccountStatesGroupedByMultiAccountDraftId';
  readonly id: Scalars['ID'];
  readonly states: ReadonlyArray<StateOutput>;
};

export type AccountStatesWhere = {
  readonly ids: ReadonlyArray<Scalars['ID']>;
};

export type AccountStatesWithAcatDrafts = {
  readonly __typename?: 'AccountStatesWithAcatDrafts';
  readonly acatDrafts: ReadonlyArray<Maybe<AcatDraft>>;
  readonly accountStateId: Scalars['ID'];
};

export type AccountStatusOutput = {
  readonly __typename?: 'AccountStatusOutput';
  readonly accountNumber: Scalars['String'];
  readonly accountType: Scalars['String'];
  readonly financialAccountId: Scalars['ID'];
  readonly financialAccountName: Scalars['String'];
  readonly owners: ReadonlyArray<StatusOwnerDetails>;
  readonly status: AccountCreationStatus;
};

export type AccountSummary = {
  readonly __typename?: 'AccountSummary';
  readonly altruistRank?: Maybe<Scalars['Float']>;
  readonly balance: Scalars['Float'];
  readonly cashPercent: Scalars['Float'];
  readonly numberOfAccounts?: Maybe<Scalars['Int']>;
};

export type AccountTaxBudget = {
  readonly __typename?: 'AccountTaxBudget';
  readonly amount: Scalars['Float'];
  readonly prorataDistribution: TaxBudgetDistribution;
  readonly units: TaxBudgetUnits;
};

export type AccountTaxBudgetInput = {
  readonly amount: Scalars['Float'];
  readonly prorataDistribution: TaxBudgetDistribution;
  readonly units: TaxBudgetUnits;
};

export type AccountTaxRates = {
  readonly __typename?: 'AccountTaxRates';
  readonly longTerm: Scalars['Float'];
  readonly shortTerm: Scalars['Float'];
};

export type AccountTaxRatesInput = {
  readonly longTerm: Scalars['Float'];
  readonly shortTerm: Scalars['Float'];
};

export type AccountTaxSettings = {
  readonly __typename?: 'AccountTaxSettings';
  readonly financialAccountId: Scalars['ID'];
  readonly taxBudget?: Maybe<AccountTaxBudget>;
  readonly taxLossHarvesting: Scalars['Boolean'];
  /** @deprecated Use taxManagementAvailable query instead, because that value is available wether or not tax settings exist */
  readonly taxManagementAvailable?: Maybe<Scalars['Boolean']>;
  readonly taxRates?: Maybe<AccountTaxRates>;
  readonly taxSensitivity: Scalars['Boolean'];
};

export type AccountType = {
  readonly __typename?: 'AccountType';
  readonly accountType: Scalars['String'];
  readonly altruistRepcode?: Maybe<Scalars['String']>;
  readonly backupWithholding?: Maybe<Scalars['Boolean']>;
  readonly beneficiaries?: Maybe<ReadonlyArray<BeneficiaryItemType>>;
  readonly corporationDetails?: Maybe<CorporationDetails>;
  readonly createdBy: Scalars['ID'];
  readonly createdByUser: User2;
  readonly decedentId?: Maybe<Scalars['ID']>;
  readonly decedentName?: Maybe<Scalars['String']>;
  readonly documents?: Maybe<ReadonlyArray<AccountStateDocument>>;
  readonly finraAffiliations?: Maybe<ReadonlyArray<AccountStateFinraAffiliation>>;
  readonly minorUserId?: Maybe<Scalars['ID']>;
  readonly planDetails?: Maybe<AccountStatePlanDetails>;
  readonly planName?: Maybe<Scalars['String']>;
  readonly planTaxID?: Maybe<Scalars['String']>;
  readonly planType?: Maybe<Scalars['String']>;
  readonly purposeOfAccount?: Maybe<PurposeOfAccount>;
  readonly retirement?: Maybe<Scalars['Boolean']>;
  readonly solo401kDetails?: Maybe<Solo401kDetailsType>;
  readonly sourceOfFunds?: Maybe<SourceOfFunds>;
  readonly stateOfFormation?: Maybe<Scalars['String']>;
  readonly trustDetails?: Maybe<TrustOutput>;
};

export type AccountsInGroupInput = {
  readonly groupId: Scalars['ID'];
};

export type ActivityReportRequestInput = {
  readonly from: Scalars['String'];
  readonly householdIds: ReadonlyArray<Scalars['ID']>;
  readonly to: Scalars['String'];
};

export type ActivityReportRequestResult = {
  readonly __typename?: 'ActivityReportRequestResult';
  readonly status: Scalars['String'];
};

export enum ActivityType {
  ALLOCATED = 'ALLOCATED',
  APPROVED = 'APPROVED',
  CANCELED = 'CANCELED',
  CANCELLED = 'CANCELLED',
  CANCEL_REJECTED = 'CANCEL_REJECTED',
  COMPLETE = 'COMPLETE',
  CREATED = 'CREATED',
  DONE_FOR_DAY = 'DONE_FOR_DAY',
  EXPIRED = 'EXPIRED',
  FILL = 'FILL',
  NEW = 'NEW',
  NEW_CANCEL = 'NEW_CANCEL',
  PARTIAL_FILL = 'PARTIAL_FILL',
  PENDING_COMPLETE = 'PENDING_COMPLETE',
  REJECTED = 'REJECTED',
  REVIEW = 'REVIEW'
}

export type AddFidelityGNumberInput = {
  readonly repCode: Scalars['String'];
};

export type AddSchwabMasterAccountNumberInput = {
  readonly repCode: Scalars['String'];
};

export type AdditionalAccounts = {
  readonly confirmEntityDoesNotIssueBearerShares: Scalars['Boolean'];
  readonly primaryOngoingFundingSource: PrimaryOngoingFundingSource;
  readonly primaryOngoingFundingSourceClarification?: InputMaybe<Scalars['String']>;
  readonly scopeOfBusiness: ScopeOfBusiness;
  readonly scopeOfBusinessClarification?: InputMaybe<Scalars['String']>;
};

export type AdditionalInfo = {
  readonly __typename?: 'AdditionalInfo';
  readonly fax?: Maybe<Scalars['String']>;
  readonly phone?: Maybe<Scalars['String']>;
};

export type Address = {
  readonly __typename?: 'Address';
  readonly addr1?: Maybe<Scalars['String']>;
  readonly addr2?: Maybe<Scalars['String']>;
  readonly addr3?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly country?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly state?: Maybe<Scalars['String']>;
  readonly zipCode?: Maybe<Scalars['String']>;
};

export type AddressDetails = {
  readonly city: Scalars['String'];
  readonly country: Scalars['String'];
  readonly postalCode: Scalars['String'];
  readonly state: Scalars['String'];
  readonly streetAddress: ReadonlyArray<Scalars['String']>;
};

export type AddressInput = {
  readonly addr1: Scalars['String'];
  readonly addr2?: InputMaybe<Scalars['String']>;
  readonly addr3?: InputMaybe<Scalars['String']>;
  readonly city: Scalars['String'];
  readonly country: Scalars['String'];
  readonly state: Scalars['String'];
  readonly zipCode: Scalars['String'];
};

export type AddressOutput = {
  readonly __typename?: 'AddressOutput';
  readonly addr1?: Maybe<Scalars['String']>;
  readonly addr2?: Maybe<Scalars['String']>;
  readonly addr3?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly country?: Maybe<Scalars['String']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly zipCode?: Maybe<Scalars['String']>;
};

export type Adjustment = {
  readonly __typename?: 'Adjustment';
  readonly flow: Scalars['String'];
  readonly flowAmount: Scalars['Float'];
  readonly flowDailyFee: Scalars['Float'];
  readonly flowDaysCount: Scalars['Int'];
  readonly flowFee: Scalars['Float'];
  readonly flowRate?: Maybe<Scalars['Float']>;
};

export type AdoptingEmployer = {
  readonly __typename?: 'AdoptingEmployer';
  readonly address?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly companyName?: Maybe<Scalars['String']>;
  readonly companyTaxId?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly phone?: Maybe<Scalars['String']>;
  readonly ssnTaxId?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly zipCode?: Maybe<Scalars['String']>;
};

export type AdoptingEmployerAddress = {
  readonly __typename?: 'AdoptingEmployerAddress';
  readonly city?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly street?: Maybe<Scalars['String']>;
  readonly zipCode?: Maybe<Scalars['String']>;
};

export type AdoptingEmployerInput = {
  readonly address?: InputMaybe<Scalars['String']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly companyName: Scalars['String'];
  readonly companyTaxId?: InputMaybe<Scalars['String']>;
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly ssnTaxId?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly zipCode?: InputMaybe<Scalars['String']>;
};

export type AdoptingEmployerProfile = {
  readonly __typename?: 'AdoptingEmployerProfile';
  readonly address?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly name?: Maybe<Scalars['String']>;
  readonly phone?: Maybe<Scalars['String']>;
  readonly ssnTaxId?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly zipCode?: Maybe<Scalars['String']>;
};

export type AdoptingEmployerWhere = {
  readonly id: Scalars['ID'];
};

export type Advisor = {
  readonly __typename?: 'Advisor';
  readonly advisorId?: Maybe<Scalars['ID']>;
  readonly avatarId?: Maybe<Scalars['String']>;
  readonly avatarURL?: Maybe<Scalars['String']>;
  /** @deprecated Use avatarURL for consistency */
  readonly avatarUrl?: Maybe<Scalars['String']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly organaizationName?: Maybe<Scalars['String']>;
  readonly organizationId?: Maybe<Scalars['String']>;
  readonly phoneNumber?: Maybe<Scalars['String']>;
  readonly profileId?: Maybe<Scalars['ID']>;
};

export type Advisor2 = InvitedTeamMemberPermission & TeamMemberPermission & User2 & {
  readonly __typename?: 'Advisor2';
  /** @deprecated Unused feature flag. */
  readonly apexEnabled?: Maybe<Scalars['Boolean']>;
  readonly archetype?: Maybe<UserArchetype>;
  readonly assignedRepcodes: ReadonlyArray<Repcode>;
  /** @deprecated Use avatarURL if provided */
  readonly avatarId?: Maybe<Scalars['ID']>;
  readonly avatarURL: Scalars['String'];
  readonly canBeAssignedRepCodes: Scalars['Boolean'];
  readonly canBeDeleted: Scalars['Boolean'];
  readonly canBeReinvited: Scalars['Boolean'];
  readonly canBeUpdated: Scalars['Boolean'];
  readonly contactId?: Maybe<Scalars['String']>;
  readonly created?: Maybe<Scalars['String']>;
  readonly email: Scalars['String'];
  readonly firstName: Scalars['String'];
  readonly fullName: Scalars['String'];
  /** @deprecated Not needed anymore. */
  readonly hasOrganization?: Maybe<Scalars['Boolean']>;
  readonly id: Scalars['ID'];
  readonly identificationStatus?: Maybe<IdentificationStatus>;
  readonly invite?: Maybe<UserInvite>;
  readonly isDfaEnabled: Scalars['Boolean'];
  readonly isTeamMember?: Maybe<Scalars['Boolean']>;
  readonly lastName: Scalars['String'];
  readonly onboardStatus?: Maybe<OnboardStatus>;
  readonly organization: Organization;
  readonly profileId: Scalars['ID'];
  readonly role: UserRole;
  readonly status: UserInviteStatus;
  readonly suffix?: Maybe<Scalars['String']>;
  readonly username?: Maybe<Scalars['String']>;
};

export type AdvisorAUM = {
  readonly __typename?: 'AdvisorAUM';
  readonly aumDataPoints?: Maybe<ReadonlyArray<Maybe<AdvisorAUMDataPoints>>>;
  readonly changePercentage?: Maybe<Scalars['Float']>;
  readonly total: Scalars['Float'];
};

export type AdvisorAUMDataPoints = {
  readonly __typename?: 'AdvisorAUMDataPoints';
  readonly accumulatedDeposit: Scalars['Float'];
  readonly aumAmount: Scalars['Float'];
  readonly aumChanged: Scalars['Float'];
  readonly cashAmount: Scalars['Float'];
  readonly date: Scalars['String'];
  readonly netDeposit: Scalars['Float'];
};

export type AdvisorAccount = {
  readonly __typename?: 'AdvisorAccount';
  readonly accountBalance: Scalars['Float'];
  readonly accountClassification?: Maybe<Scalars['String']>;
  readonly accountNickname?: Maybe<Scalars['String']>;
  readonly accountNumber: Scalars['String'];
  readonly accountType: Scalars['String'];
  readonly externalRef?: Maybe<Scalars['String']>;
  readonly financialAccountId: Scalars['ID'];
  readonly financialAccountName: Scalars['String'];
  readonly financialInstitution: Scalars['String'];
  readonly groupId?: Maybe<Scalars['String']>;
  readonly household: Scalars['String'];
  readonly inceptionDate?: Maybe<Scalars['String']>;
  readonly portfolioName?: Maybe<Scalars['String']>;
  readonly qualifiedRepCode?: Maybe<Scalars['String']>;
  readonly userProfiles: ReadonlyArray<UserProfiles>;
  readonly ytdNetDeposit: Scalars['Float'];
};

export type AdvisorAccounts = {
  readonly __typename?: 'AdvisorAccounts';
  readonly accounts: ReadonlyArray<AdvisorAccount>;
  readonly total: Scalars['Int'];
};


export type AdvisorAccountsaccountsArgs = {
  needsGroupIds?: InputMaybe<Scalars['Boolean']>;
};

export type AdvisorAccounts2 = {
  readonly __typename?: 'AdvisorAccounts2';
  readonly accountCategory?: Maybe<Scalars['String']>;
  readonly accountClassification?: Maybe<Scalars['String']>;
  readonly accountCreationDate?: Maybe<Scalars['String']>;
  readonly accountName?: Maybe<Scalars['String']>;
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly accountType?: Maybe<Scalars['String']>;
  readonly archive?: Maybe<Scalars['Boolean']>;
  readonly assigned?: Maybe<Scalars['Boolean']>;
  readonly clientId?: Maybe<Scalars['String']>;
  readonly created?: Maybe<Scalars['String']>;
  readonly externalConnection?: Maybe<Scalars['String']>;
  readonly externalRef?: Maybe<Scalars['String']>;
  readonly financialAccountCategoryMappingId?: Maybe<Scalars['String']>;
  readonly financialAccountInfoId?: Maybe<Scalars['String']>;
  readonly financialInstitutionId?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly householdId?: Maybe<Scalars['String']>;
  readonly id?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly lastUpdatedDate?: Maybe<Scalars['String']>;
  readonly managedAccount?: Maybe<Scalars['Boolean']>;
  readonly mask?: Maybe<Scalars['String']>;
  readonly organizationId?: Maybe<Scalars['String']>;
  readonly qualifiedRepCode?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
  readonly updated?: Maybe<Scalars['String']>;
  readonly updatedBy?: Maybe<Scalars['String']>;
};

export type AdvisorAccountsOptions = {
  readonly archived?: InputMaybe<Scalars['Boolean']>;
  readonly financialAccountIds?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['String']>>>;
  readonly householdId?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['String']>>>;
  readonly symbols?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['String']>>>;
  readonly unassigned?: InputMaybe<Scalars['Boolean']>;
};

export type AdvisorAddress = {
  readonly __typename?: 'AdvisorAddress';
  readonly addr1?: Maybe<Scalars['String']>;
  readonly addr2?: Maybe<Scalars['String']>;
  readonly addr3?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly country?: Maybe<Scalars['String']>;
  readonly createdBy?: Maybe<Scalars['String']>;
  readonly id?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
  readonly updatedBy?: Maybe<Scalars['String']>;
  readonly zipCode?: Maybe<Scalars['String']>;
};

export type AdvisorAddressInput = {
  readonly addr1?: InputMaybe<Scalars['String']>;
  readonly addr2?: InputMaybe<Scalars['String']>;
  readonly addr3?: InputMaybe<Scalars['String']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly country?: InputMaybe<Scalars['String']>;
  readonly createdBy?: InputMaybe<Scalars['String']>;
  readonly id?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly status?: InputMaybe<Scalars['String']>;
  readonly updatedBy?: InputMaybe<Scalars['String']>;
  readonly zipCode?: InputMaybe<Scalars['String']>;
};

export type AdvisorAddressResponse = {
  readonly __typename?: 'AdvisorAddressResponse';
  readonly address: AdvisorAddress;
  readonly id: Scalars['ID'];
};

export type AdvisorContactDetails = {
  readonly __typename?: 'AdvisorContactDetails';
  readonly email?: Maybe<Scalars['String']>;
  readonly firstName: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly lastName: Scalars['String'];
  readonly role?: Maybe<Scalars['String']>;
  readonly userId?: Maybe<Scalars['String']>;
};

export type AdvisorDocument = {
  readonly __typename?: 'AdvisorDocument';
  readonly created?: Maybe<Scalars['String']>;
  readonly createdBy?: Maybe<Scalars['String']>;
  readonly documentName?: Maybe<Scalars['String']>;
  readonly documentType?: Maybe<Scalars['String']>;
  readonly fileName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly status?: Maybe<Scalars['String']>;
  readonly userId?: Maybe<Scalars['String']>;
  readonly versionNumber?: Maybe<Scalars['Int']>;
};

export type AdvisorDraft = {
  readonly __typename?: 'AdvisorDraft';
  readonly id: Scalars['ID'];
  readonly status: Scalars['String'];
};

export type AdvisorHolding = {
  readonly __typename?: 'AdvisorHolding';
  readonly holdingPercent: Scalars['Float'];
  readonly internalAccountsCount?: Maybe<Scalars['Float']>;
  readonly marketData?: Maybe<MarketData>;
  readonly price: Scalars['Float'];
  readonly quantity: Scalars['Float'];
  readonly securityData?: Maybe<SecurityMasterRecord>;
  readonly securityName: Scalars['String'];
  readonly symbol: Scalars['String'];
  readonly value: Scalars['Float'];
};

export type AdvisorMarketplaceProfile = {
  readonly __typename?: 'AdvisorMarketplaceProfile';
  readonly addressCity?: Maybe<Scalars['String']>;
  readonly addressState?: Maybe<Scalars['String']>;
  readonly advisor?: Maybe<AdvisorDraft>;
  readonly advisorDescription?: Maybe<Scalars['String']>;
  readonly advisorId?: Maybe<Scalars['String']>;
  readonly businessDescription?: Maybe<Scalars['String']>;
  readonly calendarLink?: Maybe<Scalars['String']>;
  readonly certifications?: Maybe<ReadonlyArray<Maybe<Scalars['ID']>>>;
  readonly excludedStates?: Maybe<ReadonlyArray<Scalars['String']>>;
  readonly feeTypes?: Maybe<ReadonlyArray<Maybe<FeeTypeObjOutput>>>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly formAdvLink?: Maybe<Scalars['String']>;
  readonly iardCrd?: Maybe<Scalars['String']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly idealClients?: Maybe<ReadonlyArray<Maybe<Scalars['ID']>>>;
  readonly images?: Maybe<ReadonlyArray<Maybe<ImageMultiPartOutput>>>;
  readonly instantBook?: Maybe<Scalars['Boolean']>;
  readonly isPublished?: Maybe<Scalars['Boolean']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly meetType?: Maybe<ReadonlyArray<Maybe<MEETING_PREFERENCE>>>;
  readonly minimumInvestment?: Maybe<Scalars['ID']>;
  readonly organizationIardCrd?: Maybe<Scalars['String']>;
  readonly organizationImage?: Maybe<ImageMultiPartOutput>;
  readonly organizationName?: Maybe<Scalars['String']>;
  readonly profileImage?: Maybe<ImageMultiPartOutput>;
  readonly registration?: Maybe<REGISTRATION>;
  readonly registrationStates?: Maybe<ReadonlyArray<Maybe<STATE_INITIAL>>>;
  readonly services?: Maybe<ReadonlyArray<Maybe<SERVICES>>>;
  readonly status?: Maybe<Scalars['String']>;
};

export type AdvisorMarketplaceProfileDraftInput = {
  readonly addressCity?: InputMaybe<Scalars['String']>;
  readonly addressState?: InputMaybe<Scalars['String']>;
  readonly advisorDescription?: InputMaybe<Scalars['String']>;
  readonly advisorId?: InputMaybe<Scalars['String']>;
  readonly businessDescription?: InputMaybe<Scalars['String']>;
  readonly calendarLink?: InputMaybe<Scalars['String']>;
  readonly certifications?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['ID']>>>;
  readonly excludedStates?: InputMaybe<ReadonlyArray<Scalars['String']>>;
  readonly feeTypes?: InputMaybe<ReadonlyArray<InputMaybe<FeeTypeObjInput>>>;
  readonly files?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['Upload']>>>;
  readonly firstName?: InputMaybe<Scalars['String']>;
  readonly formAdvLink?: InputMaybe<Scalars['String']>;
  readonly iardCrd?: InputMaybe<Scalars['String']>;
  readonly idealClients?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['ID']>>>;
  readonly images?: InputMaybe<ReadonlyArray<InputMaybe<ImageMultiPart>>>;
  readonly instantBook?: InputMaybe<Scalars['Boolean']>;
  readonly lastName?: InputMaybe<Scalars['String']>;
  readonly meetType?: InputMaybe<ReadonlyArray<InputMaybe<MEETING_PREFERENCE>>>;
  readonly minimumInvestment?: InputMaybe<Scalars['ID']>;
  readonly organizationIardCrd?: InputMaybe<Scalars['String']>;
  readonly organizationImage?: InputMaybe<ImageMultiPart>;
  readonly organizationName?: InputMaybe<Scalars['String']>;
  readonly profileImage?: InputMaybe<ImageMultiPart>;
  readonly registration?: InputMaybe<REGISTRATION>;
  readonly registrationStates?: InputMaybe<ReadonlyArray<InputMaybe<STATE_INITIAL>>>;
  readonly services?: InputMaybe<ReadonlyArray<InputMaybe<SERVICES>>>;
};

export type AdvisorStatusResponse = {
  readonly __typename?: 'AdvisorStatusResponse';
  /** @deprecated Move to Advisor2 type. */
  readonly applicationStatus?: Maybe<ApplicationStatus>;
  readonly updatedWorkflow?: Maybe<Scalars['String']>;
};

export type AgeOfMajority = {
  readonly __typename?: 'AgeOfMajority';
  readonly dateOfMajority?: Maybe<Scalars['DateOnly']>;
  readonly daysToMajority?: Maybe<Scalars['Int']>;
};

export type AgeOfMajorityForState = {
  readonly __typename?: 'AgeOfMajorityForState';
  readonly abbreviatedState: Scalars['String'];
  readonly accountCategory: Scalars['String'];
  readonly majorityAge: Scalars['String'];
};

export type AggrAccountHistory = {
  readonly __typename?: 'AggrAccountHistory';
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly accountNumberWithCustodian?: Maybe<Scalars['String']>;
  readonly aggrAccountPositionsId?: Maybe<Scalars['String']>;
  readonly asOfDate?: Maybe<Scalars['DateOnly']>;
  readonly aum?: Maybe<Scalars['Float']>;
  readonly createdAt?: Maybe<Scalars['DateTime']>;
  readonly createdBy?: Maybe<Scalars['String']>;
  readonly custodianId?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly repCode?: Maybe<Scalars['String']>;
  readonly totalCash?: Maybe<Scalars['Float']>;
  readonly totalDeposit?: Maybe<Scalars['Float']>;
  readonly totalDividend?: Maybe<Scalars['Float']>;
  readonly totalFee?: Maybe<Scalars['Float']>;
  readonly totalInterest?: Maybe<Scalars['Float']>;
  readonly totalTransferIn?: Maybe<Scalars['Float']>;
  readonly totalTransferOut?: Maybe<Scalars['Float']>;
  readonly totalWithdrawal?: Maybe<Scalars['Float']>;
  readonly totalWithholding?: Maybe<Scalars['Float']>;
  readonly transactions?: Maybe<ReadonlyArray<AggrAccountHistoryTransaction>>;
};

export type AggrAccountHistoryInput = {
  readonly financialAccountIds: ReadonlyArray<Scalars['ID']>;
  readonly from: Scalars['DateOnly'];
  readonly to: Scalars['DateOnly'];
  readonly transactions?: InputMaybe<Scalars['Boolean']>;
  readonly type: AggrAccountHistoryType;
  readonly userId?: InputMaybe<Scalars['ID']>;
};

export type AggrAccountHistoryTransaction = {
  readonly __typename?: 'AggrAccountHistoryTransaction';
  readonly accountNumberWithCustodian?: Maybe<Scalars['String']>;
  readonly quantity?: Maybe<Scalars['Float']>;
  readonly symbol?: Maybe<Scalars['String']>;
  readonly transactionAmount?: Maybe<Scalars['Float']>;
  readonly transactionDescription?: Maybe<Scalars['String']>;
  readonly transactionPrice?: Maybe<Scalars['Float']>;
  readonly transactionSettleDate?: Maybe<Scalars['DateTime']>;
  readonly transactionTradeDate?: Maybe<Scalars['DateTime']>;
  readonly transactionType?: Maybe<Scalars['String']>;
  readonly transactionsId?: Maybe<Scalars['Float']>;
};

export enum AggrAccountHistoryType {
  ACCOUNT = 'ACCOUNT',
  REP_CODE = 'REP_CODE'
}

export type AggregatedLossesAccountResponseItem = {
  readonly __typename?: 'AggregatedLossesAccountResponseItem';
  readonly asOfDate?: Maybe<Scalars['DateOnly']>;
  readonly custodian?: Maybe<Scalars['String']>;
  readonly financialAccountId?: Maybe<Scalars['String']>;
  readonly isPortfolioAssigned?: Maybe<Scalars['Boolean']>;
  readonly isTaxLossHarvestingEnabled?: Maybe<Scalars['Boolean']>;
  readonly isTaxSensitivityEnabled?: Maybe<Scalars['Boolean']>;
  readonly isTaxable?: Maybe<Scalars['Boolean']>;
  readonly realizedLongTermLoss?: Maybe<Scalars['Float']>;
  readonly realizedShortTermLoss?: Maybe<Scalars['Float']>;
  readonly unrealizedLongTermLoss?: Maybe<Scalars['Float']>;
  readonly unrealizedShortTermLoss?: Maybe<Scalars['Float']>;
};

export type AggregatedLossesInput = {
  readonly realizedLossesEndDate?: InputMaybe<Scalars['DateOnly']>;
  readonly realizedLossesStartDate?: InputMaybe<Scalars['DateOnly']>;
  readonly resourceType: PerformanceAnalyticsResourceType;
  readonly resourceTypeId: Scalars['ID'];
};

export type AgreementsPdf = {
  readonly __typename?: 'AgreementsPdf';
  readonly url?: Maybe<Scalars['String']>;
};

export enum AlertPriority {
  P0 = 'P0',
  P1 = 'P1'
}

export type AllUsersInGroupOutput = {
  readonly __typename?: 'AllUsersInGroupOutput';
  readonly inviteStatus: InviteStatus;
  readonly user: User;
};

export type Allocation = {
  readonly __typename?: 'Allocation';
  readonly name: AllowedAllocation;
  readonly value: Scalars['Float'];
  readonly weight: Scalars['Float'];
};

export enum AllocationType {
  AUM = 'AUM',
  EQUAL = 'EQUAL',
  MANUAL = 'MANUAL'
}

export type Allocations = {
  readonly __typename?: 'Allocations';
  readonly allocations: ReadonlyArray<Allocation>;
  readonly totalBalance?: Maybe<Scalars['Float']>;
};

export enum AllowedAllocation {
  BONDS = 'BONDS',
  CASH = 'CASH',
  Commodities = 'Commodities',
  Equity = 'Equity',
  FUNDS_CASH = 'FUNDS_CASH',
  Fd = 'Fd',
  FixedIncome = 'FixedIncome',
  Funds = 'Funds',
  OTHER = 'OTHER',
  STOCKS = 'STOCKS'
}

export enum AllowedAumStrategy {
  DEFAULT = 'DEFAULT',
  EXCLUDE_NEGATIVE_CASH = 'EXCLUDE_NEGATIVE_CASH'
}

export enum AllowedBalanceMetrics {
  DAILY_AVG = 'DAILY_AVG',
  END_BAL = 'END_BAL',
  END_BAL_FLOWS = 'END_BAL_FLOWS'
}

export enum AllowedBillTiming {
  IN_ADVANCE = 'IN_ADVANCE',
  IN_ARREARS = 'IN_ARREARS'
}

export enum AllowedContactStatus {
  ACTIVE = 'ACTIVE',
  PENDING = 'PENDING',
  UNINVITED = 'UNINVITED'
}

export enum AllowedContactType {
  BENEFICIARY = 'BENEFICIARY',
  OWNER = 'OWNER'
}

export type AllowedContribution = {
  readonly __typename?: 'AllowedContribution';
  readonly irsCode?: Maybe<Scalars['String']>;
  readonly previousYearAllowed?: Maybe<Scalars['Boolean']>;
  readonly reason?: Maybe<ContributionReason>;
  readonly reasonEntryType?: Maybe<Scalars['String']>;
};

export enum AllowedDateIntervalUnit {
  D = 'D',
  M = 'M',
  Q = 'Q',
  S = 'S',
  Y = 'Y'
}

export enum AllowedDateRangeType {
  CUSTOM = 'CUSTOM',
  FIXED = 'FIXED'
}

export type AllowedDistribution = {
  readonly __typename?: 'AllowedDistribution';
  readonly federalWithholdingRequired?: Maybe<Scalars['Boolean']>;
  readonly irsCode?: Maybe<Scalars['String']>;
  readonly reason?: Maybe<DistributionReason>;
  readonly reasonEntryType?: Maybe<Scalars['String']>;
  readonly stateWithholdingRequired?: Maybe<Scalars['Boolean']>;
};

export enum AllowedDriftSettings {
  ACTIVE = 'ACTIVE',
  CALENDAR = 'CALENDAR',
  PASSIVE = 'PASSIVE'
}

export enum AllowedFrequency {
  annual = 'annual',
  monthly = 'monthly',
  quarterly = 'quarterly'
}

export enum AllowedInviteStatus {
  ACTIVE = 'ACTIVE',
  EXPIRED = 'EXPIRED',
  INACTIVE = 'INACTIVE',
  INVITED = 'INVITED',
  NOT_INVITED = 'NOT_INVITED',
  PENDING = 'PENDING',
  UNINVITED = 'UNINVITED'
}

export enum AllowedInvoiceView {
  CALCULATION = 'CALCULATION',
  SIMPLE = 'SIMPLE'
}

export enum AllowedPlaidStatus {
  CONNECTED = 'CONNECTED',
  NOT_CONNECTED = 'NOT_CONNECTED'
}

export enum AllowedRole {
  ADMIN = 'ADMIN',
  ADVISOR = 'ADVISOR',
  Admin = 'Admin',
  Advisor = 'Advisor',
  CLIENT = 'CLIENT',
  Client = 'Client',
  OWNER = 'OWNER',
  STAFF = 'STAFF',
  admin = 'admin',
  advisor = 'advisor',
  client = 'client'
}

export enum AllowedStatus {
  ACTIVE = 'ACTIVE',
  EXPIRED = 'EXPIRED',
  INVITED = 'INVITED',
  PENDING = 'PENDING',
  active = 'active',
  expired = 'expired',
  inactive = 'inactive',
  pending = 'pending'
}

export enum AllowedType {
  AUM = 'AUM',
  FLAT = 'FLAT',
  FLAT_AUM = 'FLAT_AUM'
}

export enum AllowedUserStatus {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
  PENDING = 'PENDING',
  UNINVITED = 'UNINVITED'
}

export enum AllowedUserType {
  BENEFICIARY = 'BENEFICIARY',
  OWNER = 'OWNER'
}

export enum ApexVerificationStatus {
  CANCELED = 'CANCELED',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED',
  VERIFIED = 'VERIFIED'
}

export type ApplicationInput = {
  readonly affiliatedOrEmployedFinra?: InputMaybe<Scalars['Boolean']>;
  readonly companyName: Scalars['String'];
  readonly disciplinaryDescription?: InputMaybe<Scalars['String']>;
  readonly disciplinaryEvents: Scalars['Boolean'];
  readonly formAdvPart1: Scalars['String'];
  readonly formAdvPart2A: Scalars['String'];
  readonly formAdvPart2B?: InputMaybe<Scalars['String']>;
  readonly formAdvPart3?: InputMaybe<Scalars['String']>;
  readonly iardCrd: Scalars['String'];
  readonly individualCrd: Scalars['String'];
  readonly phone: Scalars['String'];
  readonly registeredBy: RegisteredBy;
};

export enum ApplicationStatus {
  ACCEPTED = 'ACCEPTED',
  NEW = 'NEW',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED'
}

export type ApprovePlaidMicroDepositV2Response = {
  readonly __typename?: 'ApprovePlaidMicroDepositV2Response';
  readonly code?: Maybe<Scalars['String']>;
  readonly error?: Maybe<Scalars['String']>;
  readonly message?: Maybe<Scalars['String']>;
  readonly success: Scalars['Boolean'];
};

export type ApyInfo = {
  readonly __typename?: 'ApyInfo';
  readonly apy: Scalars['Float'];
  readonly asOf: Scalars['DateOnly'];
  readonly rate: Scalars['Float'];
};

export enum AssessedRiskRating {
  HIGH = 'HIGH',
  LOW = 'LOW',
  MEDIUM = 'MEDIUM'
}

export type AssignBenchmarkInput = {
  readonly assignmentType: BenchmarkAssignmentType;
  readonly benchmarkId: Scalars['ID'];
  readonly entityId: Scalars['ID'];
  readonly entityType: BenchmarkEntityType;
};

export type AssignBenchmarkOutput = {
  readonly __typename?: 'AssignBenchmarkOutput';
  readonly success: Scalars['Boolean'];
};

export type AssignRepcodesToUserInput = {
  readonly repcodeIds: ReadonlyArray<Scalars['ID']>;
  readonly userId: Scalars['ID'];
};

export type AssignedOutput = {
  readonly __typename?: 'AssignedOutput';
  readonly assigned: Scalars['Boolean'];
  readonly portfolio?: Maybe<AssignedPortfolioDetails>;
};

export type AssignedPortfolioDetails = {
  readonly __typename?: 'AssignedPortfolioDetails';
  readonly cashSettings?: Maybe<CashSettings>;
  readonly drift?: Maybe<DriftSettings>;
  /** @deprecated Use drift instead and create a helper function to build the correct string to display. */
  readonly driftType?: Maybe<MigratingToPortfolioDriftType>;
  readonly hasEtfOrMutualFund?: Maybe<Scalars['Boolean']>;
  readonly hasPrimaryFundsWithoutSubstitute?: Maybe<Scalars['Boolean']>;
  readonly id?: Maybe<Scalars['ID']>;
  /** @deprecated Use cashSettings.cashTarget instead. */
  readonly minCashAllocation?: Maybe<Scalars['Float']>;
  readonly name?: Maybe<Scalars['String']>;
};

export type AssignmentFinancialAccount = {
  readonly __typename?: 'AssignmentFinancialAccount';
  readonly endDate?: Maybe<Scalars['String']>;
  readonly financialAccountId: Scalars['ID'];
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
  readonly scheduleId?: Maybe<Scalars['ID']>;
  readonly scheduleName?: Maybe<Scalars['String']>;
  readonly startDate?: Maybe<Scalars['String']>;
};

export type AssignmentInput = {
  readonly financialAccountId: Scalars['ID'];
  readonly userId: Scalars['ID'];
};

export enum AssignmentLevel {
  ACCOUNT = 'ACCOUNT',
  BILLING_GROUP = 'BILLING_GROUP',
  GROUP = 'GROUP'
}

export type AssignmentOutput = {
  readonly __typename?: 'AssignmentOutput';
  readonly financialAccountId: Scalars['ID'];
};

export type Aum = {
  readonly __typename?: 'Aum';
  readonly formula?: Maybe<FormulaAum>;
  readonly parameters: ParametersAum;
};

export type AuthenticateIntegrationResponse = {
  readonly __typename?: 'AuthenticateIntegrationResponse';
  readonly completionStatus?: Maybe<Scalars['String']>;
  readonly data?: Maybe<Integration>;
  readonly errors?: Maybe<ReadonlyArray<ErrorDescription>>;
};

export type AuthenticatePartnerIntegrationInput = {
  readonly partnerId: IntegrationPartnerCode;
  readonly password: Scalars['String'];
  readonly username: Scalars['String'];
};

export type AuthorizedSigner = {
  readonly citizenshipCountry?: InputMaybe<Scalars['String']>;
  readonly dateOfBirth: Scalars['String'];
  readonly emailAddress: Scalars['String'];
  readonly isUsCitizen: Scalars['Boolean'];
  readonly name: NameDetails;
  readonly socialSecurityNumber: Scalars['String'];
  readonly title: Scalars['String'];
};

export type BalanceOutput = {
  readonly __typename?: 'BalanceOutput';
  readonly externalAccountsBalance?: Maybe<Scalars['Float']>;
  readonly managedAccountsBalance: Scalars['Float'];
  readonly totalBalance: Scalars['Float'];
};

export type BasicAccountAssignment = {
  readonly __typename?: 'BasicAccountAssignment';
  readonly financialAccountName?: Maybe<Scalars['String']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly scheduleName?: Maybe<Scalars['String']>;
};

export type BasicIntegrationSource = {
  readonly __typename?: 'BasicIntegrationSource';
  readonly brandName?: Maybe<Scalars['String']>;
  readonly description?: Maybe<Scalars['String']>;
  readonly hasValidSeries?: Maybe<Scalars['Boolean']>;
  readonly id: Scalars['ID'];
  readonly logoUrl?: Maybe<Scalars['String']>;
  readonly longDescription: Scalars['String'];
  readonly name?: Maybe<Scalars['String']>;
};

export type Batch = {
  readonly __typename?: 'Batch';
  readonly batchType?: Maybe<Scalars['String']>;
  readonly completed?: Maybe<Scalars['Float']>;
  readonly completedDate?: Maybe<Scalars['DateTime']>;
  readonly created?: Maybe<Scalars['DateTime']>;
  readonly createdBy?: Maybe<Scalars['String']>;
  readonly failed?: Maybe<Scalars['Float']>;
  readonly id: Scalars['ID'];
  readonly organizationId: Scalars['ID'];
  readonly refId: Scalars['ID'];
  readonly status?: Maybe<Scalars['String']>;
  readonly total?: Maybe<Scalars['Float']>;
  readonly updated?: Maybe<Scalars['DateTime']>;
  readonly updatedBy?: Maybe<Scalars['String']>;
};

export type BatchFilter = {
  readonly batchType?: InputMaybe<BatchType>;
  readonly refIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
};

export enum BatchStatus {
  CANCELED = 'CANCELED',
  COMPLETED = 'COMPLETED',
  FAILED = 'FAILED',
  IN_PROGRESS = 'IN_PROGRESS'
}

export enum BatchType {
  MODEL = 'MODEL',
  PORTFOLIO = 'PORTFOLIO'
}

export type BeginDocumentBundlingInput = {
  readonly documentType: InvoiceBundleUrlDocumentType;
  readonly targetId: Scalars['String'];
  readonly targetType: InvoiceBundleUrlTargetType;
};

export type Benchmark = {
  readonly __typename?: 'Benchmark';
  readonly type: Scalars['String'];
  readonly values?: Maybe<ReadonlyArray<BenchmarkValues>>;
};

export type BenchmarkAssignment = {
  readonly __typename?: 'BenchmarkAssignment';
  readonly assignmentType: BenchmarkAssignmentType;
  readonly entityId: Scalars['ID'];
  readonly entityType: BenchmarkEntityType;
};

export type BenchmarkAssignmentOutput = {
  readonly __typename?: 'BenchmarkAssignmentOutput';
  readonly assignment: BenchmarkAssignment;
  readonly benchmark: BenchmarkDetail;
};

export enum BenchmarkAssignmentType {
  PRIMARY = 'PRIMARY',
  SECONDARY = 'SECONDARY'
}

export type BenchmarkAssignmentsCountByEntityType = {
  readonly __typename?: 'BenchmarkAssignmentsCountByEntityType';
  readonly accounts: Scalars['Int'];
  readonly households: Scalars['Int'];
};

export type BenchmarkAssignmentsFilter = {
  readonly entityIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
  readonly entityType?: InputMaybe<BenchmarkEntityType>;
};

export type BenchmarkComponent = {
  readonly __typename?: 'BenchmarkComponent';
  readonly symbol: BenchmarkSymbol;
  readonly weight: Scalars['Float'];
};

export type BenchmarkComponentInput = {
  readonly securityId: Scalars['ID'];
  readonly weight: Scalars['Float'];
};

export type BenchmarkDetail = {
  readonly __typename?: 'BenchmarkDetail';
  readonly components?: Maybe<ReadonlyArray<BenchmarkComponent>>;
  readonly createdAt: Scalars['DateTime'];
  readonly id: Scalars['ID'];
  readonly inceptionDate?: Maybe<Scalars['DateOnly']>;
  readonly name: Scalars['String'];
  readonly updatedAt?: Maybe<Scalars['DateTime']>;
};

export type BenchmarkDetailOutput = {
  readonly __typename?: 'BenchmarkDetailOutput';
  readonly assignments?: Maybe<ReadonlyArray<Maybe<BenchmarkAssignment>>>;
  readonly benchmark: BenchmarkDetail;
};

export enum BenchmarkEntityType {
  ACCOUNT = 'ACCOUNT',
  HOUSEHOLD = 'HOUSEHOLD'
}

export type BenchmarkPerformanceChartDataPoint = {
  readonly __typename?: 'BenchmarkPerformanceChartDataPoint';
  readonly x: Scalars['DateOnly'];
  readonly y: Scalars['Float'];
};

export type BenchmarkPerformanceDateRange = {
  readonly __typename?: 'BenchmarkPerformanceDateRange';
  readonly endDate: Scalars['DateOnly'];
  readonly startDate: Scalars['DateOnly'];
};

export type BenchmarkPerformanceFilterInput = {
  readonly benchmarkId: Scalars['ID'];
  readonly endDate: Scalars['DateOnly'];
  readonly startDate: Scalars['DateOnly'];
};

export type BenchmarkPerformanceOutput = {
  readonly __typename?: 'BenchmarkPerformanceOutput';
  /** Cumulative Time Weighted Return */
  readonly cumulativeTWR: Scalars['Float'];
  readonly dataPoints?: Maybe<ReadonlyArray<BenchmarkPerformanceChartDataPoint>>;
  readonly dateRange: BenchmarkPerformanceDateRange;
  readonly responseCode?: Maybe<BenchmarkPerformanceResponseCode>;
};

export type BenchmarkPerformanceResponseCode = {
  readonly __typename?: 'BenchmarkPerformanceResponseCode';
  readonly code: Scalars['String'];
};

export type BenchmarkSummary = {
  readonly __typename?: 'BenchmarkSummary';
  readonly createdAt: Scalars['DateTime'];
  readonly id: Scalars['ID'];
  readonly inceptionDate?: Maybe<Scalars['DateOnly']>;
  readonly name: Scalars['String'];
  readonly numAssignmentsByEntity?: Maybe<BenchmarkAssignmentsCountByEntityType>;
  readonly numComponents: Scalars['Int'];
  readonly updatedAt?: Maybe<Scalars['DateTime']>;
};

export type BenchmarkSummaryListFilter = {
  readonly entityIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
  readonly entityType?: InputMaybe<BenchmarkEntityType>;
};

export type BenchmarkSymbol = {
  readonly __typename?: 'BenchmarkSymbol';
  readonly delistedDate?: Maybe<Scalars['DateOnly']>;
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
  readonly symbol: Scalars['String'];
  readonly totalReturnsInceptionDate?: Maybe<Scalars['DateOnly']>;
  readonly type?: Maybe<Scalars['String']>;
};

export type BenchmarkSymbolsFilter = {
  readonly limit?: InputMaybe<Scalars['Int']>;
  readonly searchTerm?: InputMaybe<Scalars['String']>;
};

export type BenchmarkValues = {
  readonly __typename?: 'BenchmarkValues';
  readonly id: Scalars['ID'];
  readonly value: Scalars['String'];
};

export type BeneficiariesByUser = {
  readonly __typename?: 'BeneficiariesByUser';
  readonly beneficiaries: ReadonlyArray<BeneficiaryOutput>;
  readonly userId: Scalars['ID'];
};

export type BeneficiariesByUserWhere = {
  readonly userIds: ReadonlyArray<Scalars['ID']>;
};

export type Beneficiary = {
  readonly __typename?: 'Beneficiary';
  readonly accountInfoId?: Maybe<Scalars['ID']>;
  readonly addressInfo?: Maybe<AccountAddress>;
  readonly designation: Scalars['String'];
  readonly designationType?: Maybe<BeneficiaryDesignationType>;
  readonly dob?: Maybe<Scalars['DateOnly']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly entityFormation?: Maybe<Scalars['DateOnly']>;
  readonly entityName?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly lastName?: Maybe<Scalars['String']>;
  readonly middleName?: Maybe<Scalars['String']>;
  readonly percentage?: Maybe<Scalars['Float']>;
  readonly phoneNumber?: Maybe<Scalars['String']>;
  readonly relatedAccountInfo: ReadonlyArray<Beneficiary>;
  readonly relatedAccountInfoIds: ReadonlyArray<Scalars['ID']>;
  readonly relationship: BeneficiaryRelationship;
  readonly status?: Maybe<Scalars['String']>;
  readonly syncState?: Maybe<Scalars['String']>;
  readonly taxId?: Maybe<Scalars['String']>;
  readonly userInfoId?: Maybe<Scalars['ID']>;
};

export type BeneficiaryAllocation = {
  readonly beneficiaryId: Scalars['ID'];
  readonly percentage: Scalars['Float'];
};

export enum BeneficiaryCategory {
  CONTINGENT = 'CONTINGENT',
  PRIMARY = 'PRIMARY'
}

export enum BeneficiaryClass {
  IRA_BENEFICIARY = 'IRA_BENEFICIARY',
  TOD_BENEFICIARY = 'TOD_BENEFICIARY'
}

export enum BeneficiaryDesignationType {
  PER_CAPITA = 'PER_CAPITA',
  PER_STIRPES = 'PER_STIRPES'
}

export type BeneficiaryForDesignationTypeUpdate = {
  readonly designation: Scalars['String'];
  readonly designationType: BeneficiaryDesignationType;
  readonly id: Scalars['ID'];
  readonly relationship: BeneficiaryRelationship;
};

export type BeneficiaryInput = {
  readonly addressInfoRequest?: InputMaybe<AccountAddressInput>;
  readonly beneficiaryClass: BeneficiaryClass;
  readonly clientId: Scalars['ID'];
  readonly designation: Scalars['String'];
  readonly designationType?: InputMaybe<BeneficiaryDesignationType>;
  readonly dob?: InputMaybe<Scalars['DateOnly']>;
  readonly email?: InputMaybe<Scalars['String']>;
  readonly entityFormation?: InputMaybe<Scalars['DateOnly']>;
  readonly entityName?: InputMaybe<Scalars['String']>;
  readonly firstName?: InputMaybe<Scalars['String']>;
  readonly lastName?: InputMaybe<Scalars['String']>;
  readonly middleName?: InputMaybe<Scalars['String']>;
  readonly phoneNumber?: InputMaybe<Scalars['String']>;
  readonly relationship: BeneficiaryRelationship;
  readonly taxId?: InputMaybe<Scalars['String']>;
};

export type BeneficiaryItemInput = {
  readonly beneficiaryId: Scalars['ID'];
  readonly category?: InputMaybe<BeneficiaryCategory>;
  readonly percentage: Scalars['Float'];
  readonly relationship?: InputMaybe<Scalars['String']>;
};

export type BeneficiaryItemType = {
  readonly __typename?: 'BeneficiaryItemType';
  readonly beneficiaryId: Scalars['ID'];
  readonly category: BeneficiaryCategory;
  readonly percentage: Scalars['Float'];
  readonly relationship: Scalars['String'];
};

export type BeneficiaryOutput = {
  readonly __typename?: 'BeneficiaryOutput';
  readonly address?: Maybe<Scalars['String']>;
  readonly category?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly companyName?: Maybe<Scalars['String']>;
  readonly dateOfBirth?: Maybe<Scalars['String']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly financialAccountId?: Maybe<Scalars['ID']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly lastName?: Maybe<Scalars['String']>;
  readonly linkedFinancialAccounts?: Maybe<ReadonlyArray<Maybe<FinancialAccount2>>>;
  readonly percentage?: Maybe<Scalars['Float']>;
  readonly phone?: Maybe<Scalars['String']>;
  readonly relationship?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly taxIdentificationNumber?: Maybe<Scalars['String']>;
  readonly type: BeneficiaryType;
  readonly zipCode?: Maybe<Scalars['String']>;
};

export enum BeneficiaryRelationship {
  ENTITY = 'ENTITY',
  NON_SPOUSE = 'NON_SPOUSE',
  SPOUSE = 'SPOUSE'
}

export enum BeneficiaryType {
  ENTITY = 'ENTITY',
  PERSON = 'PERSON'
}

export type BillingDates = {
  readonly reportingLastDay?: InputMaybe<Scalars['DateOnly']>;
  readonly reportingStartDay: Scalars['DateOnly'];
};

export type BillingGroup = {
  readonly __typename?: 'BillingGroup';
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
};

export type BillingInfo = {
  readonly __typename?: 'BillingInfo';
  readonly aumDate: Scalars['String'];
  readonly aumRate?: Maybe<Scalars['Float']>;
  readonly baseFee?: Maybe<Scalars['Float']>;
  readonly billingDate: Scalars['String'];
  readonly calculation?: Maybe<Calculation>;
  readonly endDate: Scalars['String'];
  readonly flatFee: Scalars['Float'];
  readonly frequency: AllowedFrequency;
  readonly minimumFee?: Maybe<Scalars['Float']>;
  readonly startDate: Scalars['String'];
  readonly totalAum: Scalars['Float'];
};

export type BillingPeriod = {
  readonly __typename?: 'BillingPeriod';
  readonly created?: Maybe<Scalars['String']>;
  readonly createdBy?: Maybe<Scalars['ID']>;
  readonly endDate?: Maybe<Scalars['String']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
  readonly startDate?: Maybe<Scalars['String']>;
  readonly updated?: Maybe<Scalars['String']>;
  readonly updatedBy?: Maybe<Scalars['ID']>;
};

export type BillingPeriodInput = {
  readonly endDate?: InputMaybe<Scalars['String']>;
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly scheduleAssignmentId: Scalars['ID'];
  readonly startDate: Scalars['String'];
};

export type BillingPeriodRerunInput = {
  readonly billingPeriodId: Scalars['ID'];
};

export type BillingPeriodRerunResponse = {
  readonly __typename?: 'BillingPeriodRerunResponse';
  readonly billingRerun?: Maybe<BillingRerun>;
};

export type BillingRerun = {
  readonly __typename?: 'BillingRerun';
  readonly isRerunAvailable?: Maybe<Scalars['Boolean']>;
  readonly pollingInterval?: Maybe<Scalars['Float']>;
};

export type Brand = {
  readonly __typename?: 'Brand';
  readonly brandName?: Maybe<Scalars['String']>;
  readonly logo?: Maybe<Image>;
  readonly slug?: Maybe<Scalars['String']>;
};

export type Brokerage = {
  readonly __typename?: 'Brokerage';
  readonly hasInvoices: Scalars['Boolean'];
};

export type BrokerageApplication = {
  readonly __typename?: 'BrokerageApplication';
  readonly formAdvPart1?: Maybe<Scalars['String']>;
  readonly formAdvPart2A?: Maybe<Scalars['String']>;
  readonly formAdvPart2B?: Maybe<Scalars['String']>;
  readonly formAdvPart3?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
};

export enum Bucket {
  ACAT = 'ACAT',
  ACH_TRANSFER = 'ACH_TRANSFER',
  BENEFICIARY_MANAGEMENT = 'BENEFICIARY_MANAGEMENT',
  BROKERAGE_ACCOUNT_CREATION = 'BROKERAGE_ACCOUNT_CREATION',
  FUNDING_ACCOUNT_MANAGEMENT = 'FUNDING_ACCOUNT_MANAGEMENT',
  FUNDING_ACCOUNT_PLAID = 'FUNDING_ACCOUNT_PLAID',
  HOUSE_ACCOUNT_MANAGEMENT = 'HOUSE_ACCOUNT_MANAGEMENT',
  JOURNAL_TRANSFER = 'JOURNAL_TRANSFER',
  MMA = 'MMA'
}

export type BulkAccountForInternalTransfer = {
  readonly __typename?: 'BulkAccountForInternalTransfer';
  readonly accountNumber: Scalars['String'];
  readonly accountType: Scalars['String'];
  readonly brokerageAccountId: Scalars['ID'];
  readonly financialAccountName: Scalars['String'];
  readonly hasMMAWithBrokerageAccount: Scalars['Boolean'];
  readonly owners: ReadonlyArray<StatusOwnerDetails>;
  readonly status: AccountCreationStatus;
  readonly transferableAccounts: ReadonlyArray<TransferableAccount>;
};

export type CRMContact = {
  readonly __typename?: 'CRMContact';
  readonly address?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  /** If the contact exists in our system from the crm system. */
  readonly contactExists?: Maybe<Scalars['Boolean']>;
  readonly dateOfBirth?: Maybe<Scalars['String']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly employer?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly maritalStatus?: Maybe<Scalars['String']>;
  readonly phone?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly zip?: Maybe<Scalars['String']>;
};

export type CRMContactInputFilter = {
  readonly householdId?: InputMaybe<Scalars['ID']>;
  readonly integrationPartnerCode?: InputMaybe<IntegrationPartnerCode>;
  readonly name: Scalars['String'];
  readonly partners?: InputMaybe<ReadonlyArray<IntegrationPartnerCode>>;
};

export type Calculation = {
  readonly __typename?: 'Calculation';
  readonly aum?: Maybe<Aum>;
  readonly flat?: Maybe<Flat>;
};

export type CancelDocumentAgreementInput = {
  readonly id: Scalars['String'];
};

export type CancelDocumentAgreementResponse = {
  readonly __typename?: 'CancelDocumentAgreementResponse';
  readonly documentId?: Maybe<Scalars['String']>;
  readonly success?: Maybe<Scalars['Boolean']>;
};

export type CancelOrderOutput = {
  readonly __typename?: 'CancelOrderOutput';
  readonly orderNo?: Maybe<Scalars['String']>;
};

export type CancelTradeOrderInput = {
  readonly finAcctId: Scalars['String'];
  readonly orderId: Scalars['String'];
};

export type CancelationJournalTransferResponse = {
  readonly __typename?: 'CancelationJournalTransferResponse';
  readonly journalScheduledTransferUuid: Scalars['ID'];
};

export type CancelationTransferResponse = {
  readonly __typename?: 'CancelationTransferResponse';
  readonly scheduledTransferId: Scalars['ID'];
};

export type CashAccount = {
  readonly __typename?: 'CashAccount';
  readonly accountStatus: Scalars['String'];
  readonly accountType: Scalars['String'];
  readonly financialAccountId: Scalars['String'];
};

export type CashAccountDetails = {
  readonly __typename?: 'CashAccountDetails';
  readonly cashManagementAccounts?: Maybe<ReadonlyArray<Maybe<CashAccount>>>;
  readonly hasCashManagementAccounts: Scalars['Boolean'];
};

export type CashAccountInterestInput = {
  readonly advisorId: Scalars['ID'];
};

export type CashAllocation = {
  readonly __typename?: 'CashAllocation';
  readonly cashBalance?: Maybe<FlatAllocation>;
  readonly setAside?: Maybe<FlatAllocation>;
  readonly total?: Maybe<FlatAllocation>;
};

export type CashJournalTransaction = {
  readonly __typename?: 'CashJournalTransaction';
  readonly applyToPriorYear?: Maybe<Scalars['Boolean']>;
  readonly cashTransferStatus?: Maybe<Scalars['String']>;
  readonly contributionApplyToPriorYear?: Maybe<Scalars['Boolean']>;
  readonly contributionReason?: Maybe<ContributionReason>;
  readonly createdDate?: Maybe<Scalars['String']>;
  readonly description?: Maybe<Scalars['String']>;
  readonly direction?: Maybe<Scalars['String']>;
  readonly distributionApplyToPriorYear?: Maybe<Scalars['Boolean']>;
  readonly distributionReason?: Maybe<IraDistributionReason>;
  readonly federalTaxWithholdingFormatted?: Maybe<Scalars['String']>;
  readonly federalWithheld?: Maybe<Scalars['Float']>;
  readonly federalWithholdingAmount?: Maybe<Scalars['Float']>;
  readonly federalWithholdingType?: Maybe<TaxWithholding>;
  readonly fromBrokerageAccountId: Scalars['ID'];
  readonly fromBrokerageAccountName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly initiatedBy?: Maybe<Scalars['String']>;
  readonly initiatedByName?: Maybe<Scalars['String']>;
  readonly journalActiveStatus?: Maybe<Scalars['String']>;
  readonly journalTransferStatus?: Maybe<Scalars['String']>;
  readonly journalTransferUuid?: Maybe<Scalars['String']>;
  readonly lastUpdate?: Maybe<Scalars['String']>;
  readonly netAmount?: Maybe<Scalars['Float']>;
  readonly requestedAmount?: Maybe<Scalars['Float']>;
  readonly stateTaxWithholdingFormatted?: Maybe<Scalars['String']>;
  readonly stateWithheld?: Maybe<Scalars['Float']>;
  readonly stateWithholdingAmount?: Maybe<Scalars['Float']>;
  readonly stateWithholdingType?: Maybe<TaxWithholding>;
  readonly toBrokerageAccountId: Scalars['ID'];
  readonly toBrokerageAccountName?: Maybe<Scalars['String']>;
};

export type CashLiquidation = {
  readonly __typename?: 'CashLiquidation';
  readonly amount?: Maybe<Scalars['Float']>;
  readonly asideUntil?: Maybe<Scalars['DateOnly']>;
  readonly createdBy?: Maybe<Scalars['String']>;
  readonly createdDate?: Maybe<Scalars['DateTime']>;
  readonly finAccountId: Scalars['ID'];
  readonly note?: Maybe<Scalars['String']>;
  readonly orgId: Scalars['ID'];
  readonly requestId: Scalars['ID'];
  readonly setAsideAmount?: Maybe<Scalars['Float']>;
  readonly setAsidePendingAmount?: Maybe<Scalars['Float']>;
  readonly status?: Maybe<CashLiquidationStatus>;
  readonly updatedBy?: Maybe<Scalars['String']>;
  readonly updatedDate?: Maybe<Scalars['DateTime']>;
  readonly withdrawalAmount?: Maybe<Scalars['Float']>;
};

export type CashLiquidationCancellationResponse = {
  readonly __typename?: 'CashLiquidationCancellationResponse';
  readonly success?: Maybe<Scalars['Boolean']>;
};

export type CashLiquidationCreationResponse = {
  readonly __typename?: 'CashLiquidationCreationResponse';
  readonly success?: Maybe<Scalars['Boolean']>;
};

export enum CashLiquidationStatus {
  CANCELLED = 'CANCELLED',
  COMPLETE = 'COMPLETE',
  EXPIRED = 'EXPIRED',
  FAILED = 'FAILED',
  PENDING = 'PENDING',
  PROCESSING = 'PROCESSING',
  READY = 'READY'
}

export type CashLiquidationValidationResponse = {
  readonly __typename?: 'CashLiquidationValidationResponse';
  readonly amount: Scalars['Float'];
  readonly amountSetAside: Scalars['Float'];
  readonly asideUntil: Scalars['String'];
  readonly aum: Scalars['Float'];
  readonly currentCashBalance: Scalars['Float'];
  readonly newCashTarget: Scalars['Float'];
  readonly note?: Maybe<Scalars['String']>;
  readonly rebalancerNeeded?: Maybe<Scalars['Boolean']>;
};

export enum CashManagementType {
  MODEL = 'MODEL',
  SEPARATELY = 'SEPARATELY'
}

export type CashSettings = {
  readonly __typename?: 'CashSettings';
  readonly cashTarget?: Maybe<Scalars['Float']>;
  /** @deprecated Use `cashTarget` instead. */
  readonly cashTargetAsPercentageOfAUM?: Maybe<Scalars['Float']>;
  readonly investCashThreshold?: Maybe<Scalars['Float']>;
  /** @deprecated Use `investCashThreshold` instead. */
  readonly maxCashAsPercentageOfAUM?: Maybe<Scalars['Float']>;
  readonly method?: Maybe<CashManagementType>;
  /** @deprecated Use `raiseCashThreshold` instead. */
  readonly minCashAsPercentageOfAUM?: Maybe<Scalars['Float']>;
  readonly minTradeSizeDollars?: Maybe<Scalars['Float']>;
  readonly minTradeSizePct?: Maybe<Scalars['Float']>;
  readonly raiseCashThreshold?: Maybe<Scalars['Float']>;
};

export type CashSettingsDeleteInput = {
  readonly rebalanceAsap?: InputMaybe<Scalars['Boolean']>;
  readonly skipRebalance?: InputMaybe<Scalars['Boolean']>;
};

export type CashSettingsInput = {
  readonly cashTarget?: InputMaybe<Scalars['Float']>;
  readonly cashTargetAsPercentage?: InputMaybe<Scalars['Float']>;
  readonly investCashThreshold?: InputMaybe<Scalars['Float']>;
  readonly maxCashAsPercentage?: InputMaybe<Scalars['Float']>;
  readonly method?: InputMaybe<CashManagementType>;
  readonly minCashAsPercentage?: InputMaybe<Scalars['Float']>;
  readonly minTradeSizeDollars?: InputMaybe<Scalars['Float']>;
  readonly minTradeSizePct?: InputMaybe<Scalars['Float']>;
  readonly raiseCashThreshold?: InputMaybe<Scalars['Float']>;
  readonly skipRebalance?: InputMaybe<Scalars['Boolean']>;
};

export enum CashSettingsType {
  ACCOUNT = 'ACCOUNT',
  PORTFOLIO = 'PORTFOLIO'
}

export type CashSettlement = {
  readonly __typename?: 'CashSettlement';
  readonly cash: Scalars['Float'];
  readonly utcTime: Scalars['String'];
};

export type CashTransaction = {
  readonly __typename?: 'CashTransaction';
  readonly createdBy?: Maybe<Scalars['String']>;
  readonly createdDate?: Maybe<Scalars['String']>;
  readonly description?: Maybe<Scalars['String']>;
  readonly financialAccountId: Scalars['ID'];
  readonly id: Scalars['ID'];
  readonly quantity?: Maybe<Scalars['Float']>;
  readonly requestedQuantity?: Maybe<Scalars['Float']>;
  readonly settleDate?: Maybe<Scalars['String']>;
  readonly status?: Maybe<CashTransferStatus>;
};

export enum CashTransferStatus {
  APPROVED = 'APPROVED',
  CANCELLED = 'CANCELLED',
  COMPLETE = 'COMPLETE',
  FAILED = 'FAILED',
  FUNDS_HELD = 'FUNDS_HELD',
  FUNDS_POSTED = 'FUNDS_POSTED',
  PENDING = 'PENDING',
  PENDING_CANCEL = 'PENDING_CANCEL',
  PENDING_PRINTING = 'PENDING_PRINTING',
  PENDING_RETURN = 'PENDING_RETURN',
  REJECTED = 'REJECTED',
  RETURNED = 'RETURNED',
  SENT_TO_BANK = 'SENT_TO_BANK',
  STOP_PAYMENT = 'STOP_PAYMENT',
  VOID = 'VOID'
}

export type CertificationsOutput = {
  readonly __typename?: 'CertificationsOutput';
  readonly abbreviation: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
};

export type ChangePasswordOutput = {
  readonly __typename?: 'ChangePasswordOutput';
  readonly jwt?: Maybe<Scalars['String']>;
};

export type Chart = {
  readonly __typename?: 'Chart';
  readonly dataPoints: ReadonlyArray<Maybe<DataPoints>>;
};

export type CheckInput = {
  readonly depositAmount?: InputMaybe<Scalars['String']>;
};

export type CheckType = {
  readonly __typename?: 'CheckType';
  readonly depositAmount?: Maybe<Scalars['String']>;
};

export type ClearingRepCode = {
  readonly __typename?: 'ClearingRepCode';
  readonly clearingRepCode: Scalars['String'];
  readonly officeCode: Scalars['String'];
  readonly repCode: Scalars['String'];
  readonly status: Scalars['String'];
};

export type Client = User2 & {
  readonly __typename?: 'Client';
  readonly address?: Maybe<Address>;
  /** @deprecated Unused feature flag. */
  readonly apexEnabled?: Maybe<Scalars['Boolean']>;
  readonly archetype?: Maybe<UserArchetype>;
  /** @deprecated Use avatarURL if provided */
  readonly avatarId?: Maybe<Scalars['ID']>;
  readonly avatarURL: Scalars['String'];
  readonly created?: Maybe<Scalars['String']>;
  readonly dateOfBirth?: Maybe<Scalars['DateOnly']>;
  readonly email: Scalars['String'];
  readonly firstName: Scalars['String'];
  readonly fullName: Scalars['String'];
  /** @deprecated Not needed anymore. */
  readonly hasOrganization?: Maybe<Scalars['Boolean']>;
  readonly id: Scalars['ID'];
  readonly invite?: Maybe<UserInvite>;
  readonly isTeamMember?: Maybe<Scalars['Boolean']>;
  readonly lastName: Scalars['String'];
  readonly middleName?: Maybe<Scalars['String']>;
  readonly onboardStatus?: Maybe<OnboardStatus>;
  readonly organization: Organization;
  readonly phone?: Maybe<Scalars['String']>;
  readonly profileId: Scalars['ID'];
  readonly role: UserRole;
  readonly ssnTaxId?: Maybe<Scalars['String']>;
  readonly status?: Maybe<UserInviteStatus>;
  readonly suffix?: Maybe<Scalars['String']>;
  readonly username?: Maybe<Scalars['String']>;
};

export type ClientAddressResponse = {
  readonly __typename?: 'ClientAddressResponse';
  readonly address: AdvisorAddress;
};

export type ClientDeleteStatus = {
  readonly __typename?: 'ClientDeleteStatus';
  readonly clientDeleteStatus?: Maybe<ClientDeletionInfo>;
};

export enum ClientDeletionInfo {
  HAS_ACCOUNTS_AND_FEE_SCHEDULES = 'HAS_ACCOUNTS_AND_FEE_SCHEDULES',
  NO_ACCOUNTS = 'NO_ACCOUNTS',
  NO_FEE_SCHEDULES = 'NO_FEE_SCHEDULES'
}

export type ClientDocument = {
  readonly __typename?: 'ClientDocument';
  readonly clientId?: Maybe<Scalars['ID']>;
  readonly documentId?: Maybe<Scalars['ID']>;
  readonly documentName?: Maybe<Scalars['String']>;
  readonly documentType?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly reviewed?: Maybe<Scalars['Boolean']>;
  readonly versionNumber?: Maybe<Scalars['Int']>;
};

export type ClientFinancialAccount = {
  readonly __typename?: 'ClientFinancialAccount';
  readonly accountNumber: Scalars['String'];
  readonly balance: Scalars['Float'];
  readonly category: Scalars['String'];
  readonly financialAccountId: Scalars['ID'];
  readonly institutionName: Scalars['String'];
  readonly managedAccount: Scalars['Boolean'];
};

export type ClientFinancialInfo = {
  readonly __typename?: 'ClientFinancialInfo';
  readonly financialAccounts: ReadonlyArray<ClientFinancialAccount>;
  readonly performance: Performance;
};

export type ClientsFilterInput = {
  readonly householdId?: InputMaybe<Scalars['ID']>;
};

export type ColorDraft = {
  readonly __typename?: 'ColorDraft';
  readonly draft: ColorDraftData;
  readonly id: Scalars['ID'];
};

export type ColorDraftData = {
  readonly __typename?: 'ColorDraftData';
  readonly color: Scalars['String'];
};

export type ColorDraftInput = {
  readonly accountStateId: Scalars['ID'];
  readonly draft: ColorDraftInputData;
};

export type ColorDraftInputData = {
  readonly color: Scalars['String'];
};

export type ColorDraftUpdateInput = {
  readonly draft: ColorDraftInputData;
  readonly id: Scalars['ID'];
};

export type CompanyInput = {
  readonly address?: InputMaybe<AddressInput>;
  readonly disciplinaryDescription?: InputMaybe<Scalars['String']>;
  readonly disciplinaryEvents?: InputMaybe<Scalars['Boolean']>;
  readonly file?: InputMaybe<Scalars['Upload']>;
  readonly iarNames?: InputMaybe<ReadonlyArray<InputMaybe<IarNamesInput>>>;
  readonly iardCrd?: InputMaybe<Scalars['String']>;
  readonly individualIardCrd?: InputMaybe<Scalars['String']>;
  readonly individualRegisteredBy?: InputMaybe<RegisteredBy>;
  readonly name?: InputMaybe<Scalars['String']>;
  readonly registeredBy?: InputMaybe<RegisteredBy>;
};

export type CompanyOutput = {
  readonly __typename?: 'CompanyOutput';
  readonly address?: Maybe<AddressOutput>;
  readonly disciplinaryDescription?: Maybe<Scalars['String']>;
  readonly disciplinaryEvents?: Maybe<Scalars['Boolean']>;
  readonly iarNames?: Maybe<ReadonlyArray<Maybe<IarNamesType>>>;
  readonly iardCrd?: Maybe<Scalars['String']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly individualIardCrd?: Maybe<Scalars['String']>;
  readonly individualRegisteredBy?: Maybe<RegisteredBy>;
  readonly logoUrl?: Maybe<Scalars['String']>;
  readonly name?: Maybe<Scalars['String']>;
  readonly registeredBy?: Maybe<RegisteredBy>;
};

export type Constraints = {
  readonly __typename?: 'Constraints';
  readonly allowedStates: ReadonlyArray<Scalars['String']>;
  readonly requireMarried?: Maybe<Scalars['Boolean']>;
};

export type ConstraintsWhere = {
  readonly accountType: Scalars['String'];
};

export enum ConsumerType {
  CLIENT = 'CLIENT',
  LEAD = 'LEAD',
  PROSPECT = 'PROSPECT'
}

export type Contact = {
  readonly active_clientportal?: InputMaybe<Scalars['Boolean']>;
  readonly address?: InputMaybe<Scalars['String']>;
  readonly altruist_aum?: InputMaybe<Scalars['Float']>;
  readonly altruist_portfolio_aum?: InputMaybe<Scalars['Float']>;
  readonly altruist_portfolio_user?: InputMaybe<Scalars['Boolean']>;
  readonly altruistbrokerage_live?: InputMaybe<Scalars['Boolean']>;
  readonly applied_brokerage?: InputMaybe<Scalars['Boolean']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly company?: InputMaybe<Scalars['String']>;
  readonly company_name?: InputMaybe<Scalars['String']>;
  readonly email: Scalars['String'];
  readonly fee_setup?: InputMaybe<Scalars['Boolean']>;
  readonly firstname?: InputMaybe<Scalars['String']>;
  readonly free_trial_expiration?: InputMaybe<Scalars['String']>;
  readonly houseaccount_created?: InputMaybe<Scalars['Boolean']>;
  readonly household_setup?: InputMaybe<Scalars['Boolean']>;
  readonly how_many_altruist_accounts?: InputMaybe<Scalars['Int']>;
  readonly how_many_financial_accounts?: InputMaybe<Scalars['Int']>;
  readonly lastlogin?: InputMaybe<Scalars['Float']>;
  readonly lastname?: InputMaybe<Scalars['String']>;
  readonly membership_status?: InputMaybe<Scalars['String']>;
  readonly number_altruist_portfolios?: InputMaybe<Scalars['Int']>;
  readonly number_clients?: InputMaybe<Scalars['Int']>;
  readonly number_contacts?: InputMaybe<Scalars['Int']>;
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly portfolio_created?: InputMaybe<Scalars['Boolean']>;
  readonly registered_clientportal?: InputMaybe<Scalars['Int']>;
  readonly signed_up?: InputMaybe<Scalars['Float']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly tda_connected?: InputMaybe<Scalars['Boolean']>;
  readonly trial_days_left?: InputMaybe<Scalars['Int']>;
  readonly what_s_your_current_aum_?: InputMaybe<Scalars['String']>;
  readonly zip?: InputMaybe<Scalars['String']>;
};

export type ContextFields = {
  readonly pageName: Scalars['String'];
  readonly pageUri: Scalars['String'];
};

export type ContributionConstraints = {
  readonly __typename?: 'ContributionConstraints';
  readonly allowedContributions?: Maybe<ReadonlyArray<Maybe<AllowedContribution>>>;
  readonly onlyCurrentYearAllowed?: Maybe<Scalars['Boolean']>;
};

export enum ContributionReason {
  CONVERSION = 'CONVERSION',
  EMPLOYEE = 'EMPLOYEE',
  EMPLOYER = 'EMPLOYER',
  PREMATURE = 'PREMATURE',
  RECHARACTERIZATION = 'RECHARACTERIZATION',
  REGULAR = 'REGULAR',
  ROLLOVER_60_DAY = 'ROLLOVER_60_DAY',
  ROLLOVER_DIRECT = 'ROLLOVER_DIRECT',
  TRANSFER = 'TRANSFER',
  TRUSTEE_FEE = 'TRUSTEE_FEE'
}

export enum CorpApplicationStatus {
  PENDING_SECRETARY_REVIEW = 'PENDING_SECRETARY_REVIEW',
  SECRETARY_CERTIFIED = 'SECRETARY_CERTIFIED',
  SECRETARY_REJECTED = 'SECRETARY_REJECTED',
  UNDER_SECRETARY_REVIEW = 'UNDER_SECRETARY_REVIEW'
}

export type CorpMember = {
  readonly __typename?: 'CorpMember';
  readonly corporationId: Scalars['ID'];
  readonly id: Scalars['ID'];
  readonly ownershipPercentage?: Maybe<Scalars['Int']>;
  readonly profile?: Maybe<CorpMemberProfile>;
  readonly roles: ReadonlyArray<CorpMemberRole>;
  readonly userId: Scalars['ID'];
};

export type CorpMemberProfile = {
  readonly __typename?: 'CorpMemberProfile';
  readonly address?: Maybe<Scalars['String']>;
  readonly backupWithholding?: Maybe<Scalars['Boolean']>;
  readonly brokerDealerAffiliation?: Maybe<Scalars['Boolean']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly dateOfBirth?: Maybe<Scalars['String']>;
  readonly directorStockOwner?: Maybe<Scalars['Boolean']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly employmentPosition?: Maybe<Scalars['String']>;
  readonly entityName?: Maybe<Scalars['String']>;
  readonly entityTaxId?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly lastName?: Maybe<Scalars['String']>;
  readonly middleName?: Maybe<Scalars['String']>;
  readonly phone?: Maybe<Scalars['String']>;
  readonly ssnTaxId?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly suffix?: Maybe<Scalars['String']>;
  readonly zipCode?: Maybe<Scalars['String']>;
};

export type CorpMemberProfileInput = {
  readonly address?: InputMaybe<Scalars['String']>;
  readonly backupWithholding?: InputMaybe<Scalars['Boolean']>;
  readonly brokerDealerAffiliation?: InputMaybe<Scalars['Boolean']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly dateOfBirth?: InputMaybe<Scalars['String']>;
  readonly directorStockOwner?: InputMaybe<Scalars['Boolean']>;
  readonly email?: InputMaybe<Scalars['String']>;
  readonly employmentPosition?: InputMaybe<Scalars['String']>;
  readonly entityName?: InputMaybe<Scalars['String']>;
  readonly entityTaxId?: InputMaybe<Scalars['String']>;
  readonly firstName?: InputMaybe<Scalars['String']>;
  readonly lastName?: InputMaybe<Scalars['String']>;
  readonly middleName?: InputMaybe<Scalars['String']>;
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly ssnTaxId?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly suffix?: InputMaybe<Scalars['String']>;
  readonly zipCode?: InputMaybe<Scalars['String']>;
};

export enum CorpMemberRole {
  AUTHORIZED_OFFICER = 'AUTHORIZED_OFFICER',
  AUTHORIZED_SIGNER = 'AUTHORIZED_SIGNER',
  BENEFICIAL_OWNER = 'BENEFICIAL_OWNER',
  CONTROL_PERSON = 'CONTROL_PERSON',
  ENTITY_OFFICER = 'ENTITY_OFFICER',
  PRINCIPAL_APPROVER = 'PRINCIPAL_APPROVER',
  REGISTERED_REPRESENTATIVE_APPROVER = 'REGISTERED_REPRESENTATIVE_APPROVER',
  SECRETARY = 'SECRETARY'
}

export type CorpMemberWhere = {
  readonly corporationId: Scalars['ID'];
  readonly userId: Scalars['ID'];
};

export type CorpMembersWhere = {
  readonly corporationId?: InputMaybe<Scalars['ID']>;
  readonly financialAccountId?: InputMaybe<Scalars['ID']>;
};

export type Corporation = {
  readonly __typename?: 'Corporation';
  readonly applicationStatus?: Maybe<CorpApplicationStatus>;
  readonly brokerageAccountId?: Maybe<Scalars['ID']>;
  readonly businessAddress?: Maybe<EntityAddress>;
  readonly corporationType: CorporationType;
  readonly id: Scalars['ID'];
  readonly mailingAddress?: Maybe<EntityAddress>;
  readonly profile: EntityProfile;
};

export type CorporationDetails = {
  readonly __typename?: 'CorporationDetails';
  readonly corporation?: Maybe<Corporation>;
  readonly corporationId: Scalars['ID'];
};

export type CorporationDetailsInput = {
  readonly corporationId: Scalars['ID'];
};

export type CorporationInput = {
  readonly businessAddress?: InputMaybe<EntityAddressInput>;
  readonly corporationType: CorporationType;
  readonly mailingAddress?: InputMaybe<EntityAddressInput>;
  readonly profile: EntityProfileInput;
};

export enum CorporationType {
  C_CORPORATION = 'C_CORPORATION',
  LLC_C_CORPORATION = 'LLC_C_CORPORATION',
  LLC_INDIVIDUAL = 'LLC_INDIVIDUAL',
  LLC_PARTNERSHIP = 'LLC_PARTNERSHIP',
  LLC_S_CORPORATION = 'LLC_S_CORPORATION',
  NON_PROFIT = 'NON_PROFIT',
  PARTNERSHIP = 'PARTNERSHIP',
  SINGLE_MEMBER_LLC = 'SINGLE_MEMBER_LLC',
  SOLE_PROPRIETORSHIP = 'SOLE_PROPRIETORSHIP',
  S_CORPORATION = 'S_CORPORATION'
}

export type CorporationWhere = {
  readonly financialAccountId?: InputMaybe<Scalars['ID']>;
  readonly id?: InputMaybe<Scalars['ID']>;
};

export type CostBasis = {
  readonly __typename?: 'CostBasis';
  readonly holdings: ReadonlyArray<Maybe<CostBasisHolding>>;
  readonly lastUpdatedDays?: Maybe<Scalars['Float']>;
};

export type CostBasisAccount = {
  readonly __typename?: 'CostBasisAccount';
  readonly accountID: Scalars['ID'];
  readonly accountNickname?: Maybe<Scalars['String']>;
  readonly costBasis?: Maybe<Scalars['Float']>;
  readonly financialAccountName: Scalars['String'];
  readonly gainLoss?: Maybe<Scalars['Float']>;
  readonly marketValue?: Maybe<Scalars['Float']>;
  readonly owner?: Maybe<AccountOwner>;
  readonly percentGainLoss?: Maybe<Scalars['Float']>;
  readonly purchased?: Maybe<Scalars['String']>;
  readonly quantity?: Maybe<Scalars['Float']>;
  readonly term?: Maybe<TermType>;
  readonly totalCost?: Maybe<Scalars['Float']>;
};

export type CostBasisHolding = {
  readonly __typename?: 'CostBasisHolding';
  readonly accounts: ReadonlyArray<Maybe<CostBasisAccount>>;
  readonly aggCostBasis?: Maybe<Scalars['Float']>;
  readonly aggGainLoss?: Maybe<Scalars['Float']>;
  readonly aggMarketValue?: Maybe<Scalars['Float']>;
  readonly aggPercentGainLoss?: Maybe<Scalars['Float']>;
  readonly aggQuantity?: Maybe<Scalars['Float']>;
  readonly aggTerm?: Maybe<TermType>;
  readonly aggTotalCost?: Maybe<Scalars['Float']>;
  readonly currentPrice: Scalars['Float'];
  readonly isAggregate: Scalars['Boolean'];
  readonly securityName: Scalars['String'];
  readonly tickerSymbol: Scalars['String'];
};

export type CreateAccountBeneficiaryInput = {
  readonly accountId: Scalars['ID'];
  readonly beneficiaryInput: BeneficiaryInput;
};

export type CreateAccountFromStateBulkInput = {
  readonly accounts: ReadonlyArray<CreateAccountFromStateInput>;
};

export type CreateAccountFromStateInput = {
  readonly accountStateId: Scalars['ID'];
  readonly accountType: Scalars['String'];
  readonly advisorId: Scalars['ID'];
  readonly dividendReinvestment: Scalars['Boolean'];
  readonly sweepInstructions: Scalars['Boolean'];
};

export type CreateAccountOutput = {
  readonly __typename?: 'CreateAccountOutput';
  readonly financialAccountId: Scalars['ID'];
};

export type CreateBenchmarkInput = {
  readonly components?: InputMaybe<ReadonlyArray<BenchmarkComponentInput>>;
  readonly name: Scalars['String'];
};

export type CreateBenchmarkOutput = {
  readonly __typename?: 'CreateBenchmarkOutput';
  readonly id: Scalars['ID'];
};

export type CreateBulkAccountOutput = {
  readonly __typename?: 'CreateBulkAccountOutput';
  readonly accountStateId: Scalars['ID'];
};

export type CreateContactInput = {
  readonly address?: InputMaybe<Scalars['String']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly dateOfBirth?: InputMaybe<Scalars['String']>;
  readonly email: Scalars['String'];
  readonly employer?: InputMaybe<Scalars['String']>;
  readonly firstName: Scalars['String'];
  readonly lastName: Scalars['String'];
  readonly maritalStatus?: InputMaybe<Scalars['String']>;
  readonly middleName?: InputMaybe<Scalars['String']>;
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly suffix?: InputMaybe<Scalars['String']>;
  readonly zip?: InputMaybe<Scalars['String']>;
};

export type CreateContactOutput = {
  readonly __typename?: 'CreateContactOutput';
  readonly address?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly dateOfBirth?: Maybe<Scalars['String']>;
  readonly email: Scalars['String'];
  readonly employer?: Maybe<Scalars['String']>;
  readonly firstName: Scalars['String'];
  readonly lastName: Scalars['String'];
  readonly maritalStatus?: Maybe<Scalars['String']>;
  readonly middleName?: Maybe<Scalars['String']>;
  readonly phone?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  /** @deprecated Unused */
  readonly status?: Maybe<AllowedContactStatus>;
  readonly suffix?: Maybe<Scalars['String']>;
  /** @deprecated Unused */
  readonly type?: Maybe<AllowedContactType>;
  readonly userId?: Maybe<Scalars['ID']>;
  readonly zip?: Maybe<Scalars['String']>;
};

export type CreateCorpMember = {
  readonly corporationId: Scalars['ID'];
  readonly ownershipPercentage?: InputMaybe<Scalars['Int']>;
  readonly profile?: InputMaybe<CorpMemberProfileInput>;
  readonly roles: ReadonlyArray<CorpMemberRole>;
  readonly userId?: InputMaybe<Scalars['ID']>;
};

export type CreateDocumentInput = {
  readonly message: Scalars['String'];
  readonly name: Scalars['String'];
  readonly signers: ReadonlyArray<CreateDocumentSignerInput>;
  readonly subject: Scalars['String'];
  readonly templateId: Scalars['String'];
  readonly title: Scalars['String'];
};

export type CreateDocumentSignerInput = {
  readonly name: Scalars['String'];
  readonly order: Scalars['Int'];
  readonly role: Scalars['String'];
  readonly userId: Scalars['String'];
};

export type CreateHeldAwayLinkInput = {
  readonly accounts: ReadonlyArray<HeldAwayAccountInput>;
  readonly institution: HeldAwayInstitutionInput;
  readonly publicToken: Scalars['String'];
  readonly sessionId: Scalars['String'];
};

export type CreateHouseAccountInput = {
  readonly corporationId: Scalars['ID'];
  readonly dividendReinvestment: Scalars['Boolean'];
  readonly documents?: InputMaybe<ReadonlyArray<AccountStateDocumentInput>>;
  readonly finraAffiliations?: InputMaybe<ReadonlyArray<AccountStateFinraAffiliationInput>>;
  readonly optInLendingProgram: Scalars['Boolean'];
  readonly purposeOfAccount: PurposeOfAccount;
  readonly sourceOfFunds: SourceOfFunds;
  readonly sweepInstructions: Scalars['Boolean'];
  readonly trustUserId?: InputMaybe<Scalars['ID']>;
};

export type CreateOrderOutput = {
  readonly __typename?: 'CreateOrderOutput';
  readonly orderNo: Scalars['String'];
};

export type CreateRepcodeInput = {
  readonly label: Scalars['String'];
};

export type CreateReplaceInput = {
  readonly createRequests?: InputMaybe<ReadonlyArray<MMAInput>>;
  readonly replaceRequests?: InputMaybe<ReadonlyArray<MMAInput>>;
};

export type CreateReportAssignmentInput = {
  readonly advisorId: Scalars['ID'];
  readonly entityId: Scalars['ID'];
  readonly entityType: ReportAssignmentEntityType;
  readonly recipientId: Scalars['ID'];
  readonly sendReportCopyToAdvisor: Scalars['Boolean'];
};

export type CreateReportScheduleAssignment = {
  readonly __typename?: 'CreateReportScheduleAssignment';
  readonly scheduleAssignmentId: Scalars['ID'];
};

export type CreateReportScheduleAssignmentsInput = {
  readonly assignments?: InputMaybe<ReadonlyArray<CreateReportAssignmentInput>>;
  readonly reportType: ReportScheduleType;
  readonly scheduleId: Scalars['ID'];
};

export type CreateReportScheduleAssignmentsOutput = {
  readonly __typename?: 'CreateReportScheduleAssignmentsOutput';
  readonly assignments?: Maybe<ReadonlyArray<CreateReportScheduleAssignment>>;
  readonly nextRunOn?: Maybe<Scalars['DateOnly']>;
};

export type CreateScheduleAssignmentInput = {
  readonly assignmentLevel: AssignmentLevel;
  readonly billingGroupFinancialAccountIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
  readonly billingGroupName?: InputMaybe<Scalars['String']>;
  readonly endDate?: InputMaybe<Scalars['String']>;
  readonly financialAccountId?: InputMaybe<Scalars['ID']>;
  readonly householdId: Scalars['ID'];
  readonly scheduleId: Scalars['ID'];
  readonly startDate: Scalars['String'];
};

export type CreateSchedulesInput = {
  readonly billTiming: AllowedBillTiming;
  readonly frequency: AllowedFrequency;
  readonly plans: ReadonlyArray<PlanInput>;
  readonly scheduleName: Scalars['String'];
};

export type CreateTradeOrderInput = {
  readonly calculatedSharesQuantity: Scalars['Float'];
  readonly direction: OrderSide;
  readonly finAccountId: Scalars['String'];
  readonly limitPrice?: InputMaybe<Scalars['Float']>;
  readonly marketPrice: Scalars['Float'];
  readonly notionalValue?: InputMaybe<Scalars['Float']>;
  readonly orderType: OrderType;
  readonly quantity: Scalars['Float'];
  readonly quantityType: OrderEntryType;
  readonly sharesQuantity?: InputMaybe<Scalars['Float']>;
  readonly symbol: Scalars['String'];
};

export type CreateUserInfo = {
  readonly address?: InputMaybe<Scalars['String']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly email: Scalars['String'];
  readonly firstName: Scalars['String'];
  readonly lastName: Scalars['String'];
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly zip?: InputMaybe<Scalars['String']>;
};

export type Credentials = {
  readonly password: Scalars['String'];
  readonly username: Scalars['String'];
};

export type CurrentScheduleOutput = {
  readonly __typename?: 'CurrentScheduleOutput';
  readonly balanceMetrics?: Maybe<AllowedBalanceMetrics>;
  readonly billTiming?: Maybe<AllowedBillTiming>;
  readonly billingPeriods?: Maybe<ReadonlyArray<Maybe<BillingPeriod>>>;
  readonly blendedRate?: Maybe<Scalars['Boolean']>;
  readonly fee?: Maybe<Scalars['Float']>;
  readonly frequency?: Maybe<AllowedFrequency>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly scheduleAssignmentId: Scalars['ID'];
  readonly scheduleAssignmentLevel: AssignmentLevel;
  readonly scheduleName?: Maybe<Scalars['String']>;
  readonly tiers?: Maybe<ReadonlyArray<Maybe<TierOutput>>>;
  readonly type?: Maybe<AllowedType>;
};

export type CustomMeta = {
  readonly name: Scalars['String'];
  readonly value?: InputMaybe<Scalars['String']>;
};

export type CustomerPaymentMethod = {
  readonly paymentMethodId: Scalars['String'];
  readonly setAsDefault: Scalars['Boolean'];
};

export type DataPoints = {
  readonly __typename?: 'DataPoints';
  readonly accumulatedDeposit: Scalars['Float'];
  readonly balance: Scalars['Float'];
  readonly date: Scalars['String'];
  readonly netDeposit: Scalars['Float'];
};

export type DateRangeFilter = {
  readonly from?: InputMaybe<Scalars['String']>;
  readonly to?: InputMaybe<Scalars['String']>;
};

export type DebitAccountOverride = {
  readonly __typename?: 'DebitAccountOverride';
  readonly debitFinancialAccountId: Scalars['ID'];
  readonly financialAccountId: Scalars['ID'];
};

export type DebitAccountOverrideInput = {
  readonly debitFinancialAccountId: Scalars['ID'];
  readonly financialAccountId: Scalars['ID'];
};

export type Decedent = {
  readonly __typename?: 'Decedent';
  readonly birthDate?: Maybe<Scalars['String']>;
  readonly deceasedDate?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly ssn?: Maybe<Scalars['String']>;
};

export type DecedentInput = {
  readonly birthDate?: InputMaybe<Scalars['String']>;
  readonly deceasedDate?: InputMaybe<Scalars['String']>;
  readonly firstName: Scalars['String'];
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly lastName?: InputMaybe<Scalars['String']>;
  readonly ssn?: InputMaybe<Scalars['String']>;
};

export type DecedentProfile = {
  readonly __typename?: 'DecedentProfile';
  readonly birthDate?: Maybe<Scalars['String']>;
  readonly deceasedDate?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly ssn?: Maybe<Scalars['String']>;
};

export type DecedentWhere = {
  readonly id: Scalars['ID'];
};

export type DeleteAccountBeneficiariesInput = {
  readonly accountId: Scalars['ID'];
  readonly ids: ReadonlyArray<Scalars['ID']>;
};

export type DeleteBenchmarkAssignmentInput = {
  readonly benchmarkId: Scalars['ID'];
  readonly entityId: Scalars['ID'];
  readonly entityType: BenchmarkEntityType;
};

export type DeleteBenchmarkAssignmentOutput = {
  readonly __typename?: 'DeleteBenchmarkAssignmentOutput';
  readonly success: Scalars['Boolean'];
};

export type DeleteBenchmarkOutput = {
  readonly __typename?: 'DeleteBenchmarkOutput';
  readonly success: Scalars['Boolean'];
};

export type DeleteCorpMember = {
  readonly corporationId: Scalars['ID'];
  readonly userId: Scalars['ID'];
};

export type DeleteDocumentAgreementInput = {
  readonly id: Scalars['String'];
};

export type DeleteDocumentTemplateInput = {
  readonly id: Scalars['String'];
};

export type DeleteInvoiceOutput = {
  readonly __typename?: 'DeleteInvoiceOutput';
  readonly success?: Maybe<Scalars['Boolean']>;
};

export type DeleteOutput = {
  readonly __typename?: 'DeleteOutput';
  readonly id?: Maybe<Scalars['ID']>;
};

export type DeleteRepCodeInput = {
  readonly id: Scalars['ID'];
};

export type DeleteRepCodeOutput = {
  readonly __typename?: 'DeleteRepCodeOutput';
  readonly id?: Maybe<Scalars['ID']>;
};

export type DeleteResponse = {
  readonly __typename?: 'DeleteResponse';
  readonly deletedId?: Maybe<Scalars['String']>;
  readonly success?: Maybe<Scalars['Boolean']>;
};

export enum DepositFrequency {
  BIWEEKLY = 'BIWEEKLY',
  DAILY = 'DAILY',
  MONTHLY = 'MONTHLY',
  ONCE = 'ONCE',
  QUARTERLY = 'QUARTERLY',
  WEEKLY = 'WEEKLY'
}

export type Device = {
  readonly deviceType?: InputMaybe<DeviceType>;
  readonly fingerprintHash?: InputMaybe<Scalars['String']>;
  readonly trustDevice?: InputMaybe<Scalars['Boolean']>;
};

export enum DeviceType {
  MOBILE = 'MOBILE',
  WEB = 'WEB'
}

export type DisableOrgPartnerIntegrationInput = {
  readonly integrationId: Scalars['String'];
};

export type DisconnectPartnerIntegrationInput = {
  readonly partnerId: IntegrationPartnerCode;
};

export type DisconnectPartnerIntegrationResponse = {
  readonly __typename?: 'DisconnectPartnerIntegrationResponse';
  readonly completionStatus?: Maybe<Scalars['String']>;
  readonly data?: Maybe<Integration>;
  readonly errors?: Maybe<ReadonlyArray<ErrorDescription>>;
};

export type DistributionConstraints = {
  readonly __typename?: 'DistributionConstraints';
  readonly allowedDistributions?: Maybe<ReadonlyArray<Maybe<AllowedDistribution>>>;
  readonly fullBalanceAllowed?: Maybe<Scalars['Boolean']>;
};

export enum DistributionReason {
  CONVERSION = 'CONVERSION',
  DEATH = 'DEATH',
  DISABILITY = 'DISABILITY',
  EXCESS_CONTRIBUTION_REMOVAL_AFTER_TAX_DEADLINE = 'EXCESS_CONTRIBUTION_REMOVAL_AFTER_TAX_DEADLINE',
  EXCESS_CONTRIBUTION_REMOVAL_BEFORE_TAX_DEADLINE = 'EXCESS_CONTRIBUTION_REMOVAL_BEFORE_TAX_DEADLINE',
  MANAGEMENT_FEE = 'MANAGEMENT_FEE',
  NORMAL = 'NORMAL',
  NORMAL_ROTH_IRA_GREATER_THAN_5_YEARS = 'NORMAL_ROTH_IRA_GREATER_THAN_5_YEARS',
  PREMATURE = 'PREMATURE',
  PREMATURE_SIMPLE_IRA_LESS_THAN_2_YEARS = 'PREMATURE_SIMPLE_IRA_LESS_THAN_2_YEARS',
  RECHARACTERIZATION_CURRENT_YEAR = 'RECHARACTERIZATION_CURRENT_YEAR',
  RECHARACTERIZATION_PRIOR_YEAR = 'RECHARACTERIZATION_PRIOR_YEAR',
  ROLLOVER_TO_IRA = 'ROLLOVER_TO_IRA',
  ROLLOVER_TO_QUALIFIED_PLAN = 'ROLLOVER_TO_QUALIFIED_PLAN',
  TRANSFER = 'TRANSFER'
}

export type Document = {
  readonly __typename?: 'Document';
  readonly accountId?: Maybe<Scalars['String']>;
  readonly date?: Maybe<Scalars['String']>;
  readonly description?: Maybe<Scalars['String']>;
  readonly displayName?: Maybe<Scalars['String']>;
  readonly isMmaAuthorized?: Maybe<Scalars['Boolean']>;
  readonly link?: Maybe<Scalars['String']>;
  readonly mmaId?: Maybe<Scalars['String']>;
  readonly tickerSymbol: Scalars['String'];
  readonly type?: Maybe<DocumentType>;
};

export type DocumentAgreement = {
  readonly __typename?: 'DocumentAgreement';
  readonly actionStatus?: Maybe<DocumentAgreementSignersStatus>;
  readonly claimUrl?: Maybe<Scalars['String']>;
  readonly createdDate?: Maybe<Scalars['DateTime']>;
  readonly id: Scalars['ID'];
  readonly name?: Maybe<Scalars['String']>;
  readonly signatures?: Maybe<ReadonlyArray<DocumentSignature>>;
  readonly template?: Maybe<DocumentTemplate>;
};

export type DocumentAgreementResponse = {
  readonly __typename?: 'DocumentAgreementResponse';
  readonly document?: Maybe<DocumentAgreement>;
};

export enum DocumentAgreementSignersStatus {
  CANCELLED = 'CANCELLED',
  COMPLETE = 'COMPLETE',
  ERROR = 'ERROR',
  EXPIRED = 'EXPIRED',
  PENDING_SIGNATURE = 'PENDING_SIGNATURE',
  PROCESSING = 'PROCESSING',
  READY_TO_SIGN = 'READY_TO_SIGN',
  SIGNED = 'SIGNED'
}

export type DocumentAgreementUrlInput = {
  readonly documentAgreementId: Scalars['String'];
};

export type DocumentAgreementUrlResponse = {
  readonly __typename?: 'DocumentAgreementUrlResponse';
  readonly url?: Maybe<Scalars['String']>;
};

export type DocumentAgreementsResponse = {
  readonly __typename?: 'DocumentAgreementsResponse';
  readonly data?: Maybe<ReadonlyArray<DocumentAgreement>>;
  readonly page: ESigPaginationResponse;
};

export type DocumentAgreementsWhereInput = {
  readonly actionStatuses?: InputMaybe<ReadonlyArray<DocumentAgreementSignersStatus>>;
  readonly name?: InputMaybe<Scalars['String']>;
  readonly page: Scalars['Int'];
  readonly recipient?: InputMaybe<Scalars['String']>;
  readonly size: Scalars['Int'];
  readonly sort?: InputMaybe<ReadonlyArray<SortInput>>;
};

export type DocumentBundle = {
  readonly __typename?: 'DocumentBundle';
  readonly id: Scalars['String'];
  readonly pollingInterval: Scalars['Float'];
  readonly readiness?: Maybe<DocumentReadiness>;
  readonly url: Scalars['String'];
};

export type DocumentBundleInput = {
  readonly processId: Scalars['String'];
};

export type DocumentPage = {
  readonly __typename?: 'DocumentPage';
  readonly number?: Maybe<Scalars['Int']>;
  readonly size?: Maybe<Scalars['Int']>;
  readonly totalElements?: Maybe<Scalars['Int']>;
  readonly totalPages?: Maybe<Scalars['Int']>;
};

export enum DocumentProcessingAction {
  DECLINED_PROCESSING = 'DECLINED_PROCESSING',
  SENT_PROCESSING = 'SENT_PROCESSING',
  SIGNED_PROCESSING = 'SIGNED_PROCESSING'
}

export enum DocumentReadiness {
  IN_PROGRESS = 'IN_PROGRESS',
  READY = 'READY'
}

export type DocumentSignature = {
  readonly __typename?: 'DocumentSignature';
  readonly advisorId?: Maybe<Scalars['ID']>;
  readonly dateSentForSignature?: Maybe<Scalars['DateTime']>;
  readonly documentGenerationStatus?: Maybe<ADVISOR_STATUS>;
  readonly forDocument?: Maybe<FOR_DOCUMENT>;
  readonly id: Scalars['ID'];
  readonly order?: Maybe<Scalars['Int']>;
  readonly signatureMetadata?: Maybe<SignatureMetadata>;
  readonly signerOrder?: Maybe<Scalars['Int']>;
  readonly signerRole?: Maybe<Scalars['String']>;
  readonly signerUserUuid?: Maybe<Scalars['String']>;
  readonly status?: Maybe<DocumentSignatureStatus>;
  readonly user?: Maybe<User2>;
};

export enum DocumentSignatureStatus {
  ACTIVE = 'ACTIVE',
  AWAITING_SIGNATURE = 'AWAITING_SIGNATURE',
  CANCELED = 'CANCELED',
  DECLINED = 'DECLINED',
  DECLINED_PROCESSING = 'DECLINED_PROCESSING',
  ERROR = 'ERROR',
  EXPIRED = 'EXPIRED',
  INACTIVE = 'INACTIVE',
  INITIATED = 'INITIATED',
  SENT_TO_CLIENT = 'SENT_TO_CLIENT',
  SENT_TO_SIGNER = 'SENT_TO_SIGNER',
  SIGNED = 'SIGNED',
  SIGNED_PROCESSING = 'SIGNED_PROCESSING',
  SIGNING = 'SIGNING'
}

export type DocumentTemplate = {
  readonly __typename?: 'DocumentTemplate';
  readonly defaultMessage?: Maybe<Scalars['String']>;
  readonly defaultMessageSubject?: Maybe<Scalars['String']>;
  readonly externalAt?: Maybe<Scalars['DateTime']>;
  readonly externalTemplateId?: Maybe<Scalars['String']>;
  readonly externalTemplateUrl?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly name?: Maybe<Scalars['String']>;
  readonly signers?: Maybe<ReadonlyArray<Scalars['String']>>;
  readonly signingOrderIsSet?: Maybe<Scalars['Boolean']>;
  readonly status?: Maybe<DocumentTemplateStatus>;
  readonly templateOwner?: Maybe<Advisor2>;
  readonly templateOwnerId?: Maybe<Scalars['String']>;
  readonly updatedDate?: Maybe<Scalars['DateTime']>;
};

export type DocumentTemplateEditUrlInput = {
  readonly id: Scalars['String'];
  readonly preview?: InputMaybe<Scalars['Boolean']>;
};

export enum DocumentTemplateInputStatus {
  ACTIVE = 'ACTIVE',
  DRAFT = 'DRAFT',
  INACTIVE = 'INACTIVE'
}

export type DocumentTemplateResponse = {
  readonly __typename?: 'DocumentTemplateResponse';
  readonly template?: Maybe<DocumentTemplate>;
};

export enum DocumentTemplateStatus {
  ACTIVE = 'ACTIVE',
  DRAFT = 'DRAFT',
  INACTIVE = 'INACTIVE',
  INITIATED = 'INITIATED'
}

export type DocumentTemplateUpload = {
  readonly __typename?: 'DocumentTemplateUpload';
  readonly id: Scalars['ID'];
  readonly name?: Maybe<Scalars['String']>;
  readonly status?: Maybe<DocumentTemplateStatus>;
};

export type DocumentTemplateWhereInput = {
  readonly id: Scalars['String'];
};

export type DocumentTemplatesResponse = {
  readonly __typename?: 'DocumentTemplatesResponse';
  readonly data?: Maybe<ReadonlyArray<DocumentTemplate>>;
  readonly page: ESigPaginationResponse;
};

export type DocumentTemplatesWhereInput = {
  readonly name?: InputMaybe<Scalars['String']>;
  readonly page: Scalars['Int'];
  readonly size: Scalars['Int'];
  readonly sort?: InputMaybe<ReadonlyArray<SortInput>>;
  readonly statuses?: InputMaybe<ReadonlyArray<DocumentTemplateInputStatus>>;
};

export enum DocumentType {
  ACCOUNT_OPENING = 'ACCOUNT_OPENING',
  ALL = 'ALL',
  AOP_PDF_DOCUMENT_GENERATION = 'AOP_PDF_DOCUMENT_GENERATION',
  AUTHORIZATIONS = 'AUTHORIZATIONS',
  STATEMENT = 'STATEMENT',
  TAX = 'TAX',
  TRADE = 'TRADE'
}

export type DocumentUrl = {
  readonly __typename?: 'DocumentUrl';
  readonly url: Scalars['String'];
};

export type DownloadRealizedCostBasisInput = {
  readonly financialAccountId: Scalars['ID'];
  readonly symbol?: InputMaybe<Scalars['String']>;
  readonly taxYear?: InputMaybe<Scalars['Int']>;
};

export type DraftDeleteInput = {
  readonly id: Scalars['ID'];
};

export type DraftWhere = {
  readonly id: Scalars['ID'];
};

export type DraftsWhere = {
  readonly accountStateId: Scalars['ID'];
};

export type DriftSettings = {
  readonly __typename?: 'DriftSettings';
  /** @deprecated Unused */
  readonly lowerBound?: Maybe<Scalars['Float']>;
  /** @deprecated Use 'target' instead */
  readonly maxDrift?: Maybe<Scalars['Float']>;
  readonly target?: Maybe<Scalars['Float']>;
  readonly type?: Maybe<PortfolioDriftType>;
  /** @deprecated Unused */
  readonly upperBound?: Maybe<Scalars['Float']>;
};

export type DriftSettingsInput = {
  readonly lowerBound?: InputMaybe<Scalars['Float']>;
  readonly maxDrift?: InputMaybe<Scalars['Float']>;
  readonly target?: InputMaybe<Scalars['Float']>;
  readonly type: PortfolioDriftType;
  readonly upperBound?: InputMaybe<Scalars['Float']>;
};

export enum ERROR_TYPE {
  DUPLICATE_BENEFICIARIES = 'DUPLICATE_BENEFICIARIES',
  INVALID_ADDRESS_STATE = 'INVALID_ADDRESS_STATE',
  INVALID_DATE_FORMAT = 'INVALID_DATE_FORMAT',
  INVALID_DOB_BENEFICIARIES = 'INVALID_DOB_BENEFICIARIES',
  INVALID_EMAIL_FORMAT = 'INVALID_EMAIL_FORMAT',
  INVALID_INDUSTRY_CODE = 'INVALID_INDUSTRY_CODE',
  INVALID_MARITAL_STATUS = 'INVALID_MARITAL_STATUS',
  INVALID_OWNERSHIP_PERCENTAGE = 'INVALID_OWNERSHIP_PERCENTAGE',
  MISSING_CONTROL_PERSONS = 'MISSING_CONTROL_PERSONS',
  MISSING_FIELD = 'MISSING_FIELD',
  MISSING_TRUSTED_CONTACT_INFO = 'MISSING_TRUSTED_CONTACT_INFO',
  UNKNOWN = 'UNKNOWN',
  UNSUPPORTED_BENEFICIARIES = 'UNSUPPORTED_BENEFICIARIES'
}

export type ESigPaginationResponse = {
  readonly __typename?: 'ESigPaginationResponse';
  readonly number?: Maybe<Scalars['Int']>;
  readonly size?: Maybe<Scalars['Int']>;
  readonly totalElements?: Maybe<Scalars['Int']>;
  readonly totalPages?: Maybe<Scalars['Int']>;
};

export type EditAccountBeneficiaryAllocationInput = {
  readonly accountId: Scalars['ID'];
  readonly allocations: ReadonlyArray<BeneficiaryAllocation>;
};

export type EditModelAsyncInput = {
  readonly benchmarkId?: InputMaybe<Scalars['ID']>;
  readonly cashTarget?: InputMaybe<Scalars['Float']>;
  readonly description?: InputMaybe<Scalars['String']>;
  readonly holdings: ReadonlyArray<ModelHoldingInput>;
  readonly holdingsType?: InputMaybe<HoldingType>;
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
  readonly rebalanceAsap?: InputMaybe<Scalars['Boolean']>;
  readonly skipRebalance?: InputMaybe<Scalars['Boolean']>;
};

export type EditModelAsyncOutput = {
  readonly __typename?: 'EditModelAsyncOutput';
  readonly id: Scalars['ID'];
};

export type EditPortfolioAsyncInput = {
  readonly cashSettings?: InputMaybe<CashSettingsInput>;
  readonly cashTarget?: InputMaybe<Scalars['Float']>;
  readonly drift: DriftSettingsInput;
  readonly id: Scalars['ID'];
  readonly minCash?: InputMaybe<Scalars['Float']>;
  readonly models?: InputMaybe<ReadonlyArray<InputMaybe<PortfolioModelInput>>>;
  readonly name: Scalars['String'];
  readonly rebalanceAsap?: InputMaybe<Scalars['Boolean']>;
  readonly skipRebalance?: InputMaybe<Scalars['Boolean']>;
};

export type EditPortfolioAsyncOutput = {
  readonly __typename?: 'EditPortfolioAsyncOutput';
  readonly id: Scalars['ID'];
};

export type EditTeamMemberInput = {
  readonly id: Scalars['ID'];
  readonly role: UserRole;
};

export type Emoney = {
  readonly __typename?: 'Emoney';
  readonly repCodes: ReadonlyArray<ClearingRepCode>;
};

export type EmployerEnrolledMailingAddress = {
  readonly __typename?: 'EmployerEnrolledMailingAddress';
  readonly city?: Maybe<Scalars['String']>;
  readonly postalCode?: Maybe<Scalars['String']>;
  readonly recipientName?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly streetAddress?: Maybe<ReadonlyArray<Scalars['String']>>;
};

export type EmployerEnrolledMailingAddressInput = {
  readonly city?: InputMaybe<Scalars['String']>;
  readonly postalCode?: InputMaybe<Scalars['String']>;
  readonly recipientName?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly streetAddress?: InputMaybe<ReadonlyArray<Scalars['String']>>;
};

export type EntityAccountDetails = {
  readonly __typename?: 'EntityAccountDetails';
  readonly businessAddress: Scalars['String'];
  readonly businessCity: Scalars['String'];
  readonly businessPhone: Scalars['String'];
  readonly businessState: Scalars['String'];
  readonly businessZipCode: Scalars['String'];
  readonly email: Scalars['String'];
};

export type EntityAddress = {
  readonly __typename?: 'EntityAddress';
  readonly addr1: Scalars['String'];
  readonly city: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly state: Scalars['String'];
  readonly zipCode: Scalars['String'];
};

export type EntityAddressInput = {
  readonly addr1: Scalars['String'];
  readonly city: Scalars['String'];
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly state: Scalars['String'];
  readonly zipCode: Scalars['String'];
};

export type EntityProfile = {
  readonly __typename?: 'EntityProfile';
  readonly annualIncome?: Maybe<Scalars['String']>;
  readonly backupWithholding?: Maybe<Scalars['Boolean']>;
  readonly companyName: Scalars['String'];
  readonly companyPhone?: Maybe<Scalars['String']>;
  readonly entityNature?: Maybe<AccountNature>;
  readonly entityTaxId?: Maybe<Scalars['String']>;
  readonly exemptLegalCustomer?: Maybe<Scalars['Boolean']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly foreignBank?: Maybe<Scalars['Boolean']>;
  readonly fundingSource?: Maybe<PrimaryOngoingFundingSource>;
  readonly fundingSourceClarification?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly investmentRiskTolerance?: Maybe<Scalars['String']>;
  readonly issueBearerShares?: Maybe<Scalars['Boolean']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly liquidNetWorth?: Maybe<Scalars['String']>;
  readonly maintainedForForeignFinancialInstitution?: Maybe<Scalars['Boolean']>;
  readonly negativeInformation?: Maybe<Scalars['Boolean']>;
  readonly negativeNewsInformation?: Maybe<Scalars['String']>;
  readonly pabAccount?: Maybe<Scalars['Boolean']>;
  readonly politicallyExposed?: Maybe<Scalars['Boolean']>;
  readonly primaryActivity?: Maybe<PrimaryEntityAccountActivity>;
  readonly relatedAccountNumbers?: Maybe<Scalars['String']>;
  readonly relatedAccounts?: Maybe<Scalars['Boolean']>;
  readonly scopeOfBusiness?: Maybe<ScopeOfBusiness>;
  readonly scopeOfBusinessClarification?: Maybe<Scalars['String']>;
  readonly ssnTaxId?: Maybe<Scalars['String']>;
  readonly stateOfOrigin?: Maybe<Scalars['String']>;
  readonly totalNetWorth?: Maybe<Scalars['String']>;
  readonly withdrawalFrequency?: Maybe<ExpectedWithdrawalFrequency>;
};

export type EntityProfileInput = {
  readonly annualIncome?: InputMaybe<Scalars['String']>;
  readonly backupWithholding?: InputMaybe<Scalars['Boolean']>;
  readonly companyName: Scalars['String'];
  readonly companyPhone?: InputMaybe<Scalars['String']>;
  readonly entityNature?: InputMaybe<AccountNature>;
  readonly entityTaxId?: InputMaybe<Scalars['String']>;
  readonly exemptLegalCustomer?: InputMaybe<Scalars['Boolean']>;
  readonly firstName?: InputMaybe<Scalars['String']>;
  readonly foreignBank?: InputMaybe<Scalars['Boolean']>;
  readonly fundingSource?: InputMaybe<PrimaryOngoingFundingSource>;
  readonly fundingSourceClarification?: InputMaybe<Scalars['String']>;
  readonly investmentRiskTolerance?: InputMaybe<Scalars['String']>;
  readonly issueBearerShares?: InputMaybe<Scalars['Boolean']>;
  readonly lastName?: InputMaybe<Scalars['String']>;
  readonly liquidNetWorth?: InputMaybe<Scalars['String']>;
  readonly maintainedForForeignFinancialInstitution?: InputMaybe<Scalars['Boolean']>;
  readonly negativeInformation?: InputMaybe<Scalars['Boolean']>;
  readonly negativeNewsInformation?: InputMaybe<Scalars['String']>;
  readonly pabAccount?: InputMaybe<Scalars['Boolean']>;
  readonly politicallyExposed?: InputMaybe<Scalars['Boolean']>;
  readonly primaryActivity?: InputMaybe<PrimaryEntityAccountActivity>;
  readonly relatedAccountNumbers?: InputMaybe<Scalars['String']>;
  readonly relatedAccounts?: InputMaybe<Scalars['Boolean']>;
  readonly scopeOfBusiness?: InputMaybe<ScopeOfBusiness>;
  readonly scopeOfBusinessClarification?: InputMaybe<Scalars['String']>;
  readonly ssnTaxId?: InputMaybe<Scalars['String']>;
  readonly stateOfOrigin?: InputMaybe<Scalars['String']>;
  readonly totalNetWorth?: InputMaybe<Scalars['String']>;
  readonly withdrawalFrequency?: InputMaybe<ExpectedWithdrawalFrequency>;
};

export type Equity = {
  readonly __typename?: 'Equity';
  readonly equityPositions: ReadonlyArray<EquityPosition>;
  readonly equityValue: Scalars['Float'];
};

export type EquityPosition = {
  readonly __typename?: 'EquityPosition';
  readonly availableForTradingQty: Scalars['Float'];
  readonly avgPrice: Scalars['Float'];
  readonly costBasis: Scalars['Float'];
  readonly instrumentID: Scalars['ID'];
  readonly marketValue: Scalars['Float'];
  readonly mktPrice: Scalars['Float'];
  readonly openQty: Scalars['Float'];
  readonly priorClose: Scalars['Float'];
  readonly side: Scalars['String'];
  readonly symbol: Scalars['String'];
  readonly unrealizedDayPL: Scalars['Float'];
  readonly unrealizedDayPLPercent: Scalars['Float'];
  readonly unrealizedPL: Scalars['Float'];
};

export type ErrorDescription = {
  readonly __typename?: 'ErrorDescription';
  readonly description?: Maybe<Scalars['String']>;
  readonly errorCode?: Maybe<Scalars['String']>;
};

export type EstimatedMonthlyCost = {
  readonly __typename?: 'EstimatedMonthlyCost';
  readonly activeAccounts: Scalars['Int'];
  readonly cost: Scalars['Int'];
};

export type ExchangeStatus = {
  readonly __typename?: 'ExchangeStatus';
  readonly asof?: Maybe<Scalars['DateTime']>;
  readonly cacheTime?: Maybe<Scalars['SafeInt']>;
  readonly closeDateTime?: Maybe<Scalars['DateTime']>;
  readonly closeTime?: Maybe<Scalars['SafeInt']>;
  readonly exchange?: Maybe<Scalars['String']>;
  readonly fromCache?: Maybe<Scalars['Boolean']>;
  readonly msToClose?: Maybe<Scalars['SafeInt']>;
  readonly msToOpen?: Maybe<Scalars['SafeInt']>;
  readonly open?: Maybe<Scalars['Boolean']>;
  readonly openDateTime?: Maybe<Scalars['DateTime']>;
  readonly openTime?: Maybe<Scalars['SafeInt']>;
};

export type ExecuteFeesReportDateRange = {
  readonly executionStatus?: InputMaybe<ExecutionStatus>;
  readonly lastDay: Scalars['DateOnly'];
  readonly startDay: Scalars['DateOnly'];
};

export type ExecuteManualInvoiceInput = {
  readonly accountDetails: ReadonlyArray<InputMaybe<ManualFinAccounts>>;
  readonly billingInformation: ManualBillingInformation;
  readonly householdId: Scalars['ID'];
  readonly organizationId: Scalars['ID'];
};

export type ExecutedInvoicesOutput = {
  readonly __typename?: 'ExecutedInvoicesOutput';
  readonly invoices: ReadonlyArray<Maybe<GroupInvoice>>;
};

export enum ExecutionStatus {
  Executed = 'Executed',
  Unexecuted = 'Unexecuted'
}

export enum ExpectedWithdrawalFrequency {
  FREQUENT = 'FREQUENT',
  OCCASIONAL = 'OCCASIONAL',
  WITHDRAWAL = 'WITHDRAWAL'
}

export enum ExtendedAllowedType {
  AUM = 'AUM',
  FLAT = 'FLAT',
  FLAT_AUM = 'FLAT_AUM'
}

export enum ExternalConnection {
  LINKED = 'LINKED',
  NONE = 'NONE',
  UNLINKED = 'UNLINKED'
}

export enum FEE_TYPES {
  ASSET = 'ASSET',
  FIXED = 'FIXED',
  HOURLY = 'HOURLY'
}

export type FOPInput = {
  readonly subType?: InputMaybe<FOPSubType>;
};

export enum FOPSubType {
  FULL = 'FULL',
  PARTIAL = 'PARTIAL'
}

export type FOPType = {
  readonly __typename?: 'FOPType';
  readonly subType?: Maybe<FOPSubType>;
};

export enum FOR_DOCUMENT {
  MARKETPLACE_AGREEMENT = 'MARKETPLACE_AGREEMENT'
}

export type FailedRebalanceSummary = {
  readonly __typename?: 'FailedRebalanceSummary';
  readonly basketId: Scalars['ID'];
  readonly failedCount: Scalars['Int'];
  readonly requiresModificationCount: Scalars['Int'];
};

export type FeeAllocation = {
  readonly __typename?: 'FeeAllocation';
  readonly accountAllocations: ReadonlyArray<AccountAllocation2>;
  readonly allocationType: AllocationType;
  readonly billingGroupId?: Maybe<Scalars['ID']>;
  readonly debitAccountOverrides: ReadonlyArray<DebitAccountOverride>;
  readonly heldAwayAccountAllocations: ReadonlyArray<AccountAllocation2>;
  readonly householdId?: Maybe<Scalars['ID']>;
};

export enum FeeEntityType {
  ACCOUNT = 'ACCOUNT',
  HOUSEHOLD = 'HOUSEHOLD',
  ORG = 'ORG'
}

export type FeeInvoicePeriod = {
  readonly __typename?: 'FeeInvoicePeriod';
  readonly endDate: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly numOfAccounts: Scalars['Int'];
  readonly startDate: Scalars['String'];
  readonly status: Scalars['String'];
  readonly total: Scalars['Float'];
};

export type FeeInvoicePeriodInvoice = {
  readonly __typename?: 'FeeInvoicePeriodInvoice';
  readonly errors?: Maybe<ReadonlyArray<InvoiceError>>;
  readonly groupId: Scalars['ID'];
  readonly groupName: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly invoiceAmount: Scalars['Float'];
  readonly invoiceNumber: Scalars['String'];
  readonly scheduleType?: Maybe<ScheduleType>;
  readonly status?: Maybe<Scalars['String']>;
};

export type FeeInvoiceStats = {
  readonly __typename?: 'FeeInvoiceStats';
  readonly lastExported?: Maybe<Scalars['String']>;
  readonly ytdRevenue: Scalars['Float'];
};

export enum FeeRoutingLevel {
  ACCOUNT = 'ACCOUNT',
  DEFAULT = 'DEFAULT',
  ORG = 'ORG'
}

export type FeeRoutingTypeDefinition = {
  readonly __typename?: 'FeeRoutingTypeDefinition';
  readonly description: Scalars['String'];
  readonly displayName: Scalars['String'];
  readonly type: Scalars['String'];
};

export type FeeTemplate = {
  readonly __typename?: 'FeeTemplate';
  readonly definition: FeeRoutingTypeDefinition;
  readonly entityId: Scalars['String'];
  readonly entityType: FeeEntityType;
  readonly routingLevel: FeeRoutingLevel;
  readonly scheduleId?: Maybe<Scalars['String']>;
  readonly type: Scalars['String'];
};

export type FeeTemplateHouseholdTypeFilter = {
  readonly entityId: Scalars['String'];
  readonly type?: InputMaybe<Scalars['String']>;
  readonly types?: InputMaybe<ReadonlyArray<Scalars['String']>>;
};

export type FeeTemplateTypeFilter = {
  readonly type?: InputMaybe<Scalars['String']>;
  readonly types?: InputMaybe<ReadonlyArray<Scalars['String']>>;
};

export enum FeeType {
  COMING_SOON = 'COMING_SOON',
  FREE = 'FREE',
  PAID = 'PAID',
  PRICE_VARIES = 'PRICE_VARIES'
}

export type FeeTypeObjInput = {
  readonly feeType?: InputMaybe<FEE_TYPES>;
};

export type FeeTypeObjOutput = {
  readonly __typename?: 'FeeTypeObjOutput';
  readonly feeType?: Maybe<FEE_TYPES>;
};

export type FieldsNeedingDateInput = {
  readonly last_seen: Scalars['Boolean'];
  readonly signed_up: Scalars['Boolean'];
};

export type File = {
  readonly __typename?: 'File';
  readonly id?: Maybe<Scalars['String']>;
};

export type FinAccountDetails = {
  readonly __typename?: 'FinAccountDetails';
  readonly accountClassification: InvoiceAccountClassification;
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly accountType?: Maybe<Scalars['String']>;
  readonly adjustments: ReadonlyArray<Maybe<Adjustment>>;
  readonly aum?: Maybe<Scalars['Float']>;
  readonly aumDailyFee?: Maybe<Scalars['Float']>;
  readonly aumDaysCount: Scalars['Int'];
  readonly aumFee?: Maybe<Scalars['Float']>;
  readonly aumRate?: Maybe<Scalars['Float']>;
  readonly balance?: Maybe<Scalars['Float']>;
  readonly balancedMetrics?: Maybe<AllowedBalanceMetrics>;
  readonly calculation?: Maybe<Calculation>;
  readonly debit: Scalars['Float'];
  readonly debitAccountId?: Maybe<Scalars['String']>;
  readonly description?: Maybe<Scalars['String']>;
  readonly excluded?: Maybe<Scalars['Boolean']>;
  readonly financialAccountId: Scalars['ID'];
  readonly financialAccountName: Scalars['String'];
  readonly financialInstitution?: Maybe<Scalars['String']>;
  readonly ownerFirstName?: Maybe<Scalars['String']>;
  readonly ownerLastName?: Maybe<Scalars['String']>;
  readonly periodFlatFee: Scalars['Float'];
  readonly scheduleId?: Maybe<Scalars['ID']>;
  readonly scheduleType: AllowedType;
  readonly schemaVer?: Maybe<Scalars['String']>;
  readonly timing?: Maybe<AllowedBillTiming>;
};

export type FinAccountWithAssignmentId = {
  readonly __typename?: 'FinAccountWithAssignmentId';
  readonly accountBalance: Scalars['Float'];
  readonly accountType: Scalars['String'];
  readonly endDate?: Maybe<Scalars['String']>;
  readonly financialAccountId: Scalars['ID'];
  readonly scheduleId: Scalars['ID'];
  readonly scheduleName: Scalars['String'];
  readonly startDate?: Maybe<Scalars['String']>;
};

export type FinancialAccount = {
  readonly __typename?: 'FinancialAccount';
  readonly accountCashSummary?: Maybe<AccountCashSummary>;
  readonly accountClassification?: Maybe<Scalars['String']>;
  readonly accountGroup?: Maybe<Repcode>;
  readonly accountNickname?: Maybe<Scalars['String']>;
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly accountType?: Maybe<Scalars['String']>;
  readonly ageOfMajority?: Maybe<AgeOfMajority>;
  /** @deprecated It's always 0. */
  readonly altruistRank: Scalars['Int'];
  readonly assignedPortfolio?: Maybe<AssignedPortfolioDetails>;
  readonly balance: Scalars['Float'];
  readonly canUnassign?: Maybe<Scalars['Boolean']>;
  readonly cashPercent: Scalars['Float'];
  readonly category?: Maybe<Scalars['String']>;
  readonly externalConnection?: Maybe<ExternalConnection>;
  readonly externalRef?: Maybe<Scalars['String']>;
  readonly financialAccountId?: Maybe<Scalars['ID']>;
  readonly financialAccountName: Scalars['String'];
  readonly financialInstitution: Scalars['String'];
  readonly inceptionDate?: Maybe<Scalars['String']>;
  readonly institutionName?: Maybe<Scalars['String']>;
  readonly isTaxable?: Maybe<Scalars['Boolean']>;
  readonly lastUpdatedDate?: Maybe<Scalars['String']>;
  readonly lastUpdatedDays?: Maybe<Scalars['Float']>;
  readonly managedAccount?: Maybe<Scalars['Boolean']>;
  readonly mask?: Maybe<Scalars['String']>;
  readonly originalAccountName: Scalars['String'];
  readonly ownerFirstName: Scalars['String'];
  readonly ownerLastName: Scalars['String'];
  readonly qualifiedRepCode?: Maybe<Scalars['String']>;
  readonly realTimeBalance?: Maybe<Scalars['Float']>;
  readonly reportingStartDate?: Maybe<Scalars['DateOnly']>;
  readonly return30d: Scalars['Float'];
  /** @deprecated Not used. */
  readonly returnYield?: Maybe<Scalars['Float']>;
  readonly returnYtd: Scalars['Float'];
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
  readonly status?: Maybe<Scalars['String']>;
  readonly taxSettings?: Maybe<MiniTaxSettings>;
  readonly updated?: Maybe<Scalars['String']>;
  readonly userId: Scalars['ID'];
  /** @deprecated Not used. */
  readonly ytdChange: Scalars['Float'];
};

export type FinancialAccount2 = {
  readonly __typename?: 'FinancialAccount2';
  readonly accountInfoId?: Maybe<Scalars['String']>;
  readonly accountType: Scalars['String'];
  readonly adoptingEmployer?: Maybe<AdoptingEmployerProfile>;
  readonly ageOfMajority?: Maybe<AgeOfMajority>;
  /** @deprecated Use create field and perform UI logic to get createdMinutesAgo. */
  readonly createdMinutesAgo: Scalars['Int'];
  readonly decedent?: Maybe<DecedentProfile>;
  readonly decedentName?: Maybe<Scalars['String']>;
  readonly eligibleForAcat: Scalars['Boolean'];
  readonly financialAccountId: Scalars['ID'];
  readonly financialAccountName: Scalars['String'];
  readonly fundedDay?: Maybe<Scalars['DateOnly']>;
  readonly id: Scalars['ID'];
  readonly inceptionDay?: Maybe<Scalars['DateOnly']>;
  readonly owners: ReadonlyArray<FinancialAccountOwner>;
  /** @deprecated Use planDetails.participantType */
  readonly participantType?: Maybe<Scalars['String']>;
  /** @deprecated Use planDetails.planAdministrator */
  readonly planAdministrator?: Maybe<Scalars['String']>;
  readonly planDetails?: Maybe<PlanDetails>;
  /** @deprecated Use planDetails.planEffectiveDate */
  readonly planEffectiveDate?: Maybe<Scalars['String']>;
  /** @deprecated Use planDetails.planName */
  readonly planName?: Maybe<Scalars['String']>;
  readonly properties?: Maybe<FinancialAccountProperties>;
};

export type FinancialAccountBillingDates = {
  readonly __typename?: 'FinancialAccountBillingDates';
  readonly financialAccountId?: Maybe<Scalars['ID']>;
  readonly reportingLastDate?: Maybe<Scalars['DateOnly']>;
  readonly reportingStartDate?: Maybe<Scalars['DateOnly']>;
};

/** Leaving out total because the total depends on how the client wants to dice the information. */
export type FinancialAccountCounts = {
  readonly __typename?: 'FinancialAccountCounts';
  readonly assigned?: Maybe<Scalars['Int']>;
  readonly id: Scalars['ID'];
  readonly unassigned?: Maybe<Scalars['Int']>;
};

export type FinancialAccountDetails = {
  readonly __typename?: 'FinancialAccountDetails';
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly balance: Scalars['Float'];
  readonly household?: Maybe<Scalars['String']>;
  readonly householdId?: Maybe<Scalars['ID']>;
  readonly id: Scalars['ID'];
  readonly institution?: Maybe<Scalars['String']>;
  readonly label?: Maybe<Scalars['String']>;
  readonly portfolio?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
};

export type FinancialAccountForHeldAway = {
  readonly __typename?: 'FinancialAccountForHeldAway';
  readonly heldAwayAccount?: Maybe<HeldAwayAccount>;
  readonly id: Scalars['ID'];
};

export type FinancialAccountOwner = {
  readonly __typename?: 'FinancialAccountOwner';
  readonly email?: Maybe<Scalars['String']>;
  readonly firstName: Scalars['String'];
  readonly fullName: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly lastName: Scalars['String'];
  readonly profileType: ProfileType;
  readonly state?: Maybe<Scalars['String']>;
};

export type FinancialAccountProperties = {
  readonly __typename?: 'FinancialAccountProperties';
  readonly hasMinorContactInfoConfirmed?: Maybe<Scalars['Boolean']>;
  readonly hasOfflineBeneficiary?: Maybe<Scalars['Boolean']>;
};

export type FinancialAccountPropertiesInput = {
  readonly hasMinorContactInfoConfirmed?: InputMaybe<Scalars['Boolean']>;
};

export type FinancialAccountStatusCounts = {
  readonly __typename?: 'FinancialAccountStatusCounts';
  readonly ACTIVE?: Maybe<Scalars['Int']>;
  readonly CLOSED?: Maybe<Scalars['Int']>;
  readonly COMPLETED?: Maybe<Scalars['Int']>;
  readonly DRAFT?: Maybe<Scalars['Int']>;
  readonly INACTIVE?: Maybe<Scalars['Int']>;
  readonly INVITED?: Maybe<Scalars['Int']>;
  readonly IN_PROGRESS?: Maybe<Scalars['Int']>;
  readonly NEEDS_ACTION?: Maybe<Scalars['Int']>;
  readonly PENDING?: Maybe<Scalars['Int']>;
  readonly REJECTED?: Maybe<Scalars['Int']>;
  readonly RESTRICTED?: Maybe<Scalars['Int']>;
  readonly distinctDraftsCount?: Maybe<Scalars['Int']>;
  readonly distinct_multi_draft_count?: Maybe<Scalars['Int']>;
};

export type FinancialAccountTimeWeightedReturn = {
  readonly __typename?: 'FinancialAccountTimeWeightedReturn';
  readonly endDate?: Maybe<Scalars['DateOnly']>;
  readonly financialId: Scalars['ID'];
  readonly startDate?: Maybe<Scalars['DateOnly']>;
  readonly value?: Maybe<Scalars['Float']>;
};

export type FinancialAccountWhereInput = {
  readonly financialAccountId: Scalars['ID'];
};

export type FinancialAccounts = {
  readonly __typename?: 'FinancialAccounts';
  readonly all: ReadonlyArray<Maybe<FinancialAccount>>;
  readonly external: ReadonlyArray<Maybe<FinancialAccount>>;
  readonly managed: ReadonlyArray<Maybe<FinancialAccount>>;
};

export type FinancialAccountsByUserIdWhereInput = {
  readonly accountType?: InputMaybe<Scalars['String']>;
  readonly status?: InputMaybe<Scalars['String']>;
  readonly userId: Scalars['String'];
};

export type FinancialAccountsForAccountGroupsPage = {
  readonly __typename?: 'FinancialAccountsForAccountGroupsPage';
  readonly balance: Scalars['Float'];
  readonly id: Scalars['ID'];
  readonly label?: Maybe<Scalars['String']>;
};

export type FinancialAccountsForAccountGroupsPageWhereInput = {
  readonly accountGroupId: Scalars['String'];
};

/** Purposely acts like a REST client until we figure out how to do batch requests properly. */
export type FinancialAccountsForAccountsPage = {
  readonly __typename?: 'FinancialAccountsForAccountsPage';
  readonly accountGroup?: Maybe<Scalars['String']>;
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly balance: Scalars['Float'];
  readonly cashBalance?: Maybe<Scalars['Float']>;
  readonly created?: Maybe<Scalars['DateTime']>;
  readonly externalRef?: Maybe<Scalars['String']>;
  readonly feeSchedule?: Maybe<Scalars['String']>;
  readonly household?: Maybe<Scalars['String']>;
  readonly householdId?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly inceptionDate?: Maybe<Scalars['String']>;
  readonly institution?: Maybe<Scalars['String']>;
  readonly label?: Maybe<Scalars['String']>;
  readonly lastRebalanceDate?: Maybe<Scalars['DateTime']>;
  readonly portfolio?: Maybe<Scalars['String']>;
  readonly primaryClientFullName?: Maybe<Scalars['String']>;
  readonly primaryClientStatus?: Maybe<Scalars['String']>;
  readonly qualifiedRepCode?: Maybe<Scalars['String']>;
  readonly taxSettings?: Maybe<TaxSettings>;
  readonly type?: Maybe<Scalars['String']>;
};

export type FinancialAccountsForAccountsPageWhereInput = {
  readonly assigned?: InputMaybe<Scalars['Boolean']>;
  readonly unassigned?: InputMaybe<Scalars['Boolean']>;
};

export type FinancialAccountsForAccountsReport = {
  readonly __typename?: 'FinancialAccountsForAccountsReport';
  readonly accountGroup?: Maybe<Scalars['String']>;
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly balance: Scalars['Float'];
  readonly cashBalance?: Maybe<Scalars['Float']>;
  readonly created?: Maybe<Scalars['DateTime']>;
  readonly externalRef?: Maybe<Scalars['String']>;
  readonly feeSchedule?: Maybe<Scalars['String']>;
  readonly household?: Maybe<Scalars['String']>;
  readonly householdId?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly inceptionDate?: Maybe<Scalars['String']>;
  readonly institution?: Maybe<Scalars['String']>;
  readonly label?: Maybe<Scalars['String']>;
  readonly lastRebalanceDate?: Maybe<Scalars['DateTime']>;
  readonly portfolio?: Maybe<Scalars['String']>;
  readonly primaryClientAddress?: Maybe<Address>;
  readonly primaryClientFullName?: Maybe<Scalars['String']>;
  readonly primaryClientProfile?: Maybe<UserProfiles>;
  readonly primaryClientStatus?: Maybe<Scalars['String']>;
  readonly qualifiedRepCode?: Maybe<Scalars['String']>;
  readonly taxSettings?: Maybe<TaxSettings>;
  readonly type?: Maybe<Scalars['String']>;
};

export type FinancialAccountsForHeldAwayInput = {
  readonly householdId: Scalars['String'];
};

export type FinancialAccountsForHeldAwayResponse = {
  readonly __typename?: 'FinancialAccountsForHeldAwayResponse';
  readonly accounts?: Maybe<ReadonlyArray<FinancialAccountForHeldAway>>;
};

export type FinancialAccountsForPortfolioAssignment = {
  readonly __typename?: 'FinancialAccountsForPortfolioAssignment';
  readonly accountClassification?: Maybe<Scalars['String']>;
  readonly accountGroup?: Maybe<Scalars['String']>;
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly balance: Scalars['Float'];
  readonly household?: Maybe<Scalars['String']>;
  readonly householdId?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly institution?: Maybe<Scalars['String']>;
  readonly label?: Maybe<Scalars['String']>;
  readonly portfolio?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
};

export type FinancialAccountsPerformanceInput = {
  readonly endDate: Scalars['DateOnly'];
  readonly financialAccountIds: ReadonlyArray<Scalars['ID']>;
  readonly startDate: Scalars['DateOnly'];
};

export type FinancialAccountsPerformanceResponse = {
  readonly __typename?: 'FinancialAccountsPerformanceResponse';
  readonly balance?: Maybe<Scalars['Float']>;
  readonly cashBalance?: Maybe<Scalars['Float']>;
  readonly financialAccountId: Scalars['ID'];
  readonly timeWeightedReturn?: Maybe<FinancialAccountTimeWeightedReturn>;
};

export type FinancialEntityHoldingsPerformance = {
  readonly __typename?: 'FinancialEntityHoldingsPerformance';
  readonly financialEntityId: Scalars['ID'];
  readonly holdingsPerformance?: Maybe<ReadonlyArray<HoldingPerformance>>;
};

export type FinancialEntityRiskReturnAnalytics = {
  readonly __typename?: 'FinancialEntityRiskReturnAnalytics';
  readonly analytics?: Maybe<RiskReturnAnalyticsStats>;
  readonly endDate?: Maybe<Scalars['DateOnly']>;
  readonly id: Scalars['ID'];
  readonly responseCode?: Maybe<Scalars['String']>;
  readonly returns?: Maybe<ReadonlyArray<Maybe<TimeWeightedReturn>>>;
  readonly startDate?: Maybe<Scalars['DateOnly']>;
  readonly type?: Maybe<PerformanceAnalyticsResourceType>;
};

export enum FinancialEntityType {
  ACCOUNT = 'ACCOUNT',
  HOUSEHOLD = 'HOUSEHOLD'
}

export type FinancialRiskReturnAnalyticsEntityInput = {
  readonly resourceType: PerformanceAnalyticsResourceType;
  readonly resourceTypeId: Scalars['ID'];
};

export type FinancialRiskReturnAnalyticsInput = {
  readonly dateRangeConfig: PerformanceAnalyticsDateRangeConfigInput;
  readonly entities: ReadonlyArray<FinancialRiskReturnAnalyticsEntityInput>;
  readonly includeReturns?: InputMaybe<Scalars['Boolean']>;
};

export type FinancialRiskReturnAnalyticsResponse = {
  readonly __typename?: 'FinancialRiskReturnAnalyticsResponse';
  readonly financialEntities?: Maybe<ReadonlyArray<Maybe<FinancialEntityRiskReturnAnalytics>>>;
};

export type FixedIncomeInstrument = {
  readonly __typename?: 'FixedIncomeInstrument';
  /** Indicates whether the security was part of Rule 144A, an SEC rule that provides a safe harbor exemption from the registration requirements for certain private resales of restricted securities to qualified institutional buyers. 144A applies to securities that are not registered with the SEC and are typically sold through private placements. Note: only qualified institutional buyers, such as those with with at least $100 million in investable assets, are eligible to purchase 144A securities. */
  readonly _144a?: Maybe<Scalars['Boolean']>;
  /** The interest that has accumulated on a bond in dollars per bond bewteen the last interest payment and the present date that has not yet been paid to the bondholder. */
  readonly accrued_interest?: Maybe<Scalars['Float']>;
  /** Indicates whether the bond is bank qualified, which is a type of municipal bond specifically designated for purchase by commercial banks. Bank qualified bonds can offer specific tax benefits because they allow banks to deduct interest costs of carrying the bond. For example, they must be issued by a small issuer who expects to issue no more than $10 million in bonds during a calendar year. Banks can also deduct 80% of the interest expense they incur to buy and carry these bonds. */
  readonly bank_qualified?: Maybe<Scalars['Boolean']>;
  /** A financial instrument that gives holder the right, but not the obligation, to purchase a certain number of shares of the issuer's stock at a specified price (the strike price) before the warrant expires. */
  readonly bond_warrant?: Maybe<Scalars['Boolean']>;
  /**
   * The type of call on the bond refers to one of a variety of circumstances under which a callable bond may be called. Null if the bond is not callable.
   *
   * - ordinary: The most common call type, which grants the issuer the ability to redeem the bond before its maturity date.
   * - make_whole: A make whole call provision allows an issuer to redeem the bond before maturity at a price that is usually calculated based on the present value of future coupon payments and the redemption value, discounted at a specified rate.
   * - regulatory: A regulatory call type gives the issuer the right to redeem the bond before its maturity due to changes in regulatory or tax laws that adversely affect the bond or its issuer.
   * - special: A special call allows the issuer to call the bond before its maturity under specific circumstances, conditions, or events that are outside the typical operations of the issuer, as outlined in the bond's indenture.
   *
   * ordinary | make_whole| regulatory| special
   */
  readonly call_type?: Maybe<Scalars['String']>;
  /** Whether the bond is callable, meaning the issuer has the right, but not the obligation to redeem the bond — in other words, pay out the bondholder — before its maturity date at a set price (the call price). */
  readonly callable?: Maybe<Scalars['Boolean']>;
  /** The close price, or "end of day price" is the price of the last transaction of a security before the market closes for normal trading, shown as a percentage of par value. Note: available to users licensed for end-of-day prices only. */
  readonly close_price?: Maybe<Scalars['Float']>;
  /** The timestamp on which the close price and yield were recorded in RFC-3339 format. Note: available to users licensed for end-of-day prices only. */
  readonly close_price_asof?: Maybe<Scalars['String']>;
  /** The close Yield to Worst refers to the YTW of the last transaction of a security before the market officially closes for normal trading. Note: available to users licensed for end-of-day prices only. */
  readonly close_ytw?: Maybe<Scalars['Float']>;
  /** A flag indicating whether the bond is convertible. If a bond is convertible, the investor has the right or the obligation to exchange the bond for a predetermined number of shares in the issuing company at certain times throughout a bond's lifetime. Only specified for corporate bonds issued by publicly traded companies. */
  readonly convertible?: Maybe<Scalars['Boolean']>;
  /** The country where the bond is domiciled (where it legally resides) in the 2-alpha country code format (e.g., US, CA). An example of where this differs from country_issue is if a Brazilian bond is issued in the US (country of issue) for a corporation in Brazil (country of domicile). */
  readonly country_domicile?: Maybe<Scalars['String']>;
  /** The country where the bond is issued in the 2-alpha country code format (e.g., US, CA). */
  readonly country_issue?: Maybe<Scalars['String']>;
  /** The coupon is the annual interest rate paid on the bond as a percentage of par value. This field is set to zero for zero-coupon bonds. */
  readonly coupon?: Maybe<Scalars['Float']>;
  /**
   * The frequency, or how often, the coupon is paid to the bondholder. There are a few standard coupon frequencies:
   *
   * - annual: Paid out once per year.
   * - semi_annual: Paid out twice per year.
   * - quarterly: Paid out four times per year, every quarter.
   * - monthly: Paid out twelve times per year, every month.
   * - zero: No coupon payments.
   * - at_maturity: Coupon paid out once, at maturity. Interest accrues over the life of the bond, and is repaid with the principal. Note: this typically only applies for bonds with a total life of less than 1 year.
   *
   * annual | semi_annual | quarterly | monthly | zero | at_maturity
   */
  readonly coupon_frequency?: Maybe<Scalars['String']>;
  /**
   * The type of coupon rate.
   *
   * - fixed: The coupon payment is a fixed percentage of the bond's par value.
   * - floating: The coupon payment is pegged to some benchmark rate that frequently changes based off a benchmark, such as SOFR. Typically, the coupon payment is the benchmark rate plus a spread.
   * - variable: The coupon payment changes over the life of the bond. Similar to a floating rate, but typically updates less frequently, such as once every coupon payment.
   * - zero: Zero-coupon bonds do not pay a coupon over their lifetime.
   * - complex: A coupon rate that does not belong in any of the above categories.
   * fixed | floating| variable| zero| strip| complex
   */
  readonly coupon_type?: Maybe<Scalars['String']>;
  /** The 3-alpha currency code of the currency the bond is denominated in (e.g., USD, CAD). */
  readonly currency?: Maybe<Scalars['String']>;
  /** The CUSIP is a nine-digit numeric or nine-character alphanumeric code that identifies a North American financial security. Disclaimer: Available to licensed users only. The Committee on Uniform Securities Identification Procedures (CUSIP) identifier for all stocks and registered bonds. CUSIPs are unique security identifiers managed by The CUSIP Service Bureau. */
  readonly cusip?: Maybe<Scalars['String']>;
  /** The dated date marks the beginning of the period for which interest starts accruing on the bond */
  readonly dated_date?: Maybe<Scalars['String']>;
  /**
   * The day count convention used to calculate accrued interest.
   *
   * - A/360: calculates the daily interest using a 360-day year and then multiplies that by the actual number of days in each time period.
   * - A/365: calculates the daily interest using a 365-day year and then multiplies that by the actual number of days in each time period.
   * - 30/360: calculates the daily interest using a 360-day year and then multiplies that by 30 (standardized month).
   * - A/A: calculates the daily interest using the actual number of days in the year and then multiplies that by the actual number of days in each time period.
   * - 30E/360: number of days equals to the actual number of days (for February). If the start date or the end date of the period is the 31st of a month, that date is set to the 30th. The number of days in a year is 360.
   * - B/252: calculates the daily interest using a 252-business-day year and then multiplies that by the actual number of days in each time period.
   * - A/364: calculates the daily interest using a 364-day year and then multiplies that by the actual number of days in each time period.
   *
   * A/360 | A/365 | 30/360 | 30/365 | A/A | 30E/360 | B/252 | A/364
   */
  readonly daycount?: Maybe<Scalars['String']>;
  /** Full text description of the bond following the format: [issuer] [coupon]%, [maturity_date: MM/DD/YYYY] */
  readonly description?: Maybe<Scalars['String']>;
  /** Short text description of the bond following the format: [abbreviated issuer] [coupon]% [maturity_date: MM/DD/YYYY] */
  readonly description_short?: Maybe<Scalars['String']>;
  /** The duration of a bond represents how long it will take, on average, to receive the returns of investment, including the regular interest payments on the bond as well as the final lump sum payment. Moment provides modified duration, a modification of Macaulay duration (which measures the weighted average time it takes to receive all cash flows, both interest and principal, from a bond). Modified duration provides a more direct measure of a bond's price sensitivity to interest rates, as it provides an estimate for how much the price of a bond will change in response to a 1% change in interest rates. Note: this field is available to users licensed for pricing analytics only. */
  readonly duration?: Maybe<Scalars['Float']>;
  /** The date of the first coupon payment on the bond. Null for zero-coupon bonds. */
  readonly first_coupon_date?: Maybe<Scalars['String']>;
  /** Indicates whether the bond is insured, meaning a third-party insurance company will guarantee the payment of principal and interest in case the issuer defaults. */
  readonly insured?: Maybe<Scalars['Boolean']>;
  /** The International Securities Identification Number (ISIN) is a 12-digit alphanumeric code that uniquely identifies a specific security. */
  readonly isin?: Maybe<Scalars['String']>;
  /** The date on which the bond was originally issued. */
  readonly issue_date?: Maybe<Scalars['String']>;
  /** The smallest unit of the bond that can be purchased at its initial offering. */
  readonly issue_minimum_denomination?: Maybe<Scalars['Int']>;
  /** The price at which the bond was originally issued as a percentage of par value. */
  readonly issue_price?: Maybe<Scalars['Float']>;
  /** The total size amount of the bond issue in the issuing currency. */
  readonly issue_size?: Maybe<Scalars['String']>;
  /** Name of the bond issuer */
  readonly issuer?: Maybe<Scalars['String']>;
  /** The date of the last coupon payment on the bond. Null for zero-coupon bonds. */
  readonly last_coupon_date?: Maybe<Scalars['String']>;
  /** Score (from 1-5 or null if the bond is not priced/tradable) reflecting the historical depth of executable liquidity to buy or sell (no minimum trading sizes). A higher score means that an order to buy or sell will generally have faster execution. Note: available to users licensed for pricing analytics only. See reference data glossary for breakdown of how this proprietary analytic is calculated by Moment. */
  readonly liquidity_institutional_aggregate?: Maybe<Scalars['Int']>;
  /** Score (from 1-5 or null if the bond is not priced/tradable) reflecting the historical depth of executable liquidity to buy (no minimum trading sizes). A higher score means that an order to buy will generally have faster execution. Note: available to users licensed for pricing analytics only. See reference data glossary for breakdown of how this proprietary analytic is calculated by Moment. */
  readonly liquidity_institutional_buy?: Maybe<Scalars['Int']>;
  /** Score (from 1-5 or null if the bond is not priced/tradable) reflecting the historical depth of executable liquidity to sell (no minimum trading sizes). A higher score means that an order to sell will generally have faster execution. Note: available to users licensed for pricing analytics only. See reference data glossary for breakdown of how this proprietary analytic is calculated by Moment. */
  readonly liquidity_institutional_sell?: Maybe<Scalars['Int']>;
  /** Score (from 1-5 or null if the bond is not priced/tradable) reflecting the historical depth of executable liquidity to buy or sell with minimum trading sizes less than or equal to $1,000.00. A higher score means that the bond is more likely to have bids and offers at the minimum trading size. Note: available to users licensed for pricing analytics only. See reference data glossary for breakdown of how this proprietary analytic is calculated by Moment. */
  readonly liquidity_micro_aggregate?: Maybe<Scalars['Int']>;
  /** Score (from 1-5 if the bond is priced, or null if the bond is not tradable) reflecting the historical depth of executable liquidity to buy with minimum trading sizes less than or equal to $1,000.00. A higher score means that the bond is more likely to have offers at the minimum trading size. Note: available to users licensed for pricing analytics only. See reference data glossary for breakdown of how this proprietary analytic is calculated by Moment. */
  readonly liquidity_micro_buy?: Maybe<Scalars['Int']>;
  /** Score (from 1-5 or null if the bond is not priced/tradable) reflecting the historical depth of executable liquidity to sell with minimum trading sizes less than or equal to $1,000.00. A higher score means that the bond is more likely to have bids at the minimum trading size. Note: available to users licensed for pricing analytics only. See reference data glossary for breakdown of how this proprietary analytic is calculated by Moment. */
  readonly liquidity_micro_sell?: Maybe<Scalars['Int']>;
  /** Score (from 1-5 or null if the bond is not priced/tradable) reflecting the historical depth of executable liquidity to buy or sell with minimum trading sizes less than or equal to 10,000. A higher score means that an order to buy or sell will generally have faster execution. Note: available to users licensed for pricing analytics only. See reference data glossary for breakdown of how this proprietary analytic is calculated by Moment. */
  readonly liquidity_retail_aggregate?: Maybe<Scalars['Int']>;
  /** Score (from 1-5 or null if the bond is not priced/tradable) reflecting the historical depth of executable liquidity to buy with minimum trading sizes less than or equal to $10,000.00. A higher score means that the bond is more likely to have bids and offers at the minimum trading size. Note: available to users licensed for pricing analytics only. See reference data glossary for breakdown of how this proprietary analytic is calculated by Moment. */
  readonly liquidity_retail_buy?: Maybe<Scalars['Int']>;
  /** Score (from 1-5 or null if the bond is not priced/tradable) reflecting the historical depth of executable liquidity to sell with minimum trading sizes less than or equal to 10,000. A higher score means that an order to sell will generally have faster execution. Note: available to users licensed for pricing analytics only. See reference data glossary for breakdown of how this proprietary analytic is calculated by Moment. */
  readonly liquidity_retail_sell?: Maybe<Scalars['Int']>;
  /** The date on which the bond matures and the principal amount is paid back to the bondholder by the issuer. Note: not all bonds have a maturity date, such as perpetual bonds. */
  readonly maturity_date?: Maybe<Scalars['String']>;
  /** The state or territory of the issuing municipality formatted as a two-letter USPS state abbreviation. Only specified for municipal bonds. */
  readonly municipal_state?: Maybe<Scalars['String']>;
  /** Whether a municipal bond is subject to US federal taxes. Only specified for municipal bonds. */
  readonly municipal_taxable_federal?: Maybe<Scalars['Boolean']>;
  /**
   * The type of revenue backing. General obligation bonds are not tied to any particular revenue-generating project. Revenue bonds, in contrast are repaid from the income generated by specific projects, enterprises, or sources, rather than from general taxation. Only specified for municipal bonds.
   *
   * general_obligation | revenue
   */
  readonly municipal_type?: Maybe<Scalars['String']>;
  /** The date of the next possible call on the bond. Null if the bond is not callable. */
  readonly next_call_date?: Maybe<Scalars['String']>;
  /** The price at which a callable bond can be redeemed by the issuer on the next call date, as a percentage of par. Null if the bond is not callable. */
  readonly next_call_price?: Maybe<Scalars['Float']>;
  /** The date of the next upcoming coupon payment. Null for zero-coupon bonds. Please note that this date may not be when the actual coupon is paid; most coupons will be paid on the following business day if the coupon date falls on a holiday or a weekend */
  readonly next_coupon_date?: Maybe<Scalars['String']>;
  /** The amount that the issuer of the bond will pay back to the bondholder upon maturity. Also known as face value or nominal value. */
  readonly par_value?: Maybe<Scalars['Int']>;
  /** A flag representing whether a bond is perpetual, meaning it does not have a maturity date, and pays interest indefinitely. If this is set, the bond's maturity date will be null. */
  readonly perpetual?: Maybe<Scalars['Boolean']>;
  /** Indicates whether the security was part of private placement, or issued directly to a limited pool of investors rather than through a public offering. These bonds are unregistered with the SEC and are not part of the open market. Note: retail investors may not be eligible to participate in bonds offered via private placement — please consult an attorney for more information. */
  readonly private_placement?: Maybe<Scalars['Boolean']>;
  /** The purpose of a bond refers to the specific reason or the objective for which the bond is issued. It defines how the issuer intends to use the funds raised from the bond sale. May be null for bonds where the purpose is not captured in the prospectus. */
  readonly purpose?: Maybe<Scalars['String']>;
  /** Whether the bond is puttable, meaning the bondholder has the right, but not the obligation, to sell the security back to the issuer at a predetermined price and date. */
  readonly puttable?: Maybe<Scalars['Boolean']>;
  /** Indicates whether the security falls under Regulation S, a rule that provides an exemption from the registration requirements for securities offerings made outside the United States. This regulation allows U.S. and foreign issuers to sell securities in non-U.S. markets without the need to register these securities with SEC. */
  readonly reg_s?: Maybe<Scalars['Boolean']>;
  /** Sector classification of issuer (e.x. Financial Services, Healthcare Facilities, Industrials, Professional Services, Energy, etc.) */
  readonly sector?: Maybe<Scalars['String']>;
  /** The order of priority in which bondholders are repaid in the event of a default by the issuing corporation such as bankruptcy or liquidation. This field is only populated for corporate bonds. */
  readonly seniority?: Maybe<Scalars['String']>;
  /** The standard process through which the legal transfer of bonds from the seller to the buyer is completed, and payment is made by the buyer, including details like the timing and the specific procedures involved in the settlement of a trade. */
  readonly settlement_convention?: Maybe<Scalars['String']>;
  /** A sinking fund is a pool of money a bond issuer sets aside to repay the bond, typically through periodic buybacks, offering added security to bondholders. For bonds with a sinking fund provision, the bond issuer is obligated to retire or pay down a portion of the principal at specified intervals throughout the bond's lifetime. This means bondholders receive both periodic coupon payments and portions of the principal before maturity, rather than receiving the entire principal amount solely at maturity. */
  readonly sinking_fund?: Maybe<Scalars['Boolean']>;
  /**
   * S&P's CreditWatch highlights S&P's opinion regarding the potential direction of a short-term or long-term rating. Null if no information is provided by the agency. Note: available to S&P licensed users only.
   *
   * positive | negative | developing | not_meaningful
   */
  readonly sp_creditwatch?: Maybe<Scalars['String']>;
  /** The date of the most recent Standard & Poor's CreditWatch for the bond in YYYY-MM-DD format. This field will be null if no information about outlook is available or CreditWatch date not available. Note: available to S&P licensed users only. */
  readonly sp_creditwatch_date?: Maybe<Scalars['String']>;
  /**
   * A Standard & Poor's rating outlook indicates S&P's view regarding the potential direction of a long-term credit rating over the intermediate term (2 years for investment grade, 1 year for speculative grade). Null if no information is provided by the agency. Note: available to S&P licensed users only.
   *
   * positive | negative | developing | stable | not_rated | not_meaningful
   */
  readonly sp_outlook?: Maybe<Scalars['String']>;
  /** The date of the most recent Standard & Poor's outlook for the bond in YYYY-MM-DD format. Null if no information about outlook is available or outlook date not available. Note: available to S&P licensed users only. */
  readonly sp_outlook_date?: Maybe<Scalars['String']>;
  /** Standard & Poor's rating for the bond in the standard AAA - D format. NR if marked as not rated by the agency. Null if there is no rating (AAA - D or NR) provided by the agency. Note: available to S&P licensed users only. This data includes Standard & Poor's (S&P) ratings and related financial information. S&P ratings and data are proprietary to Standard & Poor's Financial Services LLC. The information is used under license and is subject to copyright and other intellectual property rights held by Standard & Poor's or its affiliates. */
  readonly sp_rating?: Maybe<Scalars['String']>;
  /** The date in the timezone of the issuing country of the most recent Standard & Poor's rating for the bond in YYYY-MM-DD format. Null if no rating is available or rating date not available. Note: available to S&P licensed users only. */
  readonly sp_rating_date?: Maybe<Scalars['String']>;
  /**
   * The status of the bond reflects many possible states a bond can be in.
   *
   * - called: The bond has been called by the issuer.
   * - converted: The bond has been converted into another type of security. Usually this is from the issue being converted into common stock.
   * - defaulted: The bond issuer has failed to make timely payments on the bond and has entered default.
   * - funged: The bond has been funged (replaced/exchanged) for another. This sometimes happens when an issuer combines two classes of bond into one parent bond.
   * - liquidated: The issuer has been liquidated.
   * - matured: The bond has reached its maturity date and the bondholders have been repaid their principal.
   * - outstanding: The bond is still active and has not reached its maturity date.
   * - pre_issuance: There is reference data for this bond but it has not yet been issued and is unavailable for trading.
   * - put: The bond has been put (sold back) to the issuer.
   * - repaid: The bond has been repaid before its maturity date using a covenant other than an option such as a put or call. For example, through prepayment.
   * - tendered: The bond has been tendered (offered for repurchase) by the issuer.
   * - repurchased: The bond has been repurchased by the issuer.
   * - restructured: The security has been restructured into a new security or into a security with materially different terms.
   * - unknown: The status of the bond is unknown.
   *
   * called | converted | defaulted | funged | liquidated | matured | outstanding | pre_issuance | put | repaid | tendered | repurchased | restructured | unknown
   */
  readonly status?: Maybe<Scalars['String']>;
  /** The stock ticker for publicly traded companies, or a similar styled abbreviation for securities issued by a company that is not publicly traded. This field is only populated for corporate bonds. */
  readonly ticker?: Maybe<Scalars['String']>;
  /**
   * The subtype of a treasury. Possible values include:
   * - bond: long term i.e. 20, 30 years
   * - note: intermediate term i.e. 2, 3, 5, 7, and 10 years
   * - bill: short term i.e. 4, 8, 13, 17, 26, 52 weeks (<= 1 year)
   * - tips: treasury inflation-protected securities
   * - strips: separate trading of registered interest and principal of securities
   * - floating: floating rate notes
   *
   * bond | bill | note | strips | tips | floating
   */
  readonly treasury_subtype?: Maybe<Scalars['String']>;
  /**
   * The type of fixed income security.
   *
   * agency | cd | corporate | government | municipal | treasury
   */
  readonly type?: Maybe<FixedIncomeInstrumentType>;
  /** The timestamp at which the bond was last updated in Moment's system in RFC-3339 format with nanosecond precision. */
  readonly updated_at?: Maybe<Scalars['String']>;
};

export type FixedIncomeInstrumentPage = {
  readonly __typename?: 'FixedIncomeInstrumentPage';
  readonly data: ReadonlyArray<FixedIncomeInstrument>;
  readonly page: Scalars['Int'];
  readonly size: Scalars['Int'];
  readonly total: Scalars['Int'];
};

export type FixedIncomeInstrumentPrice = {
  readonly __typename?: 'FixedIncomeInstrumentPrice';
  readonly price: Scalars['Float'];
  readonly timestamp: Scalars['String'];
  readonly yield_to_maturity?: Maybe<Scalars['Float']>;
  readonly yield_to_worst: Scalars['Float'];
};

export enum FixedIncomeInstrumentPriceHistoryFrequencies {
  fifteen_minutes = 'fifteen_minutes',
  one_day = 'one_day',
  one_minute = 'one_minute'
}

export enum FixedIncomeInstrumentType {
  agency = 'agency',
  cd = 'cd',
  corporate = 'corporate',
  government = 'government',
  municipal = 'municipal',
  treasury = 'treasury',
  unknown = 'unknown'
}

export type FixedIncomeInstrumentsSearchFiltersInput = {
  readonly YTW?: InputMaybe<NumberRangeFilter>;
  readonly coupon?: InputMaybe<NumberRangeFilter>;
  readonly instrumentType?: InputMaybe<FixedIncomeInstrumentType>;
  readonly maturityDate?: InputMaybe<DateRangeFilter>;
  readonly quantity?: InputMaybe<NumberRangeFilter>;
};

export type FixedIncomeInstrumentsSearchRequestInput = {
  readonly filters?: InputMaybe<FixedIncomeInstrumentsSearchFiltersInput>;
  readonly page: Scalars['Int'];
  readonly size: Scalars['Int'];
};

export type Flat = {
  readonly __typename?: 'Flat';
  readonly formula: FormulaFlat;
  readonly parameters: ParametersFlat;
};

export type FlatAllocation = {
  readonly __typename?: 'FlatAllocation';
  readonly amount?: Maybe<Scalars['Float']>;
  readonly effectiveWeight?: Maybe<Scalars['Float']>;
  readonly held?: Maybe<Scalars['Float']>;
  readonly symbol?: Maybe<Scalars['String']>;
  readonly weight?: Maybe<Scalars['Float']>;
};

export type FlattenedAllocations = {
  readonly __typename?: 'FlattenedAllocations';
  readonly allocations: ReadonlyArray<FlatAllocation>;
  readonly analysis?: Maybe<AccountAnalysisStatus>;
  readonly cash?: Maybe<CashAllocation>;
  readonly fundSubstitutes: ReadonlyArray<FundSubstitutes>;
};


export type FlattenedAllocationsfundSubstitutesArgs = {
  portfolioId: Scalars['ID'];
};

export type FlattenedPortfolioAllocations = {
  readonly __typename?: 'FlattenedPortfolioAllocations';
  readonly allocations: ReadonlyArray<SymbolAllocation>;
};

export type FopFile = {
  readonly __typename?: 'FopFile';
  readonly id?: Maybe<Scalars['String']>;
};

/** 'Form ADV' is the uniform form used by investment advisors to register with both the SEC and state securities authorities. */
export type FormADVUrlsInput = {
  /** Information about the investment advisor’s business, ownership, clients, employees, business practices, affiliations, and any disciplinary events of the advisor or its employees. */
  readonly formAdvPart1: Scalars['String'];
  /** Disclosures of the advisor’s business practices, fees, conflicts of interest, and disciplinary information. */
  readonly formAdvPart2A: Scalars['String'];
  /** Disclosures of the advisor’s business practices, fees, conflicts of interest, and disciplinary information. */
  readonly formAdvPart2B?: InputMaybe<Scalars['String']>;
  /** A 'relationship summary' required when advisors offer services to retail investors. */
  readonly formAdvPart3?: InputMaybe<Scalars['String']>;
};

export type FormFields = {
  readonly name: Scalars['String'];
  readonly value: Scalars['String'];
};

export type FormulaAum = {
  readonly __typename?: 'FormulaAum';
  readonly rateMethod: Scalars['String'];
};

export type FormulaFlat = {
  readonly __typename?: 'FormulaFlat';
  readonly fee?: Maybe<Scalars['Float']>;
};

export type FundSubstitute = {
  readonly __typename?: 'FundSubstitute';
  readonly sellOnly?: Maybe<Scalars['Boolean']>;
  readonly symbol: Scalars['String'];
};

export type FundSubstitutes = {
  readonly __typename?: 'FundSubstitutes';
  readonly equivalentSubstitutes: ReadonlyArray<FundSubstitute>;
  readonly symbol: Scalars['String'];
  readonly transientSubstitutes: ReadonlyArray<FundSubstitute>;
};

export type FundSubstitutesType = {
  readonly __typename?: 'FundSubstitutesType';
  readonly equivalentSubstitutes?: Maybe<ReadonlyArray<Maybe<ModelSubstituteOutput>>>;
  readonly transientSubstitutes?: Maybe<ReadonlyArray<Maybe<ModelSubstituteOutput>>>;
};

export type FundedDayInput = {
  readonly financialAccountIds?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['ID']>>>;
};

export type FundedDayOutput = {
  readonly __typename?: 'FundedDayOutput';
  readonly financialAccounts?: Maybe<ReadonlyArray<Maybe<AccountFunded>>>;
};

export type FundingAccount = {
  readonly __typename?: 'FundingAccount';
  readonly accountNumber: Scalars['String'];
  readonly accountType: FundingAccountType;
  readonly authenticationStatus: PlaidAuthenticationStatus;
  readonly created: Scalars['DateTime'];
  readonly fundingAccountId: Scalars['ID'];
  readonly institutionName: Scalars['String'];
  readonly isVirtual: Scalars['Boolean'];
  readonly name?: Maybe<Scalars['String']>;
  readonly nextDayUseOnly: Scalars['Boolean'];
  readonly verificationStatus?: Maybe<ApexVerificationStatus>;
};

export enum FundingAccountStatus {
  ACTIVE = 'ACTIVE',
  CANCELLED = 'CANCELLED',
  PENDING = 'PENDING'
}

export enum FundingAccountType {
  CHECKING = 'CHECKING',
  FUNDING = 'FUNDING',
  SAVINGS = 'SAVINGS'
}

export type FundingAccountsByBrokerage = {
  readonly __typename?: 'FundingAccountsByBrokerage';
  readonly accounts: ReadonlyArray<FundingAccount>;
  readonly brokerageAccountId: Scalars['ID'];
  readonly maximumAccountsPerUser: Scalars['Int'];
};

export enum FundingEnum {
  ACAT = 'ACAT',
  ACH = 'ACH',
  CHECK = 'CHECK',
  FOP = 'FOP',
  PLAID = 'PLAID',
  ROLLOVER = 'ROLLOVER',
  WIRE = 'WIRE'
}

export type FundingInput = {
  readonly acat?: InputMaybe<ACATInput>;
  readonly ach?: InputMaybe<ACHInput>;
  readonly check?: InputMaybe<CheckInput>;
  readonly fop?: InputMaybe<FOPInput>;
  readonly plaid?: InputMaybe<PlaidInput>;
  readonly rollover?: InputMaybe<RolloverInput>;
  readonly type?: InputMaybe<FundingEnum>;
  readonly wire?: InputMaybe<WireInput>;
};

export type FundingLinkType = {
  readonly __typename?: 'FundingLinkType';
  readonly brokerageAccountId: Scalars['ID'];
  readonly fundingAccountId: Scalars['ID'];
  readonly fundingAccountLinkId: Scalars['ID'];
  readonly verificationStatus: FundingLinkVerification;
};

export enum FundingLinkVerification {
  PENDING = 'PENDING',
  VERIFIED = 'VERIFIED'
}

export type FundingType = {
  readonly __typename?: 'FundingType';
  readonly acat?: Maybe<ACATType>;
  readonly ach?: Maybe<ACHType>;
  readonly check?: Maybe<CheckType>;
  readonly fop?: Maybe<FOPType>;
  readonly plaid?: Maybe<PlaidType>;
  readonly rollover?: Maybe<RolloverType>;
  readonly type?: Maybe<FundingEnum>;
  readonly wire?: Maybe<WireType>;
};

export type GenerateDocumentTemplateUrlInput = {
  readonly id: Scalars['String'];
  readonly signers: ReadonlyArray<Scalars['String']>;
  readonly signingOrderIsSet: Scalars['Boolean'];
};

export type GenerateSignUrlInput = {
  readonly documentId: Scalars['String'];
};

export type GetAccountsWithScheduleAssignmentOutput = {
  readonly __typename?: 'GetAccountsWithScheduleAssignmentOutput';
  readonly financialAccounts: ReadonlyArray<Maybe<FinAccountWithAssignmentId>>;
};

export type GetAdvisorAddressResponse = {
  readonly __typename?: 'GetAdvisorAddressResponse';
  readonly addresses?: Maybe<ReadonlyArray<Maybe<AdvisorAddress>>>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly profileId?: Maybe<Scalars['ID']>;
};

export type GetClearingRepCodesInput = {
  readonly name?: InputMaybe<Scalars['String']>;
  readonly status?: InputMaybe<Scalars['String']>;
};

export type GetContactAnalyticsOutput = {
  readonly __typename?: 'GetContactAnalyticsOutput';
  readonly allContactsIds?: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly number_contacts?: Maybe<Scalars['Int']>;
  readonly totalRegistered?: Maybe<Scalars['Int']>;
  readonly totalRegisteredPercentage?: Maybe<Scalars['Float']>;
};

export type GetExcludeDaysBeforeFundingOutput = {
  readonly __typename?: 'GetExcludeDaysBeforeFundingOutput';
  readonly excludeDaysBeforeFunding: Scalars['Boolean'];
};

export type GetFeeAllocationInput = {
  readonly householdId: Scalars['ID'];
};

export type GetFeeInvoicePeriodByIdOutput = {
  readonly __typename?: 'GetFeeInvoicePeriodByIdOutput';
  readonly endDate?: Maybe<Scalars['DateOnly']>;
  readonly exportedLines?: Maybe<Scalars['Int']>;
  readonly id: Scalars['ID'];
  readonly institutions: ReadonlyArray<Maybe<Scalars['String']>>;
  readonly invoices: ReadonlyArray<Maybe<FeeInvoicePeriodInvoice>>;
  readonly lastCreated?: Maybe<Scalars['DateTime']>;
  readonly lastExported?: Maybe<Scalars['String']>;
  readonly startDate?: Maybe<Scalars['DateOnly']>;
  readonly total: Scalars['Float'];
  readonly unexecutedCount?: Maybe<Scalars['Int']>;
};

export type GetFinancialAccountGroupPairsOutput = {
  readonly __typename?: 'GetFinancialAccountGroupPairsOutput';
  readonly financialAccountId?: Maybe<Scalars['String']>;
  readonly groupId?: Maybe<Scalars['String']>;
};

export type GetHouseholdOutput = {
  readonly __typename?: 'GetHouseholdOutput';
  readonly id?: Maybe<Scalars['String']>;
  readonly name?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
};

export type GetIntegrationSourceSeriesResponse = {
  readonly __typename?: 'GetIntegrationSourceSeriesResponse';
  readonly portfolioStrategyUrl?: Maybe<Scalars['String']>;
  readonly series?: Maybe<ReadonlyArray<Maybe<IntegrationSourceSeries>>>;
};

export type GetPortfoliosBySeriesIDResponse = {
  readonly __typename?: 'GetPortfoliosBySeriesIDResponse';
  readonly factsheetUrl?: Maybe<Scalars['String']>;
  readonly portfolios?: Maybe<ReadonlyArray<Maybe<SeriesPortfolios>>>;
};

export type GetUserInGroupDetailsOutput = {
  readonly __typename?: 'GetUserInGroupDetailsOutput';
  readonly address: AdvisorAddress;
  readonly profile: UserInfoOutputQuery;
};

export enum Goals {
  DEBT = 'DEBT',
  EDUCATION = 'EDUCATION',
  FAMILY = 'FAMILY',
  HOME = 'HOME',
  OTHER = 'OTHER',
  PURCHASE = 'PURCHASE',
  RETIREMENT = 'RETIREMENT'
}

export type GoodFaithViolations = {
  readonly __typename?: 'GoodFaithViolations';
  readonly count: Scalars['Int'];
};

export type Group = {
  readonly __typename?: 'Group';
  readonly accountSummary?: Maybe<AccountSummary>;
  /** @deprecated Unnecessary */
  readonly groupCategory?: Maybe<Scalars['String']>;
  readonly groupDetails: GroupDetail;
  readonly groupId?: Maybe<Scalars['ID']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly name?: Maybe<Scalars['String']>;
  readonly usersInGroup: ReadonlyArray<Maybe<AllUsersInGroupOutput>>;
};

export type GroupAssignment = {
  readonly __typename?: 'GroupAssignment';
  readonly assignment: ReadonlyArray<Maybe<AssignmentOutput>>;
  readonly groupId: Scalars['ID'];
};

export type GroupDetail = {
  readonly __typename?: 'GroupDetail';
  readonly financialAccounts: FinancialAccounts;
  readonly name: Scalars['String'];
  readonly users?: Maybe<ReadonlyArray<User>>;
};

export type GroupFeeAllocations = {
  readonly __typename?: 'GroupFeeAllocations';
  readonly accountAllocations: ReadonlyArray<AccountAllocation>;
  readonly allocationType: AllocationType;
  readonly debitAccountOverrides: ReadonlyArray<DebitAccountOverride>;
  readonly financialAccounts?: Maybe<ReadonlyArray<FinancialAccount>>;
  readonly heldAwayAccountAllocations: ReadonlyArray<AccountAllocation>;
};

export type GroupInvoice = {
  readonly __typename?: 'GroupInvoice';
  readonly endDate: Scalars['String'];
  readonly groupName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly invoiceAmount: Scalars['Float'];
  readonly invoiceDate?: Maybe<Scalars['String']>;
  readonly invoiceNumber: Scalars['String'];
  readonly periodId: Scalars['ID'];
  readonly scheduleType: ScheduleType;
  readonly startDate: Scalars['String'];
  readonly status?: Maybe<Scalars['String']>;
};

export type GroupScheduleAccount = {
  readonly __typename?: 'GroupScheduleAccount';
  readonly financialAccountId: Scalars['ID'];
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
  readonly scheduleId?: Maybe<Scalars['ID']>;
};

export type GroupScheduleAssignment = ScheduleAssignment & {
  readonly __typename?: 'GroupScheduleAssignment';
  readonly endDate?: Maybe<Scalars['String']>;
  readonly groupId: Scalars['String'];
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
  readonly scheduleId?: Maybe<Scalars['ID']>;
  readonly startDate?: Maybe<Scalars['DateOnly']>;
};

export type GroupScheduleOutput = {
  readonly __typename?: 'GroupScheduleOutput';
  readonly assignmentLevel?: Maybe<AssignmentLevel>;
  readonly groupId: Scalars['ID'];
  readonly scheduleList?: Maybe<ReadonlyArray<Maybe<ScheduleList>>>;
};

export type GroupSummary = {
  readonly __typename?: 'GroupSummary';
  readonly averageBalance: Scalars['Float'];
  readonly groupSize: Scalars['Int'];
  readonly totalBalance: Scalars['Float'];
};

export type GroupsWithSchedules = {
  readonly __typename?: 'GroupsWithSchedules';
  readonly accounts?: Maybe<ReadonlyArray<BasicAccountAssignment>>;
  readonly groupAccountBalance?: Maybe<Scalars['Float']>;
  readonly groupId?: Maybe<Scalars['ID']>;
  readonly groupName?: Maybe<Scalars['String']>;
  readonly level?: Maybe<AssignmentLevel>;
  readonly numberOfAccounts?: Maybe<Scalars['Int']>;
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
  readonly scheduleFrequency?: Maybe<AllowedFrequency>;
  readonly scheduleName?: Maybe<Scalars['String']>;
  readonly scheduleTiming?: Maybe<AllowedBillTiming>;
  readonly scheduleType?: Maybe<Scalars['String']>;
  readonly startDate?: Maybe<Scalars['String']>;
};

export type HandleProcessingActionInput = {
  readonly action: DocumentProcessingAction;
  readonly externalDocumentId?: InputMaybe<Scalars['String']>;
  readonly externalSignatureId?: InputMaybe<Scalars['String']>;
  readonly id: Scalars['String'];
  readonly signRequestId?: InputMaybe<Scalars['String']>;
};

export type HeldAwayAccount = {
  readonly __typename?: 'HeldAwayAccount';
  readonly balance?: Maybe<Scalars['Float']>;
  readonly id: Scalars['ID'];
  readonly link?: Maybe<HeldAwayLink>;
  readonly name?: Maybe<Scalars['String']>;
  readonly number?: Maybe<Scalars['String']>;
  readonly owner?: Maybe<Client>;
  readonly status?: Maybe<HeldAwayAccountStatus>;
  readonly subtype?: Maybe<Scalars['String']>;
  readonly type?: Maybe<Scalars['String']>;
  readonly typeEnum?: Maybe<HeldAwayAccountType>;
  readonly updatedDate?: Maybe<Scalars['DateTime']>;
};

export type HeldAwayAccountInput = {
  readonly externalId: Scalars['String'];
  readonly name: Scalars['String'];
  readonly number?: InputMaybe<Scalars['String']>;
  readonly subtype?: InputMaybe<Scalars['String']>;
  readonly type: Scalars['String'];
};

export enum HeldAwayAccountStatus {
  ACTIVE = 'ACTIVE',
  ERROR = 'ERROR',
  EXPIRED = 'EXPIRED',
  INACTIVE = 'INACTIVE',
  INITIATED = 'INITIATED',
  LINKED = 'LINKED',
  LINK_IN_PROGRESS = 'LINK_IN_PROGRESS',
  UNLINKED = 'UNLINKED',
  UNLINK_IN_PROGRESS = 'UNLINK_IN_PROGRESS'
}

export enum HeldAwayAccountType {
  DEPOSITORY = 'DEPOSITORY',
  INVESTMENT = 'INVESTMENT',
  OTHER = 'OTHER'
}

export type HeldAwayInstitutionInput = {
  readonly externalId: Scalars['String'];
  readonly name: Scalars['String'];
};

export type HeldAwayLink = {
  readonly __typename?: 'HeldAwayLink';
  readonly accounts?: Maybe<ReadonlyArray<HeldAwayAccount>>;
  readonly id: Scalars['ID'];
  readonly institutionName?: Maybe<Scalars['String']>;
  readonly status?: Maybe<HeldAwayAccountStatus>;
};

export type HeldAwayLinksResponse = {
  readonly __typename?: 'HeldAwayLinksResponse';
  readonly links?: Maybe<ReadonlyArray<HeldAwayLink>>;
};

export type HeldAwayMutationResponse = {
  readonly __typename?: 'HeldAwayMutationResponse';
  readonly accounts?: Maybe<ReadonlyArray<HeldAwayAccount>>;
  readonly link?: Maybe<HeldAwayLink>;
};

export type HeldAwayPlaidRelinkTokenInput = {
  readonly heldAwayLinkId: Scalars['String'];
};

export type HeldAwayPlaidTokenResponse = {
  readonly __typename?: 'HeldAwayPlaidTokenResponse';
  readonly plaidToken: Scalars['String'];
};

export type HoldingPerformance = {
  readonly __typename?: 'HoldingPerformance';
  /** decimal format, so 0.13 => 13% */
  readonly accountReturnContribution?: Maybe<Scalars['Float']>;
  /** decimal format, so 0.027 => 2.7% */
  readonly cumulativeTimeWeightedReturn?: Maybe<Scalars['Float']>;
  readonly cusip?: Maybe<Scalars['String']>;
  readonly description?: Maybe<Scalars['String']>;
  readonly dividendsInterest?: Maybe<Scalars['Float']>;
  readonly endingValue?: Maybe<Scalars['Float']>;
  readonly gainLoss?: Maybe<Scalars['Float']>;
  /** decimal format, so 0.07 => 7% */
  readonly householdReturnContribution?: Maybe<Scalars['Float']>;
  readonly ticker?: Maybe<Scalars['String']>;
  readonly weight?: Maybe<Scalars['Float']>;
};

export enum HoldingType {
  ETF_OR_EQUITIES = 'ETF_OR_EQUITIES',
  MUTUAL_FUNDS = 'MUTUAL_FUNDS'
}

export type HoldingsPerformanceDateRange = {
  readonly __typename?: 'HoldingsPerformanceDateRange';
  readonly endDate?: Maybe<Scalars['DateOnly']>;
  readonly startDate?: Maybe<Scalars['DateOnly']>;
};

export type HoldingsPerformanceDateRangeInput = {
  readonly dateRangeType: PerformanceAnalyticsDateRangeType;
  readonly endDate: Scalars['DateOnly'];
  readonly startDate: Scalars['DateOnly'];
};

export type HoldingsPerformanceInput = {
  readonly dateRange: HoldingsPerformanceDateRangeInput;
  readonly financialEntityId: Scalars['ID'];
  readonly financialEntityType: PerformanceAnalyticsResourceType;
  readonly includeHoldingsPerformanceByAccount?: InputMaybe<Scalars['Boolean']>;
};

export type HoldingsPerformanceResponse = {
  readonly __typename?: 'HoldingsPerformanceResponse';
  readonly accountsHoldingsPerformance?: Maybe<ReadonlyArray<Maybe<FinancialEntityHoldingsPerformance>>>;
  readonly dateRange?: Maybe<HoldingsPerformanceDateRange>;
  readonly financialEntityHoldingsPerformance?: Maybe<FinancialEntityHoldingsPerformance>;
  readonly responseCode?: Maybe<HoldingsPerformanceResponseCode>;
};

export type HoldingsPerformanceResponseCode = {
  readonly __typename?: 'HoldingsPerformanceResponseCode';
  readonly code?: Maybe<Scalars['String']>;
  readonly description?: Maybe<Scalars['String']>;
};

export type HouseAccountCreationResponse = {
  readonly __typename?: 'HouseAccountCreationResponse';
  readonly financialAccountId: Scalars['ID'];
};

export type HouseAccountResponse = {
  readonly __typename?: 'HouseAccountResponse';
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly accountType?: Maybe<Scalars['String']>;
  readonly financialAccountId?: Maybe<Scalars['ID']>;
  readonly status?: Maybe<AccountCreationStatus>;
};

export type Household = {
  readonly __typename?: 'Household';
  readonly assignmentLevel?: Maybe<AssignmentLevel>;
  readonly balance?: Maybe<Scalars['Float']>;
  readonly id: Scalars['ID'];
  readonly name?: Maybe<Scalars['String']>;
  readonly numberOfAccounts?: Maybe<Scalars['Int']>;
  readonly scheduleAssignments: ReadonlyArray<ScheduleAssignment2>;
};

export type HubspotContact = {
  readonly __typename?: 'HubspotContact';
  readonly company?: Maybe<Scalars['String']>;
  readonly firstname?: Maybe<Scalars['String']>;
  readonly lastname?: Maybe<Scalars['String']>;
};

export type HubspotFields = {
  readonly applied_brokerage?: InputMaybe<Scalars['Boolean']>;
  readonly brokerage_application_accepted?: InputMaybe<Scalars['Boolean']>;
  readonly brokerage_application_submitted?: InputMaybe<Scalars['Boolean']>;
  readonly company?: InputMaybe<Scalars['String']>;
  readonly crd_number?: InputMaybe<Scalars['String']>;
  readonly created_a_login?: InputMaybe<Scalars['Boolean']>;
  readonly email?: InputMaybe<Scalars['String']>;
  readonly firm_crd_number?: InputMaybe<Scalars['String']>;
  readonly firstname?: InputMaybe<Scalars['String']>;
  readonly lastname?: InputMaybe<Scalars['String']>;
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly signed_up?: InputMaybe<Scalars['Boolean']>;
  readonly td_connection_email_list?: InputMaybe<Scalars['Boolean']>;
  readonly verified_email?: InputMaybe<Scalars['Boolean']>;
};

export type HubspotUser = {
  readonly __typename?: 'HubspotUser';
  readonly companyName?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
};

export type HypotheticalPerformanceInput = {
  readonly portfolioIds: ReadonlyArray<Scalars['ID']>;
};

export type HypotheticalPerformanceResponse = {
  readonly __typename?: 'HypotheticalPerformanceResponse';
  readonly metadata?: Maybe<HypotheticalPerfromanceResponseCode>;
  readonly modelPortfolioAnalytics?: Maybe<ReadonlyArray<Maybe<HypotheticalPerformanceResponseItem>>>;
};

export type HypotheticalPerformanceResponseCodeContent = {
  readonly __typename?: 'HypotheticalPerformanceResponseCodeContent';
  readonly code?: Maybe<Scalars['String']>;
  readonly description?: Maybe<Scalars['String']>;
};

export type HypotheticalPerformanceResponseItem = {
  readonly __typename?: 'HypotheticalPerformanceResponseItem';
  readonly annualizedStandardDeviation?: Maybe<Scalars['Float']>;
  readonly equityWeight?: Maybe<Scalars['Float']>;
  readonly fiveYearsAnnualizedReturn?: Maybe<Scalars['Float']>;
  readonly fiveYearsCumulativeReturn?: Maybe<Scalars['Float']>;
  readonly inceptionAnnualizedReturn?: Maybe<Scalars['Float']>;
  readonly inceptionCumulativeReturn?: Maybe<Scalars['Float']>;
  readonly maxDrawdown?: Maybe<Scalars['Float']>;
  readonly netExpenseRatio?: Maybe<Scalars['Float']>;
  readonly oneYearAnnualizedReturn?: Maybe<Scalars['Float']>;
  readonly oneYearCumulativeReturn?: Maybe<Scalars['Float']>;
  readonly oneYearDividendYield?: Maybe<Scalars['Float']>;
  readonly portfolioId?: Maybe<Scalars['ID']>;
  readonly portfolioUuid?: Maybe<Scalars['ID']>;
  readonly returnEndDate?: Maybe<Scalars['DateOnly']>;
  readonly returnStartDate?: Maybe<Scalars['DateOnly']>;
  readonly tenYearsAnnualizedReturn?: Maybe<Scalars['Float']>;
  readonly tenYearsCumulativeReturn?: Maybe<Scalars['Float']>;
  readonly twentyYearsAnnualizedReturn?: Maybe<Scalars['Float']>;
  readonly twentyYearsCumulativeReturn?: Maybe<Scalars['Float']>;
  readonly ytdCumulativeReturn?: Maybe<Scalars['Float']>;
};

export type HypotheticalPerfromanceResponseCode = {
  readonly __typename?: 'HypotheticalPerfromanceResponseCode';
  readonly responseCode?: Maybe<HypotheticalPerformanceResponseCodeContent>;
};

export type IarName = {
  readonly __typename?: 'IarName';
  readonly firstName?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
};

export type IarNamesInput = {
  readonly firstName?: InputMaybe<Scalars['String']>;
  readonly lastName?: InputMaybe<Scalars['String']>;
};

export type IarNamesType = {
  readonly __typename?: 'IarNamesType';
  readonly firstName?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
};

export type IdealClientsOutput = {
  readonly __typename?: 'IdealClientsOutput';
  readonly details: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
};

export enum IdentificationStatus {
  COMPLETE = 'COMPLETE',
  INCOMPLETE = 'INCOMPLETE',
  UNKNOWN = 'UNKNOWN'
}

export type IdentityVerificationQuestion = {
  readonly __typename?: 'IdentityVerificationQuestion';
  readonly confirmationHint?: Maybe<Scalars['String']>;
  readonly detail?: Maybe<Scalars['String']>;
  readonly hint?: Maybe<Scalars['String']>;
  readonly id: Scalars['String'];
  readonly prompt?: Maybe<Scalars['String']>;
  readonly refreshable?: Maybe<Scalars['Boolean']>;
  readonly type?: Maybe<Scalars['String']>;
  readonly user: IdentityVerificationUser;
};

export type IdentityVerificationResponse = {
  readonly __typename?: 'IdentityVerificationResponse';
  readonly accepted?: Maybe<Scalars['Boolean']>;
  readonly attemptsRemaining?: Maybe<Scalars['Int']>;
  readonly message?: Maybe<Scalars['String']>;
};

export type IdentityVerificationResponseInput = {
  readonly questionId: Scalars['String'];
  readonly response: Scalars['String'];
};

export type IdentityVerificationUser = {
  readonly __typename?: 'IdentityVerificationUser';
  readonly avatarURL?: Maybe<Scalars['String']>;
  readonly firstName: Scalars['String'];
  readonly lastName: Scalars['String'];
};

export type Image = {
  readonly __typename?: 'Image';
  readonly contentType?: Maybe<Scalars['String']>;
  readonly fileName?: Maybe<Scalars['String']>;
  readonly fileSize?: Maybe<Scalars['Int']>;
  readonly originalImageRenderUrl?: Maybe<Scalars['String']>;
  readonly s3Bucket?: Maybe<Scalars['String']>;
  readonly s3Key?: Maybe<Scalars['String']>;
};

export type ImageMultiPart = {
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly isInSettings?: InputMaybe<Scalars['Boolean']>;
  readonly type?: InputMaybe<Scalars['String']>;
};

export type ImageMultiPartOutput = {
  readonly __typename?: 'ImageMultiPartOutput';
  readonly id?: Maybe<Scalars['String']>;
  readonly url?: Maybe<Scalars['String']>;
};

export type ImpersonateInput = {
  readonly clientOrAdvisorEmail: Scalars['String'];
  readonly expirationTime?: InputMaybe<Scalars['Int']>;
};

export type IndividualHouseAccountFinraAffiliationInput = {
  readonly firmName?: InputMaybe<Scalars['String']>;
};

export enum Industries {
  AGRICULTURE = 'AGRICULTURE',
  CONSTRUCTION = 'CONSTRUCTION',
  EDUCATION = 'EDUCATION',
  ENERGY = 'ENERGY',
  ENTERTAINMENT = 'ENTERTAINMENT',
  FOOD_AND_BEVERAGE = 'FOOD_AND_BEVERAGE',
  GOVERNMENT = 'GOVERNMENT',
  HEALTHCARE = 'HEALTHCARE',
  HOSPITALITY = 'HOSPITALITY',
  MANUFACTURING = 'MANUFACTURING',
  NON_PROFIT = 'NON_PROFIT',
  NOT_EMPLOYED = 'NOT_EMPLOYED',
  OTHER = 'OTHER',
  REAL_ESTATE = 'REAL_ESTATE',
  RETAIL = 'RETAIL',
  RETIRED = 'RETIRED',
  SELF_EMPLOYED = 'SELF_EMPLOYED',
  STUDENT = 'STUDENT',
  TECHNOLOGY = 'TECHNOLOGY',
  TRANSPORTATION = 'TRANSPORTATION'
}

export type InitiateOrgPartnerIntegrationInput = {
  readonly email?: InputMaybe<Scalars['String']>;
  readonly integrationPartnerCode: IntegrationPartnerCode;
  readonly integrationType: IntegrationDirection;
  readonly metadata?: InputMaybe<ReadonlyArray<InitiateOrgPartnerIntegrationInputMetadata>>;
};

export type InitiateOrgPartnerIntegrationInputMetadata = {
  readonly key: Scalars['String'];
  readonly value: Scalars['String'];
};

export enum Institution {
  ALTRUIST = 'ALTRUIST',
  AMERITRADE = 'AMERITRADE',
  FIDELITY = 'FIDELITY',
  OTHER = 'OTHER',
  PERSHING = 'PERSHING',
  SCHWAB = 'SCHWAB'
}

export type InstitutionsListOutput = {
  readonly __typename?: 'InstitutionsListOutput';
  readonly name: Scalars['String'];
  readonly number: Scalars['ID'];
};

export type Integration = {
  readonly __typename?: 'Integration';
  readonly code: IntegrationPartnerCode;
  readonly id: Scalars['String'];
  readonly integrationDetails?: Maybe<IntegrationDetails>;
  readonly name: Scalars['String'];
  readonly status: IntegrationStatus;
};

export enum IntegrationAction {
  CONNECT = 'CONNECT',
  DEACTIVATE = 'DEACTIVATE',
  REFRESH = 'REFRESH'
}

export type IntegrationDetails = Emoney | RedTail | RightCapital | Wealthbox;

export enum IntegrationDirection {
  INBOUND = 'INBOUND',
  OUTBOUND = 'OUTBOUND'
}

export enum IntegrationPartnerCode {
  byallaccounts = 'byallaccounts',
  emoney = 'emoney',
  rcl = 'rcl',
  redtail = 'redtail',
  rightcapital = 'rightcapital',
  wbx = 'wbx'
}

export type IntegrationSource = {
  readonly __typename?: 'IntegrationSource';
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
  readonly repCodes: ReadonlyArray<Maybe<IntegrationSourceRepCodes>>;
  readonly status: Scalars['String'];
  readonly templateId?: Maybe<Scalars['ID']>;
};

export type IntegrationSourceRepCodes = {
  readonly __typename?: 'IntegrationSourceRepCodes';
  readonly externalRef: Scalars['String'];
  readonly status: Scalars['String'];
};

export type IntegrationSourceSeries = {
  readonly __typename?: 'IntegrationSourceSeries';
  readonly description?: Maybe<Scalars['String']>;
  readonly factsheetUrl?: Maybe<Scalars['String']>;
  /** @deprecated Should use feePerYear instead */
  readonly fee?: Maybe<Scalars['Float']>;
  readonly feePerYear?: Maybe<Scalars['Float']>;
  readonly feeType?: Maybe<FeeType>;
  readonly id: Scalars['ID'];
  readonly name?: Maybe<Scalars['String']>;
  readonly portfolioManagerId: Scalars['ID'];
};

export enum IntegrationStatus {
  active = 'active',
  expired = 'expired',
  inactive = 'inactive'
}

export type IntegrationStatusLegacy = {
  readonly __typename?: 'IntegrationStatusLegacy';
  readonly status: Scalars['String'];
};

export enum IntegrationType {
  CUSTODIAN = 'CUSTODIAN',
  PORTFOLIO = 'PORTFOLIO',
  THIRD_PARTY = 'THIRD_PARTY'
}

export type IntegrationsInputFilter = {
  readonly partners?: InputMaybe<ReadonlyArray<IntegrationPartnerCode>>;
  readonly status?: InputMaybe<ReadonlyArray<IntegrationStatus>>;
  readonly type?: InputMaybe<ReadonlyArray<IntegrationType>>;
};

export type InternalCashTransferAccounts = {
  readonly __typename?: 'InternalCashTransferAccounts';
  readonly accountCashSummary?: Maybe<Summary>;
  readonly accountName?: Maybe<AccountName>;
  readonly activeStatus?: Maybe<Scalars['String']>;
  readonly brokerageAccountLinkedUuid?: Maybe<Scalars['String']>;
  readonly brokerageAccountUuid?: Maybe<Scalars['String']>;
  readonly createdBy?: Maybe<Scalars['String']>;
  readonly createdDate?: Maybe<Scalars['String']>;
  readonly fromLinkIsValidForRules?: Maybe<Scalars['Boolean']>;
  readonly journalAccountLinkUuid?: Maybe<Scalars['String']>;
  readonly toLinkIsValidForRules?: Maybe<Scalars['Boolean']>;
  readonly updatedBy?: Maybe<Scalars['String']>;
  readonly updatedDate?: Maybe<Scalars['String']>;
  readonly validForOwnership?: Maybe<Scalars['Boolean']>;
  readonly validForRules?: Maybe<Scalars['Boolean']>;
};

export type InvestmentSummaryParams = {
  readonly email: Scalars['String'];
  readonly householdIds: ReadonlyArray<Scalars['ID']>;
  readonly lastDay?: InputMaybe<Scalars['String']>;
  readonly reportType: Scalars['String'];
  readonly showBenchmarks?: InputMaybe<Scalars['Boolean']>;
  readonly startDay?: InputMaybe<Scalars['String']>;
};

export type InviteClientInput = {
  readonly clientUserId: Scalars['ID'];
  readonly email: Scalars['String'];
  readonly invitedFor: InvitedFor;
  readonly personalNote?: InputMaybe<Scalars['String']>;
};

export type InviteInput = {
  readonly bypassProxy?: InputMaybe<Scalars['Boolean']>;
  readonly email: Scalars['String'];
  readonly firstName?: InputMaybe<Scalars['String']>;
  readonly invitedFor: InvitedFor;
  readonly lastName?: InputMaybe<Scalars['String']>;
  readonly personalNote?: InputMaybe<Scalars['String']>;
  readonly role: AllowedRole;
  readonly userId: Scalars['ID'];
};

export type InviteOutput = {
  readonly __typename?: 'InviteOutput';
  readonly code: Scalars['ID'];
  readonly user?: Maybe<User2>;
};

export type InviteResendInput = {
  readonly advisorName?: InputMaybe<Scalars['String']>;
  readonly email?: InputMaybe<Scalars['String']>;
  readonly firstName?: InputMaybe<Scalars['String']>;
  readonly invitedFor?: InputMaybe<Scalars['String']>;
  readonly lastName?: InputMaybe<Scalars['String']>;
  readonly role?: InputMaybe<UserRole>;
  readonly userId: Scalars['ID'];
};

export type InviteStatus = {
  readonly __typename?: 'InviteStatus';
  readonly expiredTimestamp?: Maybe<Scalars['String']>;
  readonly status?: Maybe<AllowedInviteStatus>;
};

export type InviteStatusOutput = {
  readonly __typename?: 'InviteStatusOutput';
  readonly expiredTimestamp?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
  readonly userId?: Maybe<Scalars['ID']>;
};

export enum InvitedFor {
  BROKERAGE = 'BROKERAGE',
  PAS = 'PAS'
}

export type InvitedTeamMember = InvitedTeamMemberPermission & TeamMemberPermission & {
  readonly __typename?: 'InvitedTeamMember';
  readonly canBeAssignedRepCodes: Scalars['Boolean'];
  readonly canBeDeleted: Scalars['Boolean'];
  readonly canBeReinvited: Scalars['Boolean'];
  readonly canBeUpdated: Scalars['Boolean'];
  readonly email: Scalars['String'];
  readonly expiration: Scalars['DateTime'];
  readonly firstName: Scalars['String'];
  readonly fullName: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly lastName: Scalars['String'];
  readonly role: UserRole;
  readonly status: TeamMemberStatus;
  readonly updated: Scalars['DateTime'];
};

export type InvitedTeamMemberPermission = {
  readonly canBeReinvited: Scalars['Boolean'];
};

export type InvitedUserSettings = {
  readonly __typename?: 'InvitedUserSettings';
  readonly email?: Maybe<Scalars['String']>;
  readonly expiration?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly role?: Maybe<UserRole>;
  readonly status?: Maybe<Scalars['String']>;
  readonly updated?: Maybe<Scalars['String']>;
  readonly userId?: Maybe<Scalars['String']>;
};

export type Invoice = {
  readonly __typename?: 'Invoice';
  readonly accountDetails: ReadonlyArray<FinAccountDetails>;
  readonly billingEntityType: Scalars['String'];
  readonly billingInformation: BillingInfo;
  readonly canDelete?: Maybe<Scalars['String']>;
  readonly groupId: Scalars['ID'];
  /** @deprecated Use accountDetails instead */
  readonly heldAwayAccountDetails: ReadonlyArray<LinkedFinAccountDetails>;
  readonly id: Scalars['ID'];
  readonly invoiceNumber: Scalars['String'];
  readonly minimumFeeApplied?: Maybe<Scalars['Boolean']>;
  readonly organizationId: Scalars['ID'];
  readonly total: Scalars['Float'];
  readonly view: Scalars['String'];
};

export enum InvoiceAccountClassification {
  LINKED = 'LINKED',
  MANAGED = 'MANAGED'
}

export enum InvoiceBundleUrlDocumentType {
  INVOICE = 'INVOICE'
}

export type InvoiceBundleUrlInput = {
  readonly documentType: InvoiceBundleUrlDocumentType;
  readonly targetId: Scalars['String'];
  readonly targetType: InvoiceBundleUrlTargetType;
};

export type InvoiceBundleUrlResponse = {
  readonly __typename?: 'InvoiceBundleUrlResponse';
  readonly documentId?: Maybe<Scalars['String']>;
  readonly documentType?: Maybe<InvoiceBundleUrlDocumentType>;
  readonly url?: Maybe<Scalars['String']>;
};

export enum InvoiceBundleUrlTargetType {
  HOUSEHOLD = 'HOUSEHOLD',
  INVOICE = 'INVOICE',
  ORGANIZATION = 'ORGANIZATION',
  PERIOD = 'PERIOD'
}

export type InvoiceError = {
  readonly __typename?: 'InvoiceError';
  readonly account?: Maybe<FinancialAccountForHeldAway>;
  readonly code?: Maybe<Scalars['String']>;
  readonly description?: Maybe<Scalars['String']>;
};

export type InvoiceRunnerResponse = {
  readonly __typename?: 'InvoiceRunnerResponse';
  readonly TD: InvoiceUrl;
  readonly brokerage: Brokerage;
  readonly fidelity?: Maybe<InvoiceUrl>;
  readonly pershing?: Maybe<InvoiceUrl>;
  readonly schwab?: Maybe<InvoiceUrl>;
};

export enum InvoiceStatus {
  PAID = 'PAID',
  PENDING = 'PENDING'
}

export type InvoiceUrl = {
  readonly __typename?: 'InvoiceUrl';
  readonly csvUrl?: Maybe<Scalars['String']>;
  readonly hasInvoices: Scalars['Boolean'];
};

export type InvoicesByGroupID = {
  readonly __typename?: 'InvoicesByGroupID';
  readonly groupId: Scalars['ID'];
  readonly invoices: ReadonlyArray<Maybe<GroupInvoice>>;
};

export type IraConstraintReason = {
  readonly displayEntry?: InputMaybe<Scalars['String']>;
  readonly displayReason?: InputMaybe<Scalars['String']>;
  readonly entryType?: InputMaybe<Scalars['String']>;
  readonly irsCode?: InputMaybe<Scalars['String']>;
  readonly irsCodeDescription?: InputMaybe<Scalars['String']>;
  readonly optionalCode?: InputMaybe<Scalars['String']>;
  readonly optionalCodeDescription?: InputMaybe<Scalars['String']>;
  readonly reasonType?: InputMaybe<Scalars['String']>;
};

export type IraConstraints = {
  readonly __typename?: 'IraConstraints';
  readonly contributionConstraints?: Maybe<ContributionConstraints>;
  readonly distributionConstraints?: Maybe<DistributionConstraints>;
  readonly isRetirementAccount?: Maybe<Scalars['Boolean']>;
};

export type IraContribution = {
  readonly __typename?: 'IraContribution';
  readonly accountName?: Maybe<Scalars['String']>;
  readonly birthDate?: Maybe<Scalars['DateOnly']>;
  readonly contributions?: Maybe<Scalars['Float']>;
  readonly distributions?: Maybe<Scalars['Float']>;
  readonly netContribution?: Maybe<Scalars['Float']>;
  readonly taxYear?: Maybe<Scalars['Int']>;
};

export enum IraContributionType {
  CURRENT_YEAR = 'CURRENT_YEAR',
  PRIOR_YEAR = 'PRIOR_YEAR'
}

export type IraDistribution = {
  readonly federalTaxWithholding?: InputMaybe<TaxWithholdingInput>;
  readonly reason: IraDistributionReason;
  readonly reasonEntryType?: InputMaybe<Scalars['String']>;
  readonly receivingInstitutionName?: InputMaybe<Scalars['String']>;
  readonly stateTaxWithholding?: InputMaybe<TaxWithholdingInput>;
};

export enum IraDistributionReason {
  CONVERSION = 'CONVERSION',
  DEATH = 'DEATH',
  DISABILITY = 'DISABILITY',
  EXCESS_CONTRIBUTION_REMOVAL_AFTER_TAX_DEADLINE = 'EXCESS_CONTRIBUTION_REMOVAL_AFTER_TAX_DEADLINE',
  EXCESS_CONTRIBUTION_REMOVAL_BEFORE_TAX_DEADLINE = 'EXCESS_CONTRIBUTION_REMOVAL_BEFORE_TAX_DEADLINE',
  MANAGEMENT_FEE = 'MANAGEMENT_FEE',
  NORMAL = 'NORMAL',
  NORMAL_ROTH_IRA_GREATER_THAN_5_YEARS = 'NORMAL_ROTH_IRA_GREATER_THAN_5_YEARS',
  PREMATURE = 'PREMATURE',
  PREMATURE_SIMPLE_IRA_LESS_THAN_2_YEARS = 'PREMATURE_SIMPLE_IRA_LESS_THAN_2_YEARS',
  RECHARACTERIZATION_CURRENT_YEAR = 'RECHARACTERIZATION_CURRENT_YEAR',
  RECHARACTERIZATION_PRIOR_YEAR = 'RECHARACTERIZATION_PRIOR_YEAR',
  ROLLOVER_TO_IRA = 'ROLLOVER_TO_IRA',
  ROLLOVER_TO_QUALIFIED_PLAN = 'ROLLOVER_TO_QUALIFIED_PLAN',
  TRANSFER = 'TRANSFER'
}

export type IraJournalContribution = {
  readonly applyToPriorYear?: InputMaybe<Scalars['Boolean']>;
  readonly iraConstraintReason?: InputMaybe<IraConstraintReason>;
};

export type IraJournalDistribution = {
  readonly applyToPriorYear?: InputMaybe<Scalars['Boolean']>;
  readonly federalTaxWithholding?: InputMaybe<TaxWithholdingInput>;
  readonly iraConstraintReason?: InputMaybe<IraConstraintReason>;
  readonly stateTaxWithholding?: InputMaybe<TaxWithholdingInput>;
};

export type JointOwnerInfo = {
  readonly __typename?: 'JointOwnerInfo';
  readonly address?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly phoneNumber?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly zip?: Maybe<Scalars['String']>;
};

export type JointOwnerInfoUpdateInput = {
  readonly city?: InputMaybe<Scalars['String']>;
  readonly email?: InputMaybe<Scalars['String']>;
  readonly financialAccountId: Scalars['String'];
  readonly phoneNumber?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly street?: InputMaybe<Scalars['String']>;
  readonly zip?: InputMaybe<Scalars['String']>;
};

export type LinkBeneficiaryInput = {
  readonly beneficiaryClass: BeneficiaryClass;
  readonly category: Scalars['String'];
  readonly financialAccountId: Scalars['ID'];
  readonly id: Scalars['ID'];
  readonly relationship: Scalars['String'];
};

export type LinkedFinAccountDetails = {
  readonly __typename?: 'LinkedFinAccountDetails';
  readonly account?: Maybe<FinancialAccountForHeldAway>;
  readonly adjustments: ReadonlyArray<Maybe<Adjustment>>;
  readonly aum?: Maybe<Scalars['Float']>;
  readonly aumDailyFee?: Maybe<Scalars['Float']>;
  readonly aumDaysCount: Scalars['Int'];
  readonly aumFee?: Maybe<Scalars['Float']>;
  readonly aumRate?: Maybe<Scalars['Float']>;
  readonly balance?: Maybe<Scalars['Float']>;
  readonly balancedMetrics?: Maybe<AllowedBalanceMetrics>;
  readonly calculation?: Maybe<Calculation>;
  readonly debit: Scalars['Float'];
  readonly debitAccountId?: Maybe<Scalars['String']>;
  readonly description?: Maybe<Scalars['String']>;
  readonly excluded?: Maybe<Scalars['Boolean']>;
  readonly financialAccountId: Scalars['ID'];
  readonly periodFlatFee: Scalars['Float'];
  readonly scheduleId?: Maybe<Scalars['ID']>;
  readonly scheduleType: AllowedType;
  readonly schemaVer?: Maybe<Scalars['String']>;
  readonly timing?: Maybe<AllowedBillTiming>;
};

export enum LiquidityNeeds {
  NOT_IMPORTANT = 'NOT_IMPORTANT',
  SOMEWHAT_IMPORTANT = 'SOMEWHAT_IMPORTANT',
  VERY_IMPORTANT = 'VERY_IMPORTANT'
}

export type LoginNamesOutput = {
  readonly __typename?: 'LoginNamesOutput';
  readonly id?: Maybe<Scalars['ID']>;
  readonly username?: Maybe<Scalars['String']>;
};

export type LoginResponse = {
  readonly __typename?: 'LoginResponse';
  readonly archetype: UserArchetype;
  readonly identityVerificationQuestion?: Maybe<IdentityVerificationQuestion>;
  readonly isImpersonation: Scalars['Boolean'];
  readonly isTrustedDevice?: Maybe<Scalars['Boolean']>;
  readonly mfaSettings?: Maybe<MfaSettings>;
  readonly organization?: Maybe<Organization>;
  readonly status: LoginStatus;
  readonly trustedDeviceActive?: Maybe<Scalars['Boolean']>;
  readonly user?: Maybe<User2>;
};

export enum LoginStatus {
  LOGGED_OUT = 'LOGGED_OUT',
  MFA_REQUIRED = 'MFA_REQUIRED',
  MFA_SETUP_REQUIRED = 'MFA_SETUP_REQUIRED',
  OK = 'OK'
}

export type LoginWhereInput = {
  readonly device?: InputMaybe<Device>;
  readonly fingerprintHash?: InputMaybe<Scalars['String']>;
  readonly impersonate?: InputMaybe<ImpersonateInput>;
  readonly mfa?: InputMaybe<ValidateMfaTokenRequest>;
  readonly password?: InputMaybe<Scalars['String']>;
  readonly username?: InputMaybe<Scalars['String']>;
};

export enum MEETING_PREFERENCE {
  IN_PERSON = 'IN_PERSON',
  VIRTUAL = 'VIRTUAL'
}

export type MMA = {
  readonly __typename?: 'MMA';
  readonly achs: ReadonlyArray<MMAACH>;
  readonly authTypes: ReadonlyArray<MMAAuthTypes>;
  readonly brokerageAccountDetails: FinancialAccount;
  readonly brokerageAccountId: Scalars['ID'];
  readonly id: Scalars['ID'];
  readonly internals: ReadonlyArray<MMAInternal>;
  readonly status: MMAStatus;
  readonly users: ReadonlyArray<MMAUserApproval>;
  readonly wires: ReadonlyArray<MMAWire>;
};

export type MMAACH = {
  readonly __typename?: 'MMAACH';
  readonly direction: ReadonlyArray<TransactionDirection>;
  readonly fundingAccountDetails: FundingAccount;
  readonly fundingAccountId: Scalars['ID'];
};

export enum MMAAuthTypes {
  ACH = 'ACH',
  INTERNAL = 'INTERNAL',
  WIRE = 'WIRE'
}

export type MMAInput = {
  readonly achs?: InputMaybe<ReadonlyArray<MMAInputACH>>;
  readonly authTypes: ReadonlyArray<MMAAuthTypes>;
  readonly brokerageAccountId: Scalars['ID'];
  readonly internals?: InputMaybe<ReadonlyArray<MMAInputInternal>>;
  readonly wires?: InputMaybe<ReadonlyArray<MMAInputWire>>;
};

export type MMAInputACH = {
  readonly direction: ReadonlyArray<TransactionDirection>;
  readonly fundingAccountId: Scalars['ID'];
};

export type MMAInputInternal = {
  readonly brokerageAccountId: Scalars['ID'];
};

export type MMAInputWire = {
  readonly fundingAccountId: Scalars['ID'];
};

export type MMAInternal = {
  readonly __typename?: 'MMAInternal';
  readonly brokerageAccountDetails: FinancialAccount;
  readonly brokerageAccountId: Scalars['ID'];
};

export type MMAPreviewUsersInput = {
  readonly mma: MMAInput;
};

export enum MMAStatus {
  APPROVED = 'APPROVED',
  CANCELED = 'CANCELED',
  DECLINED = 'DECLINED',
  PARTIALLY_APPROVED = 'PARTIALLY_APPROVED',
  PENDING_APPROVAL = 'PENDING_APPROVAL',
  REPLACED = 'REPLACED',
  REVOKED = 'REVOKED'
}

export type MMAUserApproval = {
  readonly __typename?: 'MMAUserApproval';
  readonly approvalStatus: MMAUserApprovalStatus;
  readonly user?: Maybe<Client>;
  readonly userId: Scalars['ID'];
};

export enum MMAUserApprovalStatus {
  APPROVED = 'APPROVED',
  DECLINED = 'DECLINED',
  PENDING_APPROVAL = 'PENDING_APPROVAL',
  REVOKED = 'REVOKED'
}

export type MMAWire = {
  readonly __typename?: 'MMAWire';
  readonly fundingAccountDetails: FundingAccount;
  readonly fundingAccountId: Scalars['ID'];
};

export type ManualBillingInformation = {
  readonly endDate: Scalars['String'];
  readonly frequency: Scalars['String'];
  readonly startDate: Scalars['String'];
};

export type ManualFinAccounts = {
  readonly debit: Scalars['Float'];
  readonly description: Scalars['String'];
  readonly financialAccountId: Scalars['ID'];
};

export type MarketData = {
  readonly __typename?: 'MarketData';
  readonly amountNetChange?: Maybe<Scalars['Float']>;
  readonly ask?: Maybe<Scalars['Float']>;
  readonly askAsOf?: Maybe<Scalars['String']>;
  readonly askMarket?: Maybe<Scalars['String']>;
  readonly askSize?: Maybe<Scalars['Int']>;
  /** @deprecated Use `askAsOf`, `bidAsOf`, or `mfAsOf`. */
  readonly asof?: Maybe<Scalars['String']>;
  readonly bid?: Maybe<Scalars['Float']>;
  readonly bidAsOf?: Maybe<Scalars['String']>;
  readonly bidMarket?: Maybe<Scalars['String']>;
  readonly bidSize?: Maybe<Scalars['Int']>;
  readonly caveatEmptorWarning?: Maybe<Scalars['String']>;
  readonly error?: Maybe<Scalars['String']>;
  readonly fromCache?: Maybe<Scalars['Boolean']>;
  readonly high?: Maybe<Scalars['Float']>;
  readonly lastTrade?: Maybe<Scalars['Float']>;
  readonly lastTradeMarket?: Maybe<Scalars['String']>;
  readonly lastVolume?: Maybe<Scalars['Int']>;
  readonly low?: Maybe<Scalars['Float']>;
  readonly maxCacheSeconds?: Maybe<Scalars['Int']>;
  readonly mfAmountNetChange?: Maybe<Scalars['Float']>;
  readonly mfAsOf?: Maybe<Scalars['String']>;
  readonly mfPercentageNetChange?: Maybe<Scalars['Float']>;
  readonly mfPrice?: Maybe<Scalars['Float']>;
  readonly name?: Maybe<Scalars['String']>;
  readonly open?: Maybe<Scalars['Float']>;
  readonly percentageNetChange?: Maybe<Scalars['Float']>;
  readonly priorClose?: Maybe<Scalars['Float']>;
  readonly requestTime?: Maybe<Scalars['String']>;
  readonly securityType?: Maybe<Scalars['String']>;
  readonly shortExchangeName?: Maybe<Scalars['String']>;
  readonly symbol?: Maybe<Scalars['String']>;
  readonly tierIdentifier?: Maybe<Scalars['String']>;
  readonly tradeIndicatorFlag?: Maybe<Scalars['String']>;
  readonly volume?: Maybe<Scalars['Int']>;
};

export enum MarketplaceAccessStatus {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
  PENDING = 'PENDING'
}

export type MarketplaceData = {
  readonly __typename?: 'MarketplaceData';
  readonly feePerYear?: Maybe<Scalars['Float']>;
  readonly feeType?: Maybe<Scalars['String']>;
  readonly isMarketplace: Scalars['Boolean'];
};

export type MarketplaceModelInfo = {
  readonly __typename?: 'MarketplaceModelInfo';
  readonly feePerYear?: Maybe<Scalars['Float']>;
  readonly feeType?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
};

export type MarketplacePortfolio = {
  readonly __typename?: 'MarketplacePortfolio';
  readonly equityPct: Scalars['Float'];
  /** @deprecated Should use feePerYear instead */
  readonly fee: Scalars['Float'];
  readonly feePerYear: Scalars['Float'];
  readonly feeType?: Maybe<Scalars['String']>;
  readonly finAcctData: Portfolio;
  readonly fixedIncomePct: Scalars['Float'];
  readonly id: Scalars['ID'];
  readonly models: ReadonlyArray<Maybe<Model>>;
  /** @deprecated Should use finAcctData/name */
  readonly name?: Maybe<Scalars['String']>;
  readonly portfolioManager: PortfolioManager;
  /** @deprecated Should use the portfolioManager field */
  readonly portfolioManagerId: Scalars['String'];
  readonly series: PortfolioSeries;
  /** @deprecated Should use the series field */
  readonly seriesId: Scalars['String'];
  readonly status: Scalars['String'];
};

export type MarketplacePortfolioAnalytic = {
  readonly __typename?: 'MarketplacePortfolioAnalytic';
  readonly annualizedStandardDeviation?: Maybe<Scalars['Float']>;
  readonly equityWeight?: Maybe<Scalars['Float']>;
  readonly factsheetUrl?: Maybe<Scalars['String']>;
  readonly feePerYear?: Maybe<Scalars['Float']>;
  readonly fiveYearsAnnualizedReturn?: Maybe<Scalars['Float']>;
  readonly id: Scalars['ID'];
  readonly maxDrawdown?: Maybe<Scalars['Float']>;
  readonly name: Scalars['String'];
  readonly netExpenseRatio?: Maybe<Scalars['Float']>;
  readonly oneYearAnnualizedReturn?: Maybe<Scalars['Float']>;
  readonly oneYearDividendYield?: Maybe<Scalars['Float']>;
  readonly portfolioManagerId: Scalars['String'];
  readonly returnEndDate?: Maybe<Scalars['DateOnly']>;
  readonly returnStartDate?: Maybe<Scalars['DateOnly']>;
  readonly securityTypes?: Maybe<ReadonlyArray<Scalars['String']>>;
  readonly tags?: Maybe<ReadonlyArray<Scalars['String']>>;
};

export type MatchConsumer = {
  readonly __typename?: 'MatchConsumer';
  readonly age: Scalars['Int'];
  readonly consumerType: ConsumerType;
  readonly disclosureUrl?: Maybe<Scalars['String']>;
  readonly email: Scalars['String'];
  readonly financialServices: ReadonlyArray<Maybe<Scalars['String']>>;
  readonly firstName: Scalars['String'];
  readonly goals?: Maybe<ReadonlyArray<Goals>>;
  readonly goalsNote?: Maybe<Scalars['String']>;
  readonly income: Scalars['ID'];
  readonly investableAssets: Scalars['ID'];
  readonly lastName: Scalars['String'];
  readonly matchRequestId: Scalars['ID'];
  readonly message?: Maybe<Scalars['String']>;
  readonly requestStatus: RequestStatus;
  readonly state: Scalars['String'];
  readonly timeframe: Scalars['String'];
  readonly workIndustry?: Maybe<Industries>;
  readonly workIndustryNote?: Maybe<Scalars['String']>;
};

export type MatchRequest = {
  readonly __typename?: 'MatchRequest';
  readonly age: Scalars['Int'];
  readonly consumerType: ConsumerType;
  readonly income: Scalars['String'];
  readonly location: Scalars['String'];
  readonly matchRequestId: Scalars['ID'];
  readonly name: Scalars['String'];
  readonly needsHelpWith: ReadonlyArray<Maybe<SERVICES>>;
  readonly requestStatus: RequestStatus;
};

export type MatchRequestInput = {
  readonly matchRequestId: Scalars['ID'];
  readonly message: Scalars['String'];
};

export type MatchUpdateInput = {
  readonly consumerType: ConsumerType;
  readonly matchRequestId: Scalars['ID'];
  readonly message?: InputMaybe<Scalars['String']>;
  readonly messageType?: InputMaybe<Scalars['String']>;
  readonly requestStatus: RequestStatus;
};

export type MembersAndManagers = {
  readonly isManager: Scalars['Boolean'];
  readonly legalName: Scalars['String'];
  readonly organizationalRole: Scalars['String'];
};

export type MetaData = {
  readonly __typename?: 'MetaData';
  readonly certifications: ReadonlyArray<Maybe<CertificationsOutput>>;
  readonly idealClients: ReadonlyArray<Maybe<IdealClientsOutput>>;
  readonly minimumInvestments: ReadonlyArray<Maybe<MinimumInvestment>>;
  readonly usStates: ReadonlyArray<Maybe<USStates>>;
};

export enum MfaMethod {
  BACKUP_CODE = 'BACKUP_CODE',
  EMAIL = 'EMAIL',
  GOOGLE_AUTHENTICATOR = 'GOOGLE_AUTHENTICATOR',
  SMS = 'SMS',
  VOICE = 'VOICE'
}

export type MfaSettings = {
  readonly __typename?: 'MfaSettings';
  readonly active: Scalars['Boolean'];
  readonly backupCodes?: Maybe<ReadonlyArray<Scalars['String']>>;
  readonly defaultMfaMethod?: Maybe<MfaMethod>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly phoneNumbers?: Maybe<ReadonlyArray<PhoneNumber>>;
};

export type MfaSettingsInput = {
  readonly active: Scalars['Boolean'];
  readonly defaultMfaMethod: MfaMethod;
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly phoneNumbers?: InputMaybe<ReadonlyArray<PhoneNumberInput>>;
};

export type MfaTokenRequest = {
  readonly credentials?: InputMaybe<Credentials>;
  readonly destination?: InputMaybe<Scalars['String']>;
  readonly mfaTypeOverride?: InputMaybe<MfaMethod>;
};

export type MfaTokenResponse = {
  readonly __typename?: 'MfaTokenResponse';
  readonly id: Scalars['ID'];
  readonly mfaMethod?: Maybe<MfaMethod>;
  readonly overrideId?: Maybe<Scalars['String']>;
  readonly validated?: Maybe<Scalars['Boolean']>;
};

export enum MigratingToPortfolioDriftType {
  ACTIVE = 'ACTIVE',
  CALENDAR = 'CALENDAR',
  DRIFT_BASED = 'DRIFT_BASED',
  EVENT_BASED = 'EVENT_BASED',
  PASSIVE = 'PASSIVE'
}

export type MigrationInfo = {
  readonly __typename?: 'MigrationInfo';
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly accountType?: Maybe<Scalars['String']>;
  readonly advisorId?: Maybe<Scalars['String']>;
  readonly apexAccountId?: Maybe<Scalars['String']>;
  readonly clientId?: Maybe<Scalars['String']>;
  readonly closedDate?: Maybe<Scalars['String']>;
  readonly createdDate?: Maybe<Scalars['String']>;
  readonly details?: Maybe<Scalars['String']>;
  readonly dwAccountId?: Maybe<Scalars['String']>;
  readonly fundingAccountId?: Maybe<Scalars['String']>;
  readonly isFundingLinked?: Maybe<Scalars['Boolean']>;
  readonly jointId?: Maybe<Scalars['String']>;
  readonly openAccountRequest?: Maybe<Scalars['String']>;
  readonly status?: Maybe<MigrationStatus>;
  readonly timestamp?: Maybe<Scalars['String']>;
};

export enum MigrationStatus {
  ERROR = 'ERROR',
  IN_PROGRESS = 'IN_PROGRESS',
  NO_MIGRATION = 'NO_MIGRATION',
  OPENED = 'OPENED'
}

export type MinHoldingWeight = {
  readonly __typename?: 'MinHoldingWeight';
  readonly symbol: Scalars['String'];
  readonly weight: Scalars['Float'];
};

export type MiniTaxSettings = {
  readonly __typename?: 'MiniTaxSettings';
  readonly taxManagementActive?: Maybe<Scalars['Boolean']>;
  readonly tlhActive?: Maybe<Scalars['Boolean']>;
};

export type MinimumInvestment = {
  readonly __typename?: 'MinimumInvestment';
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
  readonly value: Scalars['Int'];
};

export type Minor = {
  readonly __typename?: 'Minor';
  readonly address?: Maybe<Scalars['String']>;
  readonly backupWithholding?: Maybe<Scalars['Boolean']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly dateOfBirth?: Maybe<Scalars['String']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly lastName?: Maybe<Scalars['String']>;
  readonly phone?: Maybe<Scalars['String']>;
  readonly ssnTaxId?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly zipCode?: Maybe<Scalars['String']>;
};

export type MinorInput = {
  readonly address?: InputMaybe<Scalars['String']>;
  readonly backupWithholding?: InputMaybe<Scalars['Boolean']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly dateOfBirth?: InputMaybe<Scalars['String']>;
  readonly email?: InputMaybe<Scalars['String']>;
  readonly firstName: Scalars['String'];
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly lastName: Scalars['String'];
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly ssnTaxId?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly zipCode?: InputMaybe<Scalars['String']>;
};

export type MinorWhere = {
  readonly id: Scalars['ID'];
};

export type Model = {
  readonly __typename?: 'Model';
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
  readonly portfolios: ReadonlyArray<Portfolio>;
  readonly updated?: Maybe<Scalars['String']>;
  readonly weightedSecurities: ReadonlyArray<WeightedSecurity>;
};

export type ModelDetailOutput = {
  readonly __typename?: 'ModelDetailOutput';
  readonly benchmarkCategory?: Maybe<Scalars['String']>;
  readonly benchmarkId?: Maybe<Scalars['ID']>;
  readonly benchmarkValue?: Maybe<Scalars['String']>;
  /** @deprecated Cash is included in holdings as first element */
  readonly cash: ModelHoldingOutput;
  readonly description?: Maybe<Scalars['String']>;
  readonly holdings?: Maybe<ReadonlyArray<ModelHoldingOutput>>;
  readonly holdingsType?: Maybe<HoldingType>;
  readonly id: Scalars['ID'];
  readonly isMarketplaceModel: Scalars['Boolean'];
  readonly marketplaceModelInfo?: Maybe<MarketplaceModelInfo>;
  readonly name: Scalars['String'];
  readonly portfolios?: Maybe<ReadonlyArray<Maybe<PortfolioOutput>>>;
};

export type ModelHoldingInput = {
  readonly fundSubstitutes?: InputMaybe<ReadonlyArray<InputMaybe<ModelSubstituteInput>>>;
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly ticker: Scalars['String'];
  readonly weight: Scalars['Float'];
};

export type ModelHoldingOutput = {
  readonly __typename?: 'ModelHoldingOutput';
  readonly fundSubstitutes?: Maybe<FundSubstitutesType>;
  readonly id: Scalars['String'];
  readonly isIntervalFund?: Maybe<Scalars['Boolean']>;
  readonly name?: Maybe<Scalars['String']>;
  readonly shareClass?: Maybe<Scalars['String']>;
  readonly ticker?: Maybe<Scalars['String']>;
  readonly transactionFee?: Maybe<Scalars['Float']>;
  readonly type?: Maybe<Scalars['String']>;
  readonly weight?: Maybe<Scalars['Float']>;
};

export type ModelListOutput = {
  readonly __typename?: 'ModelListOutput';
  readonly cashWeight?: Maybe<Scalars['Float']>;
  readonly description?: Maybe<Scalars['String']>;
  readonly holdings?: Maybe<ReadonlyArray<ModelHoldingOutput>>;
  readonly id: Scalars['ID'];
  readonly isMarketplaceModel: Scalars['Boolean'];
  readonly latestVersionMarketplaceId?: Maybe<Scalars['ID']>;
  readonly name: Scalars['String'];
  readonly numberOfAccounts?: Maybe<Scalars['Int']>;
  readonly numberOfFunds?: Maybe<Scalars['Int']>;
  readonly numberOfFundsWithSubs?: Maybe<Scalars['Int']>;
  readonly numberOfHoldings: Scalars['Int'];
  readonly type?: Maybe<Scalars['String']>;
  readonly updated?: Maybe<Scalars['DateTime']>;
};

export type ModelPortfolioValidationResult = {
  readonly __typename?: 'ModelPortfolioValidationResult';
  readonly failureCount: Scalars['Int'];
  readonly portfolios: ReadonlyArray<Maybe<PortfolioValidationInfo>>;
  readonly success: Scalars['Boolean'];
  readonly successCount: Scalars['Int'];
};

export type ModelSubstituteInput = {
  readonly isIntervalFund?: InputMaybe<Scalars['Boolean']>;
  readonly rank: Scalars['Int'];
  readonly securityName: Scalars['String'];
  readonly sellOnly: Scalars['Boolean'];
  readonly shareClass?: InputMaybe<Scalars['String']>;
  readonly substituteGrade: SubstituteGradeType;
  readonly ticker: Scalars['String'];
  readonly transactionFee?: InputMaybe<Scalars['Float']>;
  readonly type?: InputMaybe<Scalars['String']>;
};

export type ModelSubstituteOutput = {
  readonly __typename?: 'ModelSubstituteOutput';
  readonly isIntervalFund?: Maybe<Scalars['Boolean']>;
  readonly rank: Scalars['Int'];
  readonly securityName: Scalars['String'];
  readonly sellOnly: Scalars['Boolean'];
  readonly shareClass?: Maybe<Scalars['String']>;
  readonly substituteGrade: SubstituteGradeType;
  readonly ticker: Scalars['String'];
  readonly transactionFee?: Maybe<Scalars['Float']>;
  readonly type?: Maybe<Scalars['String']>;
};

export type ModelValidationError = {
  readonly __typename?: 'ModelValidationError';
  readonly message: Scalars['String'];
  readonly ticker: Scalars['String'];
};

export type ModelValidationResult = {
  readonly __typename?: 'ModelValidationResult';
  readonly errors: ReadonlyArray<ModelValidationError>;
};

export type MutateClientResponse = {
  readonly __typename?: 'MutateClientResponse';
  readonly user?: Maybe<Client>;
};

export type Mutation = {
  readonly __typename?: 'Mutation';
  readonly ArchiveAccount?: Maybe<ReadonlyArray<Maybe<Scalars['ID']>>>;
  readonly UnarchiveAccount?: Maybe<ReadonlyArray<Maybe<Scalars['ID']>>>;
  readonly _?: Maybe<Scalars['Boolean']>;
  readonly acatResendEmail: ACATTransaction;
  readonly acatTransfer: SuccessOutput;
  readonly acceptAltruistClearingTermsOfUse: SuccessOutput;
  readonly acceptMatchConsumer?: Maybe<Scalars['String']>;
  readonly acceptUserTerms: SuccessOutput;
  readonly addBillingPeriodById?: Maybe<BillingPeriod>;
  readonly addBillingPeriods?: Maybe<ReadonlyArray<addBillingPeriodsOutput>>;
  readonly addDocumentForAccount?: Maybe<Scalars['String']>;
  readonly addExistingFundingToBrokerage: FundingAccount;
  readonly addFidelityGNumber: IntegrationSourceRepCodes;
  readonly addFundingAccount: FundingAccount;
  readonly addSchwabMasterAccountNumber: IntegrationSourceRepCodes;
  /** @deprecated This process is made on the server side now */
  readonly addTrustedDevice: TrustedDevice;
  readonly approveAcatDraft: AcatDraft;
  readonly approveAcatDraftsByAccountStateIds: AcatDraftsGroupedByAccountState;
  readonly approveMMA: MMA;
  readonly approvePlaidMicroDepositV2?: Maybe<ApprovePlaidMicroDepositV2Response>;
  readonly approvePlaidMicrodeposit?: Maybe<SuccessOutput>;
  readonly assignBenchmark?: Maybe<AssignBenchmarkOutput>;
  readonly assignPortfolio: SuccessOutput;
  readonly assignRepcodesToUser: ReadonlyArray<Repcode>;
  readonly authenticatePartnerIntegration: AuthenticateIntegrationResponse;
  readonly beginBillingPeriodRerun?: Maybe<BillingPeriodRerunResponse>;
  readonly beginDocumentBundling: DocumentBundle;
  readonly bulkApproveMMA: SuccessResponse;
  readonly bulkDeclineMMA: SuccessResponse;
  readonly cancelCashJournalTransfer: SuccessOutput;
  readonly cancelDocumentAgreement?: Maybe<CancelDocumentAgreementResponse>;
  readonly cancelMMA: MMA;
  readonly cancelPendingACAT: ACATTransaction;
  readonly cancelPendingCashTransfer: SuccessOutput;
  readonly cancelRebalance: SuccessOutput;
  readonly cancelScheduledJournalTransfer: SuccessOutput;
  readonly cancelScheduledTransfer: CancelationTransferResponse;
  readonly cancelTradeOrder: CancelOrderOutput;
  readonly cashJournalTransfer: SuccessOutput;
  readonly cashLiquidationCancellation?: Maybe<CashLiquidationCancellationResponse>;
  readonly cashLiquidationCreation?: Maybe<CashLiquidationCreationResponse>;
  readonly changePassword?: Maybe<ChangePasswordOutput>;
  readonly clientApproveACAT: ACATTransaction;
  readonly clientDeposit: SuccessOutput;
  readonly clientWithdraw: SuccessOutput;
  readonly closeAccount?: Maybe<SuccessOutput>;
  readonly connectPlaid?: Maybe<SuccessOutput>;
  readonly createAcatDraft: AcatDraft;
  readonly createAccountBeneficiary: Beneficiary;
  readonly createAccountCashSettings: SuccessOutput;
  /** @deprecated Use createAccountFromStateBulk */
  readonly createAccountFromState?: Maybe<CreateAccountOutput>;
  readonly createAccountFromStateBulk: ReadonlyArray<CreateBulkAccountOutput>;
  readonly createAccountTaxSettings: AccountTaxSettings;
  readonly createAdoptingEmployer: AdoptingEmployer;
  readonly createAdvisorDocument?: Maybe<PhotoReturn>;
  readonly createAdvisorMarketplaceProfileDraft: AdvisorMarketplaceProfile;
  readonly createBenchmark?: Maybe<CreateBenchmarkOutput>;
  /** @deprecated Use createAccountBeneficiary. */
  readonly createBeneficiary: BeneficiaryOutput;
  readonly createBulkMMA: ReadonlyArray<Scalars['ID']>;
  readonly createColorDraft: ColorDraft;
  readonly createContact: CreateContactOutput;
  readonly createCorpMember: CorpMember;
  readonly createCorporation: Corporation;
  readonly createDecedent: Decedent;
  readonly createDocumentAgreement?: Maybe<DocumentAgreementResponse>;
  /** @deprecated Use createHouseAccountV2 */
  readonly createEntityHouseAccount: HouseAccountCreationResponse;
  readonly createGroup: Group;
  readonly createHeldAwayLink?: Maybe<HeldAwayMutationResponse>;
  /** @deprecated Use createHouseAccountV2 */
  readonly createHouseAccount: HouseAccountCreationResponse;
  readonly createHouseAccountV2: HouseAccountCreationResponse;
  readonly createInvoiceBundleUrl?: Maybe<InvoiceBundleUrlResponse>;
  readonly createMMA: MMA;
  readonly createMfaSettings: MfaSettings;
  readonly createMfaSettingsWithCredentials: MfaSettings;
  readonly createMinor: Minor;
  readonly createModel: SuccessOutput;
  /** ONLY USE BELOW when first creating contact, or if not logged in */
  readonly createOrUpdateHubSpotContact: createOrUpdateHubSpotContactOutput;
  readonly createOrUpdateHubSpotTeamContact: createOrUpdateHubSpotContactOutput;
  readonly createOrUpdateZendeskContact?: Maybe<ZendeskContactOutput>;
  readonly createPendingUsername: PendingUsername;
  readonly createPlanDetails: PlanDetails;
  readonly createPortfolio: SuccessOutput;
  readonly createRepcode: Repcode;
  readonly createReportScheduleAssignments?: Maybe<CreateReportScheduleAssignmentsOutput>;
  readonly createScheduleAssignment: ScheduleAssignmentResponse;
  readonly createSchedules: SchedulesOutput;
  readonly createSingleParticipant: SingleParticipant;
  readonly createStripeCustomer: StripeCustomerOutput;
  readonly createSupportUser: SupportUser;
  readonly createTradeOrder: CreateOrderOutput;
  readonly createTrustedContact: TrustedContactOutput;
  readonly declineMMA: MMA;
  readonly declineMatchConsumer?: Maybe<Scalars['String']>;
  readonly deleteAcatDraft: Scalars['ID'];
  readonly deleteAccountBeneficiaries: SuccessOutput;
  readonly deleteAccountCashSettings: SuccessOutput;
  readonly deleteAccountDraft: SuccessOutput;
  readonly deleteAccountDraftsBulk: ReadonlyArray<Scalars['ID']>;
  readonly deleteAccountTaxSettings: SuccessOutput;
  readonly deleteAdvisorDocument?: Maybe<Scalars['String']>;
  readonly deleteAdvisorProfileDraft: DeleteResponse;
  readonly deleteBenchmark?: Maybe<DeleteBenchmarkOutput>;
  readonly deleteBenchmarkAssignment?: Maybe<DeleteBenchmarkAssignmentOutput>;
  /** @deprecated Use deleteAccountBeneficiaries. */
  readonly deleteBeneficiaries: SuccessOutput;
  /** @deprecated Use deleteAccountBeneficiaries. */
  readonly deleteBeneficiary: SuccessOutput;
  readonly deleteBillingPeriod: DeleteOutput;
  readonly deleteColorDraft: Scalars['ID'];
  readonly deleteContact?: Maybe<SuccessOutput>;
  readonly deleteCorpMember: Scalars['ID'];
  readonly deleteDocumentAgreement?: Maybe<DeleteResponse>;
  readonly deleteDocumentFromAccount?: Maybe<Scalars['String']>;
  readonly deleteDocumentTemplate?: Maybe<DeleteResponse>;
  readonly deleteFromGroup?: Maybe<Scalars['ID']>;
  readonly deleteGroup?: Maybe<Scalars['String']>;
  readonly deleteInvitedUser: Scalars['String'];
  readonly deleteInvoice?: Maybe<DeleteInvoiceOutput>;
  readonly deleteLogoFromOrganization?: Maybe<UpdateOrganizationResponse>;
  readonly deleteMember?: Maybe<Scalars['ID']>;
  readonly deleteModel: SuccessOutput;
  readonly deletePendingUsername: SuccessOutput;
  readonly deletePortfolio: SuccessOutput;
  readonly deleteRepCode: DeleteRepCodeOutput;
  readonly deleteScheduleAssignments?: Maybe<ReadonlyArray<Maybe<DeleteOutput>>>;
  readonly deleteSchedules: DeleteOutput;
  readonly deleteTrustedDevice: Scalars['Boolean'];
  readonly disableOrgPartnerIntegration: ResponseEnvelope;
  readonly disconnectPartnerIntegration: DisconnectPartnerIntegrationResponse;
  readonly editAccountBeneficiaryAllocation: ReadonlyArray<Beneficiary>;
  /** @deprecated Use updateAccountBeneficiary. */
  readonly editBeneficiary: SuccessOutput;
  /** @deprecated Use editAccountBeneficiaryAllocation. */
  readonly editBeneficiaryAllocationList: SuccessOutput;
  readonly editHouseholdScheduleAssignments: ReadonlyArray<ScheduleAssignment2>;
  readonly editModel: SuccessOutput;
  readonly editModelAsync: EditModelAsyncOutput;
  readonly editPortfolio: SuccessOutput;
  readonly editPortfolioAsync: EditPortfolioAsyncOutput;
  readonly executeFeesReport?: Maybe<executeFeesReportResponse>;
  readonly executeManualInvoice?: Maybe<executeManualInvoiceResponse>;
  readonly generateDocumentTemplateUrl?: Maybe<DocumentTemplateResponse>;
  readonly generateSignUrl?: Maybe<SignUrlResponse>;
  readonly getFundedDay: FundedDayOutput;
  readonly handleProcessingAction?: Maybe<DocumentAgreementResponse>;
  readonly hideAdvisorProfile: AdvisorMarketplaceProfile;
  readonly initiateOrgPartnerIntegration: ResponseEnvelope;
  readonly inviteClient: InviteOutput;
  readonly inviteUser: InviteOutput;
  readonly inviteUserResend: InviteOutput;
  /** @deprecated Use reuseExistingBeneficiary. */
  readonly linkExistingBeneficiary: BeneficiaryOutput;
  /** @deprecated Use loginWithCredentials instead. */
  readonly login: User2;
  readonly loginWithCredentials: LoginResponse;
  readonly loginWithMfa: LoginResponse;
  readonly logout: Scalars['Boolean'];
  readonly mfaTokenRequest: MfaTokenResponse;
  readonly mfaTokenRequestWithCredentials: MfaTokenResponse;
  readonly patchDocumentTemplate?: Maybe<DocumentTemplateResponse>;
  readonly payStripeSubscription: PaySubscriptionOutput;
  readonly previewDraft: PreviewResponse;
  readonly reExportExecutedInvoices: reExportExecutedInvoicesResponse;
  readonly reactivateSubscription: SuccessOutput;
  readonly reinviteBrokerageUser: InviteOutput;
  readonly rejectAcatDraft: AcatDraft;
  readonly relinkHeldAway?: Maybe<HeldAwayMutationResponse>;
  readonly removeFundingAccount: RemoveFunding;
  readonly renameAccount?: Maybe<renameAccountOutput>;
  readonly requestDfaAccess: IntegrationSource;
  readonly requestMarketplaceDfaAccess: Scalars['Boolean'];
  readonly requestRIAAccess: AccessRequestResponse;
  readonly rerunInvoices: RerunInvoicesOutput;
  readonly resendCorpApplicationToSecretary: Corporation;
  readonly resetPassword: ResetPasswordOutput;
  readonly reuseExistingBeneficiary: Beneficiary;
  readonly revokeMMA: MMA;
  readonly revokeThirdPartyAccess: RevokeThirdPartyAccessResponse;
  readonly saveAccountState: StateOutput;
  readonly saveAccountStatesBulk: ReadonlyArray<StateOutput>;
  readonly scheduleRebalance: RebalanceOccurence;
  readonly scheduledCashJournalTransfer: SuccessOutput;
  readonly scheduledClientDeposit: ScheduledTransferResponse;
  readonly scheduledClientWithdraw: ScheduledTransferResponse;
  readonly sendCashAccountInterestEmail: SuccessOutput;
  readonly sendCorpApplicationToSecretary?: Maybe<SuccessOutput>;
  readonly signMarketplaceAgreement: SignDocument;
  readonly spliceAccountHistory?: Maybe<SuccessOutput>;
  readonly submitActivityReportRequest?: Maybe<ActivityReportRequestResult>;
  readonly submitAdvisorIdentification?: Maybe<UpdateAdvisorResponse>;
  readonly submitBrokerageApplication: Organization;
  readonly submitForReview: ReviewReturn;
  readonly submitHubSpotFormById: SuccessOutput;
  readonly submitInvestmentSummaryReport?: Maybe<ReportsOutput>;
  readonly submitPaymentMethod: PaymentMethodOutput;
  readonly subscribeStripeCustomer: StripeSubscriptionOutput;
  /** @deprecated ADVEX-439 */
  readonly subscribeToNotification: NotificationPreference;
  readonly toggleMfaEnabled: MfaSettings;
  /** @deprecated This process is made on the server side now */
  readonly toggleTrustedDevice: TrustedDevice;
  readonly transferAccountsBetweenRepCodes?: Maybe<ReadonlyArray<Maybe<Repcode>>>;
  readonly trustDevice: TrustedDevice;
  readonly trustDeviceWithCredentials: TrustedDevice;
  readonly unAssignPortfolio: SuccessOutput;
  readonly unSpliceAccountHistory?: Maybe<SuccessOutput>;
  readonly unhideAdvisorProfile: AdvisorMarketplaceProfile;
  readonly unlinkExternalAccount?: Maybe<SuccessOutput>;
  readonly unlinkHeldAway?: Maybe<DeleteResponse>;
  /** @deprecated ADVEX-439 */
  readonly unsubscribeToNotification: NotificationPreference;
  readonly updateAcatDraft: AcatDraft;
  readonly updateAccountAddress?: Maybe<SuccessOutput>;
  readonly updateAccountAddresses?: Maybe<SuccessOutput>;
  readonly updateAccountBeneficiariesDesignation: SuccessOutput;
  readonly updateAccountBeneficiary: Beneficiary;
  readonly updateAccountCashSettings: SuccessOutput;
  readonly updateAccountEmail?: Maybe<SuccessOutput>;
  readonly updateAccountManagement?: Maybe<AccountManagementOutput>;
  readonly updateAccountPhoneNumber?: Maybe<SuccessOutput>;
  readonly updateAccountProfile?: Maybe<SuccessOutput>;
  readonly updateAccountState: SuccessOutput;
  readonly updateAccountStatesBulk: ReadonlyArray<StateOutput>;
  readonly updateAccountTaxSettings: AccountTaxSettings;
  readonly updateAdoptingEmployer: AdoptingEmployer;
  /** @deprecated Use updateADVForms in BrokerageApplication */
  readonly updateAdvisorADVForms?: Maybe<UpdateAdvisorADVFormsResponse>;
  readonly updateAdvisorAddress?: Maybe<AdvisorAddressResponse>;
  readonly updateAdvisorMarketplaceProfileDraft: AdvisorMarketplaceProfile;
  readonly updateAllNotificationStatuses: UpdateAllNotificationStatusesMutationResponse;
  /** @deprecated Use the mutation, `updateFeeAllocation` instead. */
  readonly updateAllocationForGroup: GroupFeeAllocations;
  readonly updateBenchmark?: Maybe<UpdateBenchmarkOutput>;
  readonly updateBillingGroup: UpdateBillingGroupResponse;
  readonly updateBillingPeriod?: Maybe<BillingPeriod>;
  readonly updateClient?: Maybe<MutateClientResponse>;
  readonly updateClientAddress: AdvisorAddress;
  readonly updateClientDocumentReviewStatus?: Maybe<Scalars['String']>;
  readonly updateColorDraft: ColorDraft;
  readonly updateContact: UpdateContactOutput;
  readonly updateContactEmail?: Maybe<UpdateContactEmailOutput>;
  readonly updateCorpMember: CorpMember;
  readonly updateCorporation: Corporation;
  readonly updateDocumentTemplate?: Maybe<DocumentTemplateResponse>;
  readonly updateEntityHouseAccount?: Maybe<SuccessOutput>;
  readonly updateExcludeDaysBeforeFunding: UpdateExcludeDaysBeforeFundingOutput;
  readonly updateFeeAllocation: FeeAllocation;
  readonly updateFeeRoutingHouseholdPreferences: ReadonlyArray<FeeTemplate>;
  readonly updateFeeRoutingPreferences: ReadonlyArray<FeeTemplate>;
  readonly updateFinancialAccountBillingDate?: Maybe<FinancialAccountBillingDates>;
  readonly updateFinancialAccountProperties: FinancialAccount2;
  readonly updateFormADVUrls?: Maybe<BrokerageApplication>;
  readonly updateFundingAccount: FundingAccount;
  readonly updateGroup: GroupAssignment;
  readonly updateGroupName: UpdateGroupNameOutput;
  readonly updateGroupSchedulesByAccount?: Maybe<ReadonlyArray<updateGroupSchedulesByAccountOutput>>;
  /** Use this resolver by default */
  readonly updateHubSpotContactById: SuccessOutput;
  readonly updateIntegration: UpdateIntegrationResponse;
  readonly updateJointOwnerInfo?: Maybe<SuccessOutput>;
  readonly updateJointUserProfile?: Maybe<SuccessOutput>;
  readonly updateMatchConsumer?: Maybe<Scalars['String']>;
  readonly updateMember?: Maybe<Advisor2>;
  readonly updateMfaSettings: MfaSettings;
  readonly updateMfaSettingsWithCredentials: MfaSettings;
  readonly updateMinor: Minor;
  readonly updateNotificationPreferences?: Maybe<NotificationPreferenceMutationResponse>;
  readonly updateNotificationStatuses?: Maybe<NotificationStatusesMutationResponse>;
  readonly updateOnboardStatus: OnboardStatus;
  readonly updateOrganization?: Maybe<CompanyOutput>;
  readonly updateOrganizationPreferences: OrganizationPreferences;
  readonly updatePlanDetails: PlanDetails;
  readonly updateRepcode: Repcode;
  readonly updateRepcodeAsTheDefault?: Maybe<ReadonlyArray<Repcode>>;
  readonly updateReportScheduleAssignments?: Maybe<UpdateReportScheduleAssignmentsOutput>;
  readonly updateScheduleAssignment: ScheduleAssignmentResponse;
  /** @deprecated Use the mutation, `updateScheduleAssignments` instead. */
  readonly updateScheduleAssignmentByAccount: ReadonlyArray<AccountScheduleAssignment>;
  /** @deprecated Use the mutation, `updateScheduleAssignments` instead. */
  readonly updateScheduleAssignmentByGroup?: Maybe<ReadonlyArray<GroupScheduleAssignment>>;
  readonly updateScheduleAssignments: ReadonlyArray<ScheduleAssignment>;
  readonly updateScheduleAssignmentsDate?: Maybe<ReadonlyArray<UpdateScheduleAssignmentsDateOutput>>;
  readonly updateSchedules?: Maybe<SchedulesOutput>;
  readonly updateSecurityConstraints?: Maybe<ReadonlyArray<SecurityConstraint>>;
  readonly updateSingleParticipant: SingleParticipant;
  readonly updateStripeCustomer: StripeCustomerOutput;
  readonly updateTeamMemberInfo?: Maybe<UserInfoOutputMutation>;
  readonly updateTrustedContact: TrustedContactOutput;
  /** @deprecated Use 'updateUserInGroup2' instead. */
  readonly updateUserInGroup: AdvisorAddress;
  readonly updateUserInfo?: Maybe<UserInfoOutputMutation>;
  readonly uploadDocumentTemplate?: Maybe<UploadDocumentTemplateResponse>;
  readonly uploadFees: UploadFeesResponse;
  readonly uploadOrganizationLogo?: Maybe<UpdateOrganizationResponse>;
  readonly uploadPhoto?: Maybe<File>;
  readonly validateAccountFromState: ValidateAccountOutput;
  readonly validateAccountFromStateBulk: ReadonlyArray<Maybe<ValidateAccountOutput>>;
  readonly validateCashSettings: ModelPortfolioValidationResult;
  readonly validateCreatePortfolio: PortfolioValidationResult;
  readonly validateMfaToken: ValidateMfaTokenResponse;
  readonly validateMfaTokenWithCredentials: ValidateMfaTokenResponse;
  readonly validateUpdateModel: ModelValidationResult;
  readonly validateUpdatePortfolio: PortfolioValidationResult;
  readonly verifyIdentityWithQuestion: IdentityVerificationResponse;
  readonly verifyNewUser: Scalars['Boolean'];
  readonly verifyPendingUsername: PendingUsername;
};


export type MutationArchiveAccountArgs = {
  ids?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
};


export type MutationUnarchiveAccountArgs = {
  ids?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
};


export type MutationacatResendEmailArgs = {
  id: Scalars['ID'];
};


export type MutationacatTransferArgs = {
  acat: ACATInput;
  financialAccountId: Scalars['ID'];
  userId: Scalars['ID'];
};


export type MutationacceptMatchConsumerArgs = {
  matchRequestId: Scalars['ID'];
};


export type MutationaddBillingPeriodByIdArgs = {
  BillingPeriod?: InputMaybe<BillingPeriodInput>;
};


export type MutationaddBillingPeriodsArgs = {
  scheduleAssignmentIds: ReadonlyArray<InputMaybe<Scalars['ID']>>;
  startDate: Scalars['String'];
};


export type MutationaddDocumentForAccountArgs = {
  clientId: Scalars['ID'];
  documentId: Scalars['ID'];
};


export type MutationaddExistingFundingToBrokerageArgs = {
  brokerageAccountId: Scalars['ID'];
  fundingAccountId: Scalars['ID'];
};


export type MutationaddFidelityGNumberArgs = {
  input: AddFidelityGNumberInput;
};


export type MutationaddFundingAccountArgs = {
  financialAccountId: Scalars['ID'];
  plaidMetaData: PlaidMetaData;
  publicToken: Scalars['String'];
};


export type MutationaddSchwabMasterAccountNumberArgs = {
  input: AddSchwabMasterAccountNumberInput;
};


export type MutationaddTrustedDeviceArgs = {
  trustedDeviceInput: TrustedDeviceInput;
};


export type MutationapproveAcatDraftArgs = {
  input: AcatDraftStatusChangeInput;
};


export type MutationapproveAcatDraftsByAccountStateIdsArgs = {
  input: AccountStateIdsInput;
};


export type MutationapproveMMAArgs = {
  mmaID: Scalars['ID'];
};


export type MutationapprovePlaidMicroDepositV2Args = {
  plaidAccountId: Scalars['String'];
};


export type MutationapprovePlaidMicrodepositArgs = {
  plaidAccountId: Scalars['String'];
};


export type MutationassignBenchmarkArgs = {
  assignment: AssignBenchmarkInput;
};


export type MutationassignPortfolioArgs = {
  financialAccountIds: ReadonlyArray<Scalars['ID']>;
  portfolioId: Scalars['ID'];
  rebalanceAsap?: InputMaybe<Scalars['Boolean']>;
  skipRebalance?: InputMaybe<Scalars['Boolean']>;
};


export type MutationassignRepcodesToUserArgs = {
  input: AssignRepcodesToUserInput;
};


export type MutationauthenticatePartnerIntegrationArgs = {
  input: AuthenticatePartnerIntegrationInput;
};


export type MutationbeginBillingPeriodRerunArgs = {
  input: BillingPeriodRerunInput;
};


export type MutationbeginDocumentBundlingArgs = {
  input: BeginDocumentBundlingInput;
};


export type MutationbulkApproveMMAArgs = {
  mmaUUIDs: ReadonlyArray<Scalars['ID']>;
};


export type MutationbulkDeclineMMAArgs = {
  mmaUUIDs: ReadonlyArray<Scalars['ID']>;
};


export type MutationcancelCashJournalTransferArgs = {
  cashJournalTransferId: Scalars['ID'];
};


export type MutationcancelDocumentAgreementArgs = {
  input: CancelDocumentAgreementInput;
};


export type MutationcancelMMAArgs = {
  mmaID: Scalars['ID'];
};


export type MutationcancelPendingACATArgs = {
  id: Scalars['ID'];
};


export type MutationcancelPendingCashTransferArgs = {
  transferId: Scalars['ID'];
};


export type MutationcancelRebalanceArgs = {
  financialAccountId: Scalars['ID'];
};


export type MutationcancelScheduledJournalTransferArgs = {
  journalScheduledTransferUuid: Scalars['ID'];
};


export type MutationcancelScheduledTransferArgs = {
  scheduledTransferId: Scalars['ID'];
};


export type MutationcancelTradeOrderArgs = {
  input: CancelTradeOrderInput;
};


export type MutationcashJournalTransferArgs = {
  amount: Scalars['Float'];
  fromBrokerageAccountId: Scalars['ID'];
  iraContribution?: InputMaybe<IraJournalContribution>;
  iraDistribution?: InputMaybe<IraJournalDistribution>;
  journalAccountLinkUuid: Scalars['String'];
  toBrokerageAccountId: Scalars['ID'];
};


export type MutationcashLiquidationCancellationArgs = {
  cashLiquidationId?: InputMaybe<Scalars['String']>;
};


export type MutationcashLiquidationCreationArgs = {
  amount?: InputMaybe<Scalars['Float']>;
  asideUntil?: InputMaybe<Scalars['String']>;
  finAccountId: Scalars['ID'];
  note?: InputMaybe<Scalars['String']>;
  shouldRebalanceAsap?: InputMaybe<Scalars['Boolean']>;
};


export type MutationchangePasswordArgs = {
  password: Scalars['String'];
  userId?: InputMaybe<Scalars['ID']>;
};


export type MutationclientApproveACATArgs = {
  id: Scalars['ID'];
};


export type MutationclientDepositArgs = {
  amount: Scalars['String'];
  currency: Scalars['String'];
  frequency?: InputMaybe<DepositFrequency>;
  iraContribution?: InputMaybe<IraContributionType>;
  iraContributionReason?: InputMaybe<ContributionReason>;
  sourceFinancialAccountId: Scalars['ID'];
  targetFinancialAccountId: Scalars['ID'];
};


export type MutationclientWithdrawArgs = {
  amount: Scalars['String'];
  currency: Scalars['String'];
  iraDistribution?: InputMaybe<IraDistribution>;
  sourceFinancialAccountId: Scalars['ID'];
  targetFinancialAccountId: Scalars['ID'];
};


export type MutationcloseAccountArgs = {
  federalTax?: InputMaybe<TaxWithholdingInput>;
  financialAccountId: Scalars['ID'];
  fundingAccountId?: InputMaybe<Scalars['ID']>;
  iraDistributionReason?: InputMaybe<IraDistributionReason>;
  stateTax?: InputMaybe<TaxWithholdingInput>;
};


export type MutationconnectPlaidArgs = {
  accounts: ReadonlyArray<InputMaybe<PlaidAccount>>;
  institution: PlaidInstitution;
  publicToken: Scalars['String'];
};


export type MutationcreateAcatDraftArgs = {
  input: AcatDraftInput;
};


export type MutationcreateAccountBeneficiaryArgs = {
  input: CreateAccountBeneficiaryInput;
};


export type MutationcreateAccountCashSettingsArgs = {
  financialAccountId: Scalars['ID'];
  input: CashSettingsInput;
  rebalanceAsap?: InputMaybe<Scalars['Boolean']>;
};


export type MutationcreateAccountFromStateArgs = {
  accountStateId: Scalars['ID'];
  advisorId: Scalars['ID'];
  advisorName: Scalars['String'];
  dividendReinvestment?: InputMaybe<Scalars['Boolean']>;
  sweepInstructions?: InputMaybe<Scalars['Boolean']>;
};


export type MutationcreateAccountFromStateBulkArgs = {
  input: CreateAccountFromStateBulkInput;
};


export type MutationcreateAccountTaxSettingsArgs = {
  input: TaxSettingsInput;
};


export type MutationcreateAdoptingEmployerArgs = {
  input: AdoptingEmployerInput;
};


export type MutationcreateAdvisorDocumentArgs = {
  documentName?: InputMaybe<Scalars['String']>;
  documentType?: InputMaybe<Scalars['String']>;
  file?: InputMaybe<Scalars['Upload']>;
  versionNumber?: InputMaybe<Scalars['String']>;
};


export type MutationcreateAdvisorMarketplaceProfileDraftArgs = {
  input?: InputMaybe<AdvisorMarketplaceProfileDraftInput>;
};


export type MutationcreateBenchmarkArgs = {
  benchmark: CreateBenchmarkInput;
};


export type MutationcreateBeneficiaryArgs = {
  address?: InputMaybe<Scalars['String']>;
  beneficiaryClass?: InputMaybe<BeneficiaryClass>;
  category?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  companyName?: InputMaybe<Scalars['String']>;
  dateOfBirth?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  financialAccountId?: InputMaybe<Scalars['String']>;
  firstName?: InputMaybe<Scalars['String']>;
  lastName?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  relationship?: InputMaybe<Scalars['String']>;
  state?: InputMaybe<Scalars['String']>;
  taxIdentificationNumber?: InputMaybe<Scalars['String']>;
  type: BeneficiaryType;
  userId?: InputMaybe<Scalars['ID']>;
  userIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
  zipCode?: InputMaybe<Scalars['String']>;
};


export type MutationcreateBulkMMAArgs = {
  mma: CreateReplaceInput;
};


export type MutationcreateColorDraftArgs = {
  input: ColorDraftInput;
};


export type MutationcreateContactArgs = {
  contact: CreateContactInput;
  groupId: Scalars['ID'];
};


export type MutationcreateCorpMemberArgs = {
  input: CreateCorpMember;
};


export type MutationcreateCorporationArgs = {
  input: CorporationInput;
};


export type MutationcreateDecedentArgs = {
  input: DecedentInput;
};


export type MutationcreateDocumentAgreementArgs = {
  input: CreateDocumentInput;
};


export type MutationcreateEntityHouseAccountArgs = {
  accountActivity: AccountActivity;
  accountDisclosures: AccountDisclosures;
  accountInformation: AccountInformation;
  additionalAccounts: AdditionalAccounts;
  authorizedSigner: AuthorizedSigner;
  beneficialOwners: ReadonlyArray<OwnerOrOfficer>;
  entityOfficers: ReadonlyArray<OwnerOrOfficer>;
  legalEntityIdentifier: Scalars['String'];
  membersAndManagers: ReadonlyArray<MembersAndManagers>;
  resolutionAdoptionDate: Scalars['String'];
};


export type MutationcreateGroupArgs = {
  category?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
};


export type MutationcreateHeldAwayLinkArgs = {
  input: CreateHeldAwayLinkInput;
};


export type MutationcreateHouseAccountArgs = {
  finraAffiliation?: InputMaybe<IndividualHouseAccountFinraAffiliationInput>;
  userId?: InputMaybe<Scalars['ID']>;
};


export type MutationcreateHouseAccountV2Args = {
  input: CreateHouseAccountInput;
};


export type MutationcreateInvoiceBundleUrlArgs = {
  input: InvoiceBundleUrlInput;
};


export type MutationcreateMMAArgs = {
  isReplacing?: InputMaybe<Scalars['Boolean']>;
  mma: MMAInput;
};


export type MutationcreateMfaSettingsArgs = {
  input: UpdateAndCreateMfaSettingsInput;
};


export type MutationcreateMfaSettingsWithCredentialsArgs = {
  input: UpdateAndCreateMfaSettingsInput;
};


export type MutationcreateMinorArgs = {
  input: MinorInput;
};


export type MutationcreateModelArgs = {
  baseModelId?: InputMaybe<Scalars['String']>;
  benchmarkId?: InputMaybe<Scalars['ID']>;
  cashTarget?: InputMaybe<Scalars['Float']>;
  description?: InputMaybe<Scalars['String']>;
  holdings: ReadonlyArray<ModelHoldingInput>;
  holdingsType?: InputMaybe<HoldingType>;
  name: Scalars['String'];
};


export type MutationcreateOrUpdateHubSpotContactArgs = {
  email: Scalars['String'];
  fields?: InputMaybe<HubspotFields>;
  fieldsNeedingDate?: InputMaybe<HubspotFields>;
};


export type MutationcreateOrUpdateHubSpotTeamContactArgs = {
  user?: InputMaybe<TeamMemberHS>;
  username?: InputMaybe<Scalars['String']>;
};


export type MutationcreateOrUpdateZendeskContactArgs = {
  contact: ZendeskContact;
  updateFieldsNeedingDate: FieldsNeedingDateInput;
};


export type MutationcreatePendingUsernameArgs = {
  userId?: InputMaybe<Scalars['ID']>;
  username: Scalars['String'];
};


export type MutationcreatePlanDetailsArgs = {
  input: PlanDetailsInput;
};


export type MutationcreatePortfolioArgs = {
  cashSettings?: InputMaybe<CashSettingsInput>;
  cashTarget?: InputMaybe<Scalars['Float']>;
  drift: DriftSettingsInput;
  minCash?: InputMaybe<Scalars['Float']>;
  models?: InputMaybe<ReadonlyArray<InputMaybe<PortfolioModelInput>>>;
  name: Scalars['String'];
};


export type MutationcreateRepcodeArgs = {
  input?: InputMaybe<CreateRepcodeInput>;
};


export type MutationcreateReportScheduleAssignmentsArgs = {
  input: CreateReportScheduleAssignmentsInput;
};


export type MutationcreateScheduleAssignmentArgs = {
  input: CreateScheduleAssignmentInput;
};


export type MutationcreateSchedulesArgs = {
  schedules: CreateSchedulesInput;
};


export type MutationcreateSingleParticipantArgs = {
  input: SingleParticipantInput;
};


export type MutationcreateStripeCustomerArgs = {
  stripeCustomer: StripeCustomerInput;
  userId: Scalars['ID'];
};


export type MutationcreateSupportUserArgs = {
  input: SupportUserInput;
};


export type MutationcreateTradeOrderArgs = {
  input: CreateTradeOrderInput;
};


export type MutationcreateTrustedContactArgs = {
  email?: InputMaybe<Scalars['String']>;
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  phone?: InputMaybe<Scalars['String']>;
};


export type MutationdeclineMMAArgs = {
  mmaID: Scalars['ID'];
};


export type MutationdeclineMatchConsumerArgs = {
  input?: InputMaybe<MatchRequestInput>;
};


export type MutationdeleteAcatDraftArgs = {
  input: DraftDeleteInput;
};


export type MutationdeleteAccountBeneficiariesArgs = {
  input: DeleteAccountBeneficiariesInput;
};


export type MutationdeleteAccountCashSettingsArgs = {
  financialAccountId: Scalars['ID'];
  input?: InputMaybe<CashSettingsDeleteInput>;
};


export type MutationdeleteAccountDraftArgs = {
  accountStateId: Scalars['ID'];
};


export type MutationdeleteAccountDraftsBulkArgs = {
  accountStateIds: ReadonlyArray<Scalars['ID']>;
};


export type MutationdeleteAccountTaxSettingsArgs = {
  financialAccountId: Scalars['ID'];
};


export type MutationdeleteAdvisorDocumentArgs = {
  documentId?: InputMaybe<Scalars['ID']>;
};


export type MutationdeleteBenchmarkArgs = {
  id: Scalars['ID'];
};


export type MutationdeleteBenchmarkAssignmentArgs = {
  assignment: DeleteBenchmarkAssignmentInput;
};


export type MutationdeleteBeneficiariesArgs = {
  financialAccountId?: InputMaybe<Scalars['ID']>;
  ids: ReadonlyArray<Scalars['ID']>;
};


export type MutationdeleteBeneficiaryArgs = {
  financialAccountId?: InputMaybe<Scalars['ID']>;
  id: Scalars['ID'];
};


export type MutationdeleteBillingPeriodArgs = {
  id: Scalars['ID'];
  scheduleAssignmentId: Scalars['ID'];
};


export type MutationdeleteColorDraftArgs = {
  input: DraftDeleteInput;
};


export type MutationdeleteContactArgs = {
  householdId: Scalars['ID'];
  userId: Scalars['ID'];
};


export type MutationdeleteCorpMemberArgs = {
  input: DeleteCorpMember;
};


export type MutationdeleteDocumentAgreementArgs = {
  input: DeleteDocumentAgreementInput;
};


export type MutationdeleteDocumentFromAccountArgs = {
  clientId: Scalars['ID'];
  documentId: Scalars['ID'];
};


export type MutationdeleteDocumentTemplateArgs = {
  input: DeleteDocumentTemplateInput;
};


export type MutationdeleteFromGroupArgs = {
  finacctId: Scalars['ID'];
};


export type MutationdeleteGroupArgs = {
  deleteStatus: ClientDeletionInfo;
  groupId: Scalars['String'];
};


export type MutationdeleteInvitedUserArgs = {
  email: Scalars['String'];
};


export type MutationdeleteInvoiceArgs = {
  invoiceId: Scalars['ID'];
  orgId?: InputMaybe<Scalars['ID']>;
};


export type MutationdeleteMemberArgs = {
  id: Scalars['ID'];
};


export type MutationdeleteModelArgs = {
  id: Scalars['ID'];
};


export type MutationdeletePendingUsernameArgs = {
  userId?: InputMaybe<Scalars['ID']>;
};


export type MutationdeletePortfolioArgs = {
  id: Scalars['ID'];
};


export type MutationdeleteRepCodeArgs = {
  input: DeleteRepCodeInput;
};


export type MutationdeleteScheduleAssignmentsArgs = {
  id: Scalars['ID'];
};


export type MutationdeleteSchedulesArgs = {
  id: Scalars['ID'];
  numberOfAccounts: Scalars['Int'];
};


export type MutationdeleteTrustedDeviceArgs = {
  fingerprintHash: Scalars['String'];
};


export type MutationdisableOrgPartnerIntegrationArgs = {
  input: DisableOrgPartnerIntegrationInput;
};


export type MutationdisconnectPartnerIntegrationArgs = {
  input: DisconnectPartnerIntegrationInput;
};


export type MutationeditAccountBeneficiaryAllocationArgs = {
  input: EditAccountBeneficiaryAllocationInput;
};


export type MutationeditBeneficiaryArgs = {
  address?: InputMaybe<Scalars['String']>;
  category?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  companyName?: InputMaybe<Scalars['String']>;
  dateOfBirth?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  financialAccountId?: InputMaybe<Scalars['String']>;
  firstName?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  lastName?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  relationship?: InputMaybe<Scalars['String']>;
  state?: InputMaybe<Scalars['String']>;
  taxIdentificationNumber?: InputMaybe<Scalars['String']>;
  type: BeneficiaryType;
  zipCode?: InputMaybe<Scalars['String']>;
};


export type MutationeditBeneficiaryAllocationListArgs = {
  beneficiaries: ReadonlyArray<InputMaybe<BeneficiaryAllocation>>;
  financialAccountId: Scalars['ID'];
};


export type MutationeditHouseholdScheduleAssignmentsArgs = {
  input: ScheduleAssignmentInput;
};


export type MutationeditModelArgs = {
  benchmarkId?: InputMaybe<Scalars['ID']>;
  cashTarget?: InputMaybe<Scalars['Float']>;
  description?: InputMaybe<Scalars['String']>;
  holdings: ReadonlyArray<ModelHoldingInput>;
  holdingsType?: InputMaybe<HoldingType>;
  id: Scalars['ID'];
  name: Scalars['String'];
  rebalanceAsap?: InputMaybe<Scalars['Boolean']>;
  skipRebalance?: InputMaybe<Scalars['Boolean']>;
};


export type MutationeditModelAsyncArgs = {
  input: EditModelAsyncInput;
};


export type MutationeditPortfolioArgs = {
  cashSettings?: InputMaybe<CashSettingsInput>;
  cashTarget?: InputMaybe<Scalars['Float']>;
  drift: DriftSettingsInput;
  id: Scalars['ID'];
  minCash?: InputMaybe<Scalars['Float']>;
  models?: InputMaybe<ReadonlyArray<InputMaybe<PortfolioModelInput>>>;
  name: Scalars['String'];
  rebalanceAsap?: InputMaybe<Scalars['Boolean']>;
  skipRebalance?: InputMaybe<Scalars['Boolean']>;
};


export type MutationeditPortfolioAsyncArgs = {
  input: EditPortfolioAsyncInput;
};


export type MutationexecuteFeesReportArgs = {
  dateRange: ExecuteFeesReportDateRange;
};


export type MutationexecuteManualInvoiceArgs = {
  invoice?: InputMaybe<ExecuteManualInvoiceInput>;
};


export type MutationgenerateDocumentTemplateUrlArgs = {
  input: GenerateDocumentTemplateUrlInput;
};


export type MutationgenerateSignUrlArgs = {
  input: GenerateSignUrlInput;
};


export type MutationgetFundedDayArgs = {
  financialAccountIds: FundedDayInput;
  orgId?: InputMaybe<Scalars['ID']>;
};


export type MutationhandleProcessingActionArgs = {
  input: HandleProcessingActionInput;
};


export type MutationinitiateOrgPartnerIntegrationArgs = {
  input: InitiateOrgPartnerIntegrationInput;
};


export type MutationinviteClientArgs = {
  invite?: InputMaybe<InviteClientInput>;
};


export type MutationinviteUserArgs = {
  invite?: InputMaybe<InviteInput>;
};


export type MutationinviteUserResendArgs = {
  invite?: InputMaybe<InviteResendInput>;
};


export type MutationlinkExistingBeneficiaryArgs = {
  input: LinkBeneficiaryInput;
};


export type MutationloginArgs = {
  input: LoginWhereInput;
};


export type MutationloginWithCredentialsArgs = {
  input: LoginWhereInput;
};


export type MutationloginWithMfaArgs = {
  input: LoginWhereInput;
};


export type MutationmfaTokenRequestArgs = {
  mfaTokenRequest: MfaTokenRequest;
};


export type MutationmfaTokenRequestWithCredentialsArgs = {
  mfaTokenRequest: MfaTokenRequest;
};


export type MutationpatchDocumentTemplateArgs = {
  input: PatchDocumentTemplateInput;
};


export type MutationpayStripeSubscriptionArgs = {
  subscriptionId: Scalars['String'];
  whichInvoice?: InputMaybe<Scalars['String']>;
};


export type MutationreExportExecutedInvoicesArgs = {
  format?: InputMaybe<Scalars['String']>;
  periodId: Scalars['String'];
};


export type MutationreactivateSubscriptionArgs = {
  subscriptionId: Scalars['String'];
};


export type MutationreinviteBrokerageUserArgs = {
  id?: InputMaybe<Scalars['ID']>;
  invite?: InputMaybe<InviteResendInput>;
};


export type MutationrejectAcatDraftArgs = {
  input: AcatDraftStatusChangeInput;
};


export type MutationrelinkHeldAwayArgs = {
  input: RelinkHeldAwayInput;
};


export type MutationremoveFundingAccountArgs = {
  brokerageAccountId: Scalars['ID'];
  fundingAccountId: Scalars['ID'];
};


export type MutationrenameAccountArgs = {
  accountNickname: Scalars['String'];
  financialAccountId: Scalars['String'];
};


export type MutationrerunInvoicesArgs = {
  periodId: Scalars['String'];
};


export type MutationresendCorpApplicationToSecretaryArgs = {
  input: SecretaryReviewResendInput;
};


export type MutationresetPasswordArgs = {
  email: Scalars['String'];
};


export type MutationreuseExistingBeneficiaryArgs = {
  input: ReuseExistingBeneficiaryInput;
};


export type MutationrevokeMMAArgs = {
  mmaID: Scalars['ID'];
};


export type MutationrevokeThirdPartyAccessArgs = {
  input: RevokeThirdPartyAccessInput;
};


export type MutationsaveAccountStateArgs = {
  account?: InputMaybe<AccountInput>;
  groupId: Scalars['ID'];
  jointUserIds?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['ID']>>>;
  openingInPerson?: InputMaybe<Scalars['Boolean']>;
  state?: InputMaybe<State>;
  trustUserIds?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['ID']>>>;
  userId: Scalars['ID'];
};


export type MutationsaveAccountStatesBulkArgs = {
  input: SaveStateBulkInput;
};


export type MutationscheduleRebalanceArgs = {
  financialAccountId: Scalars['ID'];
  shouldRebalanceAsap?: InputMaybe<Scalars['Boolean']>;
};


export type MutationscheduledCashJournalTransferArgs = {
  amount: Scalars['Float'];
  endDate?: InputMaybe<Scalars['DateOnly']>;
  frequency?: InputMaybe<DepositFrequency>;
  fromBrokerageAccountId: Scalars['ID'];
  iraContribution?: InputMaybe<IraJournalContribution>;
  iraDistribution?: InputMaybe<IraJournalDistribution>;
  journalAccountLinkUuid: Scalars['String'];
  startDate: Scalars['DateOnly'];
  toBrokerageAccountId: Scalars['ID'];
};


export type MutationscheduledClientDepositArgs = {
  amount: Scalars['String'];
  currency: Scalars['String'];
  endDate?: InputMaybe<Scalars['DateOnly']>;
  frequency?: InputMaybe<DepositFrequency>;
  iraContribution?: InputMaybe<IraContributionType>;
  iraContributionReason?: InputMaybe<ContributionReason>;
  sourceFinancialAccountId: Scalars['ID'];
  startDate: Scalars['DateOnly'];
  targetFinancialAccountId: Scalars['ID'];
};


export type MutationscheduledClientWithdrawArgs = {
  amount: Scalars['String'];
  currency: Scalars['String'];
  endDate?: InputMaybe<Scalars['DateOnly']>;
  frequency?: InputMaybe<DepositFrequency>;
  iraDistribution?: InputMaybe<IraDistribution>;
  sourceFinancialAccountId: Scalars['ID'];
  startDate: Scalars['DateOnly'];
  targetFinancialAccountId: Scalars['ID'];
};


export type MutationsendCashAccountInterestEmailArgs = {
  input: CashAccountInterestInput;
};


export type MutationsendCorpApplicationToSecretaryArgs = {
  input: SecretaryReviewInput;
};


export type MutationsignMarketplaceAgreementArgs = {
  input?: InputMaybe<SignMarketplaceAgreementInput>;
};


export type MutationspliceAccountHistoryArgs = {
  destinationFinancialAccountId: Scalars['ID'];
  sourceFinancialAccountId: Scalars['ID'];
  spliceDate: Scalars['DateOnly'];
};


export type MutationsubmitActivityReportRequestArgs = {
  activityReportRequestInput?: InputMaybe<ActivityReportRequestInput>;
};


export type MutationsubmitAdvisorIdentificationArgs = {
  input: SubmitAdvisorIdentificationInput;
};


export type MutationsubmitBrokerageApplicationArgs = {
  organization: ApplicationInput;
};


export type MutationsubmitHubSpotFormByIdArgs = {
  input: SubmitHubSpotFormInput;
};


export type MutationsubmitInvestmentSummaryReportArgs = {
  investmentSummaryParams: InvestmentSummaryParams;
  orgId?: InputMaybe<Scalars['ID']>;
};


export type MutationsubmitPaymentMethodArgs = {
  userInfo: SubmitPaymentMethodInput;
};


export type MutationsubscribeStripeCustomerArgs = {
  stripeSubscription: StripeSubscriptionInput;
};


export type MutationsubscribeToNotificationArgs = {
  notificationPreferenceId: Scalars['ID'];
};


export type MutationtoggleMfaEnabledArgs = {
  input: ToggleMfaEnabledInput;
};


export type MutationtoggleTrustedDeviceArgs = {
  fingerprintHash: Scalars['String'];
};


export type MutationtransferAccountsBetweenRepCodesArgs = {
  input: TransferAccountsInput;
};


export type MutationtrustDeviceArgs = {
  input: SafeTrustDeviceInput;
};


export type MutationtrustDeviceWithCredentialsArgs = {
  input: SafeTrustDeviceInput;
};


export type MutationunAssignPortfolioArgs = {
  financialAccountIds: ReadonlyArray<Scalars['ID']>;
  portfolioId: Scalars['ID'];
};


export type MutationunSpliceAccountHistoryArgs = {
  destinationFinancialAccountId: Scalars['ID'];
  sourceFinancialAccountId: Scalars['ID'];
};


export type MutationunlinkExternalAccountArgs = {
  financialAccountId: Scalars['ID'];
};


export type MutationunlinkHeldAwayArgs = {
  input: UnlinkHeldAwayInput;
};


export type MutationunsubscribeToNotificationArgs = {
  notificationPreferenceId: Scalars['ID'];
};


export type MutationupdateAcatDraftArgs = {
  input: AcatDraftUpdateInput;
};


export type MutationupdateAccountAddressArgs = {
  address: AdvisorAddressInput;
  profileId: Scalars['ID'];
  skipDW?: InputMaybe<Scalars['Boolean']>;
  userId: Scalars['ID'];
};


export type MutationupdateAccountAddressesArgs = {
  input: AccountManagementAddressInput;
};


export type MutationupdateAccountBeneficiariesDesignationArgs = {
  input: UpdateAccountBeneficiariesDesignationInput;
};


export type MutationupdateAccountBeneficiaryArgs = {
  input: UpdateAccountBeneficiaryInput;
};


export type MutationupdateAccountCashSettingsArgs = {
  financialAccountId: Scalars['ID'];
  input: CashSettingsInput;
  rebalanceAsap?: InputMaybe<Scalars['Boolean']>;
};


export type MutationupdateAccountEmailArgs = {
  input: AccountManagementEmailInput;
};


export type MutationupdateAccountManagementArgs = {
  input: AccountManagementInput;
};


export type MutationupdateAccountPhoneNumberArgs = {
  input: AccountManagementPhoneNumberInput;
};


export type MutationupdateAccountProfileArgs = {
  profile: UserInfoInput;
  skipDW?: InputMaybe<Scalars['Boolean']>;
  userId: Scalars['ID'];
};


export type MutationupdateAccountStateArgs = {
  account?: InputMaybe<AccountInput>;
  financialAccountId?: InputMaybe<Scalars['ID']>;
  groupId?: InputMaybe<Scalars['ID']>;
  id: Scalars['ID'];
  jointUserIds?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['ID']>>>;
  openingInPerson?: InputMaybe<Scalars['Boolean']>;
  state: State;
  trustUserIds?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['ID']>>>;
  userId: Scalars['ID'];
  userSwapped?: InputMaybe<Scalars['Boolean']>;
};


export type MutationupdateAccountStatesBulkArgs = {
  input: UpdateStateBulkInput;
};


export type MutationupdateAccountTaxSettingsArgs = {
  input: TaxSettingsInput;
};


export type MutationupdateAdoptingEmployerArgs = {
  input: AdoptingEmployerInput;
};


export type MutationupdateAdvisorADVFormsArgs = {
  advisorADVForms: ADVFormsInput;
};


export type MutationupdateAdvisorAddressArgs = {
  address: AdvisorAddressInput;
  id: Scalars['ID'];
};


export type MutationupdateAdvisorMarketplaceProfileDraftArgs = {
  input?: InputMaybe<AdvisorMarketplaceProfileDraftInput>;
};


export type MutationupdateAllNotificationStatusesArgs = {
  input: UpdateAllNotificationStatusesInput;
};


export type MutationupdateAllocationForGroupArgs = {
  accountAllocations: ReadonlyArray<AccountAllocationInput>;
  allocationType: AllocationType;
  debitAccountOverrides: ReadonlyArray<DebitAccountOverrideInput>;
  groupId: Scalars['ID'];
  heldAwayAccountAllocations?: InputMaybe<ReadonlyArray<AccountAllocationInput>>;
};


export type MutationupdateBenchmarkArgs = {
  benchmark: UpdateBenchmarkInput;
};


export type MutationupdateBillingGroupArgs = {
  input: UpdateBillingGroupInput;
};


export type MutationupdateBillingPeriodArgs = {
  BillingPeriod?: InputMaybe<BillingPeriodInput>;
};


export type MutationupdateClientArgs = {
  input: UpdateClientInput;
};


export type MutationupdateClientAddressArgs = {
  address: AdvisorAddressInput;
  id: Scalars['ID'];
};


export type MutationupdateClientDocumentReviewStatusArgs = {
  documentId: Scalars['ID'];
};


export type MutationupdateColorDraftArgs = {
  input: ColorDraftUpdateInput;
};


export type MutationupdateContactArgs = {
  contact: UpdateContactInput;
  groupId: Scalars['ID'];
};


export type MutationupdateContactEmailArgs = {
  email: Scalars['String'];
  userId: Scalars['ID'];
};


export type MutationupdateCorpMemberArgs = {
  input: UpdateCorpMember;
};


export type MutationupdateCorporationArgs = {
  input: UpdateCorporationInput;
};


export type MutationupdateDocumentTemplateArgs = {
  input: UpdateDocumentTemplateInput;
};


export type MutationupdateEntityHouseAccountArgs = {
  businessAddress: Scalars['String'];
  businessCity: Scalars['String'];
  businessPhone: Scalars['String'];
  businessState: Scalars['String'];
  businessZipCode: Scalars['String'];
  email: Scalars['String'];
  financialAccountId: Scalars['ID'];
};


export type MutationupdateExcludeDaysBeforeFundingArgs = {
  accountId: Scalars['ID'];
  orgId?: InputMaybe<Scalars['ID']>;
  updateExcludeDaysBeforeFunding: UpdateExcludeDaysBeforeFundingInput;
};


export type MutationupdateFeeAllocationArgs = {
  input: UpdateFeeAllocationInput;
};


export type MutationupdateFeeRoutingHouseholdPreferencesArgs = {
  templates: UpdateFeeRoutingHouseholdInput;
};


export type MutationupdateFeeRoutingPreferencesArgs = {
  templates: ReadonlyArray<UpdateFeeRoutingInput>;
};


export type MutationupdateFinancialAccountBillingDateArgs = {
  accountId: Scalars['ID'];
  billingDates: BillingDates;
};


export type MutationupdateFinancialAccountPropertiesArgs = {
  input: UpdateAccountPropertiesInput;
};


export type MutationupdateFormADVUrlsArgs = {
  formADVUrls: FormADVUrlsInput;
};


export type MutationupdateFundingAccountArgs = {
  accountName?: InputMaybe<Scalars['String']>;
  accountNumber?: InputMaybe<Scalars['String']>;
  accountType?: InputMaybe<Scalars['String']>;
  authenticationStatus?: InputMaybe<PlaidAuthenticationStatus>;
  fundingAccountId: Scalars['ID'];
  institutionName?: InputMaybe<Scalars['String']>;
  userId?: InputMaybe<Scalars['ID']>;
};


export type MutationupdateGroupArgs = {
  assignment: ReadonlyArray<AssignmentInput>;
  groupId: Scalars['String'];
};


export type MutationupdateGroupNameArgs = {
  groupId: Scalars['String'];
  name: Scalars['String'];
};


export type MutationupdateGroupSchedulesByAccountArgs = {
  groupSchedules: ReadonlyArray<UpdateGroupSchedulesByAccountInput>;
};


export type MutationupdateHubSpotContactByIdArgs = {
  contactInfo: UpdateHubspotContactByIdInput;
};


export type MutationupdateIntegrationArgs = {
  action: IntegrationAction;
  authCode?: InputMaybe<Scalars['String']>;
  integrationPartnerCode: IntegrationPartnerCode;
  partnerUserId?: InputMaybe<Scalars['String']>;
};


export type MutationupdateJointOwnerInfoArgs = {
  input: JointOwnerInfoUpdateInput;
};


export type MutationupdateJointUserProfileArgs = {
  accountStateId: Scalars['ID'];
  userProfile: UserInfoInput;
  userProfileId: Scalars['ID'];
};


export type MutationupdateMatchConsumerArgs = {
  input?: InputMaybe<MatchUpdateInput>;
};


export type MutationupdateMemberArgs = {
  userInfo: EditTeamMemberInput;
};


export type MutationupdateMfaSettingsArgs = {
  input: UpdateAndCreateMfaSettingsInput;
};


export type MutationupdateMfaSettingsWithCredentialsArgs = {
  input: UpdateAndCreateMfaSettingsInput;
};


export type MutationupdateMinorArgs = {
  input: MinorInput;
};


export type MutationupdateNotificationPreferencesArgs = {
  input: UpdateNotificationPreferencesInput;
};


export type MutationupdateNotificationStatusesArgs = {
  input: UpdateNotificationStatusesInput;
};


export type MutationupdateOnboardStatusArgs = {
  onboardStatus: OnboardStatus;
};


export type MutationupdateOrganizationArgs = {
  company: CompanyInput;
};


export type MutationupdateOrganizationPreferencesArgs = {
  aumStrategy: Scalars['String'];
  invoiceView: Scalars['String'];
  orgId?: InputMaybe<Scalars['ID']>;
};


export type MutationupdatePlanDetailsArgs = {
  input: PlanDetailsInput;
};


export type MutationupdateRepcodeArgs = {
  input: UpdateRepcodeInput;
};


export type MutationupdateRepcodeAsTheDefaultArgs = {
  input: UpdateRepcodeAsTheDefaultInput;
};


export type MutationupdateReportScheduleAssignmentsArgs = {
  input: UpdateReportScheduleAssignmentsInput;
};


export type MutationupdateScheduleAssignmentArgs = {
  input: UpdateScheduleAssignmentInput;
};


export type MutationupdateScheduleAssignmentByAccountArgs = {
  financialAccounts: ReadonlyArray<UpdateSchedulesAssignmentsByAccountsInput>;
};


export type MutationupdateScheduleAssignmentByGroupArgs = {
  schedules: ReadonlyArray<ScheduleInput>;
};


export type MutationupdateScheduleAssignmentsArgs = {
  assignmentInfo: UpdateSchedulesAssignmentsInput;
  startDate?: InputMaybe<Scalars['DateOnly']>;
};


export type MutationupdateScheduleAssignmentsDateArgs = {
  endDate?: InputMaybe<Scalars['String']>;
  groupId: Scalars['String'];
  scheduleAssignmentIds: ReadonlyArray<Scalars['String']>;
  startDate: Scalars['String'];
};


export type MutationupdateSchedulesArgs = {
  schedules: UpdateSchedulesInput;
};


export type MutationupdateSecurityConstraintsArgs = {
  input: UpdateSecurityConstraintsInput;
};


export type MutationupdateSingleParticipantArgs = {
  input: SingleParticipantInput;
};


export type MutationupdateStripeCustomerArgs = {
  stripeCustomer: UpdateStripeCustomerInput;
};


export type MutationupdateTeamMemberInfoArgs = {
  userInfo: UserInfoInput;
};


export type MutationupdateTrustedContactArgs = {
  email?: InputMaybe<Scalars['String']>;
  firstName: Scalars['String'];
  id: Scalars['ID'];
  lastName: Scalars['String'];
  phone?: InputMaybe<Scalars['String']>;
};


export type MutationupdateUserInGroupArgs = {
  user: UpdateUserInput;
};


export type MutationupdateUserInfoArgs = {
  userID?: InputMaybe<Scalars['ID']>;
  userInfo: UserInfoInput;
};


export type MutationuploadDocumentTemplateArgs = {
  input: UploadDocumentTemplateInput;
};


export type MutationuploadFeesArgs = {
  input: UploadFeesInput;
};


export type MutationuploadOrganizationLogoArgs = {
  input: UploadOrganizationLogoInput;
};


export type MutationuploadPhotoArgs = {
  file?: InputMaybe<Scalars['Upload']>;
  userID?: InputMaybe<Scalars['ID']>;
};


export type MutationvalidateAccountFromStateArgs = {
  input: CreateAccountFromStateInput;
};


export type MutationvalidateAccountFromStateBulkArgs = {
  input: CreateAccountFromStateBulkInput;
};


export type MutationvalidateCashSettingsArgs = {
  validationInput: ValidateUpdateModelInput;
};


export type MutationvalidateCreatePortfolioArgs = {
  validationInput: ValidateCreatePortfolioInput;
};


export type MutationvalidateMfaTokenArgs = {
  validateMfaTokenRequest: ValidateMfaTokenRequest;
};


export type MutationvalidateMfaTokenWithCredentialsArgs = {
  validateMfaTokenRequest: ValidateMfaTokenRequest;
};


export type MutationvalidateUpdateModelArgs = {
  validationInput: ValidateUpdateModelInput;
};


export type MutationvalidateUpdatePortfolioArgs = {
  validationInput: ValidateUpdatePortfolioInput;
};


export type MutationverifyIdentityWithQuestionArgs = {
  input: IdentityVerificationResponseInput;
};


export type MutationverifyNewUserArgs = {
  userInput: UserInfoInput;
};


export type MutationverifyPendingUsernameArgs = {
  jwt: Scalars['String'];
};

export type NameDetails = {
  readonly familyName: Scalars['String'];
  readonly givenName: Scalars['String'];
  readonly legalName: Scalars['String'];
};

export type NewAdvisor = {
  readonly companyName?: InputMaybe<Scalars['String']>;
  readonly email?: InputMaybe<Scalars['String']>;
  readonly firstName?: InputMaybe<Scalars['String']>;
  readonly hasOrganization?: InputMaybe<Scalars['Boolean']>;
  readonly id: Scalars['ID'];
  readonly lastName?: InputMaybe<Scalars['String']>;
};

export type NewTeamMember = {
  readonly companyName: Scalars['String'];
  readonly firstName: Scalars['String'];
  readonly lastName: Scalars['String'];
  readonly profileType: UserRole;
};

export type Notification = {
  readonly __typename?: 'Notification';
  readonly appUrl?: Maybe<Scalars['String']>;
  readonly body?: Maybe<Scalars['String']>;
  readonly createdAt?: Maybe<Scalars['DateTime']>;
  readonly eventCanonicalName?: Maybe<NotificationEventCanonicalName>;
  readonly id: Scalars['ID'];
  readonly severity?: Maybe<NotificationSeverity>;
  readonly status?: Maybe<NotificationStatus>;
  readonly title?: Maybe<Scalars['String']>;
};

export type NotificationCategory = {
  readonly __typename?: 'NotificationCategory';
  readonly canonicalName?: Maybe<NotificationCategoryCanonicalName>;
  readonly id: Scalars['ID'];
  readonly name?: Maybe<Scalars['String']>;
  readonly notificationEvents?: Maybe<ReadonlyArray<NotificationEvent>>;
};

export enum NotificationCategoryCanonicalName {
  client_activity = 'client_activity',
  move_money_authorization = 'move_money_authorization',
  onboarding = 'onboarding',
  trading = 'trading'
}

export type NotificationEvent = {
  readonly __typename?: 'NotificationEvent';
  readonly canonicalName?: Maybe<NotificationEventCanonicalName>;
  readonly categoryId: Scalars['ID'];
  readonly description?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly name?: Maybe<Scalars['String']>;
  readonly notificationPreferences?: Maybe<ReadonlyArray<NotificationPreference2>>;
  readonly unreadCount?: Maybe<Scalars['Int']>;
};

export enum NotificationEventCanonicalName {
  acat_complete = 'acat_complete',
  account_opened = 'account_opened',
  advisor_bank_account_added = 'advisor_bank_account_added',
  client_withdrawal_completed_advisor = 'client_withdrawal_completed_advisor',
  clients_contact_email_has_changed = 'clients_contact_email_has_changed',
  deposit_funds_available = 'deposit_funds_available',
  rebal_suspended_accounts_email = 'rebal_suspended_accounts_email'
}

export enum NotificationMethod {
  EMAIL = 'EMAIL',
  INAPP = 'INAPP'
}

export type NotificationPreference = {
  readonly __typename?: 'NotificationPreference';
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
  readonly optIn: Scalars['Boolean'];
  readonly subtitle: Scalars['String'];
  readonly title: Scalars['String'];
};

export type NotificationPreference2 = {
  readonly __typename?: 'NotificationPreference2';
  readonly categoryCanonicalName?: Maybe<NotificationCategoryCanonicalName>;
  readonly categoryId: Scalars['ID'];
  readonly categoryName?: Maybe<Scalars['String']>;
  readonly enabled: Scalars['Boolean'];
  readonly eventCanonicalName?: Maybe<NotificationEventCanonicalName>;
  readonly eventDescription?: Maybe<Scalars['String']>;
  readonly eventId: Scalars['ID'];
  readonly eventName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly method: NotificationMethod;
};

export type NotificationPreferenceInput = {
  readonly enabled: Scalars['Boolean'];
  readonly eventId: Scalars['ID'];
  readonly method: NotificationMethod;
};

export type NotificationPreferenceMutationResponse = {
  readonly __typename?: 'NotificationPreferenceMutationResponse';
  readonly notificationPreferences?: Maybe<ReadonlyArray<NotificationPreference2>>;
};

export enum NotificationSeverity {
  NORMAL = 'NORMAL',
  SEVERE = 'SEVERE'
}

export enum NotificationStatus {
  INACTIVE = 'INACTIVE',
  READ = 'READ',
  UNREAD = 'UNREAD'
}

export type NotificationStatusesMutationResponse = {
  readonly __typename?: 'NotificationStatusesMutationResponse';
  readonly notificationEvents?: Maybe<ReadonlyArray<NotificationEvent>>;
  readonly notifications?: Maybe<ReadonlyArray<Notification>>;
};

export type NotificationsResponse = {
  readonly __typename?: 'NotificationsResponse';
  readonly content?: Maybe<ReadonlyArray<Notification>>;
  readonly last?: Maybe<Scalars['Boolean']>;
};

export type NotificationsTotalUnreadResponse = {
  readonly __typename?: 'NotificationsTotalUnreadResponse';
  readonly unreadCount?: Maybe<Scalars['Int']>;
};

export type NotificationsWhereInput = {
  readonly before?: InputMaybe<Scalars['ID']>;
  readonly eventCanonicalName?: InputMaybe<NotificationEventCanonicalName>;
  readonly size: Scalars['Int'];
  readonly status?: InputMaybe<NotificationStatus>;
};

export type NumberRangeFilter = {
  readonly max?: InputMaybe<Scalars['Float']>;
  readonly min?: InputMaybe<Scalars['Float']>;
};

export enum OnboardStatus {
  BROKERAGE_ACCEPTED = 'BROKERAGE_ACCEPTED',
  BROKERAGE_APPLYING = 'BROKERAGE_APPLYING',
  BROKERAGE_PENDING = 'BROKERAGE_PENDING',
  BROKERAGE_REJECTED = 'BROKERAGE_REJECTED',
  PAYMENT_ADDED = 'PAYMENT_ADDED',
  UNKNOWN = 'UNKNOWN',
  UNREGISTERED = 'UNREGISTERED',
  UNVERIFIED = 'UNVERIFIED'
}

export type OnboardStatusResponse = {
  readonly __typename?: 'OnboardStatusResponse';
  /** @deprecated Move to Advisor2 type. */
  readonly onboardStatus?: Maybe<OnboardStatus>;
};

export type OnboardUserOutput = {
  readonly __typename?: 'OnboardUserOutput';
  readonly companyName?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly missingOrgInfo?: Maybe<Scalars['Boolean']>;
};

export type OrderActivity = {
  readonly __typename?: 'OrderActivity';
  readonly activityType: ActivityType;
  readonly price?: Maybe<Scalars['Float']>;
  readonly quantity?: Maybe<Scalars['Float']>;
  readonly timeStamp: Scalars['DateTime'];
};

export type OrderBy = {
  readonly direction: Scalars['String'];
  readonly field: Scalars['String'];
};

export type OrderDetail = {
  readonly __typename?: 'OrderDetail';
  readonly amountCash?: Maybe<Scalars['Float']>;
  readonly averagePrice?: Maybe<Scalars['Float']>;
  readonly created: Scalars['String'];
  readonly cumulativeQuantity?: Maybe<Scalars['Float']>;
  readonly fees?: Maybe<Scalars['Float']>;
  readonly id: Scalars['ID'];
  readonly orderNo: Scalars['String'];
  readonly quantity: Scalars['Float'];
  readonly side: OrderSide;
  readonly status: Scalars['String'];
  readonly symbol: Scalars['String'];
  readonly totalOrderAmount?: Maybe<Scalars['Float']>;
  readonly type: Scalars['String'];
};

export enum OrderEntryType {
  NOTIONAL = 'NOTIONAL',
  SHARES = 'SHARES'
}

export type OrderItem = {
  readonly __typename?: 'OrderItem';
  readonly actionType: Scalars['String'];
  readonly activities?: Maybe<ReadonlyArray<OrderActivity>>;
  readonly brokerageAccountUuid: Scalars['String'];
  readonly correlationId: Scalars['String'];
  readonly countryCode: Scalars['String'];
  readonly createdBy: Scalars['String'];
  readonly createdDate: Scalars['String'];
  readonly currencyCode: Scalars['String'];
  readonly cusip?: Maybe<Scalars['String']>;
  readonly executionRoute: Scalars['String'];
  readonly instrumentId: Scalars['String'];
  readonly instrumentType: Scalars['String'];
  readonly name?: Maybe<Scalars['String']>;
  readonly notes?: Maybe<Scalars['String']>;
  readonly orderType?: Maybe<OrderType>;
  readonly orderUuid: Scalars['String'];
  readonly partialFill: Scalars['Boolean'];
  readonly pendingCancel?: Maybe<Scalars['Boolean']>;
  readonly requestedPrice: Scalars['Float'];
  readonly requestedQuantity: Scalars['Float'];
  readonly settlementDate?: Maybe<Scalars['String']>;
  readonly settlementPrice?: Maybe<Scalars['Float']>;
  readonly settlementQuantity?: Maybe<Scalars['Float']>;
  readonly source: Scalars['String'];
  readonly status: Scalars['String'];
  readonly symbol: Scalars['String'];
  readonly updatedBy: Scalars['String'];
  readonly updatedDate: Scalars['String'];
};

export type OrderPage = {
  readonly __typename?: 'OrderPage';
  readonly items: ReadonlyArray<OrderItem>;
  readonly page: Scalars['Int'];
  readonly size: Scalars['Int'];
  readonly total: Scalars['Int'];
};

export enum OrderSide {
  BUY = 'BUY',
  SELL = 'SELL'
}

export enum OrderType {
  LIMIT = 'LIMIT',
  MARKET = 'MARKET',
  STOP = 'STOP'
}

export type OrgInfo = {
  readonly companyName: Scalars['String'];
  readonly firstName: Scalars['String'];
  readonly lastName: Scalars['String'];
  readonly profileType: UserRole;
};

export type OrgIntegration = {
  readonly __typename?: 'OrgIntegration';
  readonly id?: Maybe<Scalars['String']>;
  readonly metadata?: Maybe<ReadonlyArray<OrgIntegrationMetadata>>;
  readonly partnerCode?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
  readonly type?: Maybe<Scalars['String']>;
};

export type OrgIntegrationMetadata = {
  readonly __typename?: 'OrgIntegrationMetadata';
  readonly id?: Maybe<Scalars['String']>;
  readonly key?: Maybe<Scalars['String']>;
  readonly value?: Maybe<Scalars['String']>;
};

export type OrgPartnerIntegrationInputFilter = {
  readonly status: Scalars['String'];
};

export type Organization = {
  readonly __typename?: 'Organization';
  readonly address: AddressOutput;
  readonly affiliatedOrEmployedFinra: Scalars['Boolean'];
  readonly canCreateAdvisorMarketplaceProfile: Scalars['Boolean'];
  readonly canCreateDfaIntegration: Scalars['Boolean'];
  readonly canCreateFeeSchedule: Scalars['Boolean'];
  readonly canCreateHouseAccount: Scalars['Boolean'];
  readonly canCreateIntegration: Scalars['Boolean'];
  readonly canCreatePortfolio: Scalars['Boolean'];
  readonly canCreateTeamMember: Scalars['Boolean'];
  readonly canCreateTrades: Scalars['Boolean'];
  readonly canDeleteFeeSchedule: Scalars['Boolean'];
  readonly canListRepCodes: Scalars['Boolean'];
  readonly canListTeamMember: Scalars['Boolean'];
  readonly canReadAltruistSubscription: Scalars['Boolean'];
  readonly canReadCompanyInfo: Scalars['Boolean'];
  readonly canReadDfaIntegration: Scalars['Boolean'];
  readonly canReadFeeSchedule: Scalars['Boolean'];
  readonly canReadHouseAccount: Scalars['Boolean'];
  readonly canReadIntegration: Scalars['Boolean'];
  readonly canReadListIntegration: Scalars['Boolean'];
  readonly canReadListPortfolio: Scalars['Boolean'];
  readonly canReadPaymentInfo: Scalars['Boolean'];
  readonly canRunFeeBilling: Scalars['Boolean'];
  readonly canUpdateAltruistSubscription: Scalars['Boolean'];
  readonly canUpdateCompanyInfo: Scalars['Boolean'];
  readonly canUpdateFeeSchedule: Scalars['Boolean'];
  readonly canUpdateFormADVUrls: Scalars['Boolean'];
  readonly canUpdateHouseAccount: Scalars['Boolean'];
  readonly canUpdateIntegration: Scalars['Boolean'];
  readonly canUpdatePaymentInfo: Scalars['Boolean'];
  readonly canViewOrgDocumentTemplates: Scalars['Boolean'];
  readonly id: Scalars['ID'];
  readonly isAltruistClearing: Scalars['Boolean'];
  /** @deprecated Isn't needed anymore. Check datadog for usage. */
  readonly isTeamsV2Enabled: Scalars['Boolean'];
  readonly logo?: Maybe<OrganizationLogo>;
  readonly name: Scalars['String'];
  readonly repCode?: Maybe<ClearingRepCode>;
};

export type OrganizationInfo = {
  readonly __typename?: 'OrganizationInfo';
  readonly affiliatedOrEmployedFinra?: Maybe<Scalars['Boolean']>;
  readonly assetStoreId?: Maybe<Scalars['ID']>;
  readonly brand?: Maybe<Brand>;
  readonly clearingPlatform?: Maybe<Scalars['String']>;
  readonly created?: Maybe<Scalars['DateTime']>;
  readonly createdBy?: Maybe<Scalars['String']>;
  readonly disciplinaryDescription?: Maybe<Scalars['String']>;
  readonly disciplinaryEvents?: Maybe<Scalars['Boolean']>;
  readonly iarNames?: Maybe<ReadonlyArray<Maybe<IarName>>>;
  readonly iardCrd?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly individualIardCrd?: Maybe<Scalars['String']>;
  readonly individualRegisteredBy?: Maybe<Scalars['String']>;
  readonly name?: Maybe<Scalars['String']>;
  readonly registeredBy?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
  readonly teamsVersion?: Maybe<Scalars['Int']>;
  readonly updated?: Maybe<Scalars['DateTime']>;
  readonly updatedBy?: Maybe<Scalars['String']>;
};

export type OrganizationLogo = {
  readonly __typename?: 'OrganizationLogo';
  readonly id: Scalars['String'];
  readonly imageHandlerHost: Scalars['String'];
  readonly s3Bucket: Scalars['String'];
  readonly s3Key: Scalars['String'];
};

export type OrganizationPreferences = {
  readonly __typename?: 'OrganizationPreferences';
  readonly aumStrategy: AllowedAumStrategy;
  readonly invoiceView: AllowedInvoiceView;
};

export type OwnerOrOfficer = {
  readonly address?: InputMaybe<AddressDetails>;
  readonly dateOfBirth?: InputMaybe<Scalars['String']>;
  readonly name?: InputMaybe<NameDetails>;
  readonly socialSecurityNumber?: InputMaybe<Scalars['String']>;
};

export type ParametersAum = {
  readonly __typename?: 'ParametersAum';
  readonly adjustmentFee?: Maybe<Scalars['Float']>;
  readonly annualizedFee?: Maybe<Scalars['Float']>;
  readonly annualizedRate?: Maybe<Scalars['Float']>;
  readonly aum?: Maybe<Scalars['Float']>;
  readonly daysInTheYear?: Maybe<Scalars['Float']>;
  readonly manualAllocationWeight?: Maybe<Scalars['Float']>;
  readonly minimumFee?: Maybe<Scalars['Float']>;
  readonly periodDays?: Maybe<Scalars['Int']>;
  readonly periodFee?: Maybe<Scalars['Float']>;
  readonly tiers?: Maybe<ReadonlyArray<Maybe<TiersAum>>>;
};

export type ParametersFlat = {
  readonly __typename?: 'ParametersFlat';
  readonly effectiveDays?: Maybe<Scalars['Int']>;
  readonly flatFee?: Maybe<Scalars['Float']>;
  readonly periodDays?: Maybe<Scalars['Int']>;
};

export type PatchDocumentTemplateDataInput = {
  readonly defaultMessage?: InputMaybe<Scalars['String']>;
  readonly defaultMessageSubject?: InputMaybe<Scalars['String']>;
  readonly externalTemplateId?: InputMaybe<Scalars['String']>;
  readonly name?: InputMaybe<Scalars['String']>;
  readonly signers?: InputMaybe<ReadonlyArray<Scalars['String']>>;
  readonly signingOrderIsSet?: InputMaybe<Scalars['Boolean']>;
  readonly status: DocumentTemplateInputStatus;
};

export type PatchDocumentTemplateInput = {
  readonly id: Scalars['String'];
  readonly template: PatchDocumentTemplateDataInput;
};

export type PaySubscriptionOutput = {
  readonly __typename?: 'PaySubscriptionOutput';
  readonly invoiceId: Scalars['String'];
  readonly invoiceStatus: invoiceStatus;
  readonly subscriptionId: Scalars['String'];
  readonly subscriptionStatus: subscriptionStatus;
};

export type PaymentMethod = {
  readonly __typename?: 'PaymentMethod';
  readonly billingZip?: Maybe<Scalars['String']>;
  readonly cardOwnerEmail: Scalars['String'];
  readonly cardOwnerName: Scalars['String'];
  readonly expMonth: Scalars['Int'];
  readonly expYear: Scalars['Int'];
  readonly isDefault: Scalars['Boolean'];
  readonly lastFourDigits: Scalars['String'];
  readonly stripeCustomerId?: Maybe<Scalars['String']>;
};

export type PaymentMethodOutput = {
  readonly __typename?: 'PaymentMethodOutput';
  readonly stripeCustomerId: Scalars['String'];
};

export type PendingUsername = {
  readonly __typename?: 'PendingUsername';
  readonly code: Scalars['ID'];
  readonly status: Scalars['String'];
  readonly username: Scalars['String'];
};

export type Performance = {
  readonly __typename?: 'Performance';
  readonly chart: Chart;
  /** Portfolio daily Time Weighted Return chart */
  readonly portfolioDailyTWRChart?: Maybe<PortfolioDailyTWRChart>;
  readonly summary: Summary;
};

export type PerformanceAnalyticsDateRangeConfigInput = {
  readonly dateRangeType: PerformanceAnalyticsDateRangeType;
  readonly endDate?: InputMaybe<Scalars['DateOnly']>;
  readonly startDate?: InputMaybe<Scalars['DateOnly']>;
};

export enum PerformanceAnalyticsDateRangeType {
  FIXED_INTERVAL = 'FIXED_INTERVAL',
  MAX = 'MAX'
}

export enum PerformanceAnalyticsResourceType {
  ACCOUNT = 'ACCOUNT',
  BENCHMARK = 'BENCHMARK',
  HOUSEHOLD = 'HOUSEHOLD',
  MODEL_PORTFOLIO = 'MODEL_PORTFOLIO'
}

export enum PeriodType {
  annual = 'annual',
  monthly = 'monthly',
  quarterly = 'quarterly',
  weekly = 'weekly'
}

export type PhoneNumber = {
  readonly __typename?: 'PhoneNumber';
  readonly id?: Maybe<Scalars['ID']>;
  readonly isDefaultForMfa?: Maybe<Scalars['Boolean']>;
  readonly isVerified?: Maybe<Scalars['Boolean']>;
  readonly number?: Maybe<Scalars['String']>;
  readonly phoneNumberType?: Maybe<PhoneNumberType>;
};

export type PhoneNumberInput = {
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly isDefaultForMfa: Scalars['Boolean'];
  readonly number: Scalars['String'];
  readonly phoneNumberType: PhoneNumberType;
};

export enum PhoneNumberType {
  FAX = 'FAX',
  HOME = 'HOME',
  MOBILE = 'MOBILE',
  OFFICE = 'OFFICE'
}

export type PhotoReturn = {
  readonly __typename?: 'PhotoReturn';
  readonly id?: Maybe<Scalars['ID']>;
};

export type PlaidAccount = {
  readonly id?: InputMaybe<Scalars['String']>;
  readonly mask?: InputMaybe<Scalars['String']>;
  readonly name?: InputMaybe<Scalars['String']>;
  readonly subtype?: InputMaybe<Scalars['String']>;
  readonly type?: InputMaybe<Scalars['String']>;
};

export enum PlaidAuthType {
  AUTOMATED = 'AUTOMATED',
  INSTANT = 'INSTANT',
  MANUAL = 'MANUAL'
}

export enum PlaidAuthenticationStatus {
  FAILED = 'FAILED',
  PENDING = 'PENDING',
  PENDING_MANUAL = 'PENDING_MANUAL',
  PENDING_VERIFICATION = 'PENDING_VERIFICATION',
  VERIFIED = 'VERIFIED'
}

export type PlaidInput = {
  readonly depositAmount?: InputMaybe<Scalars['String']>;
  readonly iraContribution?: InputMaybe<IraContributionType>;
};

export type PlaidInstitution = {
  readonly institutionId?: InputMaybe<Scalars['String']>;
  readonly name?: InputMaybe<Scalars['String']>;
};

export type PlaidLinkTokenWhere = {
  readonly androidPackageName?: InputMaybe<Scalars['String']>;
  readonly redirectUrl?: InputMaybe<Scalars['String']>;
};

export type PlaidMetaData = {
  readonly accountMask?: InputMaybe<Scalars['String']>;
  readonly accountName: Scalars['String'];
  readonly accountType: Scalars['String'];
  readonly authType: PlaidAuthType;
  readonly institutionName?: InputMaybe<Scalars['String']>;
  readonly plaidAccountId: Scalars['String'];
};

export type PlaidToken = {
  readonly __typename?: 'PlaidToken';
  readonly linkToken: Scalars['String'];
};

export type PlaidType = {
  readonly __typename?: 'PlaidType';
  readonly depositAmount?: Maybe<Scalars['String']>;
  readonly iraContribution?: Maybe<IraContributionType>;
};

export type Plan = {
  readonly __typename?: 'Plan';
  readonly balanceMetrics?: Maybe<AllowedBalanceMetrics>;
  readonly blendedRate?: Maybe<Scalars['Boolean']>;
  readonly fee?: Maybe<Scalars['Float']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly min?: Maybe<Scalars['Float']>;
  readonly tiers?: Maybe<ReadonlyArray<Maybe<TierOutput>>>;
  readonly type?: Maybe<AllowedType>;
};

export type Plan2 = {
  readonly __typename?: 'Plan2';
  readonly balanceMetrics?: Maybe<AllowedBalanceMetrics>;
  readonly blendedRate?: Maybe<Scalars['Boolean']>;
  readonly fee?: Maybe<Scalars['Float']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly min?: Maybe<Scalars['Float']>;
  readonly tiers?: Maybe<ReadonlyArray<Maybe<Tier2>>>;
  readonly type?: Maybe<AllowedType>;
};

export type PlanDetails = {
  readonly __typename?: 'PlanDetails';
  readonly accountAssetOwnership?: Maybe<Scalars['String']>;
  readonly adoptingEmployerAddress?: Maybe<AdoptingEmployerAddress>;
  readonly adoptingEmployerName?: Maybe<Scalars['String']>;
  readonly effectiveDate?: Maybe<Scalars['String']>;
  readonly ein?: Maybe<Scalars['String']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly participantName?: Maybe<Scalars['String']>;
  readonly planAdministrator?: Maybe<Scalars['String']>;
  readonly planEffectiveDate?: Maybe<Scalars['String']>;
  readonly planName?: Maybe<Scalars['String']>;
  readonly planTaxID?: Maybe<Scalars['String']>;
  readonly planType?: Maybe<Scalars['String']>;
};

export type PlanDetailsInput = {
  readonly accountAssetOwnership?: InputMaybe<Scalars['String']>;
  readonly adoptingEmployerName?: InputMaybe<Scalars['String']>;
  readonly effectiveDate?: InputMaybe<Scalars['String']>;
  readonly ein?: InputMaybe<Scalars['String']>;
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly planName?: InputMaybe<Scalars['String']>;
  readonly planType?: InputMaybe<Scalars['String']>;
};

export type PlanDetailsWhere = {
  readonly id: Scalars['ID'];
};

export type PlanInput = {
  readonly balanceMetrics?: InputMaybe<AllowedBalanceMetrics>;
  readonly blendedRate?: InputMaybe<Scalars['Boolean']>;
  readonly fee?: InputMaybe<Scalars['Float']>;
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly min?: InputMaybe<Scalars['Float']>;
  readonly tiers?: InputMaybe<ReadonlyArray<InputMaybe<TierInput>>>;
  readonly type: AllowedType;
};

export type Portfolio = {
  readonly __typename?: 'Portfolio';
  readonly description?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly investmentMinimum?: Maybe<Scalars['Float']>;
  readonly models: ReadonlyArray<Model>;
  readonly name: Scalars['String'];
  readonly owner: PortfolioOwner;
  readonly tags?: Maybe<ReadonlyArray<Scalars['String']>>;
  readonly updated: Scalars['String'];
};

export type PortfolioAccountsOutput = {
  readonly __typename?: 'PortfolioAccountsOutput';
  readonly accountCategory?: Maybe<Scalars['String']>;
  readonly accountNickname?: Maybe<Scalars['String']>;
  readonly accountNumber?: Maybe<Scalars['String']>;
  readonly clientName?: Maybe<Scalars['String']>;
  readonly isTLHEnabled?: Maybe<Scalars['Boolean']>;
};

export type PortfolioCharacteristic = {
  readonly __typename?: 'PortfolioCharacteristic';
  readonly asOfDate?: Maybe<Scalars['DateOnly']>;
  readonly breakdown?: Maybe<ReadonlyArray<Maybe<PortfolioCharacteristicBreakdownItem>>>;
  readonly vendorAsOfDate?: Maybe<Scalars['DateOnly']>;
};

export type PortfolioCharacteristicBreakdownItem = {
  readonly __typename?: 'PortfolioCharacteristicBreakdownItem';
  readonly type: Scalars['String'];
  readonly value?: Maybe<Scalars['Float']>;
};

export enum PortfolioCharacteristicSection {
  ASSET_ALLOCATIONS = 'ASSET_ALLOCATIONS',
  REGION = 'REGION',
  SECTORS = 'SECTORS',
  STYLEBOX = 'STYLEBOX',
  YIELD_EXPENSES = 'YIELD_EXPENSES'
}

export type PortfolioCharacteristicsInput = {
  readonly resourceType: PerformanceAnalyticsResourceType;
  readonly resourceTypeId: Scalars['ID'];
  readonly sections?: InputMaybe<ReadonlyArray<PortfolioCharacteristicSection>>;
};

export type PortfolioCharacteristicsResponse = {
  readonly __typename?: 'PortfolioCharacteristicsResponse';
  readonly assetAllocations?: Maybe<PortfolioCharacteristic>;
  readonly code?: Maybe<Scalars['String']>;
  readonly regions?: Maybe<PortfolioCharacteristic>;
  readonly sectors?: Maybe<PortfolioCharacteristicsSectors>;
  readonly styleBox?: Maybe<PortfolioCharacteristicsStyleBox>;
  readonly yieldExpenses?: Maybe<PortfolioCharacteristicsYield>;
};

export type PortfolioCharacteristicsSectors = {
  readonly __typename?: 'PortfolioCharacteristicsSectors';
  readonly equity?: Maybe<PortfolioCharacteristic>;
  readonly fixedIncome?: Maybe<PortfolioCharacteristic>;
};

export type PortfolioCharacteristicsStyleBox = {
  readonly __typename?: 'PortfolioCharacteristicsStyleBox';
  readonly equity?: Maybe<PortfolioCharacteristicsStyleBoxItem>;
};

export type PortfolioCharacteristicsStyleBoxItem = {
  readonly __typename?: 'PortfolioCharacteristicsStyleBoxItem';
  readonly asOfDate?: Maybe<Scalars['DateOnly']>;
  readonly breakdown?: Maybe<ReadonlyArray<Maybe<PortfolioCharacteristicBreakdownItem>>>;
  readonly growthValueAverage?: Maybe<Scalars['String']>;
  readonly marketCapitalizationAverage?: Maybe<Scalars['String']>;
  readonly vendorAsOfDate?: Maybe<Scalars['DateOnly']>;
};

export type PortfolioCharacteristicsYield = {
  readonly __typename?: 'PortfolioCharacteristicsYield';
  readonly asOfDate?: Maybe<Scalars['DateOnly']>;
  readonly breakdown?: Maybe<ReadonlyArray<Maybe<PortfolioCharacteristicBreakdownItem>>>;
  readonly fixedIncomeAverageCreditRating?: Maybe<Scalars['String']>;
  readonly vendorAsOfDate?: Maybe<Scalars['DateOnly']>;
};

export type PortfolioDailyTWRChart = {
  readonly __typename?: 'PortfolioDailyTWRChart';
  readonly dataPoints?: Maybe<ReadonlyArray<PortfolioDailyTWRDataPoint>>;
};

export type PortfolioDailyTWRDataPoint = {
  readonly __typename?: 'PortfolioDailyTWRDataPoint';
  /** Day */
  readonly x: Scalars['DateOnly'];
  /** Portfolio Time Weighted Return */
  readonly y: Scalars['Float'];
};

/** Created new type to deprecate the old queries, types and fields */
export type PortfolioData = {
  readonly __typename?: 'PortfolioData';
  readonly description?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly investmentMinimum?: Maybe<Scalars['Float']>;
  readonly marketplaceData: MarketplaceData;
  readonly name: Scalars['String'];
  readonly updatedDate: Scalars['String'];
};

export type PortfolioDetailOutput = {
  readonly __typename?: 'PortfolioDetailOutput';
  readonly cashSettings?: Maybe<CashSettings>;
  readonly cashTarget?: Maybe<Scalars['Float']>;
  readonly cost?: Maybe<Scalars['String']>;
  readonly drift: DriftSettings;
  readonly investmentMinimum?: Maybe<Scalars['Float']>;
  readonly marketplaceData: MarketplaceData;
  /** @deprecated Use 'cashSettings' instead */
  readonly minCash?: Maybe<Scalars['Float']>;
  readonly models: ReadonlyArray<Maybe<PortfolioModelOutput>>;
  readonly name: Scalars['String'];
  readonly numOfAccounts: Scalars['Int'];
  readonly portfolioAccounts?: Maybe<ReadonlyArray<Maybe<PortfolioAccountsOutput>>>;
  readonly portfolioModels: ReadonlyArray<Maybe<PortfolioModelsDetailOutput>>;
  readonly portfolioOwner?: Maybe<PortfolioOwner>;
  readonly updated?: Maybe<Scalars['DateTime']>;
};

export type PortfolioDrift = {
  readonly __typename?: 'PortfolioDrift';
  readonly target?: Maybe<Scalars['Float']>;
  readonly type?: Maybe<PortfolioDriftType>;
};

export enum PortfolioDriftType {
  DRIFT_BASED = 'DRIFT_BASED',
  EVENT_BASED = 'EVENT_BASED'
}

export type PortfolioListOutput = {
  readonly __typename?: 'PortfolioListOutput';
  readonly cashSettings?: Maybe<CashSettings>;
  readonly cashTarget?: Maybe<Scalars['Float']>;
  readonly cost?: Maybe<Scalars['String']>;
  readonly drift: DriftSettings;
  readonly hasMutualFundFees?: Maybe<Scalars['Boolean']>;
  readonly id: Scalars['ID'];
  readonly investmentMinimum?: Maybe<Scalars['Float']>;
  /** @deprecated Use 'cashSettings' instead */
  readonly minCash?: Maybe<Scalars['Float']>;
  readonly name: Scalars['String'];
  readonly numOfAccounts: Scalars['Int'];
  readonly numOfModels?: Maybe<Scalars['Int']>;
  readonly portfolioOwner?: Maybe<PortfolioOwner>;
  readonly updated?: Maybe<Scalars['String']>;
  readonly weightedModels?: Maybe<ReadonlyArray<Maybe<WeightedModel>>>;
};

export type PortfolioManager = {
  readonly __typename?: 'PortfolioManager';
  readonly id: Scalars['ID'];
  readonly logoUrl?: Maybe<Scalars['String']>;
  readonly name?: Maybe<Scalars['String']>;
};

export type PortfolioModelInput = {
  readonly id: Scalars['ID'];
  readonly weight: Scalars['Float'];
};

export type PortfolioModelOutput = {
  readonly __typename?: 'PortfolioModelOutput';
  readonly id: Scalars['ID'];
  readonly latestVersionMarketplaceId?: Maybe<Scalars['ID']>;
  readonly weight: Scalars['Float'];
};

export type PortfolioModelSubstitute = {
  readonly __typename?: 'PortfolioModelSubstitute';
  readonly name: Scalars['String'];
  readonly symbol: Scalars['String'];
  readonly type: Scalars['String'];
};

export type PortfolioModelSubstitutes = {
  readonly __typename?: 'PortfolioModelSubstitutes';
  readonly equivalentSubstitutes: ReadonlyArray<PortfolioModelSubstitute>;
  readonly transientSubstitutes: ReadonlyArray<PortfolioModelSubstitute>;
};

export type PortfolioModelsDetailOutput = {
  readonly __typename?: 'PortfolioModelsDetailOutput';
  readonly id: Scalars['ID'];
  readonly latestVersionMarketplaceId?: Maybe<Scalars['ID']>;
  readonly name?: Maybe<Scalars['String']>;
  readonly weight: Scalars['Float'];
  readonly weightedSecurities?: Maybe<ReadonlyArray<Maybe<WeightedSecurity>>>;
};

export type PortfolioOutput = {
  readonly __typename?: 'PortfolioOutput';
  readonly driftType: PortfolioDriftType;
  readonly id: Scalars['String'];
  readonly name: Scalars['String'];
  readonly numberOfAccounts?: Maybe<Scalars['Int']>;
};

export enum PortfolioOwner {
  ALTRUIST = 'ALTRUIST',
  CUSTOM = 'CUSTOM'
}

export type PortfolioSeries = {
  readonly __typename?: 'PortfolioSeries';
  readonly description?: Maybe<Scalars['String']>;
  readonly factsheetUrl?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly name?: Maybe<Scalars['String']>;
};

export type PortfolioValidationInfo = {
  readonly __typename?: 'PortfolioValidationInfo';
  readonly cashPercentageDelta?: Maybe<Scalars['Float']>;
  readonly currentCashTarget?: Maybe<Scalars['Float']>;
  readonly effectiveCashTarget?: Maybe<Scalars['Float']>;
  readonly investCashThreshold?: Maybe<Scalars['Float']>;
  readonly name?: Maybe<Scalars['String']>;
  readonly numberOfAffectedAccounts?: Maybe<Scalars['Int']>;
  readonly portfolioId: Scalars['ID'];
  readonly raiseCashThreshold?: Maybe<Scalars['Float']>;
  readonly success: Scalars['Boolean'];
};

export type PortfolioValidationResult = {
  readonly __typename?: 'PortfolioValidationResult';
  readonly message?: Maybe<Scalars['String']>;
};

export type PreTransferInfo = {
  readonly __typename?: 'PreTransferInfo';
  readonly shouldRebalance: Scalars['Boolean'];
};

export type PreviewResponse = {
  readonly __typename?: 'PreviewResponse';
  readonly previewPageUrl: Scalars['String'];
  readonly token: Scalars['ID'];
};

export enum PrimaryEntityAccountActivity {
  ACTIVE_TRADING = 'ACTIVE_TRADING',
  LONG_TERM_INVESTING = 'LONG_TERM_INVESTING',
  SHORT_TERM_INVESTING = 'SHORT_TERM_INVESTING'
}

export enum PrimaryOngoingFundingSource {
  CORPORATE_INCOME = 'CORPORATE_INCOME',
  INVESTMENT_CAPITAL = 'INVESTMENT_CAPITAL',
  OTHER = 'OTHER'
}

export enum ProfileType {
  ADVISOR = 'ADVISOR',
  BENEFICIARY = 'BENEFICIARY',
  BENEFICIARY_ENTITY = 'BENEFICIARY_ENTITY',
  CLIENT = 'CLIENT',
  CLIENT_ACCOUNT = 'CLIENT_ACCOUNT',
  ENTITY = 'ENTITY',
  ENTITY_CONTACT = 'ENTITY_CONTACT',
  MINOR = 'MINOR',
  TRUSTED_CONTACT = 'TRUSTED_CONTACT'
}

export enum PurposeOfAccount {
  DISTRIBUTION_OF_ESTATE = 'DISTRIBUTION_OF_ESTATE',
  ESTATE_TAX_PLANNING = 'ESTATE_TAX_PLANNING',
  INVESTING_OF_TRUST_ASSETS = 'INVESTING_OF_TRUST_ASSETS',
  INVESTING_WITH_FREQUENT_WITHDRAWALS = 'INVESTING_WITH_FREQUENT_WITHDRAWALS',
  INVESTING_WITH_OCCASIONAL_WITHDRAWALS = 'INVESTING_WITH_OCCASIONAL_WITHDRAWALS',
  RETIREMENT = 'RETIREMENT',
  TUITION_SAVINGS = 'TUITION_SAVINGS'
}

export type QRCodeData = {
  readonly __typename?: 'QRCodeData';
  readonly qrCode: Scalars['String'];
  readonly secretKey: Scalars['String'];
};

export type Query = {
  readonly __typename?: 'Query';
  readonly CRMContacts: ReadonlyArray<CRMContact>;
  readonly _?: Maybe<Scalars['Boolean']>;
  readonly acatById: ACATTransaction;
  readonly acatDraft: AcatDraft;
  readonly acatDrafts: ReadonlyArray<AcatDraft>;
  readonly acatTransferStatusLogById: ReadonlyArray<Maybe<ACATTransferStatusLog>>;
  readonly acats?: Maybe<ReadonlyArray<ACATTransaction>>;
  readonly accountBeneficiaries: ReadonlyArray<Beneficiary>;
  readonly accountBeneficiariesByClients: ReadonlyArray<AccountBeneficiariesByUser>;
  readonly accountBeneficiaryById: Beneficiary;
  readonly accountInfo: ReadonlyArray<Maybe<AccountInfo>>;
  readonly accountManagement: AccountManagement;
  readonly accountStates: ReadonlyArray<StateOutput>;
  readonly accountStatesCreationStatus: ReadonlyArray<AccountStateCreationStatus>;
  readonly accountTaxSettings?: Maybe<AccountTaxSettings>;
  readonly accountsForInternalTransferByOwners: ReadonlyArray<AccountForInternalTransfer>;
  readonly adoptingEmployer: AdoptingEmployer;
  readonly advisorMarketplaceProfile: AdvisorMarketplaceProfile;
  readonly advisorMarketplaceProfileDraft?: Maybe<AdvisorMarketplaceProfile>;
  readonly aggrAccountHistory: ReadonlyArray<Maybe<AggrAccountHistory>>;
  readonly aggregatedLosses?: Maybe<AggregatedLossesAccountResponseItem>;
  readonly allAccountStatesGrouped: ReadonlyArray<AccountStatesGroupedByMultiAccountDraftId>;
  readonly allAgesOfMajority: ReadonlyArray<AgeOfMajorityForState>;
  readonly altruistClearingTermsOfUseStatus: TermsAcceptance;
  readonly apyInfo: ApyInfo;
  readonly batches?: Maybe<ReadonlyArray<Batch>>;
  readonly benchmarkAssignments?: Maybe<ReadonlyArray<BenchmarkAssignmentOutput>>;
  readonly benchmarkDetail?: Maybe<BenchmarkDetailOutput>;
  readonly benchmarkPerformance?: Maybe<BenchmarkPerformanceOutput>;
  readonly benchmarkSummaryList?: Maybe<ReadonlyArray<BenchmarkSummary>>;
  readonly benchmarkSymbols?: Maybe<ReadonlyArray<BenchmarkSymbol>>;
  /** @deprecated Use accountBeneficiariesByClients. */
  readonly beneficiariesByUsers: ReadonlyArray<BeneficiariesByUser>;
  readonly billingHouseholds: ReadonlyArray<Household>;
  readonly billingRerun?: Maybe<BillingRerun>;
  readonly brokerageApplication?: Maybe<BrokerageApplication>;
  readonly bulkAccountsForInternalTransferByOwners: ReadonlyArray<BulkAccountForInternalTransfer>;
  readonly cashAccounts: CashAccountDetails;
  readonly cashLiquidationValidation?: Maybe<CashLiquidationValidationResponse>;
  readonly cashLiquidations?: Maybe<ReadonlyArray<Maybe<CashLiquidation>>>;
  readonly certifications: ReadonlyArray<Maybe<CertificationsOutput>>;
  readonly checkClientDeleteStatus?: Maybe<ClientDeleteStatus>;
  readonly checkIfUserHasPaymentMethod: UserPaymentMethodStatus;
  readonly clearingRepCodes: ReadonlyArray<ClearingRepCode>;
  readonly clientBrokerageAccounts: ReadonlyArray<AccountStatusOutput>;
  readonly clients?: Maybe<ReadonlyArray<Client>>;
  readonly colorDraft: ColorDraft;
  readonly colorDrafts: ReadonlyArray<ColorDraft>;
  readonly constraints: Constraints;
  readonly corpMember: CorpMember;
  readonly corpMembers: ReadonlyArray<CorpMember>;
  readonly corporation: Corporation;
  readonly decedent: Decedent;
  readonly defaultTaxRates: AccountTaxRates;
  readonly delayedPriceQuote: ReadonlyArray<Maybe<MarketData>>;
  readonly documentAgreementUrl?: Maybe<DocumentAgreementUrlResponse>;
  readonly documentAgreements?: Maybe<DocumentAgreementsResponse>;
  readonly documentAgreementsPDF?: Maybe<AgreementsPdf>;
  readonly documentBundle: DocumentBundle;
  readonly documentTemplate?: Maybe<DocumentTemplate>;
  readonly documentTemplateEditUrl?: Maybe<DocumentTemplate>;
  readonly documentTemplates?: Maybe<DocumentTemplatesResponse>;
  readonly downloadRealizedCostBasis?: Maybe<ReadonlyArray<Maybe<RealizedCostBasis>>>;
  readonly failedRebalanceSummary?: Maybe<FailedRebalanceSummary>;
  readonly feeAllocations: ReadonlyArray<FeeAllocation>;
  readonly feeRoutingHouseholdPreferences: ReadonlyArray<FeeTemplate>;
  readonly feeRoutingOrgPreferences: ReadonlyArray<FeeTemplate>;
  readonly feeRoutingTypes: ReadonlyArray<FeeRoutingTypeDefinition>;
  readonly financialAccount: FinancialAccount2;
  readonly financialAccountCounts?: Maybe<FinancialAccountCounts>;
  readonly financialAccountDetails?: Maybe<FinancialAccountDetails>;
  readonly financialAccountStatusCounts?: Maybe<FinancialAccountStatusCounts>;
  readonly financialAccountsByUserId: ReadonlyArray<FinancialAccount2>;
  readonly financialAccountsForAccountGroupsPage?: Maybe<ReadonlyArray<FinancialAccountsForAccountGroupsPage>>;
  /** Purposely acts like a REST client until we figure out how to do batch requests properly. */
  readonly financialAccountsForAccountsPage?: Maybe<ReadonlyArray<FinancialAccountsForAccountsPage>>;
  readonly financialAccountsForAccountsReport?: Maybe<ReadonlyArray<FinancialAccountsForAccountsReport>>;
  readonly financialAccountsForHeldAway?: Maybe<FinancialAccountsForHeldAwayResponse>;
  readonly financialAccountsForPortfolioAssignment: ReadonlyArray<FinancialAccountsForPortfolioAssignment>;
  readonly financialAccountsPerformance?: Maybe<ReadonlyArray<FinancialAccountsPerformanceResponse>>;
  readonly financialRiskReturnAnalytics?: Maybe<FinancialRiskReturnAnalyticsResponse>;
  readonly fixedIncomeInstrumentPriceHistory: ReadonlyArray<Maybe<FixedIncomeInstrumentPrice>>;
  readonly fixedIncomeInstruments: FixedIncomeInstrumentPage;
  readonly flattenedAllocations?: Maybe<FlattenedAllocations>;
  readonly fundingAccount: FundingAccount;
  readonly fundingAccounts: ReadonlyArray<FundingAccount>;
  readonly fundingAccountsByBrokerage: ReadonlyArray<FundingAccountsByBrokerage>;
  readonly getAccountCashSettings?: Maybe<CashSettings>;
  readonly getAccountCashSummary: AccountCashSummary;
  readonly getAccountCreationStatus: ReadonlyArray<AccountStatusOutput>;
  readonly getAccountMigrationInfo: MigrationInfo;
  readonly getAccountStateById: StateOutput;
  readonly getAccountsWithScheduleAssignment: GetAccountsWithScheduleAssignmentOutput;
  /** @deprecated Use brokerageApplication in BrokerageApplication */
  readonly getAdvisorADVForms?: Maybe<ADVFormsResponse>;
  readonly getAdvisorAUM: AdvisorAUM;
  readonly getAdvisorAccounts: AdvisorAccounts;
  readonly getAdvisorAccounts2: ReadonlyArray<Maybe<AdvisorAccounts2>>;
  /** @deprecated Use 'user' instead. */
  readonly getAdvisorAddress?: Maybe<ReadonlyArray<Maybe<GetAdvisorAddressResponse>>>;
  readonly getAdvisorDocument: AdvisorDocument;
  readonly getAdvisorDocumentUrl: DocumentUrl;
  readonly getAdvisorHoldings: ReadonlyArray<Maybe<AdvisorHolding>>;
  /** @deprecated Use 'user' instead. */
  readonly getAdvisorInfo?: Maybe<ReadonlyArray<Maybe<Advisor>>>;
  readonly getAdvisorStatus: AdvisorStatusResponse;
  readonly getAdvisorsInClientAccountGroup?: Maybe<ReadonlyArray<Maybe<AdvisorContactDetails>>>;
  readonly getAllAccountsState: ReadonlyArray<Maybe<StateOutput>>;
  readonly getAllDocuments: ReadonlyArray<Maybe<Document>>;
  readonly getAllFinancialAccountIDs: ReadonlyArray<Maybe<Scalars['String']>>;
  readonly getAllFinancialAccounts?: Maybe<ReadonlyArray<Maybe<AccountDetails>>>;
  readonly getAllFinancialAccountsInGroup?: Maybe<ReadonlyArray<Maybe<AccountDetails>>>;
  readonly getAllFinancialAccountsUnfiltered?: Maybe<ReadonlyArray<Maybe<AccountDetails>>>;
  /** @deprecated Use 'clients' instead. */
  readonly getAllUsersInGroup: ReadonlyArray<AllUsersInGroupOutput>;
  readonly getAllocations: Allocations;
  readonly getBalanceBreakdown: BalanceOutput;
  readonly getBenchmarks: ReadonlyArray<Benchmark>;
  /** @deprecated Use accountBeneficiaries. */
  readonly getBeneficiaries: ReadonlyArray<Maybe<BeneficiaryOutput>>;
  /** @deprecated Use accountBeneficiaries. */
  readonly getBeneficiariesByAccountID: ReadonlyArray<BeneficiaryOutput>;
  readonly getBillingPeriodById: BillingPeriod;
  readonly getCannySSOJwt: Scalars['String'];
  readonly getCashTransferList: ReadonlyArray<Maybe<CashTransaction>>;
  /** @deprecated Use brokerageApplication in BrokerageApplication */
  readonly getClientADVForms?: Maybe<ADVFormsResponse>;
  /** @deprecated Use 'user' instead. */
  readonly getClientAddress: AdvisorAddress;
  /** @deprecated Use clientBrokerageAccounts */
  readonly getClientApexAccounts: ReadonlyArray<AccountStatusOutput>;
  readonly getClientDocumentUrl: DocumentUrl;
  readonly getClientDocuments: ReadonlyArray<ClientDocument>;
  readonly getClientFinancialInfo?: Maybe<ClientFinancialInfo>;
  /** @deprecated use signedInUser to get org object */
  readonly getCompanyInfo: CompanyOutput;
  readonly getContactAnalytics?: Maybe<GetContactAnalyticsOutput>;
  readonly getCostBasis: CostBasis;
  readonly getCostBasisHoldings: CostBasis;
  /** @deprecated Use getAdvisorAUM { total }. */
  readonly getCurrentTotalAum: Scalars['Float'];
  readonly getEntityHouseAccountDetails: EntityAccountDetails;
  readonly getExchangeStatus: ExchangeStatus;
  readonly getExcludeDaysBeforeFunding: GetExcludeDaysBeforeFundingOutput;
  readonly getExecutedInvoices: ExecutedInvoicesOutput;
  /** @deprecated Use the query, `feeAllocations` instead. */
  readonly getFeeAllocationByGroup: GroupFeeAllocations;
  readonly getFeeAllocationsBySchedule: ScheduleFeeAllocations;
  readonly getFeeInvoicePeriodById: GetFeeInvoicePeriodByIdOutput;
  readonly getFeeInvoicePeriods: ReadonlyArray<Maybe<FeeInvoicePeriod>>;
  readonly getFeeInvoiceStats: FeeInvoiceStats;
  readonly getFinancialAccountGroupPairs?: Maybe<ReadonlyArray<Maybe<GetFinancialAccountGroupPairsOutput>>>;
  /** @deprecated Use owners in FinancialAccount2 */
  readonly getFinancialAccountOwners: ReadonlyArray<Client>;
  readonly getFinancialAccountTrustDetails: TrustOutput;
  readonly getFinancialAccounts?: Maybe<ReadonlyArray<Maybe<AccountDetails>>>;
  readonly getFlattenedAllocationPreview?: Maybe<ReadonlyArray<MinHoldingWeight>>;
  readonly getFlattenedPortfolioAllocations: FlattenedPortfolioAllocations;
  /** @deprecated Use the query, `groupSchedule` instead. */
  readonly getGroupSchedule: GroupScheduleOutput;
  readonly getGroupsWithSchedules?: Maybe<ReadonlyArray<GroupsWithSchedules>>;
  readonly getHouseAccount: HouseAccountResponse;
  readonly getHousehold?: Maybe<GetHouseholdOutput>;
  readonly getHubSpotContactById?: Maybe<HubspotContact>;
  readonly getInstitutionsList: ReadonlyArray<InstitutionsListOutput>;
  /** @deprecated use integrations query */
  readonly getIntegrationSourceInfo: BasicIntegrationSource;
  /** @deprecated Use series query */
  readonly getIntegrationSourceSeries?: Maybe<GetIntegrationSourceSeriesResponse>;
  /** @deprecated Use 'clients' instead. */
  readonly getInviteStatus: ReadonlyArray<InviteStatusOutput>;
  readonly getInvoice: Invoice;
  readonly getInvoiceRunnerResponse: InvoiceRunnerResponse;
  readonly getInvoicesByGroupID: InvoicesByGroupID;
  readonly getIraConstraints?: Maybe<IraConstraints>;
  readonly getIsAltruistUser?: Maybe<isAltruistUser>;
  readonly getJointUserProfiles: ReadonlyArray<UserInfoOutputQuery>;
  readonly getLoginNames: ReadonlyArray<Maybe<LoginNamesOutput>>;
  /** @deprecated Use model(id: ID!): Model! */
  readonly getModelById: ModelDetailOutput;
  /** @deprecated Use models: [Model!]! */
  readonly getModelList: ReadonlyArray<ModelListOutput>;
  readonly getOnboardStatus: OnboardStatusResponse;
  readonly getOrderDetail: OrderDetail;
  readonly getOrganizationPreferences: OrganizationPreferences;
  readonly getPaymentMethodInfo: ReadonlyArray<Maybe<PaymentMethod>>;
  readonly getPendingUsername: PendingUsername;
  readonly getPerformance: Performance;
  /** @deprecated Use 'signedInUser' instead. */
  readonly getPhoto: File;
  readonly getPlaidLinkToken: PlaidToken;
  readonly getPortfolioAssignmentById: AssignedOutput;
  /** @deprecated Use portfolio(id: ID!): Portfolio! */
  readonly getPortfolioById: PortfolioDetailOutput;
  readonly getPortfolioIntegrationServices?: Maybe<ReadonlyArray<BasicIntegrationSource>>;
  readonly getPortfolioList: ReadonlyArray<PortfolioListOutput>;
  readonly getPortfoliosBySeriesIDWithUpdateDate?: Maybe<GetPortfoliosBySeriesIDResponse>;
  readonly getRealTimeMarketQuote: RealTimeMarketQuoteOutput;
  readonly getRealizedCostBasis?: Maybe<RealizedCostBasisResponse>;
  readonly getSSGValetForms?: Maybe<ReadonlyArray<Maybe<SSGValetForm>>>;
  readonly getScheduleAssignments: ScheduleAssignments;
  readonly getScheduleAssignmentsByGroup: ScheduleAssignmentsOutput;
  /** @deprecated Use the query, `schedule` instead. */
  readonly getScheduleById: Schedule;
  readonly getScheduledTransfersList: ReadonlyArray<ScheduledTransactions>;
  /** @deprecated Use the query, `schedules` instead. */
  readonly getSchedules: ReadonlyArray<Maybe<Schedule>>;
  readonly getSecurities: Securities;
  readonly getSecuritiesHoldings: SecuritiesHoldings;
  readonly getSecuritiesList: ReadonlyArray<SecuritiesListOutput>;
  readonly getSecuritiesListForModels: ReadonlyArray<SecuritiesListOutput>;
  readonly getSecurity: SecuritiesListOutput;
  readonly getStatesByUser: ReadonlyArray<StateOutput>;
  readonly getStripeCustomer: StripeCustomer;
  readonly getSubscriptionInfo: ReadonlyArray<Maybe<SubscriptionInfo>>;
  readonly getTermsAcceptance: TermsAcceptance;
  readonly getTopClients?: Maybe<ReadonlyArray<TopClients>>;
  readonly getTopHoldings?: Maybe<ReadonlyArray<TopHoldings>>;
  readonly getTotalAccounts: Scalars['Int'];
  readonly getTotalGroups: Scalars['Int'];
  readonly getTotalSchedules: Scalars['Int'];
  readonly getTransactions: Transactions;
  readonly getTrustedContactByID: TrustedContactOutput;
  readonly getUnassignedArchivedAccounts: ReadonlyArray<Maybe<UnassignedAccount>>;
  /** @deprecated Use 'user' instead. */
  readonly getUserInGroupDetails: GetUserInGroupDetailsOutput;
  /** @deprecated Use 'signedInUser' instead. */
  readonly getUserInfoOBO: UserInfoOBOOutput;
  readonly getUserManualAuthToken: PlaidToken;
  /** @deprecated Use 'user' instead. */
  readonly getUserProfileInfo: UserInfoOutputQuery;
  /** @deprecated Use 'signedInUser' instead. */
  readonly getUserProfileInfoOBO: UserInfoOutputQuery;
  readonly getV2ScheduledTransfersList?: Maybe<ScheduledTransactionV2>;
  readonly getZendeskSSOJwt: Scalars['String'];
  readonly googleAuthenticatorQRCode: QRCodeData;
  readonly googleAuthenticatorQRCodeWithCredentials: QRCodeData;
  readonly groupById: GroupDetail;
  readonly groupSummary: GroupSummary;
  readonly groups?: Maybe<ReadonlyArray<Group>>;
  readonly heldAwayLinks?: Maybe<HeldAwayLinksResponse>;
  readonly heldAwayPlaidRelinkToken?: Maybe<HeldAwayPlaidTokenResponse>;
  readonly heldAwayPlaidToken?: Maybe<HeldAwayPlaidTokenResponse>;
  readonly holdingsByDate?: Maybe<ReportFile>;
  readonly holdingsPerformance?: Maybe<HoldingsPerformanceResponse>;
  readonly household: Household;
  readonly householdCount?: Maybe<Scalars['Int']>;
  readonly hubspotUser: HubspotUser;
  readonly hypotheticalPerformance?: Maybe<HypotheticalPerformanceResponse>;
  readonly idealClients: ReadonlyArray<Maybe<IdealClientsOutput>>;
  readonly identityVerificationQuestionWithCredentials: IdentityVerificationQuestion;
  /** @deprecated use integrations query */
  readonly integrationSources?: Maybe<ReadonlyArray<IntegrationSource>>;
  /** @deprecated use integrations query */
  readonly integrationStatus?: Maybe<IntegrationStatusLegacy>;
  /** @deprecated use integrations query */
  readonly integrationStatusByID?: Maybe<IntegrationStatusLegacy>;
  readonly integrations: ReadonlyArray<Integration>;
  readonly internalCashTransferAccounts: ReadonlyArray<InternalCashTransferAccounts>;
  readonly invitedTeamMembers: ReadonlyArray<InvitedTeamMember>;
  /** @deprecated Use 'user' instead. */
  readonly invitedUser: InvitedUserSettings;
  /** @deprecated Use 'clients' instead. */
  readonly invitedUsers: ReadonlyArray<InvitedUserSettings>;
  readonly invoice: Invoice;
  readonly isAccountCreatedUnder24Hours: Scalars['Boolean'];
  readonly isFingerprintJsEnabled: Scalars['Boolean'];
  readonly isMMATransfer: Scalars['Boolean'];
  /** @deprecated ADVEX-439 */
  readonly isNotificationProgramEnable: Scalars['Boolean'];
  readonly lastRebalance?: Maybe<RebalanceOccurence>;
  readonly marketplaceDimensionalStatus: MarketplaceAccessStatus;
  readonly marketplacePortfolioAnalytics: ReadonlyArray<MarketplacePortfolioAnalytic>;
  readonly marketplacePortfolios: ReadonlyArray<Maybe<MarketplacePortfolio>>;
  readonly matchConsumer: MatchConsumer;
  readonly matchRequests: ReadonlyArray<Maybe<MatchRequest>>;
  readonly metadata: MetaData;
  readonly mfaSettings: MfaSettings;
  readonly mfaSettingsWithCredentials: MfaSettings;
  readonly minor: Minor;
  readonly mma: MMA;
  readonly mmaByBrokerage: ReadonlyArray<MMA>;
  readonly mmaByUser: ReadonlyArray<MMA>;
  readonly mmaPreviewUsers: ReadonlyArray<User2>;
  readonly model: Model;
  readonly models: ReadonlyArray<Model>;
  readonly nextRebalance?: Maybe<RebalanceOccurence>;
  readonly notificationEvents?: Maybe<ReadonlyArray<NotificationEvent>>;
  readonly notificationPreferenceCategories?: Maybe<ReadonlyArray<NotificationCategory>>;
  /** @deprecated ADVEX-439 */
  readonly notificationPreferences: ReadonlyArray<NotificationPreference>;
  readonly notifications?: Maybe<NotificationsResponse>;
  readonly notificationsTotalUnread?: Maybe<NotificationsTotalUnreadResponse>;
  readonly orgPartnerIntegrations?: Maybe<ReadonlyArray<OrgIntegration>>;
  readonly organization: Organization;
  readonly organizationInfo: OrganizationInfo;
  readonly planDetails: PlanDetails;
  readonly portfolio: Portfolio;
  readonly portfolioCharacteristics?: Maybe<PortfolioCharacteristicsResponse>;
  readonly portfolios: ReadonlyArray<PortfolioData>;
  readonly preTransferInfo: PreTransferInfo;
  /**
   * Do NOT cache these values because of daylight savings.
   * @deprecated Use the query, `rebalancer` instead.
   */
  readonly rebalanceCutoff: RequiredDateTimeRange;
  readonly rebalancer: Rebalancer;
  readonly remainingTaxBudget?: Maybe<RemainingTaxBudget>;
  readonly repcodes: ReadonlyArray<Repcode>;
  readonly reportSchedules?: Maybe<ReadonlyArray<ReportSchedule>>;
  readonly retirementContributionsReport?: Maybe<RetirementContributionsReport>;
  /** @deprecated Use financialRiskReturnAnalytics in PerformanceAnalytics */
  readonly riskReturnAnalytics?: Maybe<RiskReturnAnalyticsResponse>;
  readonly schedule: Schedule2;
  readonly schedules: ReadonlyArray<Schedule2>;
  readonly searchOrders: OrderPage;
  readonly searchSecurityPerformanceDateRange?: Maybe<SearchSecurityPerformanceDateRangeResponse>;
  readonly searchSpliceAccountHistory?: Maybe<ReadonlyArray<SearchSpliceAccountHistoryResponseItem>>;
  readonly securities?: Maybe<ReadonlyArray<SecurityMasterRecord>>;
  readonly securityConstraints?: Maybe<ReadonlyArray<SecurityConstraint>>;
  readonly securityPerformanceDateRange?: Maybe<SecurityPerformanceDateRangeResponse>;
  readonly series?: Maybe<ReadonlyArray<Maybe<IntegrationSourceSeries>>>;
  readonly signedInUser: User2;
  readonly singleParticipant: SingleParticipant;
  readonly taxManagementAvailable?: Maybe<Scalars['Boolean']>;
  readonly teamMembers: ReadonlyArray<Advisor2>;
  readonly thirdPartyAccessIntegrations?: Maybe<ReadonlyArray<ThirdPartyAccessIntegration>>;
  readonly timeWeightedReturns?: Maybe<TimeWeightedReturnsResponse>;
  readonly tokenExpiration: Scalars['DateTime'];
  readonly totalUnassignedAccounts: Scalars['Int'];
  /** @deprecated This process is made on the server side now */
  readonly trustedDevice?: Maybe<TrustedDevice>;
  readonly trustedDevices?: Maybe<ReadonlyArray<Maybe<TrustedDevice>>>;
  readonly unassignedAccounts?: Maybe<ReadonlyArray<UnassignedAccount>>;
  readonly upcomingRebalances?: Maybe<UpcomingRebalances>;
  readonly user?: Maybe<User2>;
  readonly userList?: Maybe<ReadonlyArray<User2>>;
  /** @deprecated Use 'user' instead. */
  readonly userProfiles?: Maybe<ReadonlyArray<Maybe<UserInfoOutputQuery>>>;
  readonly usersValidationForCashAccount: ReadonlyArray<UserValidationForCashAccount>;
  readonly ytdRealizedGainLoss: YTDRealizedGainLoss;
  readonly zendeskHelpCenterJwt: Scalars['String'];
};


export type QueryCRMContactsArgs = {
  filter: CRMContactInputFilter;
};


export type QueryacatByIdArgs = {
  where: ACATByIdWhereInput;
};


export type QueryacatDraftArgs = {
  where: DraftWhere;
};


export type QueryacatDraftsArgs = {
  where: DraftsWhere;
};


export type QueryacatTransferStatusLogByIdArgs = {
  where: ACATByIdWhereInput;
};


export type QueryacatsArgs = {
  where: AccountACATSWhereInput;
};


export type QueryaccountBeneficiariesArgs = {
  where: AccountBeneficiariesWhere;
};


export type QueryaccountBeneficiariesByClientsArgs = {
  where: AccountBeneficiariesByClientsWhere;
};


export type QueryaccountBeneficiaryByIdArgs = {
  where: AccountBeneficiaryByIdWhere;
};


export type QueryaccountInfoArgs = {
  input: AccountInfoInput;
};


export type QueryaccountManagementArgs = {
  financialAccountId: Scalars['String'];
};


export type QueryaccountStatesArgs = {
  where: AccountStatesWhere;
};


export type QueryaccountStatesCreationStatusArgs = {
  where: AccountStatesCreationStatusWhere;
};


export type QueryaccountTaxSettingsArgs = {
  financialAccountId: Scalars['ID'];
};


export type QueryaccountsForInternalTransferByOwnersArgs = {
  brokerageAccountId: Scalars['ID'];
  userIds: ReadonlyArray<Scalars['ID']>;
};


export type QueryadoptingEmployerArgs = {
  where: AdoptingEmployerWhere;
};


export type QueryaggrAccountHistoryArgs = {
  input: AggrAccountHistoryInput;
};


export type QueryaggregatedLossesArgs = {
  input?: InputMaybe<AggregatedLossesInput>;
};


export type QuerybatchesArgs = {
  filter?: InputMaybe<BatchFilter>;
};


export type QuerybenchmarkAssignmentsArgs = {
  filter?: InputMaybe<BenchmarkAssignmentsFilter>;
};


export type QuerybenchmarkDetailArgs = {
  id: Scalars['ID'];
};


export type QuerybenchmarkPerformanceArgs = {
  filter: BenchmarkPerformanceFilterInput;
};


export type QuerybenchmarkSummaryListArgs = {
  filter?: InputMaybe<BenchmarkSummaryListFilter>;
};


export type QuerybenchmarkSymbolsArgs = {
  filter?: InputMaybe<BenchmarkSymbolsFilter>;
};


export type QuerybeneficiariesByUsersArgs = {
  where: BeneficiariesByUserWhere;
};


export type QuerybulkAccountsForInternalTransferByOwnersArgs = {
  brokerageAccountIds: ReadonlyArray<Scalars['ID']>;
};


export type QuerycashAccountsArgs = {
  householdId?: InputMaybe<Scalars['String']>;
};


export type QuerycashLiquidationValidationArgs = {
  amount?: InputMaybe<Scalars['Float']>;
  asideUntil?: InputMaybe<Scalars['String']>;
  financialAccountUuid: Scalars['ID'];
  note?: InputMaybe<Scalars['String']>;
};


export type QuerycashLiquidationsArgs = {
  financialAccountId: Scalars['ID'];
};


export type QuerycheckClientDeleteStatusArgs = {
  groupId: Scalars['ID'];
};


export type QuerycheckIfUserHasPaymentMethodArgs = {
  userId?: InputMaybe<Scalars['ID']>;
};


export type QueryclearingRepCodesArgs = {
  input?: InputMaybe<GetClearingRepCodesInput>;
};


export type QueryclientBrokerageAccountsArgs = {
  status?: InputMaybe<ReadonlyArray<InputMaybe<AccountCreationStatus>>>;
  userId?: InputMaybe<Scalars['ID']>;
};


export type QueryclientsArgs = {
  filter?: InputMaybe<ClientsFilterInput>;
};


export type QuerycolorDraftArgs = {
  where: DraftWhere;
};


export type QuerycolorDraftsArgs = {
  where: DraftsWhere;
};


export type QueryconstraintsArgs = {
  where: ConstraintsWhere;
};


export type QuerycorpMemberArgs = {
  where: CorpMemberWhere;
};


export type QuerycorpMembersArgs = {
  where: CorpMembersWhere;
};


export type QuerycorporationArgs = {
  where: CorporationWhere;
};


export type QuerydecedentArgs = {
  where: DecedentWhere;
};


export type QuerydelayedPriceQuoteArgs = {
  symbols: ReadonlyArray<Scalars['String']>;
};


export type QuerydocumentAgreementUrlArgs = {
  input: DocumentAgreementUrlInput;
};


export type QuerydocumentAgreementsArgs = {
  input: DocumentAgreementsWhereInput;
};


export type QuerydocumentBundleArgs = {
  input: DocumentBundleInput;
};


export type QuerydocumentTemplateArgs = {
  input: DocumentTemplateWhereInput;
};


export type QuerydocumentTemplateEditUrlArgs = {
  input: DocumentTemplateEditUrlInput;
};


export type QuerydocumentTemplatesArgs = {
  input: DocumentTemplatesWhereInput;
};


export type QuerydownloadRealizedCostBasisArgs = {
  input?: InputMaybe<DownloadRealizedCostBasisInput>;
};


export type QueryfeeAllocationsArgs = {
  input: GetFeeAllocationInput;
};


export type QueryfeeRoutingHouseholdPreferencesArgs = {
  filter: FeeTemplateHouseholdTypeFilter;
};


export type QueryfeeRoutingOrgPreferencesArgs = {
  filter: FeeTemplateTypeFilter;
};


export type QueryfinancialAccountArgs = {
  where: FinancialAccountWhereInput;
};


export type QueryfinancialAccountDetailsArgs = {
  financialAccountId: Scalars['ID'];
};


export type QueryfinancialAccountsByUserIdArgs = {
  where: FinancialAccountsByUserIdWhereInput;
};


export type QueryfinancialAccountsForAccountGroupsPageArgs = {
  where: FinancialAccountsForAccountGroupsPageWhereInput;
};


export type QueryfinancialAccountsForAccountsPageArgs = {
  where?: InputMaybe<FinancialAccountsForAccountsPageWhereInput>;
};


export type QueryfinancialAccountsForAccountsReportArgs = {
  where?: InputMaybe<FinancialAccountsForAccountsPageWhereInput>;
};


export type QueryfinancialAccountsForHeldAwayArgs = {
  input: FinancialAccountsForHeldAwayInput;
};


export type QueryfinancialAccountsPerformanceArgs = {
  input: FinancialAccountsPerformanceInput;
};


export type QueryfinancialRiskReturnAnalyticsArgs = {
  input: FinancialRiskReturnAnalyticsInput;
};


export type QueryfixedIncomeInstrumentPriceHistoryArgs = {
  frequency: FixedIncomeInstrumentPriceHistoryFrequencies;
  from: Scalars['String'];
  isin: Scalars['String'];
  to: Scalars['String'];
};


export type QueryfixedIncomeInstrumentsArgs = {
  filter: FixedIncomeInstrumentsSearchRequestInput;
};


export type QueryflattenedAllocationsArgs = {
  financialAccountId: Scalars['ID'];
  portfolioId: Scalars['ID'];
};


export type QueryfundingAccountArgs = {
  brokerageAccountId?: InputMaybe<Scalars['ID']>;
  id: Scalars['ID'];
};


export type QueryfundingAccountsByBrokerageArgs = {
  brokerageAccountIds: ReadonlyArray<Scalars['ID']>;
  isAdvisorForMMA?: InputMaybe<Scalars['Boolean']>;
  isHouseAccount?: InputMaybe<Scalars['Boolean']>;
};


export type QuerygetAccountCashSettingsArgs = {
  financialAccountId: Scalars['ID'];
};


export type QuerygetAccountCashSummaryArgs = {
  finacctId: Scalars['String'];
};


export type QuerygetAccountCreationStatusArgs = {
  financialAccountIds: ReadonlyArray<Scalars['ID']>;
};


export type QuerygetAccountMigrationInfoArgs = {
  financialAccountId: Scalars['ID'];
};


export type QuerygetAccountStateByIdArgs = {
  id: Scalars['ID'];
};


export type QuerygetAdvisorAUMArgs = {
  dateInterval?: InputMaybe<Scalars['Int']>;
  dateIntervalUnit?: InputMaybe<AllowedDateIntervalUnit>;
  dateRange?: InputMaybe<Scalars['String']>;
  dateRangeType?: InputMaybe<AllowedDateRangeType>;
};


export type QuerygetAdvisorAccountsArgs = {
  excludeTypes?: InputMaybe<ReadonlyArray<InputMaybe<AccountClassification>>>;
  needsGroupIds?: InputMaybe<Scalars['Boolean']>;
};


export type QuerygetAdvisorAccounts2Args = {
  options?: InputMaybe<AdvisorAccountsOptions>;
};


export type QuerygetAdvisorAddressArgs = {
  id: Scalars['ID'];
};


export type QuerygetAdvisorDocumentArgs = {
  documentId: Scalars['ID'];
};


export type QuerygetAdvisorDocumentUrlArgs = {
  documentId: Scalars['ID'];
};


export type QuerygetAdvisorHoldingsArgs = {
  includeExternal?: InputMaybe<Scalars['Boolean']>;
};


export type QuerygetAllDocumentsArgs = {
  documentType?: InputMaybe<DocumentType>;
  finacctId: Scalars['String'];
  from?: InputMaybe<Scalars['String']>;
  isLegacyDwDocs?: InputMaybe<Scalars['Boolean']>;
  to?: InputMaybe<Scalars['String']>;
};


export type QuerygetAllFinancialAccountsArgs = {
  accountClassification?: InputMaybe<AccountClassification>;
  institutionName?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
};


export type QuerygetAllFinancialAccountsInGroupArgs = {
  allNonActiveForClient?: InputMaybe<Scalars['Boolean']>;
  groupIDs: ReadonlyArray<InputMaybe<AccountsInGroupInput>>;
  includeExternal?: InputMaybe<Scalars['Boolean']>;
};


export type QuerygetAllUsersInGroupArgs = {
  groupId: Scalars['ID'];
};


export type QuerygetAllocationsArgs = {
  financialIds: ReadonlyArray<Scalars['ID']>;
  includeExternal?: InputMaybe<Scalars['Boolean']>;
};


export type QuerygetBalanceBreakdownArgs = {
  financialIds: ReadonlyArray<Scalars['ID']>;
};


export type QuerygetBeneficiariesArgs = {
  ids: ReadonlyArray<Scalars['ID']>;
};


export type QuerygetBeneficiariesByAccountIDArgs = {
  financialAccountId: Scalars['ID'];
};


export type QuerygetBillingPeriodByIdArgs = {
  billingPeriodId: Scalars['ID'];
  scheduleAssignmentId: Scalars['ID'];
};


export type QuerygetCashTransferListArgs = {
  fundingAccountId: Scalars['ID'];
  status: CashTransferStatus;
};


export type QuerygetClientADVFormsArgs = {
  advisorID: Scalars['ID'];
};


export type QuerygetClientAddressArgs = {
  id: Scalars['ID'];
};


export type QuerygetClientApexAccountsArgs = {
  status?: InputMaybe<ReadonlyArray<InputMaybe<AccountCreationStatus>>>;
  userId?: InputMaybe<Scalars['ID']>;
};


export type QuerygetClientDocumentUrlArgs = {
  documentId: Scalars['ID'];
};


export type QuerygetClientFinancialInfoArgs = {
  from: Scalars['String'];
  to: Scalars['String'];
};


export type QuerygetCostBasisArgs = {
  financialIds: ReadonlyArray<Scalars['ID']>;
};


export type QuerygetCostBasisHoldingsArgs = {
  financialIds: ReadonlyArray<Scalars['ID']>;
};


export type QuerygetEntityHouseAccountDetailsArgs = {
  financialAccountId: Scalars['ID'];
};


export type QuerygetExcludeDaysBeforeFundingArgs = {
  accountId: Scalars['ID'];
  orgId?: InputMaybe<Scalars['ID']>;
};


export type QuerygetFeeAllocationByGroupArgs = {
  groupId: Scalars['ID'];
};


export type QuerygetFeeAllocationsByScheduleArgs = {
  scheduleAssignmentId: Scalars['ID'];
};


export type QuerygetFeeInvoicePeriodByIdArgs = {
  id: Scalars['String'];
};


export type QuerygetFinancialAccountGroupPairsArgs = {
  ids?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
};


export type QuerygetFinancialAccountOwnersArgs = {
  financialAccountId: Scalars['ID'];
};


export type QuerygetFinancialAccountTrustDetailsArgs = {
  financialAccountId: Scalars['ID'];
};


export type QuerygetFinancialAccountsArgs = {
  accountIDs?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['ID']>>>;
};


export type QuerygetFlattenedAllocationPreviewArgs = {
  weightedModels: ReadonlyArray<WeightedModelInput>;
};


export type QuerygetFlattenedPortfolioAllocationsArgs = {
  brokerageAccountId: Scalars['ID'];
  portfolioId: Scalars['ID'];
};


export type QuerygetGroupScheduleArgs = {
  groupId: Scalars['ID'];
};


export type QuerygetHouseholdArgs = {
  householdId: Scalars['ID'];
};


export type QuerygetHubSpotContactByIdArgs = {
  hubspotContactId: Scalars['String'];
};


export type QuerygetInstitutionsListArgs = {
  search: Scalars['String'];
};


export type QuerygetIntegrationSourceInfoArgs = {
  sourceID: Scalars['ID'];
};


export type QuerygetIntegrationSourceSeriesArgs = {
  sourceID?: InputMaybe<Scalars['ID']>;
};


export type QuerygetInviteStatusArgs = {
  userIds: ReadonlyArray<InputMaybe<Scalars['ID']>>;
};


export type QuerygetInvoiceArgs = {
  id: Scalars['ID'];
};


export type QuerygetInvoiceRunnerResponseArgs = {
  format?: InputMaybe<Scalars['String']>;
  periodId: Scalars['String'];
};


export type QuerygetInvoicesByGroupIDArgs = {
  id: Scalars['ID'];
};


export type QuerygetIraConstraintsArgs = {
  brokerageAccountId: Scalars['ID'];
  direction: TransactionDirection;
};


export type QuerygetJointUserProfilesArgs = {
  accountStateId: Scalars['ID'];
};


export type QuerygetLoginNamesArgs = {
  userID?: InputMaybe<Scalars['ID']>;
};


export type QuerygetModelByIdArgs = {
  id: Scalars['ID'];
};


export type QuerygetOrderDetailArgs = {
  orderId: Scalars['String'];
};


export type QuerygetOrganizationPreferencesArgs = {
  orgId?: InputMaybe<Scalars['ID']>;
};


export type QuerygetPaymentMethodInfoArgs = {
  userId: Scalars['ID'];
};


export type QuerygetPendingUsernameArgs = {
  userId?: InputMaybe<Scalars['ID']>;
};


export type QuerygetPerformanceArgs = {
  financialEntityId?: InputMaybe<Scalars['ID']>;
  financialEntityType?: InputMaybe<FinancialEntityType>;
  financialIds: ReadonlyArray<Scalars['ID']>;
  from: Scalars['String'];
  includeDailyTWR?: InputMaybe<Scalars['Boolean']>;
  realTime?: InputMaybe<Scalars['Boolean']>;
  removePlaceholderAumDataPoint?: InputMaybe<Scalars['Boolean']>;
  timeAlign?: InputMaybe<Scalars['Boolean']>;
  to?: InputMaybe<Scalars['String']>;
};


export type QuerygetPlaidLinkTokenArgs = {
  where?: InputMaybe<PlaidLinkTokenWhere>;
};


export type QuerygetPortfolioAssignmentByIdArgs = {
  financialAccountId: Scalars['ID'];
};


export type QuerygetPortfolioByIdArgs = {
  id: Scalars['ID'];
};


export type QuerygetPortfolioListArgs = {
  owner?: InputMaybe<PortfolioOwner>;
};


export type QuerygetPortfoliosBySeriesIDWithUpdateDateArgs = {
  seriesID?: InputMaybe<Scalars['ID']>;
};


export type QuerygetRealTimeMarketQuoteArgs = {
  finacctId: Scalars['String'];
  symbol: Scalars['String'];
  type: SecurityListType;
  userId: Scalars['String'];
};


export type QuerygetRealizedCostBasisArgs = {
  input: RealizedCostBasisInput;
};


export type QuerygetScheduleAssignmentsByGroupArgs = {
  groupId: Scalars['ID'];
};


export type QuerygetScheduleByIdArgs = {
  id: Scalars['ID'];
};


export type QuerygetScheduledTransfersListArgs = {
  financialAccountIds: ReadonlyArray<Scalars['ID']>;
};


export type QuerygetSecuritiesArgs = {
  financialIds: ReadonlyArray<Scalars['ID']>;
  securityType: SecurityType;
};


export type QuerygetSecuritiesHoldingsArgs = {
  financialIds: ReadonlyArray<Scalars['ID']>;
  securityType: SecurityType;
};


export type QuerygetSecuritiesListArgs = {
  search: Scalars['String'];
  types?: InputMaybe<ReadonlyArray<InputMaybe<SecurityListType>>>;
};


export type QuerygetSecuritiesListForModelsArgs = {
  search: Scalars['String'];
  types?: InputMaybe<ReadonlyArray<InputMaybe<SecurityListType>>>;
};


export type QuerygetSecurityArgs = {
  symbol: Scalars['String'];
};


export type QuerygetStripeCustomerArgs = {
  userId: Scalars['ID'];
};


export type QuerygetSubscriptionInfoArgs = {
  userId?: InputMaybe<Scalars['ID']>;
};


export type QuerygetTopClientsArgs = {
  offset?: InputMaybe<Scalars['Int']>;
  resultLimit?: InputMaybe<Scalars['Int']>;
  sortMethod?: InputMaybe<Scalars['String']>;
};


export type QuerygetTopHoldingsArgs = {
  offset?: InputMaybe<Scalars['Int']>;
  resultLimit?: InputMaybe<Scalars['Int']>;
  sortMethod?: InputMaybe<Scalars['String']>;
};


export type QuerygetTransactionsArgs = {
  financialIds: ReadonlyArray<Scalars['ID']>;
  from: Scalars['String'];
  isLegacyDwActivity?: InputMaybe<Scalars['Boolean']>;
  to?: InputMaybe<Scalars['String']>;
};


export type QuerygetTrustedContactByIDArgs = {
  id: Scalars['ID'];
};


export type QuerygetUserInGroupDetailsArgs = {
  userId: Scalars['ID'];
};


export type QuerygetUserManualAuthTokenArgs = {
  fundingAccountId: Scalars['ID'];
};


export type QuerygetUserProfileInfoArgs = {
  userID?: InputMaybe<Scalars['ID']>;
};


export type QuerygetV2ScheduledTransfersListArgs = {
  financialAccountIds: ReadonlyArray<Scalars['ID']>;
};


export type QuerygetZendeskSSOJwtArgs = {
  contact: ZendeskContact;
};


export type QuerygroupByIdArgs = {
  id?: InputMaybe<Scalars['String']>;
};


export type QueryheldAwayPlaidRelinkTokenArgs = {
  input: HeldAwayPlaidRelinkTokenInput;
};


export type QueryholdingsByDateArgs = {
  date: Scalars['DateTime'];
};


export type QueryholdingsPerformanceArgs = {
  input: HoldingsPerformanceInput;
};


export type QueryhouseholdArgs = {
  householdId: Scalars['ID'];
};


export type QueryhubspotUserArgs = {
  hubspotContactId?: InputMaybe<Scalars['String']>;
};


export type QueryhypotheticalPerformanceArgs = {
  input: HypotheticalPerformanceInput;
};


export type QueryintegrationStatusByIDArgs = {
  id: Scalars['String'];
};


export type QueryintegrationsArgs = {
  filter?: InputMaybe<IntegrationsInputFilter>;
};


export type QueryinternalCashTransferAccountsArgs = {
  brokerageAccountIds: ReadonlyArray<Scalars['ID']>;
};


export type QueryinvitedTeamMembersArgs = {
  filter?: InputMaybe<TeamMembersInput>;
};


export type QueryinvitedUserArgs = {
  userId: Scalars['ID'];
};


export type QueryinvoiceArgs = {
  invoiceId: Scalars['ID'];
};


export type QueryisAccountCreatedUnder24HoursArgs = {
  id: Scalars['ID'];
};


export type QueryisMMATransferArgs = {
  transferId: Scalars['ID'];
};


export type QuerylastRebalanceArgs = {
  financialAccountId: Scalars['ID'];
};


export type QuerymarketplacePortfoliosArgs = {
  portfolioIds?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['ID']>>>;
};


export type QuerymatchConsumerArgs = {
  matchRequestId: Scalars['ID'];
};


export type QueryminorArgs = {
  where: MinorWhere;
};


export type QuerymmaArgs = {
  id: Scalars['ID'];
};


export type QuerymmaByBrokerageArgs = {
  brokerageAccountIds: ReadonlyArray<Scalars['ID']>;
};


export type QuerymmaPreviewUsersArgs = {
  where: MMAPreviewUsersInput;
};


export type QuerymodelArgs = {
  id: Scalars['ID'];
};


export type QuerynextRebalanceArgs = {
  financialAccountId: Scalars['ID'];
};


export type QuerynotificationsArgs = {
  input: NotificationsWhereInput;
};


export type QueryorgPartnerIntegrationsArgs = {
  filter?: InputMaybe<OrgPartnerIntegrationInputFilter>;
};


export type QueryplanDetailsArgs = {
  where: PlanDetailsWhere;
};


export type QueryportfolioArgs = {
  id: Scalars['ID'];
};


export type QueryportfolioCharacteristicsArgs = {
  input: PortfolioCharacteristicsInput;
};


export type QuerypreTransferInfoArgs = {
  amount: Scalars['Int'];
  financialAccountId: Scalars['String'];
  transferDirection: TransferDirection;
};


export type QueryremainingTaxBudgetArgs = {
  financialAccountId: Scalars['ID'];
};


export type QueryrepcodesArgs = {
  filter?: InputMaybe<RepcodeFilter>;
};


export type QueryreportSchedulesArgs = {
  filter: ReportSchedulesFilter;
};


export type QueryretirementContributionsReportArgs = {
  input: ReportFileWhereInput;
};


export type QueryriskReturnAnalyticsArgs = {
  input: RiskReturnAnalyticsInput;
};


export type QueryscheduleArgs = {
  scheduleId: Scalars['ID'];
};


export type QuerysearchOrdersArgs = {
  options: SearchOrderInput;
};


export type QuerysearchSecurityPerformanceDateRangeArgs = {
  input: SearchSecurityPerformanceDateRangeInput;
};


export type QuerysearchSpliceAccountHistoryArgs = {
  input: SearchSpliceAccountHistoryInput;
};


export type QuerysecuritiesArgs = {
  symbols: ReadonlyArray<Scalars['String']>;
};


export type QuerysecurityConstraintsArgs = {
  financialAccountId: Scalars['ID'];
};


export type QuerysecurityPerformanceDateRangeArgs = {
  input: SecurityPerformanceDateRangeInput;
};


export type QueryseriesArgs = {
  sourceID?: InputMaybe<Scalars['ID']>;
};


export type QuerysingleParticipantArgs = {
  where: SingleParticipantWhere;
};


export type QuerytaxManagementAvailableArgs = {
  financialAccountId: Scalars['ID'];
};


export type QueryteamMembersArgs = {
  filter?: InputMaybe<TeamMembersInput>;
};


export type QuerytimeWeightedReturnsArgs = {
  input: TimeWeightedReturnsInput;
};


export type QuerytrustedDeviceArgs = {
  fingerprintHash: Scalars['String'];
};


export type QueryuserArgs = {
  filter: UserFilterInput;
};


export type QueryuserListArgs = {
  filter: UserListInput;
};


export type QueryuserProfilesArgs = {
  userIDs: ReadonlyArray<InputMaybe<Scalars['ID']>>;
};


export type QueryusersValidationForCashAccountArgs = {
  where: UsersValidationForCashAccountWhere;
};


export type QueryytdRealizedGainLossArgs = {
  financialAccountId: Scalars['ID'];
};


export type QueryzendeskHelpCenterJwtArgs = {
  contact?: InputMaybe<ZendeskContact>;
};

export enum REGISTRATION {
  SEC = 'SEC',
  STATE = 'STATE'
}

export type RealTimeMarketQuoteOutput = {
  readonly __typename?: 'RealTimeMarketQuoteOutput';
  readonly amountNetChange?: Maybe<Scalars['Float']>;
  readonly ask?: Maybe<Scalars['Float']>;
  readonly askAsOf?: Maybe<Scalars['String']>;
  readonly askMarket?: Maybe<Scalars['String']>;
  readonly askSize?: Maybe<Scalars['Int']>;
  /** @deprecated Use `askAsOf`, `bidAsOf`, or `mfAsOf`. */
  readonly asof?: Maybe<Scalars['String']>;
  readonly bid?: Maybe<Scalars['Float']>;
  readonly bidAsOf?: Maybe<Scalars['String']>;
  readonly bidMarket?: Maybe<Scalars['String']>;
  readonly bidSize?: Maybe<Scalars['Int']>;
  readonly caveatEmptorWarning?: Maybe<Scalars['String']>;
  readonly error?: Maybe<Scalars['String']>;
  readonly high?: Maybe<Scalars['Float']>;
  readonly lastTrade?: Maybe<Scalars['Float']>;
  readonly lastTradeMarket?: Maybe<Scalars['String']>;
  readonly lastVolume?: Maybe<Scalars['Int']>;
  readonly low?: Maybe<Scalars['Float']>;
  readonly mfAmountNetChange?: Maybe<Scalars['Float']>;
  readonly mfAsOf?: Maybe<Scalars['String']>;
  readonly mfPercentageNetChange?: Maybe<Scalars['Float']>;
  readonly mfPrice?: Maybe<Scalars['Float']>;
  readonly open?: Maybe<Scalars['Float']>;
  readonly percentageNetChange?: Maybe<Scalars['Float']>;
  readonly priorClose?: Maybe<Scalars['Float']>;
  readonly shortExchangeName?: Maybe<Scalars['String']>;
  readonly symbol?: Maybe<Scalars['String']>;
  readonly tierIdentifier?: Maybe<Scalars['String']>;
  readonly tradeIndicatorFlag?: Maybe<Scalars['String']>;
  readonly type?: Maybe<Scalars['String']>;
  readonly volume?: Maybe<Scalars['Int']>;
};

export type RealizedCostBasis = {
  readonly __typename?: 'RealizedCostBasis';
  readonly closeDate?: Maybe<Scalars['String']>;
  readonly closingMarketValue?: Maybe<Scalars['Float']>;
  readonly costBasis?: Maybe<Scalars['Float']>;
  readonly disallowedAmount?: Maybe<Scalars['Float']>;
  readonly openDate?: Maybe<Scalars['String']>;
  readonly quantity?: Maybe<Scalars['Float']>;
  readonly realizedGainLoss?: Maybe<Scalars['Float']>;
  readonly symbol?: Maybe<Scalars['String']>;
  readonly term?: Maybe<Scalars['String']>;
  readonly washSaleIndicator?: Maybe<Scalars['String']>;
};

export type RealizedCostBasisInput = {
  readonly financialAccountId: Scalars['ID'];
  readonly pageNum?: InputMaybe<Scalars['Float']>;
  readonly pageSize?: InputMaybe<Scalars['Float']>;
  readonly sortBy?: InputMaybe<Scalars['String']>;
  readonly sortOrder?: InputMaybe<SortOrder>;
  readonly symbol?: InputMaybe<Scalars['String']>;
  readonly taxYear?: InputMaybe<Scalars['Int']>;
};

export type RealizedCostBasisResponse = {
  readonly __typename?: 'RealizedCostBasisResponse';
  readonly currentPage?: Maybe<Scalars['Float']>;
  readonly pageSize?: Maybe<Scalars['Float']>;
  readonly realizedCostBasisList?: Maybe<ReadonlyArray<Maybe<RealizedCostBasis>>>;
  readonly totalItems?: Maybe<Scalars['Float']>;
  readonly totalPages?: Maybe<Scalars['Float']>;
};

export type RebalanceOccurence = {
  readonly __typename?: 'RebalanceOccurence';
  readonly completed?: Maybe<Scalars['DateTime']>;
  readonly id: Scalars['ID'];
  readonly lastExecuted?: Maybe<Scalars['DateTime']>;
  readonly scheduleId?: Maybe<Scalars['ID']>;
  readonly scheduleType?: Maybe<RebalanceScheduleType>;
  readonly source: Scalars['String'];
  readonly startDate?: Maybe<Scalars['DateTime']>;
  /** @deprecated Temporary field to work around backend DateTime issues */
  readonly startDateOnly?: Maybe<Scalars['DateOnly']>;
};

export enum RebalanceScheduleType {
  CASH_LIQUIDATION = 'CASH_LIQUIDATION',
  REGULAR = 'REGULAR',
  SAME_DAY = 'SAME_DAY'
}

export type Rebalancer = {
  readonly __typename?: 'Rebalancer';
  /** Do NOT cache this value because of daylight savings. */
  readonly generateProposalTime: Scalars['DateTime'];
  /** Do NOT cache this value because of daylight savings. */
  readonly portfolioUpdateCutoffTimeForProposals: Scalars['DateTime'];
  /** Do NOT cache this value because of daylight savings. */
  readonly submitProposalTime: Scalars['DateTime'];
};

export enum RecordType {
  allocationActivity = 'allocationActivity',
  orderActivity = 'orderActivity'
}

export type RedTail = {
  readonly __typename?: 'RedTail';
  readonly type: Scalars['String'];
};

export enum RegisteredBy {
  SEC = 'SEC',
  STATE = 'STATE'
}

export type RelinkHeldAwayInput = {
  readonly accounts: ReadonlyArray<HeldAwayAccountInput>;
  readonly heldAwayLinkId: Scalars['String'];
  readonly institution: HeldAwayInstitutionInput;
  readonly publicToken: Scalars['String'];
  readonly sessionId: Scalars['String'];
};

export type RemainingTaxBudget = {
  readonly __typename?: 'RemainingTaxBudget';
  readonly allocatedForCurrentPeriod: Scalars['Float'];
  readonly annualBudget: Scalars['Float'];
  readonly annualBudgetRemaining: Scalars['Float'];
  readonly financialAccountId: Scalars['ID'];
  readonly prorataPeriod: TaxBudgetDistribution;
  readonly remainingForCurrentPeriod: Scalars['Float'];
  readonly taxBudgetType: TaxBudgetUnits;
  readonly ytdBudgetConsumed: Scalars['Float'];
};

export type RemoveFunding = {
  readonly __typename?: 'RemoveFunding';
  readonly fundingAccountId: Scalars['ID'];
};

export type Repcode = {
  readonly __typename?: 'Repcode';
  readonly accountsState?: Maybe<ReadonlyArray<StateOutput>>;
  readonly assignedUsers: ReadonlyArray<Advisor2>;
  readonly balance: Scalars['Float'];
  readonly id: Scalars['ID'];
  readonly institution: Institution;
  readonly isDefault?: Maybe<Scalars['Boolean']>;
  readonly label: Scalars['String'];
  readonly numberOfAccounts: Scalars['Int'];
  readonly repcode: Scalars['String'];
};

export type RepcodeFilter = {
  readonly institution?: InputMaybe<Institution>;
};

export enum ReportAssignmentAction {
  ASSIGN = 'ASSIGN',
  UNASSIGN = 'UNASSIGN'
}

export enum ReportAssignmentEntityType {
  ACCOUNT = 'ACCOUNT',
  HOUSEHOLD = 'HOUSEHOLD'
}

export type ReportAssignmentUpdateOutput = {
  readonly __typename?: 'ReportAssignmentUpdateOutput';
  readonly scheduleAssignmentId: Scalars['ID'];
  readonly status: Scalars['String'];
};

export type ReportFile = {
  readonly __typename?: 'ReportFile';
  readonly endDate?: Maybe<Scalars['String']>;
  readonly organizationId?: Maybe<Scalars['ID']>;
  readonly pathToFile?: Maybe<Scalars['String']>;
  readonly reportId?: Maybe<Scalars['ID']>;
  readonly startDate?: Maybe<Scalars['String']>;
  readonly status?: Maybe<ReportStatus>;
  readonly type?: Maybe<ReportType>;
};

export type ReportFileWhereInput = {
  readonly date: Scalars['DateOnly'];
  readonly financialAccountId?: InputMaybe<Scalars['ID']>;
  readonly householdId?: InputMaybe<Scalars['ID']>;
};

export type ReportSchedule = {
  readonly __typename?: 'ReportSchedule';
  readonly assignments?: Maybe<ReadonlyArray<ReportScheduleAssignment>>;
  readonly dayOfMonth?: Maybe<Scalars['Int']>;
  readonly dayOfWeek?: Maybe<ReportScheduleDayOfWeek>;
  readonly frequency: ReportScheduleFrequency;
  readonly id: Scalars['ID'];
  readonly lastRunAt?: Maybe<Scalars['DateOnly']>;
  readonly nextRunAt?: Maybe<Scalars['DateOnly']>;
  readonly reportType: ReportScheduleType;
  readonly status?: Maybe<ReportScheduleStatus>;
  readonly timeOfDay?: Maybe<Scalars['String']>;
};

export type ReportScheduleAssignment = {
  readonly __typename?: 'ReportScheduleAssignment';
  readonly advisorId: Scalars['ID'];
  readonly entityId: Scalars['ID'];
  readonly entityType: ReportAssignmentEntityType;
  readonly id: Scalars['ID'];
  readonly lastRunAt?: Maybe<Scalars['DateOnly']>;
  readonly recipientId: Scalars['ID'];
  readonly sendReportCopyToAdvisor: Scalars['Boolean'];
};

export enum ReportScheduleDayOfWeek {
  FRI = 'FRI',
  MON = 'MON',
  SAT = 'SAT',
  SUN = 'SUN',
  THURS = 'THURS',
  TUES = 'TUES',
  WED = 'WED'
}

export enum ReportScheduleFrequency {
  DAILY = 'DAILY',
  MONTHLY = 'MONTHLY',
  QUARTERLY = 'QUARTERLY',
  UNSCHEDULED = 'UNSCHEDULED',
  WEEKLY = 'WEEKLY',
  YEARLY = 'YEARLY'
}

export enum ReportScheduleStatus {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE'
}

export enum ReportScheduleType {
  INVESTMENT_SUMMARY = 'INVESTMENT_SUMMARY',
  PERFORMANCE_SUMMARY = 'PERFORMANCE_SUMMARY'
}

export type ReportSchedulesFilter = {
  readonly reportType: ReportScheduleType;
  readonly status?: InputMaybe<ReportScheduleStatus>;
};

export enum ReportStatus {
  FAILED = 'FAILED',
  GENERATED = 'GENERATED',
  GENERATING = 'GENERATING'
}

export type ReportSummaryOutput = {
  readonly __typename?: 'ReportSummaryOutput';
  readonly householdId: Scalars['ID'];
  readonly reportLastDay?: Maybe<Scalars['DateOnly']>;
  readonly reportStartDay?: Maybe<Scalars['DateOnly']>;
  readonly status: Scalars['String'];
};

export enum ReportType {
  HOLDINGS_BY_DATE = 'HOLDINGS_BY_DATE',
  RETIREMENT_CONTRIBUTIONS = 'RETIREMENT_CONTRIBUTIONS'
}

export type ReportsOutput = {
  readonly __typename?: 'ReportsOutput';
  readonly reports?: Maybe<ReadonlyArray<Maybe<ReportSummaryOutput>>>;
};

export enum RequestStatus {
  ACCEPTED = 'ACCEPTED',
  ACTIVE = 'ACTIVE',
  BOOKED = 'BOOKED',
  EXPIRED = 'EXPIRED',
  NEEDS_REVIEW = 'NEEDS_REVIEW',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED'
}

export type RequiredDateTimeRange = {
  readonly __typename?: 'RequiredDateTimeRange';
  readonly end: Scalars['DateTime'];
  readonly start: Scalars['DateTime'];
};

export type RerunInvoicesOutput = {
  readonly __typename?: 'RerunInvoicesOutput';
  readonly id: Scalars['ID'];
  readonly institutions?: Maybe<ReadonlyArray<Maybe<Scalars['String']>>>;
  readonly invoices?: Maybe<ReadonlyArray<Maybe<FeeInvoicePeriodInvoice>>>;
  readonly lastCreated?: Maybe<Scalars['String']>;
  readonly lastExported?: Maybe<Scalars['String']>;
  readonly total?: Maybe<Scalars['Float']>;
};

export type ResetPasswordOutput = {
  readonly __typename?: 'ResetPasswordOutput';
  readonly jwt: Scalars['String'];
};

export type ResponseEnvelope = {
  readonly __typename?: 'ResponseEnvelope';
  readonly completionStatus?: Maybe<Scalars['String']>;
  readonly data?: Maybe<OrgIntegration>;
  readonly errors?: Maybe<ReadonlyArray<ErrorDescription>>;
};

export type RetirementContributionsReport = {
  readonly __typename?: 'RetirementContributionsReport';
  readonly endDate?: Maybe<Scalars['DateOnly']>;
  readonly iraContributions?: Maybe<ReadonlyArray<IraContribution>>;
  readonly pathToFile?: Maybe<Scalars['String']>;
  readonly reportId?: Maybe<Scalars['ID']>;
  readonly startDate?: Maybe<Scalars['DateOnly']>;
  readonly status?: Maybe<ReportStatus>;
  readonly type?: Maybe<ReportType>;
};

export type ReuseExistingBeneficiaryInput = {
  readonly accountId: Scalars['ID'];
  readonly accountTargetId: Scalars['ID'];
  readonly beneficiaryClass: BeneficiaryClass;
  readonly designation: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly relationship: Scalars['String'];
};

export type ReviewReturn = {
  readonly __typename?: 'ReviewReturn';
  readonly advisorId?: Maybe<Scalars['ID']>;
  readonly complianceUser?: Maybe<Scalars['String']>;
  readonly newAdvisorStatus?: Maybe<Scalars['String']>;
  readonly newDraftStatus?: Maybe<Scalars['String']>;
};

export type RevokeThirdPartyAccessInput = {
  readonly clientId: Scalars['String'];
};

export type RevokeThirdPartyAccessResponse = {
  readonly __typename?: 'RevokeThirdPartyAccessResponse';
  readonly clientId: Scalars['ID'];
  readonly clientName?: Maybe<Scalars['String']>;
  readonly partnerName?: Maybe<Scalars['String']>;
  readonly status?: Maybe<ThirdPartyAccessStatus>;
};

export type RightCapital = {
  readonly __typename?: 'RightCapital';
  readonly type: Scalars['String'];
};

export type RiskReturnAnalytics = {
  readonly __typename?: 'RiskReturnAnalytics';
  readonly analytics?: Maybe<RiskReturnAnalyticsStats>;
  readonly responseCode?: Maybe<Scalars['String']>;
};

export type RiskReturnAnalyticsInput = {
  readonly benchmarkId?: InputMaybe<Scalars['ID']>;
  readonly dateRangeConfig: PerformanceAnalyticsDateRangeConfigInput;
  readonly resourceType: PerformanceAnalyticsResourceType;
  readonly resourceTypeId: Scalars['ID'];
};

export type RiskReturnAnalyticsResponse = {
  readonly __typename?: 'RiskReturnAnalyticsResponse';
  readonly account?: Maybe<RiskReturnAnalytics>;
  readonly benchmark?: Maybe<RiskReturnAnalytics>;
  readonly endDate?: Maybe<Scalars['DateOnly']>;
  readonly startDate?: Maybe<Scalars['DateOnly']>;
};

export type RiskReturnAnalyticsStats = {
  readonly __typename?: 'RiskReturnAnalyticsStats';
  readonly annualizedTimeWeightedReturn?: Maybe<Scalars['Float']>;
  readonly beta?: Maybe<Scalars['Float']>;
  readonly cumulativeTimeWeightedReturn?: Maybe<Scalars['Float']>;
  readonly downsideCapture?: Maybe<Scalars['Float']>;
  readonly maxDrawdown?: Maybe<Scalars['Float']>;
  readonly rSquared?: Maybe<Scalars['Float']>;
  readonly sharpeRatio?: Maybe<Scalars['Float']>;
  readonly standardDeviation?: Maybe<Scalars['Float']>;
  readonly upsideCapture?: Maybe<Scalars['Float']>;
};

export enum RolloverEnum {
  CHECK = 'CHECK',
  WIRE = 'WIRE'
}

export type RolloverInput = {
  readonly type?: InputMaybe<RolloverEnum>;
};

export type RolloverType = {
  readonly __typename?: 'RolloverType';
  readonly type?: Maybe<RolloverEnum>;
};

export enum SERVICES {
  BUSINESS = 'BUSINESS',
  FINANCIAL = 'FINANCIAL',
  INVESTMENT = 'INVESTMENT',
  RETIREMENT = 'RETIREMENT',
  TAX = 'TAX',
  WEALTH = 'WEALTH'
}

export type SSGValetForm = {
  readonly __typename?: 'SSGValetForm';
  readonly fileCategory?: Maybe<Scalars['String']>;
  readonly fileDescription?: Maybe<Scalars['String']>;
  readonly fileTitle?: Maybe<Scalars['String']>;
  readonly objectKey?: Maybe<Scalars['String']>;
  readonly updatedAt?: Maybe<Scalars['String']>;
};

export enum STATE_INITIAL {
  AK = 'AK',
  AL = 'AL',
  AR = 'AR',
  AZ = 'AZ',
  CA = 'CA',
  CO = 'CO',
  CT = 'CT',
  DC = 'DC',
  DE = 'DE',
  FL = 'FL',
  GA = 'GA',
  HI = 'HI',
  IA = 'IA',
  ID = 'ID',
  IL = 'IL',
  IN = 'IN',
  KS = 'KS',
  KY = 'KY',
  LA = 'LA',
  MA = 'MA',
  MD = 'MD',
  ME = 'ME',
  MI = 'MI',
  MN = 'MN',
  MO = 'MO',
  MS = 'MS',
  MT = 'MT',
  NC = 'NC',
  ND = 'ND',
  NE = 'NE',
  NH = 'NH',
  NJ = 'NJ',
  NM = 'NM',
  NV = 'NV',
  NY = 'NY',
  OH = 'OH',
  OK = 'OK',
  OR = 'OR',
  PA = 'PA',
  PR = 'PR',
  RI = 'RI',
  SC = 'SC',
  SD = 'SD',
  TN = 'TN',
  TX = 'TX',
  UT = 'UT',
  VA = 'VA',
  VI = 'VI',
  VT = 'VT',
  WA = 'WA',
  WI = 'WI',
  WV = 'WV',
  WY = 'WY'
}

export type SafeTrustDeviceInput = {
  readonly deviceType: DeviceType;
};

export type SaveStateBulkInput = {
  readonly states: ReadonlyArray<SaveStateInput>;
};

export type SaveStateInput = {
  readonly account?: InputMaybe<AccountInput>;
  readonly groupId: Scalars['ID'];
  readonly jointUserIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
  readonly openingInPerson?: InputMaybe<Scalars['Boolean']>;
  readonly state?: InputMaybe<State>;
  readonly trustUserIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
  readonly userId: Scalars['ID'];
};

export type Schedule = {
  readonly __typename?: 'Schedule';
  readonly billTiming?: Maybe<AllowedBillTiming>;
  readonly fee?: Maybe<Scalars['Float']>;
  readonly frequency?: Maybe<AllowedFrequency>;
  readonly hasAccountLevelAssignments?: Maybe<Scalars['Boolean']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly numberOfAccounts?: Maybe<Scalars['Int']>;
  readonly numberOfHouseholds?: Maybe<Scalars['Int']>;
  readonly plans?: Maybe<ReadonlyArray<Maybe<Plan>>>;
  readonly scheduleName?: Maybe<Scalars['String']>;
  readonly type?: Maybe<ExtendedAllowedType>;
};

export type Schedule2 = {
  readonly __typename?: 'Schedule2';
  readonly balanceMetrics?: Maybe<AllowedBalanceMetrics>;
  readonly billTiming?: Maybe<AllowedBillTiming>;
  readonly blendedRate?: Maybe<Scalars['Boolean']>;
  readonly fee?: Maybe<Scalars['Float']>;
  readonly frequency?: Maybe<AllowedFrequency>;
  readonly hasAccountLevelAssignments?: Maybe<Scalars['Boolean']>;
  readonly id: Scalars['ID'];
  readonly numberOfAccounts?: Maybe<Scalars['Int']>;
  readonly numberOfHouseholds?: Maybe<Scalars['Int']>;
  readonly plans?: Maybe<ReadonlyArray<Maybe<Plan2>>>;
  readonly scheduleName?: Maybe<Scalars['String']>;
  readonly tiers?: Maybe<ReadonlyArray<Maybe<Tier2>>>;
  readonly type?: Maybe<AllowedType>;
};

export type ScheduleAssignment = {
  readonly endDate?: Maybe<Scalars['String']>;
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
  readonly scheduleId?: Maybe<Scalars['ID']>;
  readonly startDate?: Maybe<Scalars['DateOnly']>;
};

export type ScheduleAssignment2 = {
  readonly __typename?: 'ScheduleAssignment2';
  readonly account?: Maybe<FinancialAccount2>;
  readonly billingGroup?: Maybe<BillingGroup>;
  readonly billingGroupId?: Maybe<Scalars['ID']>;
  readonly billingPeriod?: Maybe<BillingPeriod>;
  readonly schedule?: Maybe<Schedule2>;
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
};

export type ScheduleAssignmentGroup = {
  readonly __typename?: 'ScheduleAssignmentGroup';
  readonly endDate?: Maybe<Scalars['String']>;
  readonly financialAccounts?: Maybe<ReadonlyArray<Maybe<FinancialAccount>>>;
  readonly groupId?: Maybe<Scalars['String']>;
  readonly level?: Maybe<AssignmentLevel>;
  readonly scheduleAssignmentId?: Maybe<Scalars['String']>;
  readonly scheduleId?: Maybe<Scalars['String']>;
  readonly scheduleName?: Maybe<Scalars['String']>;
  readonly startDate?: Maybe<Scalars['String']>;
};

export type ScheduleAssignmentInput = {
  readonly endDate?: InputMaybe<Scalars['String']>;
  readonly financialAccountId?: InputMaybe<Scalars['ID']>;
  readonly householdIds: ReadonlyArray<Scalars['ID']>;
  readonly scheduleId?: InputMaybe<Scalars['ID']>;
  readonly startDate?: InputMaybe<Scalars['String']>;
};

export type ScheduleAssignmentResponse = {
  readonly __typename?: 'ScheduleAssignmentResponse';
  readonly assignmentLevel: AssignmentLevel;
  readonly scheduleAssignmentId: Scalars['ID'];
};

export type ScheduleAssignments = {
  readonly __typename?: 'ScheduleAssignments';
  readonly groups?: Maybe<ReadonlyArray<Maybe<ScheduleAssignmentGroup>>>;
};

export type ScheduleAssignmentsOutput = {
  readonly __typename?: 'ScheduleAssignmentsOutput';
  readonly endDate?: Maybe<Scalars['String']>;
  readonly financialAccounts?: Maybe<ReadonlyArray<AssignmentFinancialAccount>>;
  readonly groupId: Scalars['ID'];
  readonly level: AssignmentLevel;
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
  readonly scheduleId?: Maybe<Scalars['ID']>;
  readonly scheduleName?: Maybe<Scalars['String']>;
  readonly startDate?: Maybe<Scalars['String']>;
};

export type ScheduleFeeAllocations = {
  readonly __typename?: 'ScheduleFeeAllocations';
  readonly accountAllocations?: Maybe<ReadonlyArray<Maybe<AccountAllocation>>>;
  readonly id: Scalars['ID'];
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
};

export type ScheduleInput = {
  readonly groupId?: InputMaybe<Scalars['ID']>;
  readonly scheduleId?: InputMaybe<Scalars['ID']>;
};

export type ScheduleList = {
  readonly __typename?: 'ScheduleList';
  readonly account?: Maybe<GroupScheduleAccount>;
  readonly balanceMetrics?: Maybe<AllowedBalanceMetrics>;
  readonly billTiming?: Maybe<AllowedBillTiming>;
  readonly billingPeriods?: Maybe<ReadonlyArray<Maybe<BillingPeriod>>>;
  readonly blendedRate?: Maybe<Scalars['Boolean']>;
  readonly fee?: Maybe<Scalars['Float']>;
  readonly frequency?: Maybe<AllowedFrequency>;
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
  readonly scheduleId?: Maybe<Scalars['ID']>;
  readonly scheduleName?: Maybe<Scalars['String']>;
  readonly tiers?: Maybe<ReadonlyArray<Maybe<TierOutput>>>;
  readonly type?: Maybe<AllowedType>;
};

export enum ScheduleType {
  AUM = 'AUM',
  FLAT = 'FLAT',
  FLAT_AUM = 'FLAT_AUM'
}

export type ScheduledJournalTransaction = {
  readonly __typename?: 'ScheduledJournalTransaction';
  readonly amount: Scalars['Float'];
  readonly contributionReason?: Maybe<ContributionReason>;
  readonly contributionYear?: Maybe<IraContributionType>;
  readonly description?: Maybe<Scalars['String']>;
  readonly distributionReason?: Maybe<DistributionReason>;
  readonly distributionYear?: Maybe<IraContributionType>;
  readonly endDate?: Maybe<Scalars['String']>;
  readonly federalTaxWithholdingFormatted?: Maybe<Scalars['String']>;
  readonly federalWithholdingAmount?: Maybe<Scalars['Float']>;
  readonly federalWithholdingType?: Maybe<TaxWithholding>;
  readonly fromBrokerageAccountId: Scalars['ID'];
  readonly fromBrokerageAccountName: Scalars['String'];
  readonly initiatedBy: Scalars['String'];
  readonly journalScheduledTransferUuid?: Maybe<Scalars['ID']>;
  readonly journalTransferStatus: Scalars['String'];
  readonly netAmount?: Maybe<Scalars['Float']>;
  readonly nextTransferDate?: Maybe<Scalars['String']>;
  readonly notes?: Maybe<Scalars['String']>;
  readonly startDate: Scalars['String'];
  readonly stateTaxWithholdingFormatted?: Maybe<Scalars['String']>;
  readonly stateWithholdingAmount?: Maybe<Scalars['Float']>;
  readonly stateWithholdingType?: Maybe<TaxWithholding>;
  readonly toBrokerageAccountId: Scalars['ID'];
  readonly toBrokerageAccountName: Scalars['String'];
};

export type ScheduledTransactionV2 = {
  readonly __typename?: 'ScheduledTransactionV2';
  readonly achs: ReadonlyArray<ScheduledTransactions>;
  readonly journals: ReadonlyArray<ScheduledJournalTransaction>;
};

export type ScheduledTransactions = {
  readonly __typename?: 'ScheduledTransactions';
  readonly amount: Scalars['Float'];
  readonly brokerageAccountId: Scalars['ID'];
  readonly contributionReason?: Maybe<ContributionReason>;
  readonly contributionYear?: Maybe<IraContributionType>;
  readonly direction: TransactionDirection;
  readonly distributionReason?: Maybe<DistributionReason>;
  readonly endDate?: Maybe<Scalars['String']>;
  readonly federalTaxWithholdingFormatted?: Maybe<Scalars['String']>;
  readonly federalWithholdingAmount?: Maybe<Scalars['Float']>;
  readonly federalWithholdingType?: Maybe<TaxWithholding>;
  readonly fundingAccountId: Scalars['ID'];
  readonly initiatedBy: Scalars['String'];
  readonly initiatedById?: Maybe<Scalars['ID']>;
  /** @deprecated use isMMATransfer instead */
  readonly isMMA: Scalars['Boolean'];
  readonly netAmount?: Maybe<Scalars['Float']>;
  readonly nextTransferDate?: Maybe<Scalars['String']>;
  readonly notes?: Maybe<Scalars['String']>;
  readonly scheduledTransferId: Scalars['ID'];
  readonly startDate?: Maybe<Scalars['String']>;
  readonly stateTaxWithholdingFormatted?: Maybe<Scalars['String']>;
  readonly stateWithholdingAmount?: Maybe<Scalars['Float']>;
  readonly stateWithholdingType?: Maybe<TaxWithholding>;
  readonly status?: Maybe<ScheduledTransactionsStatus>;
};

export enum ScheduledTransactionsStatus {
  ACTIVE = 'ACTIVE',
  CANCELLED = 'CANCELLED',
  COMPLETE = 'COMPLETE'
}

export type ScheduledTransferResponse = {
  readonly __typename?: 'ScheduledTransferResponse';
  readonly id: Scalars['ID'];
};

export type SchedulesOutput = {
  readonly __typename?: 'SchedulesOutput';
  readonly billTiming?: Maybe<AllowedBillTiming>;
  readonly frequency?: Maybe<AllowedFrequency>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly plans?: Maybe<ReadonlyArray<Maybe<Plan>>>;
  readonly scheduleName?: Maybe<Scalars['String']>;
};

export enum ScopeOfBusiness {
  COMMERCIAL_RETAIL_BUSINESS = 'COMMERCIAL_RETAIL_BUSINESS',
  FINANCIAL_SERVICES_BUSINESS = 'FINANCIAL_SERVICES_BUSINESS',
  OTHER = 'OTHER'
}

export type SearchOrderInput = {
  readonly actionTypes?: InputMaybe<ReadonlyArray<Scalars['String']>>;
  readonly financialAccountIds?: InputMaybe<ReadonlyArray<Scalars['String']>>;
  readonly from: Scalars['String'];
  readonly includeAllPending?: InputMaybe<Scalars['Boolean']>;
  readonly orderBy?: InputMaybe<OrderBy>;
  readonly orderSources?: InputMaybe<ReadonlyArray<Scalars['String']>>;
  readonly orderStatuses?: InputMaybe<ReadonlyArray<Scalars['String']>>;
  readonly page: Scalars['Int'];
  readonly repCodes?: InputMaybe<ReadonlyArray<Scalars['String']>>;
  readonly size: Scalars['Int'];
  readonly to: Scalars['String'];
};

export type SearchSecurityPerformanceDateRangeInput = {
  readonly resources: ReadonlyArray<SecurityPerformanceDateRangeInput>;
};

export type SearchSecurityPerformanceDateRangeItem = {
  readonly __typename?: 'SearchSecurityPerformanceDateRangeItem';
  readonly maxDate?: Maybe<Scalars['DateOnly']>;
  readonly minDate?: Maybe<Scalars['DateOnly']>;
  readonly resourceId: Scalars['ID'];
};

export type SearchSecurityPerformanceDateRangeResponse = {
  readonly __typename?: 'SearchSecurityPerformanceDateRangeResponse';
  readonly items: ReadonlyArray<SearchSecurityPerformanceDateRangeItem>;
};

export type SearchSpliceAccountHistoryInput = {
  readonly destinationFinancialAccountIds: ReadonlyArray<Scalars['ID']>;
};

export type SearchSpliceAccountHistoryResponseItem = {
  readonly __typename?: 'SearchSpliceAccountHistoryResponseItem';
  readonly created?: Maybe<Scalars['DateTime']>;
  readonly createdBy?: Maybe<Scalars['ID']>;
  readonly destination?: Maybe<SearchSpliceDestination>;
  readonly source?: Maybe<SearchSpliceSource>;
  readonly spliceDate?: Maybe<Scalars['DateOnly']>;
};

export type SearchSpliceDestination = {
  readonly __typename?: 'SearchSpliceDestination';
  readonly financialAccountId?: Maybe<Scalars['ID']>;
};

export type SearchSpliceSource = {
  readonly __typename?: 'SearchSpliceSource';
  readonly accountName?: Maybe<Scalars['String']>;
  readonly accountNumber?: Maybe<Scalars['ID']>;
  readonly custodianId?: Maybe<Scalars['String']>;
  readonly financialAccountId?: Maybe<Scalars['ID']>;
};

export type SecretaryReviewInput = {
  readonly authorizedSignerUserId: Scalars['String'];
  readonly corporationId: Scalars['String'];
};

export type SecretaryReviewResendInput = {
  readonly authorizedSignerUserId: Scalars['String'];
  readonly corporationId: Scalars['String'];
  readonly email: Scalars['String'];
};

export type Securities = {
  readonly __typename?: 'Securities';
  readonly holdings: ReadonlyArray<Maybe<Security>>;
  readonly totalHoldings?: Maybe<Scalars['Float']>;
};

export type SecuritiesHoldings = {
  readonly __typename?: 'SecuritiesHoldings';
  readonly holdings: ReadonlyArray<Maybe<SecurityHolding>>;
  readonly totalHoldings?: Maybe<Scalars['Float']>;
};

export type SecuritiesListOutput = {
  readonly __typename?: 'SecuritiesListOutput';
  readonly cusip: Scalars['String'];
  readonly fractionalSupport?: Maybe<Scalars['Boolean']>;
  readonly id: Scalars['ID'];
  readonly isDfa: Scalars['Boolean'];
  readonly isIntervalFund?: Maybe<Scalars['Boolean']>;
  readonly isTradable: Scalars['Boolean'];
  readonly name: Scalars['String'];
  readonly shareClass?: Maybe<Scalars['String']>;
  readonly symbol: Scalars['String'];
  readonly transactionFee?: Maybe<Scalars['Float']>;
  readonly type: Scalars['String'];
};

export type Security = {
  readonly __typename?: 'Security';
  readonly accounts: ReadonlyArray<Maybe<SecurityAccount>>;
  readonly isAggregate: Scalars['Boolean'];
  /** Security details from Elastic Search */
  readonly securityDetails?: Maybe<SecurityDetails>;
  readonly securityName: Scalars['String'];
  readonly securityType: SecurityType;
  readonly tickerSymbol: Scalars['String'];
  readonly totals: Totals;
};

export type SecurityAccount = {
  readonly __typename?: 'SecurityAccount';
  readonly accountID: Scalars['ID'];
  readonly accountName: Scalars['String'];
  readonly accountNickname?: Maybe<Scalars['String']>;
  readonly financialAccountName: Scalars['String'];
  readonly holdingValue: Scalars['Float'];
  readonly owner?: Maybe<AccountOwner>;
  readonly price: Scalars['Float'];
  readonly quantity: Scalars['Float'];
  readonly weight: Scalars['Float'];
};

export type SecurityConstraint = {
  readonly __typename?: 'SecurityConstraint';
  readonly constraintType: SecurityConstraintType;
  readonly name?: Maybe<Scalars['String']>;
  readonly symbol: Scalars['String'];
};

export type SecurityConstraintInput = {
  readonly constraintType: SecurityConstraintType;
  readonly symbol: Scalars['String'];
};

export enum SecurityConstraintType {
  Exclude = 'Exclude',
  NeverBuy = 'NeverBuy',
  NeverSell = 'NeverSell',
  NeverTrade = 'NeverTrade'
}

export type SecurityDetails = {
  readonly __typename?: 'SecurityDetails';
  readonly isIntervalFund?: Maybe<Scalars['Boolean']>;
  readonly securityType?: Maybe<Scalars['String']>;
  readonly shareClass?: Maybe<Scalars['String']>;
  readonly symbol: Scalars['String'];
  readonly transactionFee?: Maybe<Scalars['Float']>;
};

export type SecurityHolding = {
  readonly __typename?: 'SecurityHolding';
  readonly accounts: ReadonlyArray<Maybe<SecurityAccount>>;
  readonly isAggregate: Scalars['Boolean'];
  readonly securityName: Scalars['String'];
  readonly securityType: SecurityType;
  readonly tickerSymbol: Scalars['String'];
  readonly totalHoldingValue: Scalars['Float'];
  readonly totalPrice: Scalars['Float'];
  readonly totalQuantity: Scalars['Float'];
  readonly totalWeight: Scalars['Float'];
};

export enum SecurityListType {
  CASH = 'CASH',
  EQUITY = 'EQUITY',
  ETF = 'ETF',
  FUND = 'FUND',
  FUNDS = 'FUNDS',
  MUTUAL_FUND = 'MUTUAL_FUND',
  OPTION = 'OPTION',
  STOCKS = 'STOCKS'
}

export type SecurityMasterRecord = {
  readonly __typename?: 'SecurityMasterRecord';
  readonly access_group?: Maybe<Scalars['String']>;
  readonly clearing_institution?: Maybe<Scalars['String']>;
  readonly current_market_price?: Maybe<Scalars['Float']>;
  readonly cusip_code?: Maybe<Scalars['String']>;
  readonly fractional_support?: Maybe<Scalars['Boolean']>;
  readonly is_tradable?: Maybe<Scalars['Boolean']>;
  readonly name?: Maybe<Scalars['String']>;
  readonly partner_security_availability_status?: Maybe<Scalars['String']>;
  readonly partner_security_trading_status?: Maybe<Scalars['String']>;
  readonly security_type?: Maybe<Scalars['String']>;
  readonly security_type_uuid?: Maybe<Scalars['String']>;
  readonly security_uuid?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
  readonly symbol?: Maybe<Scalars['String']>;
  readonly updated?: Maybe<Scalars['String']>;
};

export type SecurityPerformanceDateRangeInput = {
  readonly resourceType: PerformanceAnalyticsResourceType;
  readonly resourceTypeId: Scalars['ID'];
};

export type SecurityPerformanceDateRangeResponse = {
  readonly __typename?: 'SecurityPerformanceDateRangeResponse';
  readonly maxDate?: Maybe<Scalars['DateOnly']>;
  readonly minDate?: Maybe<Scalars['DateOnly']>;
};

export enum SecurityType {
  ALL = 'ALL',
  BONDS = 'BONDS',
  CASH = 'CASH',
  FUNDS = 'FUNDS',
  OTHER = 'OTHER',
  STOCKS = 'STOCKS'
}

export type SeriesPortfolios = {
  readonly __typename?: 'SeriesPortfolios';
  readonly assetPortfolioId?: Maybe<Scalars['ID']>;
  readonly brokeragePortfolioId: Scalars['ID'];
  readonly drift?: Maybe<PortfolioDrift>;
  readonly equityPct?: Maybe<Scalars['Int']>;
  readonly fixedIncomePct?: Maybe<Scalars['Int']>;
  /** @deprecated API does not return this data anymore */
  readonly inceptionDate?: Maybe<Scalars['String']>;
  readonly investmentMinimum?: Maybe<Scalars['Float']>;
  readonly name?: Maybe<Scalars['String']>;
  /** @deprecated API does not return this data anymore */
  readonly performanceSinceInception?: Maybe<Scalars['Float']>;
  readonly portfolioManagerId?: Maybe<Scalars['ID']>;
  readonly seriesId: Scalars['ID'];
  readonly taxManaged?: Maybe<Scalars['Boolean']>;
  readonly updateDate?: Maybe<Scalars['String']>;
};

export type SignDocument = {
  readonly __typename?: 'SignDocument';
  readonly advisorId?: Maybe<Scalars['ID']>;
  readonly documentGenerationStatus?: Maybe<ADVISOR_STATUS>;
  readonly forDocument?: Maybe<FOR_DOCUMENT>;
  readonly id: Scalars['ID'];
  readonly signatureMetadata?: Maybe<SignatureMetadata>;
};

export type SignMarketplaceAgreementInput = {
  readonly signatureMetadata: SignatureMetadataInput;
};

export type SignUrlResponse = {
  readonly __typename?: 'SignUrlResponse';
  readonly signUrl?: Maybe<Scalars['String']>;
  readonly signatureRequestId?: Maybe<Scalars['String']>;
};

export type SignatureMetadata = {
  readonly __typename?: 'SignatureMetadata';
  readonly signedAt?: Maybe<Scalars['String']>;
  readonly signedName?: Maybe<Scalars['String']>;
};

export type SignatureMetadataInput = {
  readonly signedName?: InputMaybe<Scalars['String']>;
};

export type SingleParticipant = {
  readonly __typename?: 'SingleParticipant';
  readonly firstName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly lastName?: Maybe<Scalars['String']>;
};

export type SingleParticipantInput = {
  readonly firstName?: InputMaybe<Scalars['String']>;
  readonly id?: InputMaybe<Scalars['ID']>;
  readonly lastName?: InputMaybe<Scalars['String']>;
};

export type SingleParticipantWhere = {
  readonly id?: InputMaybe<Scalars['ID']>;
};

export type Solo401kDetailsInput = {
  readonly employerUserId?: InputMaybe<Scalars['String']>;
  readonly participantType?: InputMaybe<Scalars['String']>;
  readonly planAdministrator?: InputMaybe<Scalars['String']>;
  readonly planEffectiveDate?: InputMaybe<Scalars['String']>;
};

export type Solo401kDetailsType = {
  readonly __typename?: 'Solo401kDetailsType';
  readonly employerUserId?: Maybe<Scalars['String']>;
  readonly participantType?: Maybe<Scalars['String']>;
  readonly planAdministrator?: Maybe<Scalars['String']>;
  readonly planEffectiveDate?: Maybe<Scalars['String']>;
};

export type SortInput = {
  readonly field: Scalars['String'];
  readonly order: SortOrder;
};

export enum SortOrder {
  ASC = 'ASC',
  DESC = 'DESC'
}

export enum SourceOfFunds {
  ALIMONY_CHILD_SUPPORT = 'ALIMONY_CHILD_SUPPORT',
  BUSINESS_CAPITAL = 'BUSINESS_CAPITAL',
  BUSINESS_INCOME_PROFIT = 'BUSINESS_INCOME_PROFIT',
  DONATION_GIFT_INHERITANCE = 'DONATION_GIFT_INHERITANCE',
  EMPLOYMENT_INCOME = 'EMPLOYMENT_INCOME',
  GIFT_INHERITANCE = 'GIFT_INHERITANCE',
  GRANT = 'GRANT',
  INSURANCE_LEGAL_SETTLEMENT = 'INSURANCE_LEGAL_SETTLEMENT',
  SAVINGS_INVESTMENTS = 'SAVINGS_INVESTMENTS',
  UNEMPLOYMENT_DISABILITY = 'UNEMPLOYMENT_DISABILITY'
}

export enum State {
  CLOSED = 'CLOSED',
  CLOSE_PENDING = 'CLOSE_PENDING',
  COMPLETED = 'COMPLETED',
  DRAFT = 'DRAFT',
  FUNDED = 'FUNDED',
  INVITED = 'INVITED',
  IN_PROGRESS = 'IN_PROGRESS',
  LIQUIDATION_FAILED = 'LIQUIDATION_FAILED',
  NEEDS_ACTION = 'NEEDS_ACTION',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED',
  RESTRICTED = 'RESTRICTED',
  UNKNOWN = 'UNKNOWN'
}

export type StateOutput = {
  readonly __typename?: 'StateOutput';
  readonly account?: Maybe<AccountType>;
  readonly accountGroup?: Maybe<Repcode>;
  readonly accountInfoId?: Maybe<Scalars['ID']>;
  readonly clientName?: Maybe<Scalars['String']>;
  readonly created: Scalars['String'];
  readonly financialAccountId?: Maybe<Scalars['ID']>;
  readonly groupId?: Maybe<Scalars['ID']>;
  readonly id: Scalars['ID'];
  readonly jointUserIds?: Maybe<ReadonlyArray<Scalars['ID']>>;
  readonly multiAccountDraftId?: Maybe<Scalars['ID']>;
  readonly openingInPerson?: Maybe<Scalars['Boolean']>;
  readonly state: State;
  readonly trustUserIds?: Maybe<ReadonlyArray<Scalars['ID']>>;
  readonly updated?: Maybe<Scalars['String']>;
  readonly userId: Scalars['ID'];
};

export type StatusOwnerDetails = {
  readonly __typename?: 'StatusOwnerDetails';
  readonly firstName: Scalars['String'];
  readonly lastName: Scalars['String'];
};

export type StripeCustomer = {
  readonly __typename?: 'StripeCustomer';
  readonly stripeCustomerId: Scalars['ID'];
};

export type StripeCustomerInput = {
  readonly accountEmail: Scalars['String'];
  readonly description?: InputMaybe<Scalars['String']>;
  readonly fullName: Scalars['String'];
  readonly orgId: Scalars['String'];
  readonly orgName: Scalars['String'];
  readonly userId: Scalars['String'];
};

export type StripeCustomerOutput = {
  readonly __typename?: 'StripeCustomerOutput';
  readonly stripeCustomerId?: Maybe<Scalars['String']>;
};

export type StripeSubscriptionInput = {
  readonly isNewCustomer: Scalars['Boolean'];
  readonly userId: Scalars['ID'];
};

export type StripeSubscriptionOutput = {
  readonly __typename?: 'StripeSubscriptionOutput';
  readonly subscriptionId: Scalars['String'];
};

export type SubmitAdvisorIdentificationInput = {
  readonly addr1: Scalars['String'];
  readonly city: Scalars['String'];
  readonly dateOfBirth: Scalars['DateOnly'];
  readonly firstName: Scalars['String'];
  readonly lastName: Scalars['String'];
  readonly ssnTaxId: Scalars['String'];
  readonly state: Scalars['String'];
  readonly zipCode: Scalars['String'];
};

export type SubmitHubSpotFormInput = {
  readonly fields: ReadonlyArray<FormFields>;
  readonly formContext: ContextFields;
  readonly formId: Scalars['String'];
  readonly portalId: Scalars['String'];
};

export type SubmitPaymentMethodInput = {
  readonly accountEmail: Scalars['String'];
  readonly fullName: Scalars['String'];
  readonly paymentMethodId: Scalars['String'];
  readonly userId: Scalars['ID'];
};

export type Subscription = {
  readonly __typename?: 'Subscription';
  readonly _?: Maybe<Scalars['Boolean']>;
};

export type SubscriptionInfo = {
  readonly __typename?: 'SubscriptionInfo';
  readonly currentPeriodEnd: Scalars['String'];
  readonly daysLeftInGracePeriod?: Maybe<Scalars['Int']>;
  readonly daysRemainingInTrial?: Maybe<Scalars['Int']>;
  readonly estimatedMonthlyCost: EstimatedMonthlyCost;
  readonly latestInvoiceAttemptCount: Scalars['Int'];
  readonly latestInvoiceCreated: Scalars['Float'];
  readonly latestInvoiceId: Scalars['String'];
  readonly latestInvoiceNextPaymentAttempt: Scalars['Float'];
  readonly latestInvoiceStatus: invoiceStatus;
  readonly planNickname: Scalars['String'];
  readonly subscriptionId: Scalars['String'];
  readonly subscriptionStatus: Scalars['String'];
  readonly trialEndDate?: Maybe<Scalars['String']>;
};

export enum SubstituteGradeType {
  Equivalent = 'Equivalent',
  Temporary = 'Temporary'
}

export type SuccessOutput = {
  readonly __typename?: 'SuccessOutput';
  readonly success?: Maybe<Scalars['Boolean']>;
};

export type SuccessResponse = {
  readonly __typename?: 'SuccessResponse';
  readonly success: Scalars['Boolean'];
};

export type SuitabilityProfile = {
  readonly liquidityNeeds: LiquidityNeeds;
  readonly timeHorizon: TimeHorizon;
};

export type Summary = {
  readonly __typename?: 'Summary';
  readonly annualizedTimeWeightedReturn?: Maybe<Scalars['Float']>;
  readonly apexAccountId?: Maybe<Scalars['String']>;
  readonly availableForTrade?: Maybe<Scalars['Float']>;
  readonly availableForWithdrawal?: Maybe<Scalars['Float']>;
  readonly balance: Scalars['Float'];
  readonly cumulativeTimeWeightedReturn?: Maybe<Scalars['Float']>;
  readonly deposit: Scalars['Float'];
  readonly depositWithdrawal: Scalars['Float'];
  readonly dividend: Scalars['Float'];
  readonly earnings: Scalars['Float'];
  readonly externalAccountsBalance?: Maybe<Scalars['Float']>;
  readonly fee: Scalars['Float'];
  readonly financialAccountId: Scalars['ID'];
  readonly gainLoss: Scalars['Float'];
  readonly interest: Scalars['Float'];
  readonly managedAccountsBalance?: Maybe<Scalars['Float']>;
  readonly moneyWeightedReturn: Scalars['Float'];
  readonly netDeposits: Scalars['Float'];
  readonly otherTaxWithholding?: Maybe<Scalars['Float']>;
  readonly pendingBuyOrdersNotional?: Maybe<Scalars['Float']>;
  readonly pendingCreditAmount?: Maybe<Scalars['Float']>;
  readonly pendingDebitAmount?: Maybe<Scalars['Float']>;
  readonly pendingSellOrdersNotional?: Maybe<Scalars['Float']>;
  readonly pendingWithdrawals?: Maybe<Scalars['Float']>;
  readonly startBalance: Scalars['Float'];
  readonly startDate?: Maybe<Scalars['String']>;
  /** @deprecated Use cumulativeTimeWeightedReturn */
  readonly timeWeightedReturn?: Maybe<Scalars['Float']>;
  readonly transferIn: Scalars['Float'];
  readonly transferInOut: Scalars['Float'];
  readonly transferOut: Scalars['Float'];
  readonly unsettledCash?: Maybe<Scalars['Float']>;
  readonly withdrawal: Scalars['Float'];
  readonly withhold: Scalars['Float'];
};

export type SupportUser = {
  readonly __typename?: 'SupportUser';
  readonly archetype: SupportUserRole;
  readonly createdBy: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly oauthScopes: Scalars['String'];
  readonly orgId: Scalars['ID'];
  readonly status: TeamMemberStatus;
  readonly userId: Scalars['ID'];
  readonly userName: Scalars['String'];
};

export type SupportUserInput = {
  readonly password: Scalars['String'];
  readonly userEmail: Scalars['String'];
};

export enum SupportUserRole {
  ALTRUIST_SUPPORT = 'ALTRUIST_SUPPORT',
  NOOP = 'NOOP'
}

export type SymbolAllocation = {
  readonly __typename?: 'SymbolAllocation';
  readonly symbol: Scalars['String'];
  readonly weight: Scalars['Float'];
};

export enum TaxBudgetDistribution {
  ANNUALLY = 'ANNUALLY',
  MONTHLY = 'MONTHLY',
  QUARTERLY = 'QUARTERLY',
  SEMIANNUALLY = 'SEMIANNUALLY'
}

export enum TaxBudgetUnits {
  PCT = 'PCT',
  USD = 'USD'
}

export type TaxSettings = {
  readonly __typename?: 'TaxSettings';
  readonly taxManagementActive?: Maybe<Scalars['Boolean']>;
  readonly tlhActive?: Maybe<Scalars['Boolean']>;
};

export type TaxSettingsInput = {
  readonly financialAccountId: Scalars['ID'];
  readonly taxBudget?: InputMaybe<AccountTaxBudgetInput>;
  readonly taxLossHarvesting: Scalars['Boolean'];
  readonly taxRates?: InputMaybe<AccountTaxRatesInput>;
  readonly taxSensitivity: Scalars['Boolean'];
};

export enum TaxWithholding {
  FIXED = 'FIXED',
  PERCENTAGE = 'PERCENTAGE'
}

export type TaxWithholdingInput = {
  readonly amount: Scalars['Float'];
  readonly requestedAmount?: InputMaybe<Scalars['Float']>;
  readonly type: TaxWithholding;
};

export type TeamMemberHS = {
  readonly avatarURL?: InputMaybe<Scalars['String']>;
  readonly contactId?: InputMaybe<Scalars['String']>;
  readonly created?: InputMaybe<Scalars['String']>;
  readonly email: Scalars['String'];
  readonly firstName: Scalars['String'];
  readonly id?: InputMaybe<Scalars['String']>;
  readonly lastName: Scalars['String'];
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly profileId?: InputMaybe<Scalars['String']>;
  readonly role?: InputMaybe<Scalars['String']>;
  readonly teamMember?: InputMaybe<Scalars['Boolean']>;
};

export type TeamMemberPermission = {
  readonly canBeAssignedRepCodes: Scalars['Boolean'];
  readonly canBeDeleted: Scalars['Boolean'];
  readonly canBeUpdated: Scalars['Boolean'];
};

export enum TeamMemberStatus {
  ACTIVE = 'ACTIVE',
  EXPIRED = 'EXPIRED',
  INACTIVE = 'INACTIVE',
  INVITED = 'INVITED',
  NOT_INVITED = 'NOT_INVITED',
  PENDING = 'PENDING',
  UNINVITED = 'UNINVITED',
  UNKNOWN = 'UNKNOWN'
}

export type TeamMembersInput = {
  readonly organizationId: Scalars['ID'];
};

export enum TermType {
  LONG_TERM = 'LONG_TERM',
  MIX = 'MIX',
  SHORT_TERM = 'SHORT_TERM'
}

export type TermsAcceptance = {
  readonly __typename?: 'TermsAcceptance';
  readonly isAccepted?: Maybe<Scalars['Boolean']>;
};

export type ThirdPartyAccessIntegration = {
  readonly __typename?: 'ThirdPartyAccessIntegration';
  readonly clientId?: Maybe<Scalars['String']>;
  readonly clientName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly partnerName?: Maybe<Scalars['String']>;
  readonly status?: Maybe<ThirdPartyAccessStatus>;
};

export enum ThirdPartyAccessStatus {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE'
}

export type Tier2 = {
  readonly __typename?: 'Tier2';
  readonly id?: Maybe<Scalars['ID']>;
  readonly rate?: Maybe<Scalars['Float']>;
  readonly upperBound?: Maybe<Scalars['Float']>;
};

export type TierInput = {
  readonly rate?: InputMaybe<Scalars['Float']>;
  readonly upperBound?: InputMaybe<Scalars['Float']>;
};

export type TierOutput = {
  readonly __typename?: 'TierOutput';
  readonly id?: Maybe<Scalars['ID']>;
  readonly rate?: Maybe<Scalars['Float']>;
  readonly scheduleID?: Maybe<Scalars['ID']>;
  readonly upperBound?: Maybe<Scalars['Float']>;
};

export type TiersAum = {
  readonly __typename?: 'TiersAum';
  readonly tierAum?: Maybe<Scalars['Float']>;
  readonly tierFee?: Maybe<Scalars['Float']>;
  readonly tierRate?: Maybe<Scalars['Float']>;
};

export enum TimeHorizon {
  AVERAGE = 'AVERAGE',
  LONGEST = 'LONGEST',
  SHORT = 'SHORT'
}

export type TimeWeightedReturn = {
  readonly __typename?: 'TimeWeightedReturn';
  readonly date: Scalars['DateOnly'];
  readonly return: Scalars['Float'];
};

export type TimeWeightedReturnAnalytics = {
  readonly __typename?: 'TimeWeightedReturnAnalytics';
  readonly annualizedReturn?: Maybe<Scalars['Float']>;
  readonly cumulativeReturn?: Maybe<Scalars['Float']>;
};

export type TimeWeightedReturnsDateRange = {
  readonly __typename?: 'TimeWeightedReturnsDateRange';
  readonly endDate?: Maybe<Scalars['DateOnly']>;
  readonly startDate?: Maybe<Scalars['DateOnly']>;
};

export type TimeWeightedReturnsInput = {
  readonly dateRangeConfig: PerformanceAnalyticsDateRangeConfigInput;
  readonly includeReturns: Scalars['Boolean'];
  readonly resourceType: PerformanceAnalyticsResourceType;
  readonly resourceTypeId: Scalars['ID'];
};

export type TimeWeightedReturnsResponse = {
  readonly __typename?: 'TimeWeightedReturnsResponse';
  readonly analytics?: Maybe<TimeWeightedReturnAnalytics>;
  readonly dateRange?: Maybe<TimeWeightedReturnsDateRange>;
  readonly responseCode?: Maybe<TimeWeightedReturnsResponseCode>;
  readonly returns?: Maybe<ReadonlyArray<Maybe<TimeWeightedReturn>>>;
};

export type TimeWeightedReturnsResponseCode = {
  readonly __typename?: 'TimeWeightedReturnsResponseCode';
  readonly code?: Maybe<Scalars['String']>;
  readonly description?: Maybe<Scalars['String']>;
};

export type ToggleMfaEnabledInput = {
  readonly includeBackupCodes?: InputMaybe<Scalars['Boolean']>;
  readonly mfaSettingsId: Scalars['ID'];
  readonly mfaWorkflowState?: InputMaybe<Scalars['Boolean']>;
};

export type TopClients = {
  readonly __typename?: 'TopClients';
  readonly aum: Scalars['Float'];
  readonly cashPercentage: Scalars['Float'];
  readonly groupId: Scalars['ID'];
  readonly name: Scalars['String'];
  readonly numberOfAccounts?: Maybe<Scalars['Float']>;
};

export type TopHoldings = {
  readonly __typename?: 'TopHoldings';
  readonly holdingValue: Scalars['Float'];
  readonly price: Scalars['Float'];
  readonly securityName: Scalars['String'];
  readonly ticker: Scalars['String'];
};

export type Totals = {
  readonly __typename?: 'Totals';
  readonly totalHoldingValue: Scalars['Float'];
  readonly totalPrice: Scalars['Float'];
  readonly totalQuantity: Scalars['Float'];
  readonly totalWeight: Scalars['Float'];
};

export type TradingStyleInput = {
  readonly automatic?: InputMaybe<Scalars['Boolean']>;
  readonly portfolioId?: InputMaybe<Scalars['ID']>;
};

export type TradingStyleType = {
  readonly __typename?: 'TradingStyleType';
  readonly automatic?: Maybe<Scalars['Boolean']>;
  readonly portfolioId?: Maybe<Scalars['ID']>;
};

export type Transaction = {
  readonly __typename?: 'Transaction';
  readonly amount: Scalars['Float'];
  readonly contributionReason?: Maybe<ContributionReason>;
  readonly contributionYear?: Maybe<IraContributionType>;
  readonly createdBy?: Maybe<Scalars['ID']>;
  readonly date: Scalars['String'];
  readonly distributionReason?: Maybe<IraDistributionReason>;
  readonly federalTaxWithholdingFormatted?: Maybe<Scalars['String']>;
  readonly federalWithholdingAmount?: Maybe<Scalars['Float']>;
  readonly federalWithholdingType?: Maybe<TaxWithholding>;
  readonly financialInstitutionName?: Maybe<Scalars['String']>;
  readonly fundingAccountNumber?: Maybe<Scalars['String']>;
  readonly fundingAccountType?: Maybe<FundingAccountType>;
  readonly id: Scalars['ID'];
  readonly initiatedBy: Scalars['String'];
  /** @deprecated use isMMATransfer instead */
  readonly isMMA: Scalars['Boolean'];
  readonly netAmount?: Maybe<Scalars['Float']>;
  readonly partnerTransactionId?: Maybe<Scalars['String']>;
  readonly price?: Maybe<Scalars['Float']>;
  readonly quantity: Scalars['Float'];
  readonly requestedAmount?: Maybe<Scalars['Float']>;
  readonly stateTaxWithholdingFormatted?: Maybe<Scalars['String']>;
  readonly stateWithholdingAmount?: Maybe<Scalars['Float']>;
  readonly stateWithholdingType?: Maybe<TaxWithholding>;
  readonly tickerSymbol?: Maybe<Scalars['String']>;
  readonly transactionTableSymbol: Scalars['String'];
  readonly trxStatus?: Maybe<Scalars['String']>;
  readonly txnDescription?: Maybe<Scalars['String']>;
  readonly type: Scalars['String'];
  readonly uiStatus?: Maybe<UIACHStatus>;
  readonly updatedDate?: Maybe<Scalars['String']>;
};

export enum TransactionDirection {
  DEPOSIT = 'DEPOSIT',
  WITHDRAWAL = 'WITHDRAWAL'
}

export type Transactions = {
  readonly __typename?: 'Transactions';
  readonly accounts: ReadonlyArray<Account>;
};

export type TransferAccountsInput = {
  readonly financialAccountIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
  readonly fromAccountGroupId: Scalars['ID'];
  readonly toAccountGroupId: Scalars['ID'];
};

export enum TransferDirection {
  DEPOSIT = 'DEPOSIT',
  WITHDRAWAL = 'WITHDRAWAL'
}

export type TransferableAccount = {
  readonly __typename?: 'TransferableAccount';
  readonly accountType: Scalars['String'];
  readonly brokerageAccountId: Scalars['ID'];
  readonly financialAccountName: Scalars['String'];
  readonly hasMMAWithBrokerageAccount: Scalars['Boolean'];
  readonly hasPendingMMA: Scalars['Boolean'];
  readonly isValid: Scalars['Boolean'];
};

export type TrustInput = {
  readonly address?: InputMaybe<Scalars['String']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly dateEstablished?: InputMaybe<Scalars['String']>;
  readonly name?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly stateEstablished?: InputMaybe<Scalars['String']>;
  readonly supportedChecked?: InputMaybe<Scalars['Boolean']>;
  readonly taxIdType?: InputMaybe<TrustTaxIdType>;
  readonly taxIdentificationNumber?: InputMaybe<Scalars['String']>;
  readonly trustAdditionalDisclosuresChecked?: InputMaybe<Scalars['Boolean']>;
  readonly trustRevocability?: InputMaybe<TrustRevocable>;
  readonly zipCode?: InputMaybe<Scalars['String']>;
};

export type TrustOutput = {
  readonly __typename?: 'TrustOutput';
  readonly address?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly dateEstablished?: Maybe<Scalars['String']>;
  readonly financialAccountId?: Maybe<Scalars['ID']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly name?: Maybe<Scalars['String']>;
  /** @deprecated Legacy field */
  readonly permission?: Maybe<TrustPermission>;
  readonly state?: Maybe<Scalars['String']>;
  readonly stateEstablished?: Maybe<Scalars['String']>;
  readonly supportedChecked?: Maybe<Scalars['Boolean']>;
  readonly taxIdType?: Maybe<TrustTaxIdType>;
  readonly taxIdentificationNumber?: Maybe<Scalars['String']>;
  readonly trustAdditionalDisclosuresChecked?: Maybe<Scalars['Boolean']>;
  /** @deprecated Legacy field */
  readonly trustDocumentId?: Maybe<Scalars['ID']>;
  readonly trustRevocability?: Maybe<TrustRevocable>;
  readonly zipCode?: Maybe<Scalars['String']>;
};

export enum TrustPermission {
  MODIFY_TRUST = 'MODIFY_TRUST',
  MODIFY_TRUSTEES = 'MODIFY_TRUSTEES',
  TRUSTEE_AND_GRANTOR = 'TRUSTEE_AND_GRANTOR',
  TRUSTEE_ONLY = 'TRUSTEE_ONLY'
}

export enum TrustRevocable {
  IRREVOCABLE = 'IRREVOCABLE',
  REVOCABLE = 'REVOCABLE'
}

export enum TrustTaxIdType {
  EIN = 'EIN',
  SSN = 'SSN'
}

export type TrustedContactOutput = {
  readonly __typename?: 'TrustedContactOutput';
  readonly email?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly lastName?: Maybe<Scalars['String']>;
  readonly phone?: Maybe<Scalars['String']>;
};

export type TrustedDevice = {
  readonly __typename?: 'TrustedDevice';
  readonly browser: Scalars['String'];
  readonly created: Scalars['DateTime'];
  readonly dateLastUsed?: Maybe<Scalars['DateTime']>;
  readonly deviceType: DeviceType;
  readonly fingerprintHash: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly ip: Scalars['String'];
  readonly location: Scalars['String'];
  readonly name?: Maybe<Scalars['String']>;
  readonly trustedUntilDate?: Maybe<Scalars['DateTime']>;
  readonly updated: Scalars['DateTime'];
  readonly userAgent: Scalars['String'];
  readonly userId: Scalars['String'];
};

export type TrustedDeviceInput = {
  readonly deviceType: DeviceType;
  readonly fingerprintHash: Scalars['String'];
};

export enum UIACATStatus {
  ACCEPTED = 'ACCEPTED',
  ASSETS_UNDER_REVIEW = 'ASSETS_UNDER_REVIEW',
  CANCELLED = 'CANCELLED',
  COMPLETE = 'COMPLETE',
  EXPIRED = 'EXPIRED',
  PREPPING_SETTLEMENT = 'PREPPING_SETTLEMENT',
  PROCESSING = 'PROCESSING',
  REJECTED = 'REJECTED',
  SENT = 'SENT',
  UNDEFINED = 'UNDEFINED',
  UNDER_REVIEW = 'UNDER_REVIEW'
}

export enum UIACHStatus {
  CANCELLED = 'CANCELLED',
  COMPLETE = 'COMPLETE',
  FAILED = 'FAILED',
  IN_PROGRESS = 'IN_PROGRESS',
  LIQUIDATING = 'LIQUIDATING',
  REJECTED = 'REJECTED',
  RETURNED = 'RETURNED',
  SUBMITTED = 'SUBMITTED',
  UNKNOWN = 'UNKNOWN'
}

export type USStates = {
  readonly __typename?: 'USStates';
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
};

export type UnassignedAccount = {
  readonly __typename?: 'UnassignedAccount';
  readonly accountGroup?: Maybe<Repcode>;
  readonly accountNickname?: Maybe<Scalars['String']>;
  readonly accountNumber: Scalars['String'];
  readonly accountType: Scalars['String'];
  readonly addresses: ReadonlyArray<Maybe<Address>>;
  readonly balance?: Maybe<Scalars['Float']>;
  readonly externalRef?: Maybe<Scalars['String']>;
  readonly financialAccountId: Scalars['ID'];
  readonly financialAccountName: Scalars['String'];
  readonly firstName: Scalars['String'];
  readonly inceptionDate?: Maybe<Scalars['String']>;
  readonly lastFourDigitSSN: Scalars['String'];
  readonly lastName: Scalars['String'];
  readonly qualifiedRepCode?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
  readonly updated?: Maybe<Scalars['String']>;
  readonly userId: Scalars['ID'];
};

export type UnlinkHeldAwayInput = {
  readonly heldAwayLinkId: Scalars['String'];
};

export type UpcomingRebalances = {
  readonly __typename?: 'UpcomingRebalances';
  readonly nextMarketOpen?: Maybe<Scalars['DateOnly']>;
  readonly nextRebalance?: Maybe<Scalars['DateTime']>;
};

export type UpdateAccountBeneficiariesDesignationInput = {
  readonly accountId: Scalars['ID'];
  readonly beneficiaries: ReadonlyArray<BeneficiaryForDesignationTypeUpdate>;
  readonly designation?: InputMaybe<Scalars['String']>;
  readonly designationType: Scalars['String'];
};

export type UpdateAccountBeneficiaryInput = {
  readonly accountId: Scalars['ID'];
  readonly applyForAll?: InputMaybe<Scalars['Boolean']>;
  readonly beneficiaryInput: BeneficiaryInput;
  readonly id: Scalars['ID'];
};

export type UpdateAccountPropertiesInput = {
  readonly financialAccountId: Scalars['ID'];
  readonly properties: FinancialAccountPropertiesInput;
};

export type UpdateAdvisorADVFormsResponse = {
  readonly __typename?: 'UpdateAdvisorADVFormsResponse';
  readonly success?: Maybe<Scalars['Boolean']>;
};

export type UpdateAdvisorResponse = {
  readonly __typename?: 'UpdateAdvisorResponse';
  readonly user?: Maybe<Advisor2>;
};

export type UpdateAllNotificationStatusesInput = {
  readonly beforeNotificationId: Scalars['ID'];
  readonly eventCanonicalName?: InputMaybe<NotificationEventCanonicalName>;
  readonly status: NotificationStatus;
};

export type UpdateAllNotificationStatusesMutationResponse = {
  readonly __typename?: 'UpdateAllNotificationStatusesMutationResponse';
  readonly beforeNotificationId: Scalars['ID'];
  readonly eventCanonicalName?: Maybe<NotificationEventCanonicalName>;
  readonly status: NotificationStatus;
};

export type UpdateAndCreateMfaSettingsInput = {
  readonly includeBackupCodes?: InputMaybe<Scalars['Boolean']>;
  readonly mfaSettings: MfaSettingsInput;
  readonly mfaWorkflowState?: InputMaybe<Scalars['Boolean']>;
};

export type UpdateBenchmarkInput = {
  readonly components?: InputMaybe<ReadonlyArray<BenchmarkComponentInput>>;
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
};

export type UpdateBenchmarkOutput = {
  readonly __typename?: 'UpdateBenchmarkOutput';
  readonly success: Scalars['Boolean'];
};

export type UpdateBillingGroupInput = {
  readonly billingGroupFinancialAccountIds: ReadonlyArray<Scalars['ID']>;
  readonly billingGroupId: Scalars['ID'];
  readonly billingGroupName: Scalars['String'];
  readonly householdId: Scalars['ID'];
};

export type UpdateBillingGroupResponse = {
  readonly __typename?: 'UpdateBillingGroupResponse';
  readonly billingGroup: BillingGroup;
};

export type UpdateClientInput = {
  readonly addr1?: InputMaybe<Scalars['String']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly country?: InputMaybe<Scalars['String']>;
  readonly dateOfBirth?: InputMaybe<Scalars['DateOnly']>;
  readonly email?: InputMaybe<Scalars['String']>;
  readonly firstName?: InputMaybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly lastName?: InputMaybe<Scalars['String']>;
  readonly middleName?: InputMaybe<Scalars['String']>;
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly ssnTaxId?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly suffix?: InputMaybe<Scalars['String']>;
  readonly zipCode?: InputMaybe<Scalars['String']>;
};

export type UpdateContactEmailOutput = {
  readonly __typename?: 'UpdateContactEmailOutput';
  readonly email: Scalars['String'];
  readonly id: Scalars['ID'];
};

export type UpdateContactInput = {
  readonly address?: InputMaybe<Scalars['String']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly email: Scalars['String'];
  readonly firstName: Scalars['String'];
  readonly lastName: Scalars['String'];
  readonly middleName?: InputMaybe<Scalars['String']>;
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly suffix?: InputMaybe<Scalars['String']>;
  readonly userId: Scalars['ID'];
  readonly zip?: InputMaybe<Scalars['String']>;
};

export type UpdateContactOutput = {
  readonly __typename?: 'UpdateContactOutput';
  readonly success?: Maybe<Scalars['Boolean']>;
};

export type UpdateCorpMember = {
  readonly corporationId: Scalars['ID'];
  readonly ownershipPercentage?: InputMaybe<Scalars['Int']>;
  readonly profile?: InputMaybe<CorpMemberProfileInput>;
  readonly roles?: InputMaybe<ReadonlyArray<CorpMemberRole>>;
  readonly userId: Scalars['ID'];
};

export type UpdateCorporationInput = {
  readonly businessAddress?: InputMaybe<EntityAddressInput>;
  readonly id: Scalars['ID'];
  readonly mailingAddress?: InputMaybe<EntityAddressInput>;
  readonly profile: EntityProfileInput;
};

export type UpdateDocumentTemplateDataInput = {
  readonly defaultMessage: Scalars['String'];
  readonly defaultMessageSubject: Scalars['String'];
  readonly externalTemplateId: Scalars['String'];
  readonly name: Scalars['String'];
  readonly signers: ReadonlyArray<Scalars['String']>;
  readonly signingOrderIsSet: Scalars['Boolean'];
  readonly status: DocumentTemplateInputStatus;
};

export type UpdateDocumentTemplateInput = {
  readonly id: Scalars['String'];
  readonly template: UpdateDocumentTemplateDataInput;
};

export type UpdateExcludeDaysBeforeFundingInput = {
  readonly excludeDaysBeforeFunding: Scalars['Boolean'];
};

export type UpdateExcludeDaysBeforeFundingOutput = {
  readonly __typename?: 'UpdateExcludeDaysBeforeFundingOutput';
  readonly excludeDaysBeforeFunding: Scalars['String'];
};

export type UpdateFeeAllocationInput = {
  readonly accountAllocations: ReadonlyArray<AccountAllocation2Input>;
  readonly allocationType: AllocationType;
  readonly billingGroupId?: InputMaybe<Scalars['ID']>;
  readonly debitAccountOverrides: ReadonlyArray<DebitAccountOverrideInput>;
  readonly heldAwayAccountAllocations: ReadonlyArray<AccountAllocation2Input>;
  readonly householdId?: InputMaybe<Scalars['ID']>;
};

export type UpdateFeeRoutingHouseholdInput = {
  readonly householdId: Scalars['String'];
  readonly templates: ReadonlyArray<UpdateFeeRoutingInput>;
  readonly type?: InputMaybe<Scalars['String']>;
};

export type UpdateFeeRoutingInput = {
  readonly entityId: Scalars['String'];
  readonly entityType: FeeEntityType;
  readonly routingLevel: FeeRoutingLevel;
  readonly scheduleId?: InputMaybe<Scalars['String']>;
  readonly type: Scalars['String'];
};

export type UpdateGroupNameOutput = {
  readonly __typename?: 'UpdateGroupNameOutput';
  readonly category?: Maybe<Scalars['String']>;
  readonly created?: Maybe<Scalars['String']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly name?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
  readonly updated?: Maybe<Scalars['String']>;
  readonly updatedBy?: Maybe<Scalars['String']>;
};

export type UpdateGroupSchedulesByAccountInput = {
  readonly endDate?: InputMaybe<Scalars['String']>;
  readonly financialAccountId?: InputMaybe<Scalars['ID']>;
  readonly groupId?: InputMaybe<Scalars['ID']>;
  readonly scheduleId?: InputMaybe<Scalars['ID']>;
  readonly startDate?: InputMaybe<Scalars['String']>;
};

export type UpdateHubspotContactByIdInput = {
  readonly contactId: Scalars['ID'];
  readonly fields?: InputMaybe<HubspotFields>;
  readonly fieldsNeedingDate?: InputMaybe<HubspotFields>;
};

export type UpdateIntegrationResponse = {
  readonly __typename?: 'UpdateIntegrationResponse';
  readonly authUrl?: Maybe<Scalars['String']>;
  readonly status?: Maybe<IntegrationStatus>;
};


export type UpdateIntegrationResponsestatusArgs = {
  action: IntegrationAction;
};

export type UpdateNotificationPreferencesInput = {
  readonly notificationPreferences: ReadonlyArray<NotificationPreferenceInput>;
};

export type UpdateNotificationStatusesInput = {
  readonly notificationIds: ReadonlyArray<Scalars['ID']>;
  readonly status: NotificationStatus;
};

export type UpdateOrganizationResponse = {
  readonly __typename?: 'UpdateOrganizationResponse';
  readonly organization?: Maybe<Organization>;
};

export type UpdateRepcodeAsTheDefaultInput = {
  readonly repCodeId: Scalars['ID'];
};

export type UpdateRepcodeInput = {
  readonly label?: InputMaybe<Scalars['String']>;
  readonly repCodeId: Scalars['ID'];
};

export type UpdateReportScheduleAssignment = {
  readonly action: ReportAssignmentAction;
  readonly advisorId: Scalars['ID'];
  readonly entityId: Scalars['ID'];
  readonly entityType: ReportAssignmentEntityType;
  readonly scheduleAssignmentId: Scalars['ID'];
  readonly scheduleId: Scalars['ID'];
  readonly sendReportCopyToAdvisor: Scalars['Boolean'];
};

export type UpdateReportScheduleAssignmentsInput = {
  readonly assignments?: InputMaybe<ReadonlyArray<UpdateReportScheduleAssignment>>;
  readonly reportType: ReportScheduleType;
  readonly scheduleId?: InputMaybe<Scalars['ID']>;
};

export type UpdateReportScheduleAssignmentsOutput = {
  readonly __typename?: 'UpdateReportScheduleAssignmentsOutput';
  readonly assignments?: Maybe<ReadonlyArray<ReportAssignmentUpdateOutput>>;
};

export type UpdateScheduleAssignmentInput = {
  readonly billingGroupId?: InputMaybe<Scalars['ID']>;
  readonly endDate?: InputMaybe<Scalars['String']>;
  readonly financialAccountId?: InputMaybe<Scalars['ID']>;
  readonly householdId: Scalars['ID'];
  readonly scheduleId: Scalars['ID'];
  readonly startDate: Scalars['String'];
};

export type UpdateScheduleAssignmentsDateOutput = {
  readonly __typename?: 'UpdateScheduleAssignmentsDateOutput';
  readonly endDate?: Maybe<Scalars['String']>;
  readonly scheduleAssignmentId: Scalars['String'];
  readonly startDate: Scalars['String'];
};

export type UpdateSchedulesAssignmentsByAccountsInput = {
  readonly financialAccountId: Scalars['ID'];
  readonly scheduleId: Scalars['ID'];
};

export type UpdateSchedulesAssignmentsByGroupOutput = {
  readonly __typename?: 'UpdateSchedulesAssignmentsByGroupOutput';
  readonly endDate?: Maybe<Scalars['String']>;
  readonly groupId: Scalars['String'];
  readonly scheduleAssignmentId?: Maybe<Scalars['ID']>;
  readonly startDate?: Maybe<Scalars['String']>;
};

export type UpdateSchedulesAssignmentsInput = {
  readonly financialAccountIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
  readonly groupIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
  readonly scheduleId?: InputMaybe<Scalars['ID']>;
};

export type UpdateSchedulesInput = {
  readonly billTiming?: InputMaybe<AllowedBillTiming>;
  readonly frequency?: InputMaybe<AllowedFrequency>;
  readonly id: Scalars['ID'];
  readonly plans?: InputMaybe<ReadonlyArray<InputMaybe<PlanInput>>>;
  readonly scheduleName?: InputMaybe<Scalars['String']>;
};

export type UpdateSecurityConstraintsInput = {
  readonly constraints: ReadonlyArray<SecurityConstraintInput>;
  readonly financialAccountId: Scalars['ID'];
};

export type UpdateStateBulkInput = {
  readonly states: ReadonlyArray<UpdateStateInput>;
};

export type UpdateStateInput = {
  readonly account?: InputMaybe<AccountInput>;
  readonly groupId: Scalars['ID'];
  readonly id: Scalars['ID'];
  readonly jointUserIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
  readonly openingInPerson?: InputMaybe<Scalars['Boolean']>;
  readonly state?: InputMaybe<State>;
  readonly trustUserIds?: InputMaybe<ReadonlyArray<Scalars['ID']>>;
  readonly userId: Scalars['ID'];
  readonly userSwapped?: InputMaybe<Scalars['Boolean']>;
};

export type UpdateStripeCustomerInput = {
  readonly accountEmail?: InputMaybe<Scalars['String']>;
  readonly fullName?: InputMaybe<Scalars['String']>;
  readonly paymentMethod?: InputMaybe<CustomerPaymentMethod>;
  readonly userId: Scalars['ID'];
};

export type UpdateUserInput = {
  readonly address?: InputMaybe<Scalars['String']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly country?: InputMaybe<Scalars['String']>;
  readonly dateOfBirth?: InputMaybe<Scalars['DateOnly']>;
  readonly email?: InputMaybe<Scalars['String']>;
  readonly firstName: Scalars['String'];
  readonly lastName: Scalars['String'];
  readonly middleName?: InputMaybe<Scalars['String']>;
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly ssnTaxId?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly suffix?: InputMaybe<Scalars['String']>;
  readonly userId: Scalars['ID'];
  readonly zip?: InputMaybe<Scalars['String']>;
};

export type UploadDocumentTemplateInput = {
  readonly file: Scalars['Upload'];
};

export type UploadDocumentTemplateResponse = {
  readonly __typename?: 'UploadDocumentTemplateResponse';
  readonly uploadTemplate?: Maybe<DocumentTemplateUpload>;
};

export type UploadFeesInput = {
  readonly file: Scalars['Upload'];
};

export type UploadFeesResponse = {
  readonly __typename?: 'UploadFeesResponse';
  readonly status: UploadFeesResult;
  readonly url?: Maybe<Scalars['String']>;
};

export enum UploadFeesResult {
  ERROR = 'ERROR',
  PARTIAL = 'PARTIAL',
  SUCCESS = 'SUCCESS'
}

export type UploadOrganizationLogoInput = {
  readonly file: Scalars['Upload'];
};

export type User = {
  readonly __typename?: 'User';
  readonly address?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly dateOfBirth?: Maybe<Scalars['DateOnly']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly firstName: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly inviteStatus?: Maybe<InviteStatus>;
  readonly lastName: Scalars['String'];
  readonly middleName?: Maybe<Scalars['String']>;
  readonly phone?: Maybe<Scalars['String']>;
  readonly ssnTaxId?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly suffix?: Maybe<Scalars['String']>;
  readonly userId: Scalars['ID'];
  readonly userStatus?: Maybe<AllowedUserStatus>;
  readonly userType?: Maybe<AllowedUserType>;
  readonly zip?: Maybe<Scalars['String']>;
};

export type User2 = {
  /** @deprecated Unused feature flag. */
  readonly apexEnabled?: Maybe<Scalars['Boolean']>;
  readonly archetype?: Maybe<UserArchetype>;
  /** @deprecated Use avatarURL if provided */
  readonly avatarId?: Maybe<Scalars['ID']>;
  readonly avatarURL: Scalars['String'];
  readonly created?: Maybe<Scalars['String']>;
  readonly email: Scalars['String'];
  readonly firstName: Scalars['String'];
  readonly fullName: Scalars['String'];
  /** @deprecated Not needed anymore. */
  readonly hasOrganization?: Maybe<Scalars['Boolean']>;
  readonly id: Scalars['ID'];
  readonly invite?: Maybe<UserInvite>;
  readonly isTeamMember?: Maybe<Scalars['Boolean']>;
  readonly lastName: Scalars['String'];
  readonly onboardStatus?: Maybe<OnboardStatus>;
  readonly organization: Organization;
  readonly profileId: Scalars['ID'];
  readonly role: UserRole;
  readonly status?: Maybe<UserInviteStatus>;
  readonly suffix?: Maybe<Scalars['String']>;
  readonly username?: Maybe<Scalars['String']>;
};

export enum UserArchetype {
  ADVISOR = 'ADVISOR',
  CLIENT = 'CLIENT'
}

export type UserFieldsInput = {
  readonly last_seen?: InputMaybe<Scalars['String']>;
  readonly login_email?: InputMaybe<Scalars['String']>;
  readonly rep_code?: InputMaybe<Scalars['String']>;
  readonly role?: InputMaybe<Scalars['String']>;
  readonly signed_up?: InputMaybe<Scalars['String']>;
  readonly user_id?: InputMaybe<Scalars['String']>;
};

export type UserFilterInput = {
  readonly id: Scalars['ID'];
};

export type UserInfo = {
  readonly companyName: Scalars['String'];
  readonly email: Scalars['String'];
  readonly firstName: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly lastName: Scalars['String'];
};

export type UserInfoInput = {
  readonly address?: InputMaybe<Scalars['String']>;
  readonly annualIncome?: InputMaybe<Scalars['String']>;
  readonly avatarURL?: InputMaybe<Scalars['String']>;
  readonly backupWithholding?: InputMaybe<Scalars['Boolean']>;
  readonly brokerDealerAffiliation?: InputMaybe<Scalars['Boolean']>;
  readonly citizenShip?: InputMaybe<Scalars['String']>;
  readonly city?: InputMaybe<Scalars['String']>;
  readonly companyEmail?: InputMaybe<Scalars['String']>;
  readonly companyLegalName?: InputMaybe<Scalars['String']>;
  readonly companyName?: InputMaybe<Scalars['String']>;
  readonly companyPhone?: InputMaybe<Scalars['String']>;
  readonly companyTaxId?: InputMaybe<Scalars['String']>;
  readonly companyWebsite?: InputMaybe<Scalars['String']>;
  readonly countryOfBirth?: InputMaybe<Scalars['String']>;
  readonly customerAgreementDisclosure?: InputMaybe<Scalars['Boolean']>;
  readonly dateOfBirth?: InputMaybe<Scalars['String']>;
  readonly directorStockOwner?: InputMaybe<Scalars['Boolean']>;
  readonly email?: InputMaybe<Scalars['String']>;
  readonly employer?: InputMaybe<Scalars['String']>;
  readonly employerAddress?: InputMaybe<EntityAddressInput>;
  readonly employmentIndustry?: InputMaybe<Scalars['String']>;
  readonly employmentPosition?: InputMaybe<Scalars['String']>;
  readonly employmentStatus?: InputMaybe<Scalars['String']>;
  readonly entityName?: InputMaybe<Scalars['String']>;
  readonly entityNature?: InputMaybe<AccountNature>;
  readonly entityTaxId?: InputMaybe<Scalars['String']>;
  readonly externalRef?: InputMaybe<Scalars['String']>;
  readonly findersFeeDisclosure?: InputMaybe<Scalars['Boolean']>;
  readonly firstName?: InputMaybe<Scalars['String']>;
  readonly gender?: InputMaybe<Scalars['String']>;
  readonly investmentExperience?: InputMaybe<Scalars['String']>;
  readonly investmentObjective?: InputMaybe<Scalars['String']>;
  readonly investmentRiskTolerance?: InputMaybe<Scalars['String']>;
  readonly isCorrespondentEmployee?: InputMaybe<Scalars['Boolean']>;
  readonly language?: InputMaybe<Scalars['String']>;
  readonly lastName?: InputMaybe<Scalars['String']>;
  readonly liquidNetworth?: InputMaybe<Scalars['String']>;
  readonly maritalStatus?: InputMaybe<Scalars['String']>;
  readonly marketDataAgreementDisclosure?: InputMaybe<Scalars['Boolean']>;
  readonly middleName?: InputMaybe<Scalars['String']>;
  readonly occupation?: InputMaybe<Scalars['String']>;
  readonly optInLendingProgram?: InputMaybe<Scalars['Boolean']>;
  readonly permanentResident?: InputMaybe<Scalars['Boolean']>;
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly politicallyExposed?: InputMaybe<Scalars['Boolean']>;
  readonly rule14bDisclosure?: InputMaybe<Scalars['Boolean']>;
  readonly signedBy?: InputMaybe<Scalars['String']>;
  readonly spouseDateOfBirth?: InputMaybe<Scalars['String']>;
  readonly ssnTaxId?: InputMaybe<Scalars['String']>;
  readonly state?: InputMaybe<Scalars['String']>;
  readonly suffix?: InputMaybe<Scalars['String']>;
  readonly taxPayer?: InputMaybe<Scalars['Boolean']>;
  readonly termsOfUseDisclosure?: InputMaybe<Scalars['Boolean']>;
  readonly totalNetworth?: InputMaybe<Scalars['String']>;
  readonly userAddress?: InputMaybe<EntityAddressInput>;
  readonly visaExpirationDate?: InputMaybe<Scalars['String']>;
  readonly visaType?: InputMaybe<VisaType>;
  readonly zipCode?: InputMaybe<Scalars['String']>;
  readonly zipcode?: InputMaybe<Scalars['String']>;
};

export type UserInfoOBOOutput = {
  readonly __typename?: 'UserInfoOBOOutput';
  readonly apexEnabled?: Maybe<Scalars['Boolean']>;
  readonly hasOrganization?: Maybe<Scalars['Boolean']>;
  readonly id?: Maybe<Scalars['ID']>;
  readonly profileId?: Maybe<Scalars['ID']>;
  readonly role?: Maybe<UserRole>;
  readonly teamMember?: Maybe<Scalars['Boolean']>;
};

export type UserInfoOutputMutation = {
  readonly __typename?: 'UserInfoOutputMutation';
  readonly annualIncome?: Maybe<Scalars['String']>;
  readonly avatarURL?: Maybe<Scalars['String']>;
  readonly backupWithholding?: Maybe<Scalars['Boolean']>;
  readonly brokerDealerAffiliation?: Maybe<Scalars['Boolean']>;
  readonly citizenShip?: Maybe<Scalars['String']>;
  readonly companyEmail?: Maybe<Scalars['String']>;
  readonly companyLegalName?: Maybe<Scalars['String']>;
  readonly companyName?: Maybe<Scalars['String']>;
  readonly companyPhone?: Maybe<Scalars['String']>;
  readonly companyTaxId?: Maybe<Scalars['String']>;
  readonly companyWebsite?: Maybe<Scalars['String']>;
  readonly countryOfBirth?: Maybe<Scalars['String']>;
  readonly customerAgreementDisclosure?: Maybe<Scalars['Boolean']>;
  readonly dateOfBirth?: Maybe<Scalars['String']>;
  readonly directorStockOwner?: Maybe<Scalars['Boolean']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly employer?: Maybe<Scalars['String']>;
  readonly employerAddress?: Maybe<EntityAddress>;
  readonly employmentIndustry?: Maybe<Scalars['String']>;
  readonly employmentPosition?: Maybe<Scalars['String']>;
  readonly employmentStatus?: Maybe<Scalars['String']>;
  readonly entityName?: Maybe<Scalars['String']>;
  readonly entityNature?: Maybe<AccountNature>;
  readonly entityTaxId?: Maybe<Scalars['String']>;
  readonly findersFeeDisclosure?: Maybe<Scalars['Boolean']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly gender?: Maybe<Scalars['String']>;
  readonly investmentExperience?: Maybe<Scalars['String']>;
  readonly investmentObjective?: Maybe<Scalars['String']>;
  readonly investmentRiskTolerance?: Maybe<Scalars['String']>;
  readonly isCorrespondentEmployee?: Maybe<Scalars['Boolean']>;
  readonly language?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly liquidNetworth?: Maybe<Scalars['String']>;
  readonly maritalStatus?: Maybe<Scalars['String']>;
  readonly marketDataAgreementDisclosure?: Maybe<Scalars['Boolean']>;
  readonly occupation?: Maybe<Scalars['String']>;
  readonly optInLendingProgram?: Maybe<Scalars['Boolean']>;
  readonly permanentResident?: Maybe<Scalars['Boolean']>;
  readonly phone?: Maybe<Scalars['String']>;
  readonly politicallyExposed?: Maybe<Scalars['Boolean']>;
  readonly rule14bDisclosure?: Maybe<Scalars['Boolean']>;
  readonly signedBy?: Maybe<Scalars['String']>;
  readonly spouseDateOfBirth?: Maybe<Scalars['String']>;
  readonly ssnTaxId?: Maybe<Scalars['String']>;
  readonly taxPayer?: Maybe<Scalars['Boolean']>;
  readonly termsOfUseDisclosure?: Maybe<Scalars['Boolean']>;
  readonly totalNetworth?: Maybe<Scalars['String']>;
  readonly userAddress?: Maybe<EntityAddress>;
  readonly visaExpirationDate?: Maybe<Scalars['String']>;
  readonly visaType?: Maybe<VisaType>;
};

export type UserInfoOutputQuery = {
  readonly __typename?: 'UserInfoOutputQuery';
  readonly address?: Maybe<Scalars['String']>;
  readonly annualIncome?: Maybe<Scalars['String']>;
  readonly avatarURL?: Maybe<Scalars['String']>;
  readonly backupWithholding?: Maybe<Scalars['Boolean']>;
  readonly brokerDealerAffiliation?: Maybe<Scalars['Boolean']>;
  readonly citizenShip?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly companyEmail?: Maybe<Scalars['String']>;
  readonly companyLegalName?: Maybe<Scalars['String']>;
  readonly companyName?: Maybe<Scalars['String']>;
  readonly companyPhone?: Maybe<Scalars['String']>;
  readonly companyTaxId?: Maybe<Scalars['String']>;
  readonly companyWebsite?: Maybe<Scalars['String']>;
  readonly countryOfBirth?: Maybe<Scalars['String']>;
  readonly created: Scalars['String'];
  readonly customerAgreementDisclosure?: Maybe<Scalars['Boolean']>;
  readonly dateOfBirth?: Maybe<Scalars['String']>;
  readonly directorStockOwner?: Maybe<Scalars['Boolean']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly employer?: Maybe<Scalars['String']>;
  readonly employerAddress?: Maybe<EntityAddress>;
  readonly employmentIndustry?: Maybe<Scalars['String']>;
  readonly employmentPosition?: Maybe<Scalars['String']>;
  readonly employmentStatus?: Maybe<Scalars['String']>;
  readonly entityName?: Maybe<Scalars['String']>;
  readonly entityNature?: Maybe<AccountNature>;
  readonly entityTaxId?: Maybe<Scalars['String']>;
  readonly externalRef?: Maybe<Scalars['String']>;
  readonly findersFeeDisclosure?: Maybe<Scalars['Boolean']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly gender?: Maybe<Scalars['String']>;
  readonly id: Scalars['ID'];
  readonly investmentExperience?: Maybe<Scalars['String']>;
  readonly investmentObjective?: Maybe<Scalars['String']>;
  readonly investmentRiskTolerance?: Maybe<Scalars['String']>;
  readonly isCorrespondentEmployee?: Maybe<Scalars['Boolean']>;
  readonly language?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly liquidNetworth?: Maybe<Scalars['String']>;
  readonly maritalStatus?: Maybe<Scalars['String']>;
  readonly marketDataAgreementDisclosure?: Maybe<Scalars['Boolean']>;
  readonly middleName?: Maybe<Scalars['String']>;
  readonly occupation?: Maybe<Scalars['String']>;
  readonly optInLendingProgram?: Maybe<Scalars['Boolean']>;
  readonly permanentResident?: Maybe<Scalars['Boolean']>;
  readonly phone?: Maybe<Scalars['String']>;
  readonly politicallyExposed?: Maybe<Scalars['Boolean']>;
  readonly rule14bDisclosure?: Maybe<Scalars['Boolean']>;
  readonly spouseDateOfBirth?: Maybe<Scalars['String']>;
  readonly ssnTaxId?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly suffix?: Maybe<Scalars['String']>;
  readonly taxPayer?: Maybe<Scalars['Boolean']>;
  readonly termsOfUseDisclosure?: Maybe<Scalars['Boolean']>;
  readonly totalNetworth?: Maybe<Scalars['String']>;
  readonly userAddress?: Maybe<EntityAddress>;
  readonly visaExpirationDate?: Maybe<Scalars['String']>;
  readonly visaType?: Maybe<VisaType>;
  readonly zipCode?: Maybe<Scalars['String']>;
  readonly zipcode?: Maybe<Scalars['String']>;
};

export type UserInvite = {
  readonly __typename?: 'UserInvite';
  readonly expired?: Maybe<Scalars['DateTime']>;
  readonly status?: Maybe<UserInviteStatus>;
};

export enum UserInviteStatus {
  ACTIVE = 'ACTIVE',
  EXPIRED = 'EXPIRED',
  INACTIVE = 'INACTIVE',
  INVITED = 'INVITED',
  NOT_INVITED = 'NOT_INVITED',
  PENDING = 'PENDING',
  UNINVITED = 'UNINVITED',
  UNKNOWN = 'UNKNOWN'
}

export type UserListFilter = {
  readonly roles: ReadonlyArray<UserListRole>;
  readonly searchTerm?: InputMaybe<Scalars['String']>;
};

export type UserListInput = {
  readonly filter: UserListFilter;
};

export enum UserListRole {
  ADMIN = 'ADMIN',
  ADVISOR = 'ADVISOR',
  CLIENT = 'CLIENT',
  OWNER = 'OWNER',
  STAFF = 'STAFF'
}

export type UserPaymentMethodStatus = {
  readonly __typename?: 'UserPaymentMethodStatus';
  readonly hasPaymentMethod: Scalars['Boolean'];
  readonly id: Scalars['ID'];
  readonly isBetaUser: Scalars['Boolean'];
};

export type UserProfiles = {
  readonly __typename?: 'UserProfiles';
  readonly additionalInfo?: Maybe<AdditionalInfo>;
  readonly companyName?: Maybe<Scalars['String']>;
  readonly created?: Maybe<Scalars['String']>;
  readonly dateOfBirth?: Maybe<Scalars['String']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly firstName: Scalars['String'];
  readonly id: Scalars['ID'];
  readonly inceptionDate?: Maybe<Scalars['String']>;
  readonly lastName: Scalars['String'];
  readonly ssnTaxId?: Maybe<Scalars['String']>;
  readonly status?: Maybe<Scalars['String']>;
  readonly updated?: Maybe<Scalars['String']>;
};

export type UserResponse = {
  readonly __typename?: 'UserResponse';
  readonly address?: Maybe<Scalars['String']>;
  readonly city?: Maybe<Scalars['String']>;
  readonly email?: Maybe<Scalars['String']>;
  readonly firstName?: Maybe<Scalars['String']>;
  readonly lastName?: Maybe<Scalars['String']>;
  readonly phone?: Maybe<Scalars['String']>;
  readonly state?: Maybe<Scalars['String']>;
  readonly userId: Scalars['ID'];
  readonly zip?: Maybe<Scalars['String']>;
};

export enum UserRole {
  ADMIN = 'ADMIN',
  ADVISOR = 'ADVISOR',
  CLIENT = 'CLIENT',
  OWNER = 'OWNER',
  STAFF = 'STAFF',
  UNKNOWN = 'UNKNOWN'
}

export type UserValidationForCashAccount = {
  readonly __typename?: 'UserValidationForCashAccount';
  readonly hasIndividualCashAccount: Scalars['Boolean'];
  readonly hasJointCashAccount: Scalars['Boolean'];
  readonly id?: Maybe<Scalars['ID']>;
};

export type UsersValidationForCashAccountWhere = {
  readonly userIds: ReadonlyArray<Scalars['ID']>;
};

export type ValidateAccountOutput = {
  readonly __typename?: 'ValidateAccountOutput';
  readonly accountStateId: Scalars['ID'];
  readonly errorType?: Maybe<ERROR_TYPE>;
  readonly failedProperty?: Maybe<Scalars['String']>;
  readonly failedPropertyDisplay?: Maybe<Scalars['String']>;
  readonly ownerId?: Maybe<Scalars['String']>;
  readonly ownerName?: Maybe<Scalars['String']>;
  readonly success: Scalars['Boolean'];
};

export type ValidateCreatePortfolioInput = {
  readonly cashSettings?: InputMaybe<CashSettingsInput>;
  readonly cashTarget?: InputMaybe<Scalars['Float']>;
  readonly drift: DriftSettingsInput;
  readonly minCash?: InputMaybe<Scalars['Float']>;
  readonly models?: InputMaybe<ReadonlyArray<InputMaybe<PortfolioModelInput>>>;
  readonly name: Scalars['String'];
};

export type ValidateMfaTokenRequest = {
  readonly overrideId?: InputMaybe<Scalars['String']>;
  readonly token: Scalars['String'];
};

export type ValidateMfaTokenResponse = {
  readonly __typename?: 'ValidateMfaTokenResponse';
  readonly id: Scalars['ID'];
  readonly mfaMethod?: Maybe<MfaMethod>;
  readonly validated: Scalars['Boolean'];
};

export type ValidateUpdateModelInput = {
  readonly cashTarget?: InputMaybe<Scalars['Float']>;
  readonly description?: InputMaybe<Scalars['String']>;
  readonly holdings: ReadonlyArray<ModelHoldingInput>;
  readonly id: Scalars['ID'];
  readonly name: Scalars['String'];
};

export type ValidateUpdatePortfolioInput = {
  readonly cashSettings?: InputMaybe<CashSettingsInput>;
  readonly cashTarget?: InputMaybe<Scalars['Float']>;
  readonly drift: DriftSettingsInput;
  readonly id: Scalars['ID'];
  readonly minCash?: InputMaybe<Scalars['Float']>;
  readonly models?: InputMaybe<ReadonlyArray<InputMaybe<PortfolioModelInput>>>;
  readonly name: Scalars['String'];
  readonly rebalanceAsap?: InputMaybe<Scalars['Boolean']>;
  readonly skipRebalance?: InputMaybe<Scalars['Boolean']>;
};

export type Violations = {
  readonly __typename?: 'Violations';
  readonly details?: Maybe<Scalars['String']>;
  readonly goodFaithViolations?: Maybe<GoodFaithViolations>;
  readonly patternDayTrades?: Maybe<Scalars['String']>;
};

export enum VisaType {
  E1 = 'E1',
  E2 = 'E2',
  E3 = 'E3',
  F1 = 'F1',
  G4 = 'G4',
  H1B = 'H1B',
  L1 = 'L1',
  O1 = 'O1',
  OTHER = 'OTHER',
  TN1 = 'TN1'
}

export type Wealthbox = {
  readonly __typename?: 'Wealthbox';
  readonly callbackUrl: Scalars['String'];
  readonly contacts: ReadonlyArray<Maybe<CRMContact>>;
  readonly wealthboxClientId: Scalars['String'];
  readonly workspaces?: Maybe<ReadonlyArray<Scalars['String']>>;
};

export type WeightedModel = {
  readonly __typename?: 'WeightedModel';
  readonly driftSettings: DriftSettings;
  readonly latestVersionMarketplaceId?: Maybe<Scalars['ID']>;
  readonly modelId: Scalars['String'];
  readonly weight: Scalars['Float'];
};

export type WeightedModelInput = {
  readonly modelId: Scalars['ID'];
  readonly weight: Scalars['Float'];
};

export type WeightedSecurity = {
  readonly __typename?: 'WeightedSecurity';
  readonly fundSubstitutes?: Maybe<PortfolioModelSubstitutes>;
  readonly isIntervalFund?: Maybe<Scalars['Boolean']>;
  readonly name: Scalars['String'];
  readonly shareClass?: Maybe<Scalars['String']>;
  readonly symbol: Scalars['String'];
  readonly transactionFee?: Maybe<Scalars['Float']>;
  readonly type: Scalars['String'];
  readonly weight: Scalars['Float'];
};

export type WireInput = {
  readonly depositAmount?: InputMaybe<Scalars['String']>;
  readonly iraContribution?: InputMaybe<IraContributionType>;
};

export type WireType = {
  readonly __typename?: 'WireType';
  readonly depositAmount?: Maybe<Scalars['String']>;
  readonly iraContribution?: Maybe<IraContributionType>;
};

export type YTDRealizedGainLoss = {
  readonly __typename?: 'YTDRealizedGainLoss';
  readonly long: Scalars['Float'];
  readonly short: Scalars['Float'];
  readonly total: Scalars['Float'];
};

export type ZendeskContact = {
  readonly email?: InputMaybe<Scalars['String']>;
  readonly iat?: InputMaybe<Scalars['Float']>;
  readonly jti?: InputMaybe<Scalars['String']>;
  readonly name?: InputMaybe<Scalars['String']>;
  readonly phone?: InputMaybe<Scalars['String']>;
  readonly user_fields?: InputMaybe<UserFieldsInput>;
};

export type ZendeskContactFieldsOutput = {
  readonly __typename?: 'ZendeskContactFieldsOutput';
  readonly email: Scalars['String'];
};

export type ZendeskContactOutput = {
  readonly __typename?: 'ZendeskContactOutput';
  readonly status?: Maybe<Scalars['Int']>;
  readonly user?: Maybe<ZendeskContactFieldsOutput>;
};

export type addBillingPeriodsOutput = {
  readonly __typename?: 'addBillingPeriodsOutput';
  readonly scheduleAssignmentId: Scalars['ID'];
  readonly startDate: Scalars['String'];
};

export type createOrUpdateHubSpotContactOutput = {
  readonly __typename?: 'createOrUpdateHubSpotContactOutput';
  readonly email?: Maybe<Scalars['String']>;
};

export type executeFeesReportResponse = {
  readonly __typename?: 'executeFeesReportResponse';
  readonly csv?: Maybe<Scalars['String']>;
  readonly fileName?: Maybe<Scalars['String']>;
};

export type executeManualInvoiceResponse = {
  readonly __typename?: 'executeManualInvoiceResponse';
  readonly TD: InvoiceUrl;
  readonly brokerage: Brokerage;
  readonly fidelity?: Maybe<InvoiceUrl>;
  readonly pershing?: Maybe<InvoiceUrl>;
  readonly schwab?: Maybe<InvoiceUrl>;
};

export enum invoiceStatus {
  draft = 'draft',
  open = 'open',
  paid = 'paid',
  uncollectible = 'uncollectible',
  void = 'void'
}

export type isAltruistUser = {
  readonly __typename?: 'isAltruistUser';
  readonly isAltruistUser?: Maybe<Scalars['Boolean']>;
};

export type reExportExecutedInvoicesResponse = {
  readonly __typename?: 'reExportExecutedInvoicesResponse';
  readonly TD: InvoiceUrl;
  readonly brokerage: Brokerage;
  readonly fidelity?: Maybe<InvoiceUrl>;
  readonly pershing?: Maybe<InvoiceUrl>;
  readonly schwab?: Maybe<InvoiceUrl>;
};

export type renameAccountOutput = {
  readonly __typename?: 'renameAccountOutput';
  readonly accountNickname: Scalars['String'];
  readonly accountNumber: Scalars['String'];
  readonly financialAccountId: Scalars['ID'];
};

export enum subscriptionStatus {
  active = 'active',
  canceled = 'canceled',
  incomplete = 'incomplete',
  incomplete_expired = 'incomplete_expired',
  past_due = 'past_due',
  trialing = 'trialing',
  unpaid = 'unpaid'
}

export type updateGroupSchedulesByAccountOutput = {
  readonly __typename?: 'updateGroupSchedulesByAccountOutput';
  readonly endDate?: Maybe<Scalars['String']>;
  readonly financialAccountId?: Maybe<Scalars['ID']>;
  readonly groupId?: Maybe<Scalars['ID']>;
  readonly scheduleId?: Maybe<Scalars['ID']>;
  readonly startDate?: Maybe<Scalars['String']>;
};

export type acceptAltruistClearingTermsOfUseMutationVariables = Exact<{ [key: string]: never; }>;


export type acceptAltruistClearingTermsOfUseMutation = { readonly __typename?: 'Mutation', readonly acceptAltruistClearingTermsOfUse: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } };

export type AcatDraftAOPFragment = { readonly __typename?: 'AcatDraft', readonly id: string, readonly draft: { readonly __typename?: 'AcatDraftData', readonly acatType: ACATTransferType, readonly accountNumber: string, readonly participantNumber: string, readonly clientApprovalStatus: AcatDraftStatus } };

export type AcatDraftsQueryVariables = Exact<{
  where: DraftsWhere;
}>;


export type AcatDraftsQuery = { readonly __typename?: 'Query', readonly acatDrafts: ReadonlyArray<{ readonly __typename?: 'AcatDraft', readonly id: string, readonly draft: { readonly __typename?: 'AcatDraftData', readonly acatType: ACATTransferType, readonly accountNumber: string, readonly participantNumber: string, readonly clientApprovalStatus: AcatDraftStatus } }> };

export type GetInstitutionsListQueryVariables = Exact<{
  search: Scalars['String'];
}>;


export type GetInstitutionsListQuery = { readonly __typename?: 'Query', readonly getInstitutionsList: ReadonlyArray<{ readonly __typename?: 'InstitutionsListOutput', readonly number: string, readonly name: string }> };

export type ApproveAcatDraftMutationVariables = Exact<{
  input: AcatDraftStatusChangeInput;
}>;


export type ApproveAcatDraftMutation = { readonly __typename?: 'Mutation', readonly approveAcatDraft: { readonly __typename?: 'AcatDraft', readonly id: string, readonly draft: { readonly __typename?: 'AcatDraftData', readonly acatType: ACATTransferType, readonly accountNumber: string, readonly participantNumber: string, readonly clientApprovalStatus: AcatDraftStatus } } };

export type RejectAcatDraftMutationVariables = Exact<{
  input: AcatDraftStatusChangeInput;
}>;


export type RejectAcatDraftMutation = { readonly __typename?: 'Mutation', readonly rejectAcatDraft: { readonly __typename?: 'AcatDraft', readonly id: string, readonly draft: { readonly __typename?: 'AcatDraftData', readonly acatType: ACATTransferType, readonly accountNumber: string, readonly participantNumber: string, readonly clientApprovalStatus: AcatDraftStatus } } };

export type ApproveAcatDraftsByAccountStateIdsMutationVariables = Exact<{
  input: AccountStateIdsInput;
}>;


export type ApproveAcatDraftsByAccountStateIdsMutation = { readonly __typename?: 'Mutation', readonly approveAcatDraftsByAccountStateIds: { readonly __typename?: 'AcatDraftsGroupedByAccountState', readonly accountStates: ReadonlyArray<{ readonly __typename?: 'AccountStatesWithAcatDrafts', readonly accountStateId: string, readonly acatDrafts: ReadonlyArray<{ readonly __typename?: 'AcatDraft', readonly id: string, readonly draft: { readonly __typename?: 'AcatDraftData', readonly acatType: ACATTransferType, readonly accountNumber: string, readonly participantNumber: string, readonly clientApprovalStatus: AcatDraftStatus } } | null> } | null> } };

export type UpdateAccountManagementMutationVariables = Exact<{
  input: AccountManagementInput;
}>;


export type UpdateAccountManagementMutation = { readonly __typename?: 'Mutation', readonly updateAccountManagement?: { readonly __typename?: 'AccountManagementOutput', readonly email?: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } | null, readonly phoneNumber?: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } | null, readonly address?: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } | null } | null };

export type GetAccountManagementQueryVariables = Exact<{
  financialAccountId: Scalars['String'];
}>;


export type GetAccountManagementQuery = { readonly __typename?: 'Query', readonly accountManagement: { readonly __typename?: 'AccountManagement', readonly phoneNumber?: string | null, readonly email?: string | null, readonly addresses?: ReadonlyArray<{ readonly __typename?: 'AccountAddress', readonly type?: string | null, readonly street?: string | null, readonly state?: string | null, readonly city?: string | null, readonly zip?: string | null }> | null } };

export type getAdvisorInfoQueryVariables = Exact<{ [key: string]: never; }>;


export type getAdvisorInfoQuery = { readonly __typename?: 'Query', readonly getAdvisorInfo?: ReadonlyArray<{ readonly __typename?: 'Advisor', readonly organizationId?: string | null, readonly organaizationName?: string | null, readonly profileId?: string | null, readonly avatarId?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly phoneNumber?: string | null, readonly email?: string | null, readonly advisorId?: string | null } | null> | null };

export type formADVUrlsQueryVariables = Exact<{ [key: string]: never; }>;


export type formADVUrlsQuery = { readonly __typename?: 'Query', readonly brokerageApplication?: { readonly __typename?: 'BrokerageApplication', readonly id: string, readonly formAdvPart1?: string | null, readonly formAdvPart2A?: string | null, readonly formAdvPart2B?: string | null, readonly formAdvPart3?: string | null } | null };

export type createAdvisorDocumentMutationVariables = Exact<{
  documentName: Scalars['String'];
  documentType: Scalars['String'];
  file?: InputMaybe<Scalars['Upload']>;
  versionNumber?: InputMaybe<Scalars['String']>;
}>;


export type createAdvisorDocumentMutation = { readonly __typename?: 'Mutation', readonly createAdvisorDocument?: { readonly __typename?: 'PhotoReturn', readonly id?: string | null } | null };

export type getAdvisorDocumentQueryVariables = Exact<{
  documentId: Scalars['ID'];
}>;


export type getAdvisorDocumentQuery = { readonly __typename?: 'Query', readonly getAdvisorDocument: { readonly __typename?: 'AdvisorDocument', readonly fileName?: string | null }, readonly getAdvisorDocumentUrl: { readonly __typename?: 'DocumentUrl', readonly url: string } };

export type loginWithCredentialsMutationVariables = Exact<{
  input: LoginWhereInput;
}>;


export type loginWithCredentialsMutation = { readonly __typename?: 'Mutation', readonly loginWithCredentials: { readonly __typename?: 'LoginResponse', readonly status: LoginStatus, readonly archetype: UserArchetype, readonly trustedDeviceActive?: boolean | null, readonly user?: { readonly __typename?: 'Advisor2', readonly id: string, readonly role: UserRole, readonly email: string, readonly firstName: string, readonly lastName: string, readonly profileId: string, readonly created?: string | null } | { readonly __typename?: 'Client', readonly id: string, readonly role: UserRole, readonly email: string, readonly firstName: string, readonly lastName: string, readonly profileId: string, readonly created?: string | null } | null, readonly mfaSettings?: { readonly __typename?: 'MfaSettings', readonly id?: string | null, readonly active: boolean, readonly defaultMfaMethod?: MfaMethod | null, readonly phoneNumbers?: ReadonlyArray<{ readonly __typename?: 'PhoneNumber', readonly id?: string | null, readonly number?: string | null, readonly phoneNumberType?: PhoneNumberType | null, readonly isDefaultForMfa?: boolean | null, readonly isVerified?: boolean | null }> | null } | null, readonly identityVerificationQuestion?: { readonly __typename?: 'IdentityVerificationQuestion', readonly id: string, readonly refreshable?: boolean | null, readonly detail?: string | null, readonly hint?: string | null, readonly prompt?: string | null, readonly type?: string | null, readonly user: { readonly __typename?: 'IdentityVerificationUser', readonly avatarURL?: string | null, readonly firstName: string, readonly lastName: string } } | null } };

export type loginUserMfaMutationVariables = Exact<{
  input: LoginWhereInput;
}>;


export type loginUserMfaMutation = { readonly __typename?: 'Mutation', readonly loginWithMfa: { readonly __typename?: 'LoginResponse', readonly status: LoginStatus, readonly archetype: UserArchetype, readonly trustedDeviceActive?: boolean | null, readonly isImpersonation: boolean, readonly user?: { readonly __typename?: 'Advisor2', readonly id: string, readonly role: UserRole, readonly firstName: string, readonly lastName: string, readonly email: string, readonly profileId: string } | { readonly __typename?: 'Client', readonly id: string, readonly role: UserRole, readonly firstName: string, readonly lastName: string, readonly email: string, readonly profileId: string } | null, readonly mfaSettings?: { readonly __typename?: 'MfaSettings', readonly id?: string | null, readonly active: boolean, readonly defaultMfaMethod?: MfaMethod | null, readonly phoneNumbers?: ReadonlyArray<{ readonly __typename?: 'PhoneNumber', readonly id?: string | null, readonly number?: string | null, readonly phoneNumberType?: PhoneNumberType | null, readonly isDefaultForMfa?: boolean | null, readonly isVerified?: boolean | null }> | null } | null } };

export type VerifyIdentityWithQuestionMutationVariables = Exact<{
  input: IdentityVerificationResponseInput;
}>;


export type VerifyIdentityWithQuestionMutation = { readonly __typename?: 'Mutation', readonly verifyIdentityWithQuestion: { readonly __typename?: 'IdentityVerificationResponse', readonly accepted?: boolean | null, readonly attemptsRemaining?: number | null, readonly message?: string | null } };

export type IdentityVerificationQuestionWithCredentialsQueryVariables = Exact<{ [key: string]: never; }>;


export type IdentityVerificationQuestionWithCredentialsQuery = { readonly __typename?: 'Query', readonly identityVerificationQuestionWithCredentials: { readonly __typename?: 'IdentityVerificationQuestion', readonly id: string } };

export type changePasswordMutationVariables = Exact<{
  password: Scalars['String'];
}>;


export type changePasswordMutation = { readonly __typename?: 'Mutation', readonly changePassword?: { readonly __typename?: 'ChangePasswordOutput', readonly jwt?: string | null } | null };

export type VerifyNewUserMutationVariables = Exact<{
  userInput: UserInfoInput;
}>;


export type VerifyNewUserMutation = { readonly __typename?: 'Mutation', readonly verifyNewUser: boolean };

export type BootstrapCacheQueryVariables = Exact<{ [key: string]: never; }>;


export type BootstrapCacheQuery = { readonly __typename?: 'Query', readonly signedInUser: { readonly __typename?: 'Advisor2', readonly id: string, readonly firstName: string, readonly lastName: string, readonly role: UserRole, readonly profileId: string, readonly email: string } | { readonly __typename?: 'Client', readonly id: string, readonly firstName: string, readonly lastName: string, readonly role: UserRole, readonly profileId: string, readonly email: string } };

export type GetUserProfileInfoQueryVariables = Exact<{
  userID?: InputMaybe<Scalars['ID']>;
}>;


export type GetUserProfileInfoQuery = { readonly __typename?: 'Query', readonly getUserProfileInfo: { readonly __typename?: 'UserInfoOutputQuery', readonly firstName?: string | null, readonly middleName?: string | null, readonly lastName?: string | null, readonly suffix?: string | null, readonly email?: string | null, readonly dateOfBirth?: string | null, readonly ssnTaxId?: string | null, readonly countryOfBirth?: string | null, readonly visaType?: VisaType | null, readonly permanentResident?: boolean | null, readonly visaExpirationDate?: string | null, readonly maritalStatus?: string | null, readonly phone?: string | null, readonly companyName?: string | null, readonly employer?: string | null, readonly brokerDealerAffiliation?: boolean | null, readonly directorStockOwner?: boolean | null, readonly annualIncome?: string | null, readonly liquidNetworth?: string | null, readonly occupation?: string | null, readonly backupWithholding?: boolean | null, readonly spouseDateOfBirth?: string | null, readonly employerAddress?: { readonly __typename?: 'EntityAddress', readonly addr1: string, readonly city: string, readonly id: string, readonly state: string, readonly zipCode: string } | null } };

export type updateUserInfoMutationVariables = Exact<{
  userInfo: UserInfoInput;
  userID?: InputMaybe<Scalars['ID']>;
}>;


export type updateUserInfoMutation = { readonly __typename?: 'Mutation', readonly updateUserInfo?: { readonly __typename?: 'UserInfoOutputMutation', readonly dateOfBirth?: string | null, readonly ssnTaxId?: string | null, readonly countryOfBirth?: string | null, readonly maritalStatus?: string | null, readonly phone?: string | null, readonly companyName?: string | null, readonly companyTaxId?: string | null, readonly companyLegalName?: string | null, readonly employer?: string | null, readonly brokerDealerAffiliation?: boolean | null, readonly directorStockOwner?: boolean | null, readonly annualIncome?: string | null, readonly liquidNetworth?: string | null, readonly investmentRiskTolerance?: string | null, readonly permanentResident?: boolean | null, readonly visaType?: VisaType | null, readonly visaExpirationDate?: string | null, readonly firstName?: string | null, readonly lastName?: string | null } | null };

export type updateAccountStateMutationVariables = Exact<{
  id: Scalars['ID'];
  userId: Scalars['ID'];
  groupId?: InputMaybe<Scalars['ID']>;
  account?: InputMaybe<AccountInput>;
  state: State;
  jointUserIds?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['ID']>>>;
  openingInPerson?: InputMaybe<Scalars['Boolean']>;
  trustUserIds?: InputMaybe<ReadonlyArray<InputMaybe<Scalars['ID']>>>;
}>;


export type updateAccountStateMutation = { readonly __typename?: 'Mutation', readonly updateAccountState: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } };

export type getJointUserProfilesQueryVariables = Exact<{
  accountStateId: Scalars['ID'];
}>;


export type getJointUserProfilesQuery = { readonly __typename?: 'Query', readonly getJointUserProfiles: ReadonlyArray<{ readonly __typename?: 'UserInfoOutputQuery', readonly id: string, readonly firstName?: string | null, readonly lastName?: string | null, readonly address?: string | null, readonly state?: string | null, readonly zipcode?: string | null, readonly email?: string | null, readonly phone?: string | null, readonly companyName?: string | null, readonly ssnTaxId?: string | null, readonly externalRef?: string | null, readonly dateOfBirth?: string | null, readonly avatarURL?: string | null, readonly language?: string | null, readonly gender?: string | null, readonly employmentStatus?: string | null, readonly employer?: string | null, readonly annualIncome?: string | null, readonly liquidNetworth?: string | null, readonly maritalStatus?: string | null, readonly brokerDealerAffiliation?: boolean | null, readonly directorStockOwner?: boolean | null, readonly politicallyExposed?: boolean | null, readonly investmentObjective?: string | null, readonly investmentExperience?: string | null, readonly termsOfUseDisclosure?: boolean | null, readonly marketDataAgreementDisclosure?: boolean | null, readonly customerAgreementDisclosure?: boolean | null, readonly rule14bDisclosure?: boolean | null, readonly findersFeeDisclosure?: boolean | null, readonly countryOfBirth?: string | null, readonly permanentResident?: boolean | null, readonly visaType?: VisaType | null, readonly visaExpirationDate?: string | null, readonly occupation?: string | null, readonly backupWithholding?: boolean | null, readonly employerAddress?: { readonly __typename?: 'EntityAddress', readonly addr1: string, readonly city: string, readonly state: string, readonly zipCode: string } | null }> };

export type updateJointUserProfileMutationVariables = Exact<{
  accountStateId: Scalars['ID'];
  userProfileId: Scalars['ID'];
  userProfile: UserInfoInput;
}>;


export type updateJointUserProfileMutation = { readonly __typename?: 'Mutation', readonly updateJointUserProfile?: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } | null };

export type getStatesByIdFieldsFragment = { readonly __typename?: 'StateOutput', readonly id: string, readonly userId: string, readonly groupId?: string | null, readonly multiAccountDraftId?: string | null, readonly created: string, readonly financialAccountId?: string | null, readonly jointUserIds?: ReadonlyArray<string> | null, readonly state: State, readonly trustUserIds?: ReadonlyArray<string> | null, readonly accountInfoId?: string | null, readonly account?: { readonly __typename?: 'AccountType', readonly accountType: string, readonly retirement?: boolean | null, readonly planName?: string | null, readonly decedentName?: string | null, readonly decedentId?: string | null, readonly altruistRepcode?: string | null, readonly minorUserId?: string | null, readonly sourceOfFunds?: SourceOfFunds | null, readonly planTaxID?: string | null, readonly planType?: string | null, readonly purposeOfAccount?: PurposeOfAccount | null, readonly stateOfFormation?: string | null, readonly backupWithholding?: boolean | null, readonly corporationDetails?: { readonly __typename?: 'CorporationDetails', readonly corporationId: string, readonly corporation?: { readonly __typename?: 'Corporation', readonly id: string, readonly applicationStatus?: CorpApplicationStatus | null } | null } | null, readonly documents?: ReadonlyArray<{ readonly __typename?: 'AccountStateDocument', readonly userId?: string | null, readonly documentId: string, readonly documentType: AccountDocumentType }> | null, readonly finraAffiliations?: ReadonlyArray<{ readonly __typename?: 'AccountStateFinraAffiliation', readonly userId: string, readonly firmName?: string | null }> | null, readonly solo401kDetails?: { readonly __typename?: 'Solo401kDetailsType', readonly planAdministrator?: string | null, readonly planEffectiveDate?: string | null, readonly employerUserId?: string | null } | null, readonly beneficiaries?: ReadonlyArray<{ readonly __typename?: 'BeneficiaryItemType', readonly beneficiaryId: string, readonly percentage: number, readonly category: BeneficiaryCategory, readonly relationship: string }> | null, readonly planDetails?: { readonly __typename?: 'AccountStatePlanDetails', readonly participantUserId?: string | null, readonly planDetailsId?: string | null } | null, readonly trustDetails?: { readonly __typename?: 'TrustOutput', readonly name?: string | null, readonly taxIdentificationNumber?: string | null, readonly taxIdType?: TrustTaxIdType | null, readonly dateEstablished?: string | null, readonly stateEstablished?: string | null, readonly address?: string | null, readonly city?: string | null, readonly state?: string | null, readonly zipCode?: string | null, readonly supportedChecked?: boolean | null, readonly trustRevocability?: TrustRevocable | null } | null } | null };

export type getStatesFieldsFragment = { readonly __typename?: 'StateOutput', readonly id: string, readonly userId: string, readonly multiAccountDraftId?: string | null, readonly created: string, readonly jointUserIds?: ReadonlyArray<string> | null, readonly state: State, readonly financialAccountId?: string | null, readonly account?: { readonly __typename?: 'AccountType', readonly accountType: string, readonly retirement?: boolean | null, readonly solo401kDetails?: { readonly __typename?: 'Solo401kDetailsType', readonly planAdministrator?: string | null, readonly planEffectiveDate?: string | null, readonly employerUserId?: string | null } | null, readonly planDetails?: { readonly __typename?: 'AccountStatePlanDetails', readonly participantUserId?: string | null, readonly planDetailsId?: string | null } | null, readonly beneficiaries?: ReadonlyArray<{ readonly __typename?: 'BeneficiaryItemType', readonly beneficiaryId: string, readonly percentage: number }> | null, readonly corporationDetails?: { readonly __typename?: 'CorporationDetails', readonly corporationId: string, readonly corporation?: { readonly __typename?: 'Corporation', readonly id: string, readonly applicationStatus?: CorpApplicationStatus | null, readonly profile: { readonly __typename?: 'EntityProfile', readonly id: string, readonly companyName: string } } | null } | null } | null };

export type GetStatesByUserQueryVariables = Exact<{ [key: string]: never; }>;


export type GetStatesByUserQuery = { readonly __typename?: 'Query', readonly getStatesByUser: ReadonlyArray<{ readonly __typename?: 'StateOutput', readonly id: string, readonly userId: string, readonly multiAccountDraftId?: string | null, readonly created: string, readonly jointUserIds?: ReadonlyArray<string> | null, readonly state: State, readonly financialAccountId?: string | null, readonly account?: { readonly __typename?: 'AccountType', readonly accountType: string, readonly retirement?: boolean | null, readonly solo401kDetails?: { readonly __typename?: 'Solo401kDetailsType', readonly planAdministrator?: string | null, readonly planEffectiveDate?: string | null, readonly employerUserId?: string | null } | null, readonly planDetails?: { readonly __typename?: 'AccountStatePlanDetails', readonly participantUserId?: string | null, readonly planDetailsId?: string | null } | null, readonly beneficiaries?: ReadonlyArray<{ readonly __typename?: 'BeneficiaryItemType', readonly beneficiaryId: string, readonly percentage: number }> | null, readonly corporationDetails?: { readonly __typename?: 'CorporationDetails', readonly corporationId: string, readonly corporation?: { readonly __typename?: 'Corporation', readonly id: string, readonly applicationStatus?: CorpApplicationStatus | null, readonly profile: { readonly __typename?: 'EntityProfile', readonly id: string, readonly companyName: string } } | null } | null } | null }> };

export type GetAccountStateByIdQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetAccountStateByIdQuery = { readonly __typename?: 'Query', readonly getAccountStateById: { readonly __typename?: 'StateOutput', readonly id: string, readonly userId: string, readonly groupId?: string | null, readonly multiAccountDraftId?: string | null, readonly created: string, readonly financialAccountId?: string | null, readonly jointUserIds?: ReadonlyArray<string> | null, readonly state: State, readonly trustUserIds?: ReadonlyArray<string> | null, readonly accountInfoId?: string | null, readonly account?: { readonly __typename?: 'AccountType', readonly accountType: string, readonly retirement?: boolean | null, readonly planName?: string | null, readonly decedentName?: string | null, readonly decedentId?: string | null, readonly altruistRepcode?: string | null, readonly minorUserId?: string | null, readonly sourceOfFunds?: SourceOfFunds | null, readonly planTaxID?: string | null, readonly planType?: string | null, readonly purposeOfAccount?: PurposeOfAccount | null, readonly stateOfFormation?: string | null, readonly backupWithholding?: boolean | null, readonly corporationDetails?: { readonly __typename?: 'CorporationDetails', readonly corporationId: string, readonly corporation?: { readonly __typename?: 'Corporation', readonly id: string, readonly applicationStatus?: CorpApplicationStatus | null } | null } | null, readonly documents?: ReadonlyArray<{ readonly __typename?: 'AccountStateDocument', readonly userId?: string | null, readonly documentId: string, readonly documentType: AccountDocumentType }> | null, readonly finraAffiliations?: ReadonlyArray<{ readonly __typename?: 'AccountStateFinraAffiliation', readonly userId: string, readonly firmName?: string | null }> | null, readonly solo401kDetails?: { readonly __typename?: 'Solo401kDetailsType', readonly planAdministrator?: string | null, readonly planEffectiveDate?: string | null, readonly employerUserId?: string | null } | null, readonly beneficiaries?: ReadonlyArray<{ readonly __typename?: 'BeneficiaryItemType', readonly beneficiaryId: string, readonly percentage: number, readonly category: BeneficiaryCategory, readonly relationship: string }> | null, readonly planDetails?: { readonly __typename?: 'AccountStatePlanDetails', readonly participantUserId?: string | null, readonly planDetailsId?: string | null } | null, readonly trustDetails?: { readonly __typename?: 'TrustOutput', readonly name?: string | null, readonly taxIdentificationNumber?: string | null, readonly taxIdType?: TrustTaxIdType | null, readonly dateEstablished?: string | null, readonly stateEstablished?: string | null, readonly address?: string | null, readonly city?: string | null, readonly state?: string | null, readonly zipCode?: string | null, readonly supportedChecked?: boolean | null, readonly trustRevocability?: TrustRevocable | null } | null } | null } };

export type FullUserStateFragment = { readonly __typename?: 'StateOutput', readonly id: string, readonly userId: string, readonly groupId?: string | null, readonly multiAccountDraftId?: string | null, readonly created: string, readonly financialAccountId?: string | null, readonly jointUserIds?: ReadonlyArray<string> | null, readonly state: State, readonly trustUserIds?: ReadonlyArray<string> | null, readonly accountInfoId?: string | null, readonly account?: { readonly __typename?: 'AccountType', readonly accountType: string, readonly retirement?: boolean | null, readonly planName?: string | null, readonly decedentName?: string | null, readonly decedentId?: string | null, readonly altruistRepcode?: string | null, readonly minorUserId?: string | null, readonly sourceOfFunds?: SourceOfFunds | null, readonly planTaxID?: string | null, readonly planType?: string | null, readonly purposeOfAccount?: PurposeOfAccount | null, readonly stateOfFormation?: string | null, readonly backupWithholding?: boolean | null, readonly corporationDetails?: { readonly __typename?: 'CorporationDetails', readonly corporationId: string, readonly corporation?: { readonly __typename?: 'Corporation', readonly id: string, readonly applicationStatus?: CorpApplicationStatus | null } | null } | null, readonly documents?: ReadonlyArray<{ readonly __typename?: 'AccountStateDocument', readonly userId?: string | null, readonly documentId: string, readonly documentType: AccountDocumentType }> | null, readonly finraAffiliations?: ReadonlyArray<{ readonly __typename?: 'AccountStateFinraAffiliation', readonly userId: string, readonly firmName?: string | null }> | null, readonly solo401kDetails?: { readonly __typename?: 'Solo401kDetailsType', readonly planAdministrator?: string | null, readonly planEffectiveDate?: string | null, readonly employerUserId?: string | null } | null, readonly beneficiaries?: ReadonlyArray<{ readonly __typename?: 'BeneficiaryItemType', readonly beneficiaryId: string, readonly percentage: number, readonly category: BeneficiaryCategory, readonly relationship: string }> | null, readonly planDetails?: { readonly __typename?: 'AccountStatePlanDetails', readonly participantUserId?: string | null, readonly planDetailsId?: string | null } | null, readonly trustDetails?: { readonly __typename?: 'TrustOutput', readonly name?: string | null, readonly taxIdentificationNumber?: string | null, readonly taxIdType?: TrustTaxIdType | null, readonly dateEstablished?: string | null, readonly stateEstablished?: string | null, readonly address?: string | null, readonly city?: string | null, readonly state?: string | null, readonly zipCode?: string | null, readonly supportedChecked?: boolean | null, readonly trustRevocability?: TrustRevocable | null } | null } | null };

export type getClientDocumentsQueryVariables = Exact<{ [key: string]: never; }>;


export type getClientDocumentsQuery = { readonly __typename?: 'Query', readonly getClientDocuments: ReadonlyArray<{ readonly __typename?: 'ClientDocument', readonly id: string, readonly documentName?: string | null, readonly documentType?: string | null, readonly documentId?: string | null, readonly reviewed?: boolean | null }> };

export type updateClientDocumentReviewStatusMutationVariables = Exact<{
  documentId: Scalars['ID'];
}>;


export type updateClientDocumentReviewStatusMutation = { readonly __typename?: 'Mutation', readonly updateClientDocumentReviewStatus?: string | null };

export type beneficiaryFieldsFragment = { readonly __typename?: 'BeneficiaryOutput', readonly id: string, readonly type: BeneficiaryType, readonly companyName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly phone?: string | null, readonly dateOfBirth?: string | null, readonly taxIdentificationNumber?: string | null, readonly category?: string | null, readonly relationship?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null };

export type beneficiaryWithoutCategoryFieldsFragment = { readonly __typename?: 'BeneficiaryOutput', readonly id: string, readonly type: BeneficiaryType, readonly companyName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly phone?: string | null, readonly dateOfBirth?: string | null, readonly taxIdentificationNumber?: string | null, readonly relationship?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null };

export type createBeneficiaryMutationVariables = Exact<{
  userIds: ReadonlyArray<Scalars['ID']>;
  beneficiaryClass?: InputMaybe<BeneficiaryClass>;
  type: BeneficiaryType;
  companyName?: InputMaybe<Scalars['String']>;
  firstName?: InputMaybe<Scalars['String']>;
  lastName?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  dateOfBirth: Scalars['String'];
  taxIdentificationNumber?: InputMaybe<Scalars['String']>;
  financialAccountId?: InputMaybe<Scalars['String']>;
  category?: InputMaybe<Scalars['String']>;
  relationship?: InputMaybe<Scalars['String']>;
  address?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  zipCode?: InputMaybe<Scalars['String']>;
  state?: InputMaybe<Scalars['String']>;
}>;


export type createBeneficiaryMutation = { readonly __typename?: 'Mutation', readonly createBeneficiary: { readonly __typename?: 'BeneficiaryOutput', readonly id: string, readonly type: BeneficiaryType, readonly companyName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly phone?: string | null, readonly dateOfBirth?: string | null, readonly taxIdentificationNumber?: string | null, readonly category?: string | null, readonly relationship?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null } };

export type editBeneficiaryMutationVariables = Exact<{
  id: Scalars['ID'];
  type: BeneficiaryType;
  companyName?: InputMaybe<Scalars['String']>;
  firstName?: InputMaybe<Scalars['String']>;
  lastName?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  dateOfBirth: Scalars['String'];
  taxIdentificationNumber?: InputMaybe<Scalars['String']>;
  financialAccountId?: InputMaybe<Scalars['String']>;
  category?: InputMaybe<Scalars['String']>;
  relationship?: InputMaybe<Scalars['String']>;
  address?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  zipCode?: InputMaybe<Scalars['String']>;
  state?: InputMaybe<Scalars['String']>;
}>;


export type editBeneficiaryMutation = { readonly __typename?: 'Mutation', readonly editBeneficiary: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } };

export type deleteBeneficiaryMutationVariables = Exact<{
  id: Scalars['ID'];
  financialAccountId?: InputMaybe<Scalars['ID']>;
}>;


export type deleteBeneficiaryMutation = { readonly __typename?: 'Mutation', readonly deleteBeneficiary: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } };

export type DeleteBeneficiariesMutationVariables = Exact<{
  ids: ReadonlyArray<Scalars['ID']>;
  financialAccountId?: InputMaybe<Scalars['ID']>;
}>;


export type DeleteBeneficiariesMutation = { readonly __typename?: 'Mutation', readonly deleteBeneficiaries: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } };

export type getBeneficiariesByAccountIDQueryVariables = Exact<{
  financialAccountId: Scalars['ID'];
}>;


export type getBeneficiariesByAccountIDQuery = { readonly __typename?: 'Query', readonly getBeneficiariesByAccountID: ReadonlyArray<{ readonly __typename?: 'BeneficiaryOutput', readonly percentage?: number | null, readonly id: string, readonly type: BeneficiaryType, readonly companyName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly phone?: string | null, readonly dateOfBirth?: string | null, readonly taxIdentificationNumber?: string | null, readonly category?: string | null, readonly relationship?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null }>, readonly getFinancialAccountOwners: ReadonlyArray<{ readonly __typename?: 'Client', readonly id: string }> };

export type editBeneficiaryAllocationListMutationVariables = Exact<{
  financialAccountId: Scalars['ID'];
  beneficiaries: ReadonlyArray<InputMaybe<BeneficiaryAllocation>>;
}>;


export type editBeneficiaryAllocationListMutation = { readonly __typename?: 'Mutation', readonly editBeneficiaryAllocationList: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } };

export type GetAccountCreationStatusQueryVariables = Exact<{
  financialAccountIds: ReadonlyArray<Scalars['ID']>;
}>;


export type GetAccountCreationStatusQuery = { readonly __typename?: 'Query', readonly getAccountCreationStatus: ReadonlyArray<{ readonly __typename?: 'AccountStatusOutput', readonly financialAccountId: string, readonly accountNumber: string, readonly accountType: string, readonly financialAccountName: string, readonly status: AccountCreationStatus, readonly owners: ReadonlyArray<{ readonly __typename?: 'StatusOwnerDetails', readonly firstName: string, readonly lastName: string }> }> };

export type createTrustedContactMutationVariables = Exact<{
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  email?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
}>;


export type createTrustedContactMutation = { readonly __typename?: 'Mutation', readonly createTrustedContact: { readonly __typename?: 'TrustedContactOutput', readonly id: string } };

export type updateTrustedContactMutationVariables = Exact<{
  id: Scalars['ID'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  email?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
}>;


export type updateTrustedContactMutation = { readonly __typename?: 'Mutation', readonly updateTrustedContact: { readonly __typename?: 'TrustedContactOutput', readonly id: string } };

export type getTrustedContactByIDQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type getTrustedContactByIDQuery = { readonly __typename?: 'Query', readonly getTrustedContactByID: { readonly __typename?: 'TrustedContactOutput', readonly id: string, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly phone?: string | null } };

export type getBeneficiariesQueryVariables = Exact<{
  ids: ReadonlyArray<Scalars['ID']>;
}>;


export type getBeneficiariesQuery = { readonly __typename?: 'Query', readonly getBeneficiaries: ReadonlyArray<{ readonly __typename?: 'BeneficiaryOutput', readonly id: string, readonly type: BeneficiaryType, readonly companyName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly phone?: string | null, readonly dateOfBirth?: string | null, readonly taxIdentificationNumber?: string | null, readonly relationship?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null, readonly linkedFinancialAccounts?: ReadonlyArray<{ readonly __typename?: 'FinancialAccount2', readonly financialAccountId: string } | null> | null } | null> };

export type GetIraConstraintsQueryVariables = Exact<{
  brokerageAccountId: Scalars['ID'];
  direction: TransactionDirection;
}>;


export type GetIraConstraintsQuery = { readonly __typename?: 'Query', readonly getIraConstraints?: { readonly __typename?: 'IraConstraints', readonly distributionConstraints?: { readonly __typename?: 'DistributionConstraints', readonly fullBalanceAllowed?: boolean | null, readonly allowedDistributions?: ReadonlyArray<{ readonly __typename?: 'AllowedDistribution', readonly federalWithholdingRequired?: boolean | null, readonly reason?: DistributionReason | null, readonly stateWithholdingRequired?: boolean | null, readonly reasonEntryType?: string | null } | null> | null } | null } | null };

export type updateAccountAddressMutationVariables = Exact<{
  userId: Scalars['ID'];
  profileId: Scalars['ID'];
  address: AdvisorAddressInput;
  skipDW?: InputMaybe<Scalars['Boolean']>;
}>;


export type updateAccountAddressMutation = { readonly __typename?: 'Mutation', readonly updateAccountAddress?: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } | null };

export type getAccountCashSummaryQueryVariables = Exact<{
  finacctId: Scalars['String'];
}>;


export type getAccountCashSummaryQuery = { readonly __typename?: 'Query', readonly getAccountCashSummary: { readonly __typename?: 'AccountCashSummary', readonly availableForTrade: number, readonly availableForWithdrawal: number, readonly balance: number } };

export type getBalanceBreakdownQueryVariables = Exact<{
  financialIds: ReadonlyArray<Scalars['ID']>;
}>;


export type getBalanceBreakdownQuery = { readonly __typename?: 'Query', readonly getBalanceBreakdown: { readonly __typename?: 'BalanceOutput', readonly totalBalance: number, readonly managedAccountsBalance: number, readonly externalAccountsBalance?: number | null } };

export type beneficiariesByUsersQueryVariables = Exact<{
  where: BeneficiariesByUserWhere;
}>;


export type beneficiariesByUsersQuery = { readonly __typename?: 'Query', readonly beneficiariesByUsers: ReadonlyArray<{ readonly __typename?: 'BeneficiariesByUser', readonly userId: string, readonly beneficiaries: ReadonlyArray<{ readonly __typename?: 'BeneficiaryOutput', readonly id: string, readonly type: BeneficiaryType, readonly companyName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly phone?: string | null, readonly dateOfBirth?: string | null, readonly taxIdentificationNumber?: string | null, readonly relationship?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null, readonly linkedFinancialAccounts?: ReadonlyArray<{ readonly __typename?: 'FinancialAccount2', readonly financialAccountId: string } | null> | null }> }> };

export type AccountBeneficiariesByClientsQueryVariables = Exact<{
  where: AccountBeneficiariesByClientsWhere;
}>;


export type AccountBeneficiariesByClientsQuery = { readonly __typename?: 'Query', readonly accountBeneficiariesByClients: ReadonlyArray<{ readonly __typename?: 'AccountBeneficiariesByUser', readonly userId: string, readonly beneficiaries: ReadonlyArray<{ readonly __typename?: 'Beneficiary', readonly id: string, readonly accountInfoId?: string | null, readonly dob?: any | null, readonly entityFormation?: any | null, readonly entityName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly relationship: BeneficiaryRelationship, readonly relatedAccountInfoIds: ReadonlyArray<string>, readonly designationType?: BeneficiaryDesignationType | null }> }> };

export type linkExistingBeneficiaryMutationVariables = Exact<{
  input: LinkBeneficiaryInput;
}>;


export type linkExistingBeneficiaryMutation = { readonly __typename?: 'Mutation', readonly linkExistingBeneficiary: { readonly __typename?: 'BeneficiaryOutput', readonly id: string } };

export type sendCorpApplicationToSecretaryMutationVariables = Exact<{
  input: SecretaryReviewInput;
}>;


export type sendCorpApplicationToSecretaryMutation = { readonly __typename?: 'Mutation', readonly sendCorpApplicationToSecretary?: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } | null };

export type resendCorpApplicationToSecretaryMutationVariables = Exact<{
  input: SecretaryReviewResendInput;
}>;


export type resendCorpApplicationToSecretaryMutation = { readonly __typename?: 'Mutation', readonly resendCorpApplicationToSecretary: { readonly __typename?: 'Corporation', readonly id: string, readonly applicationStatus?: CorpApplicationStatus | null } };

export type GetActiveBrokerageAccountsQueryVariables = Exact<{
  userId: Scalars['ID'];
}>;


export type GetActiveBrokerageAccountsQuery = { readonly __typename?: 'Query', readonly clientBrokerageAccounts: ReadonlyArray<{ readonly __typename?: 'AccountStatusOutput', readonly financialAccountId: string }> };

export type AllAccountStatesGroupedQueryVariables = Exact<{ [key: string]: never; }>;


export type AllAccountStatesGroupedQuery = { readonly __typename?: 'Query', readonly allAccountStatesGrouped: ReadonlyArray<{ readonly __typename?: 'AccountStatesGroupedByMultiAccountDraftId', readonly id: string, readonly states: ReadonlyArray<{ readonly __typename?: 'StateOutput', readonly id: string, readonly userId: string, readonly groupId?: string | null, readonly multiAccountDraftId?: string | null, readonly created: string, readonly financialAccountId?: string | null, readonly jointUserIds?: ReadonlyArray<string> | null, readonly state: State, readonly trustUserIds?: ReadonlyArray<string> | null, readonly accountInfoId?: string | null, readonly account?: { readonly __typename?: 'AccountType', readonly accountType: string, readonly retirement?: boolean | null, readonly planName?: string | null, readonly decedentName?: string | null, readonly decedentId?: string | null, readonly altruistRepcode?: string | null, readonly minorUserId?: string | null, readonly sourceOfFunds?: SourceOfFunds | null, readonly planTaxID?: string | null, readonly planType?: string | null, readonly purposeOfAccount?: PurposeOfAccount | null, readonly stateOfFormation?: string | null, readonly backupWithholding?: boolean | null, readonly corporationDetails?: { readonly __typename?: 'CorporationDetails', readonly corporationId: string, readonly corporation?: { readonly __typename?: 'Corporation', readonly id: string, readonly applicationStatus?: CorpApplicationStatus | null } | null } | null, readonly documents?: ReadonlyArray<{ readonly __typename?: 'AccountStateDocument', readonly userId?: string | null, readonly documentId: string, readonly documentType: AccountDocumentType }> | null, readonly finraAffiliations?: ReadonlyArray<{ readonly __typename?: 'AccountStateFinraAffiliation', readonly userId: string, readonly firmName?: string | null }> | null, readonly solo401kDetails?: { readonly __typename?: 'Solo401kDetailsType', readonly planAdministrator?: string | null, readonly planEffectiveDate?: string | null, readonly employerUserId?: string | null } | null, readonly beneficiaries?: ReadonlyArray<{ readonly __typename?: 'BeneficiaryItemType', readonly beneficiaryId: string, readonly percentage: number, readonly category: BeneficiaryCategory, readonly relationship: string }> | null, readonly planDetails?: { readonly __typename?: 'AccountStatePlanDetails', readonly participantUserId?: string | null, readonly planDetailsId?: string | null } | null, readonly trustDetails?: { readonly __typename?: 'TrustOutput', readonly name?: string | null, readonly taxIdentificationNumber?: string | null, readonly taxIdType?: TrustTaxIdType | null, readonly dateEstablished?: string | null, readonly stateEstablished?: string | null, readonly address?: string | null, readonly city?: string | null, readonly state?: string | null, readonly zipCode?: string | null, readonly supportedChecked?: boolean | null, readonly trustRevocability?: TrustRevocable | null } | null } | null }> }> };

export type FullAccountStateGroupedFragment = { readonly __typename?: 'AccountStatesGroupedByMultiAccountDraftId', readonly id: string, readonly states: ReadonlyArray<{ readonly __typename?: 'StateOutput', readonly id: string, readonly userId: string, readonly groupId?: string | null, readonly multiAccountDraftId?: string | null, readonly created: string, readonly financialAccountId?: string | null, readonly jointUserIds?: ReadonlyArray<string> | null, readonly state: State, readonly trustUserIds?: ReadonlyArray<string> | null, readonly accountInfoId?: string | null, readonly account?: { readonly __typename?: 'AccountType', readonly accountType: string, readonly retirement?: boolean | null, readonly planName?: string | null, readonly decedentName?: string | null, readonly decedentId?: string | null, readonly altruistRepcode?: string | null, readonly minorUserId?: string | null, readonly sourceOfFunds?: SourceOfFunds | null, readonly planTaxID?: string | null, readonly planType?: string | null, readonly purposeOfAccount?: PurposeOfAccount | null, readonly stateOfFormation?: string | null, readonly backupWithholding?: boolean | null, readonly corporationDetails?: { readonly __typename?: 'CorporationDetails', readonly corporationId: string, readonly corporation?: { readonly __typename?: 'Corporation', readonly id: string, readonly applicationStatus?: CorpApplicationStatus | null } | null } | null, readonly documents?: ReadonlyArray<{ readonly __typename?: 'AccountStateDocument', readonly userId?: string | null, readonly documentId: string, readonly documentType: AccountDocumentType }> | null, readonly finraAffiliations?: ReadonlyArray<{ readonly __typename?: 'AccountStateFinraAffiliation', readonly userId: string, readonly firmName?: string | null }> | null, readonly solo401kDetails?: { readonly __typename?: 'Solo401kDetailsType', readonly planAdministrator?: string | null, readonly planEffectiveDate?: string | null, readonly employerUserId?: string | null } | null, readonly beneficiaries?: ReadonlyArray<{ readonly __typename?: 'BeneficiaryItemType', readonly beneficiaryId: string, readonly percentage: number, readonly category: BeneficiaryCategory, readonly relationship: string }> | null, readonly planDetails?: { readonly __typename?: 'AccountStatePlanDetails', readonly participantUserId?: string | null, readonly planDetailsId?: string | null } | null, readonly trustDetails?: { readonly __typename?: 'TrustOutput', readonly name?: string | null, readonly taxIdentificationNumber?: string | null, readonly taxIdType?: TrustTaxIdType | null, readonly dateEstablished?: string | null, readonly stateEstablished?: string | null, readonly address?: string | null, readonly city?: string | null, readonly state?: string | null, readonly zipCode?: string | null, readonly supportedChecked?: boolean | null, readonly trustRevocability?: TrustRevocable | null } | null } | null }> };

export type CreateAccountFromStateBulkMutationVariables = Exact<{
  input: CreateAccountFromStateBulkInput;
}>;


export type CreateAccountFromStateBulkMutation = { readonly __typename?: 'Mutation', readonly createAccountFromStateBulk: ReadonlyArray<{ readonly __typename?: 'CreateBulkAccountOutput', readonly accountStateId: string }> };

export type SaveAccountStateBulkMutationVariables = Exact<{
  input: SaveStateBulkInput;
}>;


export type SaveAccountStateBulkMutation = { readonly __typename?: 'Mutation', readonly saveAccountStatesBulk: ReadonlyArray<{ readonly __typename?: 'StateOutput', readonly id: string }> };

export type UpdateAccountStatesBulkMutationVariables = Exact<{
  input: UpdateStateBulkInput;
}>;


export type UpdateAccountStatesBulkMutation = { readonly __typename?: 'Mutation', readonly updateAccountStatesBulk: ReadonlyArray<{ readonly __typename?: 'StateOutput', readonly id: string }> };

export type AccountStatesCreationStatusQueryVariables = Exact<{
  where: AccountStatesCreationStatusWhere;
}>;


export type AccountStatesCreationStatusQuery = { readonly __typename?: 'Query', readonly accountStatesCreationStatus: ReadonlyArray<{ readonly __typename?: 'AccountStateCreationStatus', readonly accountStateId: string, readonly status?: { readonly __typename?: 'AccountStatusOutput', readonly financialAccountId: string, readonly status: AccountCreationStatus } | null }> };

export type AccountBeneficiaryFragment = { readonly __typename?: 'Beneficiary', readonly id: string, readonly accountInfoId?: string | null, readonly designation: string, readonly dob?: any | null, readonly email?: string | null, readonly entityFormation?: any | null, readonly entityName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly phoneNumber?: string | null, readonly relationship: BeneficiaryRelationship, readonly taxId?: string | null, readonly designationType?: BeneficiaryDesignationType | null, readonly addressInfo?: { readonly __typename?: 'AccountAddress', readonly street?: string | null, readonly state?: string | null, readonly city?: string | null, readonly zip?: string | null } | null };

export type CreateAccountBeneficiaryMutationVariables = Exact<{
  input: CreateAccountBeneficiaryInput;
}>;


export type CreateAccountBeneficiaryMutation = { readonly __typename?: 'Mutation', readonly createAccountBeneficiary: { readonly __typename?: 'Beneficiary', readonly percentage?: number | null, readonly id: string, readonly accountInfoId?: string | null, readonly designation: string, readonly dob?: any | null, readonly email?: string | null, readonly entityFormation?: any | null, readonly entityName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly phoneNumber?: string | null, readonly relationship: BeneficiaryRelationship, readonly taxId?: string | null, readonly designationType?: BeneficiaryDesignationType | null, readonly addressInfo?: { readonly __typename?: 'AccountAddress', readonly street?: string | null, readonly state?: string | null, readonly city?: string | null, readonly zip?: string | null } | null } };

export type UpdateAccountBeneficiaryMutationVariables = Exact<{
  input: UpdateAccountBeneficiaryInput;
}>;


export type UpdateAccountBeneficiaryMutation = { readonly __typename?: 'Mutation', readonly updateAccountBeneficiary: { readonly __typename?: 'Beneficiary', readonly id: string, readonly accountInfoId?: string | null, readonly designation: string, readonly dob?: any | null, readonly email?: string | null, readonly entityFormation?: any | null, readonly entityName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly phoneNumber?: string | null, readonly relationship: BeneficiaryRelationship, readonly taxId?: string | null, readonly designationType?: BeneficiaryDesignationType | null, readonly addressInfo?: { readonly __typename?: 'AccountAddress', readonly street?: string | null, readonly state?: string | null, readonly city?: string | null, readonly zip?: string | null } | null } };

export type DeleteAccountBeneficiariesMutationVariables = Exact<{
  input: DeleteAccountBeneficiariesInput;
}>;


export type DeleteAccountBeneficiariesMutation = { readonly __typename?: 'Mutation', readonly deleteAccountBeneficiaries: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } };

export type AccountBeneficiariesQueryVariables = Exact<{
  where: AccountBeneficiariesWhere;
}>;


export type AccountBeneficiariesQuery = { readonly __typename?: 'Query', readonly accountBeneficiaries: ReadonlyArray<{ readonly __typename?: 'Beneficiary', readonly percentage?: number | null, readonly id: string, readonly accountInfoId?: string | null, readonly designation: string, readonly dob?: any | null, readonly email?: string | null, readonly entityFormation?: any | null, readonly entityName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly phoneNumber?: string | null, readonly relationship: BeneficiaryRelationship, readonly taxId?: string | null, readonly designationType?: BeneficiaryDesignationType | null, readonly addressInfo?: { readonly __typename?: 'AccountAddress', readonly street?: string | null, readonly state?: string | null, readonly city?: string | null, readonly zip?: string | null } | null }> };

export type EditAccountBeneficiaryAllocationMutationVariables = Exact<{
  input: EditAccountBeneficiaryAllocationInput;
}>;


export type EditAccountBeneficiaryAllocationMutation = { readonly __typename?: 'Mutation', readonly editAccountBeneficiaryAllocation: ReadonlyArray<{ readonly __typename?: 'Beneficiary', readonly id: string, readonly percentage?: number | null }> };

export type BeneficiaryRelatedAccountsQueryVariables = Exact<{
  where: AccountBeneficiaryByIdWhere;
}>;


export type BeneficiaryRelatedAccountsQuery = { readonly __typename?: 'Query', readonly accountBeneficiaryById: { readonly __typename?: 'Beneficiary', readonly id: string, readonly accountInfoId?: string | null, readonly relatedAccountInfoIds: ReadonlyArray<string> } };

export type ReuseExistingBeneficiaryMutationVariables = Exact<{
  input: ReuseExistingBeneficiaryInput;
}>;


export type ReuseExistingBeneficiaryMutation = { readonly __typename?: 'Mutation', readonly reuseExistingBeneficiary: { readonly __typename?: 'Beneficiary', readonly id: string, readonly accountInfoId?: string | null, readonly designation: string, readonly dob?: any | null, readonly email?: string | null, readonly entityFormation?: any | null, readonly entityName?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly phoneNumber?: string | null, readonly relationship: BeneficiaryRelationship, readonly taxId?: string | null, readonly designationType?: BeneficiaryDesignationType | null, readonly addressInfo?: { readonly __typename?: 'AccountAddress', readonly street?: string | null, readonly state?: string | null, readonly city?: string | null, readonly zip?: string | null } | null } };

export type UpdateAccountBeneficiariesDesignationMutationVariables = Exact<{
  input: UpdateAccountBeneficiariesDesignationInput;
}>;


export type UpdateAccountBeneficiariesDesignationMutation = { readonly __typename?: 'Mutation', readonly updateAccountBeneficiariesDesignation: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } };

export type ConstraintsQueryVariables = Exact<{
  where: ConstraintsWhere;
}>;


export type ConstraintsQuery = { readonly __typename?: 'Query', readonly constraints: { readonly __typename?: 'Constraints', readonly allowedStates: ReadonlyArray<string>, readonly requireMarried?: boolean | null } };

export type corporationQueryVariables = Exact<{
  where: CorporationWhere;
}>;


export type corporationQuery = { readonly __typename?: 'Query', readonly corporation: { readonly __typename?: 'Corporation', readonly id: string, readonly applicationStatus?: CorpApplicationStatus | null, readonly businessAddress?: { readonly __typename?: 'EntityAddress', readonly id: string, readonly addr1: string, readonly city: string, readonly state: string, readonly zipCode: string } | null, readonly mailingAddress?: { readonly __typename?: 'EntityAddress', readonly id: string, readonly addr1: string, readonly city: string, readonly state: string, readonly zipCode: string } | null, readonly profile: { readonly __typename?: 'EntityProfile', readonly id: string, readonly companyName: string, readonly entityTaxId?: string | null, readonly stateOfOrigin?: string | null, readonly entityNature?: AccountNature | null, readonly maintainedForForeignFinancialInstitution?: boolean | null, readonly foreignBank?: boolean | null, readonly pabAccount?: boolean | null, readonly exemptLegalCustomer?: boolean | null, readonly annualIncome?: string | null, readonly backupWithholding?: boolean | null, readonly liquidNetWorth?: string | null } } };

export type createCorporationMutationVariables = Exact<{
  input: CorporationInput;
}>;


export type createCorporationMutation = { readonly __typename?: 'Mutation', readonly createCorporation: { readonly __typename?: 'Corporation', readonly id: string, readonly businessAddress?: { readonly __typename?: 'EntityAddress', readonly id: string, readonly addr1: string, readonly city: string, readonly state: string, readonly zipCode: string } | null, readonly mailingAddress?: { readonly __typename?: 'EntityAddress', readonly id: string, readonly addr1: string, readonly city: string, readonly state: string, readonly zipCode: string } | null, readonly profile: { readonly __typename?: 'EntityProfile', readonly id: string, readonly companyName: string, readonly entityTaxId?: string | null, readonly stateOfOrigin?: string | null, readonly entityNature?: AccountNature | null, readonly maintainedForForeignFinancialInstitution?: boolean | null, readonly foreignBank?: boolean | null, readonly pabAccount?: boolean | null, readonly exemptLegalCustomer?: boolean | null, readonly annualIncome?: string | null, readonly backupWithholding?: boolean | null, readonly liquidNetWorth?: string | null } } };

export type updateCorporationMutationVariables = Exact<{
  input: UpdateCorporationInput;
}>;


export type updateCorporationMutation = { readonly __typename?: 'Mutation', readonly updateCorporation: { readonly __typename?: 'Corporation', readonly id: string, readonly businessAddress?: { readonly __typename?: 'EntityAddress', readonly id: string, readonly addr1: string, readonly city: string, readonly state: string, readonly zipCode: string } | null, readonly mailingAddress?: { readonly __typename?: 'EntityAddress', readonly id: string, readonly addr1: string, readonly city: string, readonly state: string, readonly zipCode: string } | null, readonly profile: { readonly __typename?: 'EntityProfile', readonly id: string, readonly companyName: string, readonly entityTaxId?: string | null, readonly stateOfOrigin?: string | null, readonly entityNature?: AccountNature | null, readonly maintainedForForeignFinancialInstitution?: boolean | null, readonly foreignBank?: boolean | null, readonly pabAccount?: boolean | null, readonly exemptLegalCustomer?: boolean | null, readonly annualIncome?: string | null, readonly backupWithholding?: boolean | null, readonly liquidNetWorth?: string | null } } };

export type CreateHeldAwayLinkMutationVariables = Exact<{
  input: CreateHeldAwayLinkInput;
}>;


export type CreateHeldAwayLinkMutation = { readonly __typename?: 'Mutation', readonly createHeldAwayLink?: { readonly __typename?: 'HeldAwayMutationResponse', readonly link?: { readonly __typename?: 'HeldAwayLink', readonly id: string, readonly accounts?: ReadonlyArray<{ readonly __typename?: 'HeldAwayAccount', readonly id: string, readonly name?: string | null, readonly type?: string | null, readonly subtype?: string | null }> | null } | null } | null };

export type getDashboardStatesFieldsFragment = { readonly __typename?: 'StateOutput', readonly id: string, readonly userId: string, readonly multiAccountDraftId?: string | null, readonly created: string, readonly jointUserIds?: ReadonlyArray<string> | null, readonly state: State, readonly financialAccountId?: string | null, readonly account?: { readonly __typename?: 'AccountType', readonly accountType: string, readonly retirement?: boolean | null, readonly corporationDetails?: { readonly __typename?: 'CorporationDetails', readonly corporationId: string, readonly corporation?: { readonly __typename?: 'Corporation', readonly id: string, readonly applicationStatus?: CorpApplicationStatus | null, readonly profile: { readonly __typename?: 'EntityProfile', readonly id: string, readonly companyName: string } } | null } | null } | null };

export type getDashboardScreenGQLQueryVariables = Exact<{
  groupIDs: ReadonlyArray<InputMaybe<AccountsInGroupInput>>;
  includeExternal?: InputMaybe<Scalars['Boolean']>;
}>;


export type getDashboardScreenGQLQuery = { readonly __typename?: 'Query', readonly getAllFinancialAccountsInGroup?: ReadonlyArray<{ readonly __typename?: 'AccountDetails', readonly financialAccountId?: string | null, readonly accountNumber?: string | null, readonly mask?: string | null, readonly institutionName?: string | null, readonly balance?: number | null, readonly realTimeBalance?: number | null, readonly managedAccount?: boolean | null, readonly accountClassification?: string | null, readonly ownerFirstName?: string | null, readonly ownerLastName?: string | null, readonly accountNickname?: string | null, readonly accountType?: string | null, readonly status?: string | null, readonly financialAccountName: string } | null> | null, readonly getStatesByUser: ReadonlyArray<{ readonly __typename?: 'StateOutput', readonly id: string, readonly userId: string, readonly multiAccountDraftId?: string | null, readonly created: string, readonly jointUserIds?: ReadonlyArray<string> | null, readonly state: State, readonly financialAccountId?: string | null, readonly account?: { readonly __typename?: 'AccountType', readonly accountType: string, readonly retirement?: boolean | null, readonly corporationDetails?: { readonly __typename?: 'CorporationDetails', readonly corporationId: string, readonly corporation?: { readonly __typename?: 'Corporation', readonly id: string, readonly applicationStatus?: CorpApplicationStatus | null, readonly profile: { readonly __typename?: 'EntityProfile', readonly id: string, readonly companyName: string } } | null } | null } | null }> };

export type DeleteHeldAwayLinkMutationVariables = Exact<{
  input: UnlinkHeldAwayInput;
}>;


export type DeleteHeldAwayLinkMutation = { readonly __typename?: 'Mutation', readonly unlinkHeldAway?: { readonly __typename?: 'DeleteResponse', readonly success?: boolean | null, readonly deletedId?: string | null } | null };

export type getAllDocumentsQueryVariables = Exact<{
  finacctId: Scalars['String'];
  documentType?: InputMaybe<DocumentType>;
  from?: InputMaybe<Scalars['String']>;
  to?: InputMaybe<Scalars['String']>;
}>;


export type getAllDocumentsQuery = { readonly __typename?: 'Query', readonly getAllDocuments: ReadonlyArray<{ readonly __typename?: 'Document', readonly accountId?: string | null, readonly link?: string | null, readonly date?: string | null, readonly displayName?: string | null, readonly tickerSymbol: string, readonly description?: string | null, readonly mmaId?: string | null, readonly isMmaAuthorized?: boolean | null } | null> };

export type DeleteAdvisorDocumentMutationVariables = Exact<{
  documentId: Scalars['ID'];
}>;


export type DeleteAdvisorDocumentMutation = { readonly __typename?: 'Mutation', readonly deleteAdvisorDocument?: string | null };

export type DeleteDocumentFromAccountMutationVariables = Exact<{
  documentId: Scalars['ID'];
  clientId: Scalars['ID'];
}>;


export type DeleteDocumentFromAccountMutation = { readonly __typename?: 'Mutation', readonly deleteDocumentFromAccount?: string | null };

export type GetClientDocumentAgreementsQueryVariables = Exact<{
  input: DocumentAgreementsWhereInput;
}>;


export type GetClientDocumentAgreementsQuery = { readonly __typename?: 'Query', readonly documentAgreements?: { readonly __typename?: 'DocumentAgreementsResponse', readonly data?: ReadonlyArray<{ readonly __typename?: 'DocumentAgreement', readonly id: string, readonly name?: string | null, readonly actionStatus?: DocumentAgreementSignersStatus | null, readonly createdDate?: any | null, readonly signatures?: ReadonlyArray<{ readonly __typename?: 'DocumentSignature', readonly id: string, readonly user?: { readonly __typename?: 'Advisor2', readonly avatarURL: string } | { readonly __typename?: 'Client', readonly avatarURL: string } | null }> | null }> | null, readonly page: { readonly __typename?: 'ESigPaginationResponse', readonly number?: number | null, readonly size?: number | null, readonly totalElements?: number | null, readonly totalPages?: number | null } } | null };

export type GetDocumentAgreementUrlQueryVariables = Exact<{
  input: DocumentAgreementUrlInput;
}>;


export type GetDocumentAgreementUrlQuery = { readonly __typename?: 'Query', readonly documentAgreementUrl?: { readonly __typename?: 'DocumentAgreementUrlResponse', readonly url?: string | null } | null };

export type GenerateSignUrlMutationVariables = Exact<{
  input: GenerateSignUrlInput;
}>;


export type GenerateSignUrlMutation = { readonly __typename?: 'Mutation', readonly generateSignUrl?: { readonly __typename?: 'SignUrlResponse', readonly signUrl?: string | null, readonly signatureRequestId?: string | null } | null };

export type HandleProcessingActionMutationVariables = Exact<{
  input: HandleProcessingActionInput;
}>;


export type HandleProcessingActionMutation = { readonly __typename?: 'Mutation', readonly handleProcessingAction?: { readonly __typename?: 'DocumentAgreementResponse', readonly document?: { readonly __typename?: 'DocumentAgreement', readonly id: string, readonly name?: string | null } | null } | null };

export type createAdoptingEmployerMutationVariables = Exact<{
  input: AdoptingEmployerInput;
}>;


export type createAdoptingEmployerMutation = { readonly __typename?: 'Mutation', readonly createAdoptingEmployer: { readonly __typename?: 'AdoptingEmployer', readonly id: string, readonly companyName?: string | null, readonly ssnTaxId?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null, readonly companyTaxId?: string | null } };

export type adoptingEmployerQueryVariables = Exact<{
  where: AdoptingEmployerWhere;
}>;


export type adoptingEmployerQuery = { readonly __typename?: 'Query', readonly adoptingEmployer: { readonly __typename?: 'AdoptingEmployer', readonly id: string, readonly companyName?: string | null, readonly ssnTaxId?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null, readonly companyTaxId?: string | null } };

export type updateAdoptingEmployerMutationVariables = Exact<{
  input: AdoptingEmployerInput;
}>;


export type updateAdoptingEmployerMutation = { readonly __typename?: 'Mutation', readonly updateAdoptingEmployer: { readonly __typename?: 'AdoptingEmployer', readonly id: string, readonly companyName?: string | null, readonly ssnTaxId?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null, readonly companyTaxId?: string | null } };

export type financialAccountFieldsFragment = { readonly __typename?: 'AccountDetails', readonly financialAccountId?: string | null, readonly accountNumber?: string | null, readonly mask?: string | null, readonly institutionName?: string | null, readonly balance?: number | null, readonly realTimeBalance?: number | null, readonly managedAccount?: boolean | null, readonly accountClassification?: string | null, readonly ownerFirstName?: string | null, readonly ownerLastName?: string | null, readonly accountNickname?: string | null, readonly accountType?: string | null, readonly status?: string | null, readonly financialAccountName: string };

export type userFinancialAccountFieldsFragment = { readonly __typename?: 'AccountDetails', readonly financialAccountId?: string | null, readonly accountNumber?: string | null, readonly mask?: string | null, readonly institutionName?: string | null, readonly balance?: number | null, readonly realTimeBalance?: number | null, readonly managedAccount?: boolean | null, readonly accountClassification?: string | null, readonly ownerFirstName?: string | null, readonly ownerLastName?: string | null, readonly financialAccountName: string, readonly accountNickname?: string | null, readonly accountType?: string | null, readonly status?: string | null, readonly accountCashSummary?: { readonly __typename?: 'AccountCashSummary', readonly availableForWithdrawal: number } | null };

export type FullAccountDetailsFragment = { readonly __typename?: 'AccountDetails', readonly financialAccountId?: string | null, readonly accountNumber?: string | null, readonly mask?: string | null, readonly institutionName?: string | null, readonly balance?: number | null, readonly realTimeBalance?: number | null, readonly managedAccount?: boolean | null, readonly accountClassification?: string | null, readonly ownerFirstName?: string | null, readonly ownerLastName?: string | null, readonly accountNickname?: string | null, readonly accountType?: string | null, readonly status?: string | null, readonly financialAccountName: string };

export type getAllFinancialAccountsInGroupQueryVariables = Exact<{
  groupIDs: ReadonlyArray<InputMaybe<AccountsInGroupInput>>;
  includeExternal?: InputMaybe<Scalars['Boolean']>;
}>;


export type getAllFinancialAccountsInGroupQuery = { readonly __typename?: 'Query', readonly getAllFinancialAccountsInGroup?: ReadonlyArray<{ readonly __typename?: 'AccountDetails', readonly financialAccountId?: string | null, readonly accountNumber?: string | null, readonly mask?: string | null, readonly institutionName?: string | null, readonly balance?: number | null, readonly realTimeBalance?: number | null, readonly managedAccount?: boolean | null, readonly accountClassification?: string | null, readonly ownerFirstName?: string | null, readonly ownerLastName?: string | null, readonly accountNickname?: string | null, readonly accountType?: string | null, readonly status?: string | null, readonly financialAccountName: string } | null> | null };

export type getAllFinancialAccountsQueryVariables = Exact<{
  institutionName?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
}>;


export type getAllFinancialAccountsQuery = { readonly __typename?: 'Query', readonly getAllFinancialAccounts?: ReadonlyArray<{ readonly __typename?: 'AccountDetails', readonly financialAccountId?: string | null, readonly accountNumber?: string | null, readonly mask?: string | null, readonly institutionName?: string | null, readonly balance?: number | null, readonly realTimeBalance?: number | null, readonly managedAccount?: boolean | null, readonly accountClassification?: string | null, readonly ownerFirstName?: string | null, readonly ownerLastName?: string | null, readonly financialAccountName: string, readonly accountNickname?: string | null, readonly accountType?: string | null, readonly status?: string | null, readonly accountCashSummary?: { readonly __typename?: 'AccountCashSummary', readonly availableForWithdrawal: number } | null } | null> | null };

export type getFinancialAccountTrustDetailsQueryVariables = Exact<{
  financialAccountId: Scalars['ID'];
}>;


export type getFinancialAccountTrustDetailsQuery = { readonly __typename?: 'Query', readonly getFinancialAccountTrustDetails: { readonly __typename?: 'TrustOutput', readonly name?: string | null, readonly financialAccountId?: string | null, readonly id?: string | null, readonly taxIdentificationNumber?: string | null, readonly taxIdType?: TrustTaxIdType | null, readonly dateEstablished?: string | null, readonly stateEstablished?: string | null, readonly address?: string | null, readonly city?: string | null, readonly state?: string | null, readonly zipCode?: string | null, readonly trustRevocability?: TrustRevocable | null, readonly supportedChecked?: boolean | null } };

export type financialAccountQueryVariables = Exact<{
  where: FinancialAccountWhereInput;
}>;


export type financialAccountQuery = { readonly __typename?: 'Query', readonly financialAccount: { readonly __typename?: 'FinancialAccount2', readonly id: string, readonly decedentName?: string | null, readonly planName?: string | null } };

export type financialAccountPropertiesQueryVariables = Exact<{
  where: FinancialAccountWhereInput;
}>;


export type financialAccountPropertiesQuery = { readonly __typename?: 'Query', readonly financialAccount: { readonly __typename?: 'FinancialAccount2', readonly id: string, readonly properties?: { readonly __typename?: 'FinancialAccountProperties', readonly hasOfflineBeneficiary?: boolean | null } | null } };

export type financialAccountSettingsQueryVariables = Exact<{
  where: FinancialAccountWhereInput;
}>;


export type financialAccountSettingsQuery = { readonly __typename?: 'Query', readonly financialAccount: { readonly __typename?: 'FinancialAccount2', readonly adoptingEmployer?: { readonly __typename?: 'AdoptingEmployerProfile', readonly name?: string | null, readonly phone?: string | null, readonly ssnTaxId?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null } | null, readonly planDetails?: { readonly __typename?: 'PlanDetails', readonly planName?: string | null, readonly planType?: string | null, readonly planTaxID?: string | null, readonly planEffectiveDate?: string | null, readonly planAdministrator?: string | null, readonly adoptingEmployerName?: string | null } | null } };

export type GetRetirementTrustDetailsQueryVariables = Exact<{
  where: FinancialAccountWhereInput;
}>;


export type GetRetirementTrustDetailsQuery = { readonly __typename?: 'Query', readonly financialAccount: { readonly __typename?: 'FinancialAccount2', readonly planDetails?: { readonly __typename?: 'PlanDetails', readonly planName?: string | null, readonly planType?: string | null, readonly planTaxID?: string | null, readonly planEffectiveDate?: string | null, readonly participantName?: string | null, readonly planAdministrator?: string | null, readonly adoptingEmployerName?: string | null, readonly ein?: string | null, readonly accountAssetOwnership?: string | null } | null } };

export type financialAccountOwnersQueryVariables = Exact<{
  where: FinancialAccountWhereInput;
}>;


export type financialAccountOwnersQuery = { readonly __typename?: 'Query', readonly financialAccount: { readonly __typename?: 'FinancialAccount2', readonly id: string, readonly owners: ReadonlyArray<{ readonly __typename?: 'FinancialAccountOwner', readonly id: string, readonly profileType: ProfileType }> } };

export type financialAccountInfoIdQueryVariables = Exact<{
  where: FinancialAccountWhereInput;
}>;


export type financialAccountInfoIdQuery = { readonly __typename?: 'Query', readonly financialAccount: { readonly __typename?: 'FinancialAccount2', readonly id: string, readonly accountInfoId?: string | null } };

export type GetClientPendingBrokerageAccountsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetClientPendingBrokerageAccountsQuery = { readonly __typename?: 'Query', readonly clientBrokerageAccounts: ReadonlyArray<{ readonly __typename?: 'AccountStatusOutput', readonly financialAccountId: string, readonly financialAccountName: string, readonly status: AccountCreationStatus, readonly accountType: string }> };

export type GetClientBrokerageAccountsQueryVariables = Exact<{
  status?: InputMaybe<ReadonlyArray<InputMaybe<AccountCreationStatus>>>;
}>;


export type GetClientBrokerageAccountsQuery = { readonly __typename?: 'Query', readonly clientBrokerageAccounts: ReadonlyArray<{ readonly __typename?: 'AccountStatusOutput', readonly financialAccountId: string, readonly financialAccountName: string, readonly status: AccountCreationStatus, readonly accountType: string }> };

export type DecedentQueryVariables = Exact<{
  where: DecedentWhere;
}>;


export type DecedentQuery = { readonly __typename?: 'Query', readonly decedent: { readonly __typename?: 'Decedent', readonly id?: string | null, readonly birthDate?: string | null, readonly deceasedDate?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly ssn?: string | null } };

export type CreateDecedentMutationVariables = Exact<{
  input: DecedentInput;
}>;


export type CreateDecedentMutation = { readonly __typename?: 'Mutation', readonly createDecedent: { readonly __typename?: 'Decedent', readonly id?: string | null, readonly firstName?: string | null, readonly lastName?: string | null, readonly birthDate?: string | null, readonly deceasedDate?: string | null, readonly ssn?: string | null } };

export type GetPlanDetailsQueryVariables = Exact<{
  where: PlanDetailsWhere;
}>;


export type GetPlanDetailsQuery = { readonly __typename?: 'Query', readonly planDetails: { readonly __typename?: 'PlanDetails', readonly id?: string | null, readonly planType?: string | null, readonly planName?: string | null, readonly effectiveDate?: string | null, readonly adoptingEmployerName?: string | null, readonly ein?: string | null, readonly accountAssetOwnership?: string | null } };

export type UpdatePlanDetailsMutationVariables = Exact<{
  input: PlanDetailsInput;
}>;


export type UpdatePlanDetailsMutation = { readonly __typename?: 'Mutation', readonly updatePlanDetails: { readonly __typename?: 'PlanDetails', readonly id?: string | null, readonly planType?: string | null, readonly planName?: string | null, readonly effectiveDate?: string | null, readonly adoptingEmployerName?: string | null, readonly ein?: string | null, readonly accountAssetOwnership?: string | null } };

export type CreatePlanDetailsMutationVariables = Exact<{
  input: PlanDetailsInput;
}>;


export type CreatePlanDetailsMutation = { readonly __typename?: 'Mutation', readonly createPlanDetails: { readonly __typename?: 'PlanDetails', readonly id?: string | null, readonly planType?: string | null, readonly planName?: string | null, readonly effectiveDate?: string | null, readonly adoptingEmployerName?: string | null, readonly ein?: string | null, readonly accountAssetOwnership?: string | null } };

export type GetSingleParticipantQueryVariables = Exact<{
  where: SingleParticipantWhere;
}>;


export type GetSingleParticipantQuery = { readonly __typename?: 'Query', readonly singleParticipant: { readonly __typename?: 'SingleParticipant', readonly id: string, readonly firstName?: string | null, readonly lastName?: string | null } };

export type UpdateSingleParticipantMutationVariables = Exact<{
  input: SingleParticipantInput;
}>;


export type UpdateSingleParticipantMutation = { readonly __typename?: 'Mutation', readonly updateSingleParticipant: { readonly __typename?: 'SingleParticipant', readonly id: string, readonly firstName?: string | null, readonly lastName?: string | null } };

export type CreateSingleParticipantMutationVariables = Exact<{
  input: SingleParticipantInput;
}>;


export type CreateSingleParticipantMutation = { readonly __typename?: 'Mutation', readonly createSingleParticipant: { readonly __typename?: 'SingleParticipant', readonly id: string, readonly firstName?: string | null, readonly lastName?: string | null } };

export type GetApyInfoQueryVariables = Exact<{ [key: string]: never; }>;


export type GetApyInfoQuery = { readonly __typename?: 'Query', readonly apyInfo: { readonly __typename?: 'ApyInfo', readonly apy: number, readonly asOf: any, readonly rate: number } };

export type SendCashAccountInterestEmailMutationVariables = Exact<{
  input: CashAccountInterestInput;
}>;


export type SendCashAccountInterestEmailMutation = { readonly __typename?: 'Mutation', readonly sendCashAccountInterestEmail: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } };

export type GetAdvisorsInClientAccountGroupQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAdvisorsInClientAccountGroupQuery = { readonly __typename?: 'Query', readonly getAdvisorsInClientAccountGroup?: ReadonlyArray<{ readonly __typename?: 'AdvisorContactDetails', readonly id: string, readonly firstName: string, readonly lastName: string, readonly email?: string | null, readonly userId?: string | null } | null> | null };

export type fundingAccountFieldsFragment = { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly institutionName: string, readonly isVirtual: boolean, readonly created: any, readonly nextDayUseOnly: boolean, readonly authenticationStatus: PlaidAuthenticationStatus, readonly verificationStatus?: ApexVerificationStatus | null };

export type fundingAccountAllFieldsFragment = { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly institutionName: string, readonly isVirtual: boolean, readonly created: any, readonly nextDayUseOnly: boolean, readonly authenticationStatus: PlaidAuthenticationStatus };

export type fundingAccountQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type fundingAccountQuery = { readonly __typename?: 'Query', readonly fundingAccount: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly institutionName: string, readonly isVirtual: boolean, readonly created: any, readonly nextDayUseOnly: boolean, readonly authenticationStatus: PlaidAuthenticationStatus, readonly verificationStatus?: ApexVerificationStatus | null } };

export type fundingAccountsByBrokerageQueryVariables = Exact<{
  brokerageAccountIds: ReadonlyArray<Scalars['ID']>;
}>;


export type fundingAccountsByBrokerageQuery = { readonly __typename?: 'Query', readonly fundingAccountsByBrokerage: ReadonlyArray<{ readonly __typename?: 'FundingAccountsByBrokerage', readonly brokerageAccountId: string, readonly maximumAccountsPerUser: number, readonly accounts: ReadonlyArray<{ readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly institutionName: string, readonly isVirtual: boolean, readonly created: any, readonly nextDayUseOnly: boolean, readonly authenticationStatus: PlaidAuthenticationStatus, readonly verificationStatus?: ApexVerificationStatus | null }> }> };

export type removeFundingAccountMutationVariables = Exact<{
  fundingAccountId: Scalars['ID'];
  brokerageAccountId: Scalars['ID'];
}>;


export type removeFundingAccountMutation = { readonly __typename?: 'Mutation', readonly removeFundingAccount: { readonly __typename?: 'RemoveFunding', readonly fundingAccountId: string } };

export type addFundingAccountMutationVariables = Exact<{
  financialAccountId: Scalars['ID'];
  publicToken: Scalars['String'];
  plaidMetaData: PlaidMetaData;
}>;


export type addFundingAccountMutation = { readonly __typename?: 'Mutation', readonly addFundingAccount: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly institutionName: string, readonly isVirtual: boolean, readonly created: any, readonly nextDayUseOnly: boolean, readonly authenticationStatus: PlaidAuthenticationStatus, readonly verificationStatus?: ApexVerificationStatus | null } };

export type addExistingFundingToBrokerageMutationVariables = Exact<{
  fundingAccountId: Scalars['ID'];
  brokerageAccountId: Scalars['ID'];
}>;


export type addExistingFundingToBrokerageMutation = { readonly __typename?: 'Mutation', readonly addExistingFundingToBrokerage: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly institutionName: string, readonly isVirtual: boolean, readonly created: any, readonly nextDayUseOnly: boolean, readonly authenticationStatus: PlaidAuthenticationStatus, readonly verificationStatus?: ApexVerificationStatus | null } };

export type fundingAccountsQueryVariables = Exact<{ [key: string]: never; }>;


export type fundingAccountsQuery = { readonly __typename?: 'Query', readonly fundingAccounts: ReadonlyArray<{ readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly institutionName: string, readonly isVirtual: boolean, readonly created: any, readonly nextDayUseOnly: boolean, readonly authenticationStatus: PlaidAuthenticationStatus }> };

export type getPlaidLinkTokenQueryVariables = Exact<{
  where?: InputMaybe<PlaidLinkTokenWhere>;
}>;


export type getPlaidLinkTokenQuery = { readonly __typename?: 'Query', readonly getPlaidLinkToken: { readonly __typename?: 'PlaidToken', readonly linkToken: string } };

export type getUserManualAuthTokenQueryVariables = Exact<{
  fundingAccountId: Scalars['ID'];
}>;


export type getUserManualAuthTokenQuery = { readonly __typename?: 'Query', readonly getUserManualAuthToken: { readonly __typename?: 'PlaidToken', readonly linkToken: string } };

export type approvePlaidMicrodepositMutationVariables = Exact<{
  plaidAccountId: Scalars['String'];
}>;


export type approvePlaidMicrodepositMutation = { readonly __typename?: 'Mutation', readonly approvePlaidMicrodeposit?: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } | null };

export type updateFundingAccountMutationVariables = Exact<{
  accountName?: InputMaybe<Scalars['String']>;
  accountNumber?: InputMaybe<Scalars['String']>;
  accountType?: InputMaybe<Scalars['String']>;
  authenticationStatus?: InputMaybe<PlaidAuthenticationStatus>;
  fundingAccountId: Scalars['ID'];
  institutionName?: InputMaybe<Scalars['String']>;
  userId: Scalars['ID'];
}>;


export type updateFundingAccountMutation = { readonly __typename?: 'Mutation', readonly updateFundingAccount: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly institutionName: string, readonly isVirtual: boolean, readonly created: any, readonly nextDayUseOnly: boolean, readonly authenticationStatus: PlaidAuthenticationStatus, readonly verificationStatus?: ApexVerificationStatus | null, readonly name?: string | null } };

export type getGreetingScreenGQLQueryVariables = Exact<{
  groupIDs: ReadonlyArray<InputMaybe<AccountsInGroupInput>>;
  includeExternal?: InputMaybe<Scalars['Boolean']>;
}>;


export type getGreetingScreenGQLQuery = { readonly __typename?: 'Query', readonly getAllFinancialAccountsInGroup?: ReadonlyArray<{ readonly __typename?: 'AccountDetails', readonly financialAccountId?: string | null, readonly accountNumber?: string | null, readonly mask?: string | null, readonly institutionName?: string | null, readonly balance?: number | null, readonly realTimeBalance?: number | null, readonly managedAccount?: boolean | null, readonly accountClassification?: string | null, readonly ownerFirstName?: string | null, readonly ownerLastName?: string | null, readonly accountNickname?: string | null, readonly accountType?: string | null, readonly status?: string | null, readonly financialAccountName: string } | null> | null, readonly getAllFinancialAccounts?: ReadonlyArray<{ readonly __typename?: 'AccountDetails', readonly financialAccountId?: string | null, readonly accountNumber?: string | null, readonly mask?: string | null, readonly institutionName?: string | null, readonly balance?: number | null, readonly realTimeBalance?: number | null, readonly managedAccount?: boolean | null, readonly accountClassification?: string | null, readonly ownerFirstName?: string | null, readonly ownerLastName?: string | null, readonly financialAccountName: string, readonly accountNickname?: string | null, readonly accountType?: string | null, readonly status?: string | null, readonly accountCashSummary?: { readonly __typename?: 'AccountCashSummary', readonly availableForWithdrawal: number } | null } | null> | null, readonly altruistClearingTermsOfUseStatus: { readonly __typename?: 'TermsAcceptance', readonly isAccepted?: boolean | null }, readonly signedInUser: { readonly __typename?: 'Advisor2', readonly id: string, readonly organization: { readonly __typename?: 'Organization', readonly id: string, readonly name: string, readonly address: { readonly __typename?: 'AddressOutput', readonly addr1?: string | null, readonly addr2?: string | null, readonly addr3?: string | null } } } | { readonly __typename?: 'Client', readonly id: string, readonly organization: { readonly __typename?: 'Organization', readonly id: string, readonly name: string, readonly address: { readonly __typename?: 'AddressOutput', readonly addr1?: string | null, readonly addr2?: string | null, readonly addr3?: string | null } } }, readonly apyInfo: { readonly __typename?: 'ApyInfo', readonly apy: number, readonly asOf: any, readonly rate: number } };

export type GetOrganizationLogoQueryVariables = Exact<{ [key: string]: never; }>;


export type GetOrganizationLogoQuery = { readonly __typename?: 'Query', readonly organization: { readonly __typename?: 'Organization', readonly logo?: { readonly __typename?: 'OrganizationLogo', readonly id: string, readonly imageHandlerHost: string, readonly s3Bucket: string, readonly s3Key: string } | null } };

export type HeldAwayLinksQueryVariables = Exact<{ [key: string]: never; }>;


export type HeldAwayLinksQuery = { readonly __typename?: 'Query', readonly heldAwayLinks?: { readonly __typename?: 'HeldAwayLinksResponse', readonly links?: ReadonlyArray<{ readonly __typename?: 'HeldAwayLink', readonly id: string, readonly institutionName?: string | null, readonly accounts?: ReadonlyArray<{ readonly __typename?: 'HeldAwayAccount', readonly id: string, readonly name?: string | null, readonly number?: string | null, readonly balance?: number | null, readonly status?: HeldAwayAccountStatus | null, readonly updatedDate?: any | null, readonly type?: string | null }> | null }> | null } | null };

export type GetHeldAwayPlaidTokenQueryVariables = Exact<{ [key: string]: never; }>;


export type GetHeldAwayPlaidTokenQuery = { readonly __typename?: 'Query', readonly heldAwayPlaidToken?: { readonly __typename?: 'HeldAwayPlaidTokenResponse', readonly plaidToken: string } | null };

export type GetHeldAwayPlaidRelinkTokenQueryVariables = Exact<{
  input: HeldAwayPlaidRelinkTokenInput;
}>;


export type GetHeldAwayPlaidRelinkTokenQuery = { readonly __typename?: 'Query', readonly heldAwayPlaidRelinkToken?: { readonly __typename?: 'HeldAwayPlaidTokenResponse', readonly plaidToken: string } | null };

export type getInvoiceQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type getInvoiceQuery = { readonly __typename?: 'Query', readonly getInvoice: { readonly __typename?: 'Invoice', readonly id: string, readonly invoiceNumber: string, readonly total: number, readonly groupId: string, readonly billingEntityType: string, readonly billingInformation: { readonly __typename?: 'BillingInfo', readonly frequency: AllowedFrequency, readonly billingDate: string, readonly startDate: string, readonly endDate: string, readonly aumDate: string, readonly flatFee: number, readonly totalAum: number }, readonly accountDetails: ReadonlyArray<{ readonly __typename?: 'FinAccountDetails', readonly financialAccountId: string, readonly scheduleType: AllowedType, readonly aum?: number | null, readonly debit: number, readonly periodFlatFee: number, readonly accountType?: string | null, readonly accountNumber?: string | null, readonly ownerFirstName?: string | null, readonly ownerLastName?: string | null, readonly aumRate?: number | null, readonly aumDaysCount: number, readonly aumDailyFee?: number | null, readonly aumFee?: number | null, readonly financialAccountName: string, readonly adjustments: ReadonlyArray<{ readonly __typename?: 'Adjustment', readonly flow: string, readonly flowAmount: number, readonly flowDailyFee: number, readonly flowDaysCount: number, readonly flowFee: number } | null> }> }, readonly getCompanyInfo: { readonly __typename?: 'CompanyOutput', readonly name?: string | null } };

export type getExecutedInvoicesQueryVariables = Exact<{ [key: string]: never; }>;


export type getExecutedInvoicesQuery = { readonly __typename?: 'Query', readonly getExecutedInvoices: { readonly __typename?: 'ExecutedInvoicesOutput', readonly invoices: ReadonlyArray<{ readonly __typename?: 'GroupInvoice', readonly id: string, readonly invoiceNumber: string, readonly scheduleType: ScheduleType, readonly invoiceAmount: number, readonly periodId: string, readonly startDate: string, readonly endDate: string, readonly invoiceDate?: string | null, readonly groupName?: string | null } | null> } };

export type getInvoiceCompanyInfoQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type getInvoiceCompanyInfoQuery = { readonly __typename?: 'Query', readonly getInvoice: { readonly __typename?: 'Invoice', readonly id: string }, readonly getCompanyInfo: { readonly __typename?: 'CompanyOutput', readonly name?: string | null } };

export type mfaSettingsQueryVariables = Exact<{ [key: string]: never; }>;


export type mfaSettingsQuery = { readonly __typename?: 'Query', readonly mfaSettings: { readonly __typename?: 'MfaSettings', readonly id?: string | null, readonly active: boolean, readonly defaultMfaMethod?: MfaMethod | null, readonly backupCodes?: ReadonlyArray<string> | null, readonly phoneNumbers?: ReadonlyArray<{ readonly __typename?: 'PhoneNumber', readonly id?: string | null, readonly number?: string | null, readonly phoneNumberType?: PhoneNumberType | null, readonly isDefaultForMfa?: boolean | null, readonly isVerified?: boolean | null }> | null } };

export type createMfaSettingsMutationVariables = Exact<{
  input: UpdateAndCreateMfaSettingsInput;
}>;


export type createMfaSettingsMutation = { readonly __typename?: 'Mutation', readonly createMfaSettings: { readonly __typename?: 'MfaSettings', readonly id?: string | null, readonly active: boolean, readonly defaultMfaMethod?: MfaMethod | null, readonly backupCodes?: ReadonlyArray<string> | null, readonly phoneNumbers?: ReadonlyArray<{ readonly __typename?: 'PhoneNumber', readonly id?: string | null, readonly number?: string | null, readonly phoneNumberType?: PhoneNumberType | null, readonly isDefaultForMfa?: boolean | null }> | null } };

export type updateMfaSettingsMutationVariables = Exact<{
  input: UpdateAndCreateMfaSettingsInput;
}>;


export type updateMfaSettingsMutation = { readonly __typename?: 'Mutation', readonly updateMfaSettings: { readonly __typename?: 'MfaSettings', readonly id?: string | null, readonly active: boolean, readonly defaultMfaMethod?: MfaMethod | null, readonly backupCodes?: ReadonlyArray<string> | null, readonly phoneNumbers?: ReadonlyArray<{ readonly __typename?: 'PhoneNumber', readonly id?: string | null, readonly number?: string | null, readonly phoneNumberType?: PhoneNumberType | null, readonly isDefaultForMfa?: boolean | null }> | null } };

export type mfaTokenRequestWithCredentialsMutationVariables = Exact<{
  mfaTokenRequest: MfaTokenRequest;
}>;


export type mfaTokenRequestWithCredentialsMutation = { readonly __typename?: 'Mutation', readonly mfaTokenRequestWithCredentials: { readonly __typename?: 'MfaTokenResponse', readonly id: string, readonly validated?: boolean | null, readonly overrideId?: string | null, readonly mfaMethod?: MfaMethod | null } };

export type validateMfaTokenMutationVariables = Exact<{
  validateMfaTokenRequest: ValidateMfaTokenRequest;
}>;


export type validateMfaTokenMutation = { readonly __typename?: 'Mutation', readonly validateMfaToken: { readonly __typename?: 'ValidateMfaTokenResponse', readonly id: string, readonly validated: boolean, readonly mfaMethod?: MfaMethod | null } };

export type googleAuthenticatorQRCodeQueryVariables = Exact<{ [key: string]: never; }>;


export type googleAuthenticatorQRCodeQuery = { readonly __typename?: 'Query', readonly googleAuthenticatorQRCode: { readonly __typename?: 'QRCodeData', readonly secretKey: string } };

export type trustedDeviceQueryVariables = Exact<{
  fingerprintHash: Scalars['String'];
}>;


export type trustedDeviceQuery = { readonly __typename?: 'Query', readonly trustedDevice?: { readonly __typename?: 'TrustedDevice', readonly trustedUntilDate?: any | null, readonly id: string } | null };

export type trustedDevicesQueryVariables = Exact<{ [key: string]: never; }>;


export type trustedDevicesQuery = { readonly __typename?: 'Query', readonly trustedDevices?: ReadonlyArray<{ readonly __typename?: 'TrustedDevice', readonly id: string, readonly ip: string, readonly userId: string, readonly userAgent: string, readonly browser: string, readonly fingerprintHash: string, readonly deviceType: DeviceType, readonly dateLastUsed?: any | null, readonly trustedUntilDate?: any | null, readonly created: any, readonly updated: any, readonly location: string, readonly name?: string | null } | null> | null };

export type deleteTrustedDeviceMutationVariables = Exact<{
  fingerprintHash: Scalars['String'];
}>;


export type deleteTrustedDeviceMutation = { readonly __typename?: 'Mutation', readonly deleteTrustedDevice: boolean };

export type MfaSettingsWithCredentialsQueryVariables = Exact<{ [key: string]: never; }>;


export type MfaSettingsWithCredentialsQuery = { readonly __typename?: 'Query', readonly mfaSettingsWithCredentials: { readonly __typename?: 'MfaSettings', readonly id?: string | null, readonly active: boolean, readonly defaultMfaMethod?: MfaMethod | null, readonly backupCodes?: ReadonlyArray<string> | null, readonly phoneNumbers?: ReadonlyArray<{ readonly __typename?: 'PhoneNumber', readonly id?: string | null, readonly number?: string | null, readonly phoneNumberType?: PhoneNumberType | null, readonly isDefaultForMfa?: boolean | null, readonly isVerified?: boolean | null }> | null } };

export type CreateMfaSettingsWithCredentialsMutationVariables = Exact<{
  input: UpdateAndCreateMfaSettingsInput;
}>;


export type CreateMfaSettingsWithCredentialsMutation = { readonly __typename?: 'Mutation', readonly createMfaSettingsWithCredentials: { readonly __typename?: 'MfaSettings', readonly id?: string | null, readonly active: boolean, readonly defaultMfaMethod?: MfaMethod | null, readonly backupCodes?: ReadonlyArray<string> | null, readonly phoneNumbers?: ReadonlyArray<{ readonly __typename?: 'PhoneNumber', readonly id?: string | null, readonly number?: string | null, readonly phoneNumberType?: PhoneNumberType | null, readonly isDefaultForMfa?: boolean | null, readonly isVerified?: boolean | null }> | null } };

export type UpdateMfaSettingsWithCredentialsMutationVariables = Exact<{
  input: UpdateAndCreateMfaSettingsInput;
}>;


export type UpdateMfaSettingsWithCredentialsMutation = { readonly __typename?: 'Mutation', readonly updateMfaSettingsWithCredentials: { readonly __typename?: 'MfaSettings', readonly id?: string | null, readonly active: boolean, readonly defaultMfaMethod?: MfaMethod | null, readonly backupCodes?: ReadonlyArray<string> | null, readonly phoneNumbers?: ReadonlyArray<{ readonly __typename?: 'PhoneNumber', readonly id?: string | null, readonly number?: string | null, readonly phoneNumberType?: PhoneNumberType | null, readonly isDefaultForMfa?: boolean | null, readonly isVerified?: boolean | null }> | null } };

export type ValidateMfaTokenWithCredentialsMutationVariables = Exact<{
  validateMfaTokenRequest: ValidateMfaTokenRequest;
}>;


export type ValidateMfaTokenWithCredentialsMutation = { readonly __typename?: 'Mutation', readonly validateMfaTokenWithCredentials: { readonly __typename?: 'ValidateMfaTokenResponse', readonly id: string, readonly validated: boolean, readonly mfaMethod?: MfaMethod | null } };

export type GoogleAuthenticatorQRCodeWithCredentialsQueryVariables = Exact<{ [key: string]: never; }>;


export type GoogleAuthenticatorQRCodeWithCredentialsQuery = { readonly __typename?: 'Query', readonly googleAuthenticatorQRCodeWithCredentials: { readonly __typename?: 'QRCodeData', readonly secretKey: string } };

export type TrustDeviceMutationVariables = Exact<{
  input: SafeTrustDeviceInput;
}>;


export type TrustDeviceMutation = { readonly __typename?: 'Mutation', readonly trustDevice: { readonly __typename?: 'TrustedDevice', readonly id: string } };

export type TrustDeviceWithCredentialsMutationVariables = Exact<{
  input: SafeTrustDeviceInput;
}>;


export type TrustDeviceWithCredentialsMutation = { readonly __typename?: 'Mutation', readonly trustDeviceWithCredentials: { readonly __typename?: 'TrustedDevice', readonly id: string } };

export type minorQueryVariables = Exact<{
  where: MinorWhere;
}>;


export type minorQuery = { readonly __typename?: 'Query', readonly minor: { readonly __typename?: 'Minor', readonly address?: string | null, readonly id: string, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly phone?: string | null, readonly dateOfBirth?: string | null, readonly ssnTaxId?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null, readonly backupWithholding?: boolean | null } };

export type financialAccountMinorQueryVariables = Exact<{
  where: FinancialAccountWhereInput;
}>;


export type financialAccountMinorQuery = { readonly __typename?: 'Query', readonly financialAccount: { readonly __typename?: 'FinancialAccount2', readonly financialAccountId: string, readonly id: string, readonly properties?: { readonly __typename?: 'FinancialAccountProperties', readonly hasMinorContactInfoConfirmed?: boolean | null } | null, readonly ageOfMajority?: { readonly __typename?: 'AgeOfMajority', readonly dateOfMajority?: any | null, readonly daysToMajority?: number | null } | null } };

export type updateMinorMutationVariables = Exact<{
  input: MinorInput;
}>;


export type updateMinorMutation = { readonly __typename?: 'Mutation', readonly updateMinor: { readonly __typename?: 'Minor', readonly id: string } };

export type allAgesOfMajorityQueryVariables = Exact<{ [key: string]: never; }>;


export type allAgesOfMajorityQuery = { readonly __typename?: 'Query', readonly allAgesOfMajority: ReadonlyArray<{ readonly __typename?: 'AgeOfMajorityForState', readonly majorityAge: string, readonly abbreviatedState: string, readonly accountCategory: string }> };

export type updateFinancialAccountPropertiesMutationVariables = Exact<{
  input: UpdateAccountPropertiesInput;
}>;


export type updateFinancialAccountPropertiesMutation = { readonly __typename?: 'Mutation', readonly updateFinancialAccountProperties: { readonly __typename?: 'FinancialAccount2', readonly id: string } };

export type createMinorMutationVariables = Exact<{
  input: MinorInput;
}>;


export type createMinorMutation = { readonly __typename?: 'Mutation', readonly createMinor: { readonly __typename?: 'Minor', readonly id: string } };

export type mmaFieldsFragment = { readonly __typename?: 'MMA', readonly id: string, readonly brokerageAccountId: string, readonly status: MMAStatus, readonly authTypes: ReadonlyArray<MMAAuthTypes>, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string, readonly accountType?: string | null }, readonly achs: ReadonlyArray<{ readonly __typename?: 'MMAACH', readonly fundingAccountId: string, readonly direction: ReadonlyArray<TransactionDirection>, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly wires: ReadonlyArray<{ readonly __typename?: 'MMAWire', readonly fundingAccountId: string, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly internals: ReadonlyArray<{ readonly __typename?: 'MMAInternal', readonly brokerageAccountId: string, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string } }>, readonly users: ReadonlyArray<{ readonly __typename?: 'MMAUserApproval', readonly userId: string, readonly approvalStatus: MMAUserApprovalStatus }> };

export type mmaQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type mmaQuery = { readonly __typename?: 'Query', readonly mma: { readonly __typename?: 'MMA', readonly id: string, readonly brokerageAccountId: string, readonly status: MMAStatus, readonly authTypes: ReadonlyArray<MMAAuthTypes>, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string, readonly accountType?: string | null }, readonly achs: ReadonlyArray<{ readonly __typename?: 'MMAACH', readonly fundingAccountId: string, readonly direction: ReadonlyArray<TransactionDirection>, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly wires: ReadonlyArray<{ readonly __typename?: 'MMAWire', readonly fundingAccountId: string, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly internals: ReadonlyArray<{ readonly __typename?: 'MMAInternal', readonly brokerageAccountId: string, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string } }>, readonly users: ReadonlyArray<{ readonly __typename?: 'MMAUserApproval', readonly userId: string, readonly approvalStatus: MMAUserApprovalStatus }> } };

export type mmaByUserQueryVariables = Exact<{ [key: string]: never; }>;


export type mmaByUserQuery = { readonly __typename?: 'Query', readonly mmaByUser: ReadonlyArray<{ readonly __typename?: 'MMA', readonly id: string, readonly brokerageAccountId: string, readonly status: MMAStatus, readonly authTypes: ReadonlyArray<MMAAuthTypes>, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string }, readonly achs: ReadonlyArray<{ readonly __typename?: 'MMAACH', readonly fundingAccountId: string, readonly direction: ReadonlyArray<TransactionDirection>, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly wires: ReadonlyArray<{ readonly __typename?: 'MMAWire', readonly fundingAccountId: string }>, readonly internals: ReadonlyArray<{ readonly __typename?: 'MMAInternal', readonly brokerageAccountId: string, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string } }>, readonly users: ReadonlyArray<{ readonly __typename?: 'MMAUserApproval', readonly userId: string, readonly approvalStatus: MMAUserApprovalStatus }> }> };

export type mmaByBrokerageQueryVariables = Exact<{
  brokerageAccountIds: ReadonlyArray<Scalars['ID']>;
}>;


export type mmaByBrokerageQuery = { readonly __typename?: 'Query', readonly mmaByBrokerage: ReadonlyArray<{ readonly __typename?: 'MMA', readonly id: string, readonly brokerageAccountId: string, readonly status: MMAStatus, readonly authTypes: ReadonlyArray<MMAAuthTypes>, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string, readonly accountType?: string | null }, readonly achs: ReadonlyArray<{ readonly __typename?: 'MMAACH', readonly fundingAccountId: string, readonly direction: ReadonlyArray<TransactionDirection>, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly wires: ReadonlyArray<{ readonly __typename?: 'MMAWire', readonly fundingAccountId: string, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly internals: ReadonlyArray<{ readonly __typename?: 'MMAInternal', readonly brokerageAccountId: string, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string } }>, readonly users: ReadonlyArray<{ readonly __typename?: 'MMAUserApproval', readonly userId: string, readonly approvalStatus: MMAUserApprovalStatus }> }> };

export type approveMMAMutationVariables = Exact<{
  mmaID: Scalars['ID'];
}>;


export type approveMMAMutation = { readonly __typename?: 'Mutation', readonly approveMMA: { readonly __typename?: 'MMA', readonly id: string, readonly brokerageAccountId: string, readonly status: MMAStatus, readonly authTypes: ReadonlyArray<MMAAuthTypes>, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string, readonly accountType?: string | null }, readonly achs: ReadonlyArray<{ readonly __typename?: 'MMAACH', readonly fundingAccountId: string, readonly direction: ReadonlyArray<TransactionDirection>, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly wires: ReadonlyArray<{ readonly __typename?: 'MMAWire', readonly fundingAccountId: string, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly internals: ReadonlyArray<{ readonly __typename?: 'MMAInternal', readonly brokerageAccountId: string, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string } }>, readonly users: ReadonlyArray<{ readonly __typename?: 'MMAUserApproval', readonly userId: string, readonly approvalStatus: MMAUserApprovalStatus }> } };

export type declineMMAMutationVariables = Exact<{
  mmaID: Scalars['ID'];
}>;


export type declineMMAMutation = { readonly __typename?: 'Mutation', readonly declineMMA: { readonly __typename?: 'MMA', readonly id: string, readonly brokerageAccountId: string, readonly status: MMAStatus, readonly authTypes: ReadonlyArray<MMAAuthTypes>, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string, readonly accountType?: string | null }, readonly achs: ReadonlyArray<{ readonly __typename?: 'MMAACH', readonly fundingAccountId: string, readonly direction: ReadonlyArray<TransactionDirection>, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly wires: ReadonlyArray<{ readonly __typename?: 'MMAWire', readonly fundingAccountId: string, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly internals: ReadonlyArray<{ readonly __typename?: 'MMAInternal', readonly brokerageAccountId: string, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string } }>, readonly users: ReadonlyArray<{ readonly __typename?: 'MMAUserApproval', readonly userId: string, readonly approvalStatus: MMAUserApprovalStatus }> } };

export type revokeMMAMutationVariables = Exact<{
  mmaID: Scalars['ID'];
}>;


export type revokeMMAMutation = { readonly __typename?: 'Mutation', readonly revokeMMA: { readonly __typename?: 'MMA', readonly id: string, readonly brokerageAccountId: string, readonly status: MMAStatus, readonly authTypes: ReadonlyArray<MMAAuthTypes>, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string, readonly accountType?: string | null }, readonly achs: ReadonlyArray<{ readonly __typename?: 'MMAACH', readonly fundingAccountId: string, readonly direction: ReadonlyArray<TransactionDirection>, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly wires: ReadonlyArray<{ readonly __typename?: 'MMAWire', readonly fundingAccountId: string, readonly fundingAccountDetails: { readonly __typename?: 'FundingAccount', readonly fundingAccountId: string, readonly institutionName: string, readonly accountNumber: string, readonly accountType: FundingAccountType, readonly isVirtual: boolean } }>, readonly internals: ReadonlyArray<{ readonly __typename?: 'MMAInternal', readonly brokerageAccountId: string, readonly brokerageAccountDetails: { readonly __typename?: 'FinancialAccount', readonly financialAccountName: string } }>, readonly users: ReadonlyArray<{ readonly __typename?: 'MMAUserApproval', readonly userId: string, readonly approvalStatus: MMAUserApprovalStatus }> } };

export type BulkApproveMMAMutationVariables = Exact<{
  mmaUUIDs: ReadonlyArray<Scalars['ID']>;
}>;


export type BulkApproveMMAMutation = { readonly __typename?: 'Mutation', readonly bulkApproveMMA: { readonly __typename?: 'SuccessResponse', readonly success: boolean } };

export type BulkDeclineMMAMutationVariables = Exact<{
  mmaUUIDs: ReadonlyArray<Scalars['ID']>;
}>;


export type BulkDeclineMMAMutation = { readonly __typename?: 'Mutation', readonly bulkDeclineMMA: { readonly __typename?: 'SuccessResponse', readonly success: boolean } };

export type corpMembersQueryVariables = Exact<{
  where: CorpMembersWhere;
}>;


export type corpMembersQuery = { readonly __typename?: 'Query', readonly corpMembers: ReadonlyArray<{ readonly __typename?: 'CorpMember', readonly id: string, readonly roles: ReadonlyArray<CorpMemberRole>, readonly userId: string, readonly ownershipPercentage?: number | null, readonly profile?: { readonly __typename?: 'CorpMemberProfile', readonly id: string, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly dateOfBirth?: string | null, readonly ssnTaxId?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null, readonly employmentPosition?: string | null } | null }> };

export type corpMemberQueryVariables = Exact<{
  where: CorpMemberWhere;
}>;


export type corpMemberQuery = { readonly __typename?: 'Query', readonly corpMember: { readonly __typename?: 'CorpMember', readonly id: string, readonly userId: string, readonly roles: ReadonlyArray<CorpMemberRole>, readonly ownershipPercentage?: number | null, readonly profile?: { readonly __typename?: 'CorpMemberProfile', readonly id: string, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly dateOfBirth?: string | null, readonly ssnTaxId?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null, readonly employmentPosition?: string | null } | null } };

export type createCorpMemberMutationVariables = Exact<{
  input: CreateCorpMember;
}>;


export type createCorpMemberMutation = { readonly __typename?: 'Mutation', readonly createCorpMember: { readonly __typename?: 'CorpMember', readonly id: string, readonly userId: string, readonly roles: ReadonlyArray<CorpMemberRole>, readonly ownershipPercentage?: number | null, readonly profile?: { readonly __typename?: 'CorpMemberProfile', readonly id: string, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly dateOfBirth?: string | null, readonly ssnTaxId?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null, readonly employmentPosition?: string | null } | null } };

export type updateCorpMemberMutationVariables = Exact<{
  input: UpdateCorpMember;
}>;


export type updateCorpMemberMutation = { readonly __typename?: 'Mutation', readonly updateCorpMember: { readonly __typename?: 'CorpMember', readonly id: string, readonly userId: string, readonly roles: ReadonlyArray<CorpMemberRole>, readonly ownershipPercentage?: number | null, readonly profile?: { readonly __typename?: 'CorpMemberProfile', readonly id: string, readonly firstName?: string | null, readonly lastName?: string | null, readonly email?: string | null, readonly dateOfBirth?: string | null, readonly ssnTaxId?: string | null, readonly address?: string | null, readonly city?: string | null, readonly zipCode?: string | null, readonly state?: string | null, readonly employmentPosition?: string | null } | null } };

export type deleteCorpMemberMutationVariables = Exact<{
  input: DeleteCorpMember;
}>;


export type deleteCorpMemberMutation = { readonly __typename?: 'Mutation', readonly deleteCorpMember: string };

export type GetChartPerformanceQueryVariables = Exact<{
  financialIds: ReadonlyArray<Scalars['ID']>;
  from: Scalars['String'];
  to?: InputMaybe<Scalars['String']>;
}>;


export type GetChartPerformanceQuery = { readonly __typename?: 'Query', readonly getPerformance: { readonly __typename?: 'Performance', readonly chart: { readonly __typename?: 'Chart', readonly dataPoints: ReadonlyArray<{ readonly __typename?: 'DataPoints', readonly date: string, readonly balance: number, readonly netDeposit: number, readonly accumulatedDeposit: number } | null> } } };

export type GetPerformanceSummaryQueryVariables = Exact<{
  financialIds: ReadonlyArray<Scalars['ID']>;
  from: Scalars['String'];
  to?: InputMaybe<Scalars['String']>;
  realTime?: InputMaybe<Scalars['Boolean']>;
}>;


export type GetPerformanceSummaryQuery = { readonly __typename?: 'Query', readonly getPerformance: { readonly __typename?: 'Performance', readonly summary: { readonly __typename?: 'Summary', readonly balance: number, readonly netDeposits: number, readonly earnings: number, readonly timeWeightedReturn?: number | null, readonly depositWithdrawal: number, readonly transferInOut: number, readonly startBalance: number, readonly deposit: number, readonly withdrawal: number, readonly transferIn: number, readonly transferOut: number, readonly dividend: number, readonly interest: number, readonly gainLoss: number, readonly startDate?: string | null, readonly withhold: number, readonly fee: number } } };

export type getAllocationsQueryVariables = Exact<{
  financialIds: ReadonlyArray<Scalars['ID']>;
  includeExternal?: InputMaybe<Scalars['Boolean']>;
}>;


export type getAllocationsQuery = { readonly __typename?: 'Query', readonly getAllocations: { readonly __typename?: 'Allocations', readonly totalBalance?: number | null, readonly allocations: ReadonlyArray<{ readonly __typename?: 'Allocation', readonly name: AllowedAllocation, readonly value: number }> } };

export type acceptUserTermsMutationVariables = Exact<{ [key: string]: never; }>;


export type acceptUserTermsMutation = { readonly __typename?: 'Mutation', readonly acceptUserTerms: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } };

export type RelinkHeldAwayMutationVariables = Exact<{
  input: RelinkHeldAwayInput;
}>;


export type RelinkHeldAwayMutation = { readonly __typename?: 'Mutation', readonly relinkHeldAway?: { readonly __typename?: 'HeldAwayMutationResponse', readonly link?: { readonly __typename?: 'HeldAwayLink', readonly id: string, readonly accounts?: ReadonlyArray<{ readonly __typename?: 'HeldAwayAccount', readonly id: string, readonly status?: HeldAwayAccountStatus | null, readonly balance?: number | null, readonly updatedDate?: any | null }> | null } | null } | null };

export type signedInUserQueryVariables = Exact<{ [key: string]: never; }>;


export type signedInUserQuery = { readonly __typename?: 'Query', readonly signedInUser: { readonly __typename?: 'Advisor2', readonly id: string, readonly organization: { readonly __typename?: 'Organization', readonly id: string, readonly name: string, readonly address: { readonly __typename?: 'AddressOutput', readonly addr1?: string | null, readonly addr2?: string | null, readonly addr3?: string | null } } } | { readonly __typename?: 'Client', readonly id: string, readonly organization: { readonly __typename?: 'Organization', readonly id: string, readonly name: string, readonly address: { readonly __typename?: 'AddressOutput', readonly addr1?: string | null, readonly addr2?: string | null, readonly addr3?: string | null } } } };

export type isAccountCreatedUnder24HoursQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type isAccountCreatedUnder24HoursQuery = { readonly __typename?: 'Query', readonly isAccountCreatedUnder24Hours: boolean };

export type getClientDocumentUrlQueryVariables = Exact<{
  documentId: Scalars['ID'];
}>;


export type getClientDocumentUrlQuery = { readonly __typename?: 'Query', readonly getClientDocumentUrl: { readonly __typename?: 'DocumentUrl', readonly url: string } };

export type getPersonalInfoQueryVariables = Exact<{
  userID?: InputMaybe<Scalars['ID']>;
}>;


export type getPersonalInfoQuery = { readonly __typename?: 'Query', readonly getUserProfileInfo: { readonly __typename?: 'UserInfoOutputQuery', readonly id: string, readonly firstName?: string | null, readonly middleName?: string | null, readonly lastName?: string | null, readonly suffix?: string | null, readonly phone?: string | null, readonly email?: string | null }, readonly getLoginNames: ReadonlyArray<{ readonly __typename?: 'LoginNamesOutput', readonly id?: string | null, readonly username?: string | null } | null>, readonly getPendingUsername: { readonly __typename?: 'PendingUsername', readonly code: string, readonly username: string, readonly status: string } };

export type getClientInfoQueryVariables = Exact<{ [key: string]: never; }>;


export type getClientInfoQuery = { readonly __typename?: 'Query', readonly getUserProfileInfo: { readonly __typename?: 'UserInfoOutputQuery', readonly id: string, readonly firstName?: string | null, readonly middleName?: string | null, readonly lastName?: string | null, readonly suffix?: string | null, readonly phone?: string | null, readonly email?: string | null } };

export type getPendingUsernameQueryVariables = Exact<{
  userId: Scalars['ID'];
}>;


export type getPendingUsernameQuery = { readonly __typename?: 'Query', readonly getPendingUsername: { readonly __typename?: 'PendingUsername', readonly code: string, readonly username: string, readonly status: string } };

export type createPendingUsernameMutationVariables = Exact<{
  username: Scalars['String'];
  userId: Scalars['ID'];
}>;


export type createPendingUsernameMutation = { readonly __typename?: 'Mutation', readonly createPendingUsername: { readonly __typename?: 'PendingUsername', readonly code: string, readonly username: string, readonly status: string } };

export type deletePendingUsernameMutationVariables = Exact<{
  userId: Scalars['ID'];
}>;


export type deletePendingUsernameMutation = { readonly __typename?: 'Mutation', readonly deletePendingUsername: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } };

export type updateAccountProfileMutationVariables = Exact<{
  userId: Scalars['ID'];
  profile: UserInfoInput;
  skipDW?: InputMaybe<Scalars['Boolean']>;
}>;


export type updateAccountProfileMutation = { readonly __typename?: 'Mutation', readonly updateAccountProfile?: { readonly __typename?: 'SuccessOutput', readonly success?: boolean | null } | null };
