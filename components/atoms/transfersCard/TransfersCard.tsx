import moment from 'moment';
import React from 'react';
import { Dimensions, Text, TouchableOpacity, View } from 'react-native';

import { styles } from '~/components/atoms/transfersCard/TransfersCardStyles';
import { currency } from '~/utils/formatter';

type Props = {
  item: {
    type?: string;
    amount?: number;
    financialAccountName?: string;
    date?: string;
  };
  index?: number;
  transfersCount?: number;
  onPressCard: (item: Props['item']) => void;
};

const numberOfLines = Dimensions.get('screen').height < 569 ? 1 : 5;

const TransfersCard = ({ item, index, transfersCount, onPressCard }: Props) => {
  return (
    <TouchableOpacity
      style={styles.cardContainer}
      activeOpacity={0.8}
      onPress={() => onPressCard(item)}
    >
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Pending {item?.type}</Text>
      </View>

      <Text style={styles.balanceValue}>{currency(item?.amount ?? 0)}</Text>

      <View style={styles.descriptionContainer}>
        <Text style={styles.descriptionText} numberOfLines={numberOfLines}>
          {item?.financialAccountName}
        </Text>
      </View>

      <View style={styles.footerContainer}>
        <Text style={styles.additionalText}>{moment(item?.date).format('L')}</Text>

        {index && (
          <Text style={styles.additionalText}>
            {index}/{transfersCount}
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

export default TransfersCard;
