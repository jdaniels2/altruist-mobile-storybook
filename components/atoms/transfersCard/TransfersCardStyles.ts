import { Dimensions, StyleSheet } from 'react-native';

import { theme } from '~/theme/Theme';
import { font, responsivePixelValue } from '~/theme/utilities';

const cardWidth = Dimensions.get('screen').width - 60;

export const styles = StyleSheet.create({
  cardContainer: {
    width: '100%',
    flex: 1,
    borderRadius: 12,
    overflow: 'hidden',
    backgroundColor: theme.colors['White'],
    paddingHorizontal: 100 * (40 / cardWidth) + '%',
    paddingVertical: 100 * (53 / cardWidth) + '%',
  },
  headerText: {
    color: theme.colors['70Black'],
    ...font(14, 14, 0.2),
  },
  hoursAgoText: {
    paddingTop: 3,
    color: theme.colors['40Black'],
    ...font(12, 12),
  },
  headerContainer: {
    flexDirection: 'row',
    paddingBottom: 24,
    justifyContent: 'space-between',
  },
  balanceValue: {
    ...font(26, 26, 0.5),
    flex: 1,
  },
  descriptionContainer: {
    justifyContent: 'flex-end',
    paddingBottom: responsivePixelValue(25),
  },
  descriptionText: {
    ...font(16, 26, 0.2),
    height: 'auto',
    width: '100%',
  },
  footerContainer: {
    ...font(16, 26, 0.2),
    borderTopWidth: 1,
    borderTopColor: theme.colors['Light Gray'],
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    paddingTop: 23,
  },
  additionalText: {
    color: theme.colors['60Black'],
    ...font(14, 14, 0.2),
  },
});
