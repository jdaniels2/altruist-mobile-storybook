import React from 'react';
import {
  Pressable,
  StyleSheet,
  Text,
  ViewStyle,
  View,
  GestureResponderEvent,
  TextStyle,
  Platform,
} from 'react-native';

import RadioIcon from '~/components/atoms/radioIcon/RadioIcon';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

type Props = {
  text: string;
  isSelected?: boolean;
  onPress?: (arg0: GestureResponderEvent, arg1?: GestureResponderEvent) => void;
  iconPrefix?: () => JSX.Element;
  isDisabled?: boolean;
  subText?: string;
  additionalStyles?: ViewStyle;
};

const RadioRow = ({
  text,
  isSelected,
  onPress,
  iconPrefix,
  isDisabled,
  subText,
  additionalStyles,
}: Props) => {
  return (
    <Pressable
      accessibilityLabel={`${text} Row`}
      style={
        isSelected
          ? [styles.containerSelected, additionalStyles]
          : [styles.container, additionalStyles]
      }
      disabled={!onPress || isDisabled}
      onPress={onPress}
    >
      <View style={styles.textContainer}>
        <View>
          <Text
            style={
              isDisabled ? styles.textDisabled : isSelected ? styles.textSelected : styles.text
            }
            numberOfLines={2}
          >
            {text}
          </Text>

          {subText && (
            <Text
              style={
                isDisabled
                  ? styles.subTextDisabled
                  : isSelected
                  ? styles.subTextSelected
                  : styles.subText
              }
              numberOfLines={1}
            >
              {subText}
            </Text>
          )}
        </View>
        {iconPrefix?.()}
      </View>

      {!isDisabled && <RadioIcon isSelected={isSelected} />}
    </Pressable>
  );
};

const styles = StyleSheet.create({
  ...(() => {
    const container: ViewStyle = {
      ...Platform.select({
        android: {
          flex: 1,
        },
      }),
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      height: 72,
      paddingHorizontal: 20,
      borderRadius: 2,
    };
    const textContainer: ViewStyle = {
      flexDirection: 'row',
      alignItems: 'center',
      width: '85%',
    };
    const text: TextStyle = {
      marginRight: 8,
    };
    return {
      container: {
        ...container,
        backgroundColor: theme.colors['White'],
      },
      containerSelected: {
        ...container,
        backgroundColor: theme.colors['Dark Gray'],
      },
      textContainer,
      text: {
        ...text,
        ...font(14, 18),
        color: theme.colors['Dark Gray'],
      },
      textSelected: {
        ...text,
        ...font(14, 18, undefined, '500'),
        color: theme.colors['White'],
      },
      textDisabled: {
        ...text,
        ...font(14, 18),
        color: theme.colors['40Black'],
      },
      subText: {
        ...text,
        ...font(12, 12),
        paddingTop: 14,
        color: theme.colors['Dark Gray'],
      },
      subTextSelected: {
        ...text,
        ...font(12, 12),
        paddingTop: 14,
        color: theme.colors['White'],
      },
      subTextDisabled: {
        ...text,
        ...font(12, 12),
        paddingTop: 14,
        color: theme.colors['40Black'],
      },
    };
  })(),
});

export default RadioRow;
