import type {Meta, StoryObj} from '@storybook/react';

import CheckBox from './CheckBox';

const meta = {
  title: 'atoms/CheckBox',
  component: CheckBox,
} satisfies Meta<typeof CheckBox>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    value: true, style: {backgroundColor: 'lightgray'}, onPress: () => {}, accessibilityLabel: 'label', checkColor: 'darkGrey'
  },
};
