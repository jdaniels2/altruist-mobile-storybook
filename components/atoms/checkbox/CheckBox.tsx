import React from 'react';
import { TouchableOpacity, Image, StyleSheet, ViewStyle } from 'react-native';

import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';
import { theme } from '~/theme/Theme';

const checkImages = {
  white: require('~/assets/images/check/whiteCheck.png'),
  lightGrey: require('~/assets/images/check/GreyCheck.png'),
  darkGrey: require('~/assets/images/check/check.png'),
};

export type Props = {
  value?: boolean;
  onPress: () => void;
  style?: ViewStyle;
  checkColor?: keyof typeof checkImages;
  accessibilityLabel?: string;
};

const CheckBox = React.memo(({ value, style, onPress, accessibilityLabel, checkColor }: Props) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      hitSlop={TOUCHABLE_HIT_SLOP}
      style={[styles.checkbox, style]}
      onPress={onPress}
      accessibilityLabel={accessibilityLabel}
    >
      {value ? (
        <Image source={checkColor ? checkImages[checkColor] : checkImages.darkGrey} />
      ) : null}
    </TouchableOpacity>
  );
});

const styles = StyleSheet.create({
  checkbox: {
    width: 20,
    height: 20,
    borderRadius: 3,
    backgroundColor: theme.colors['White'],
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default CheckBox;
