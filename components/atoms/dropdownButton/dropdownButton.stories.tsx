import type {Meta, StoryObj} from '@storybook/react';

import DropdownButton from './dropdownButton';

const meta = {
  title: 'atoms/DropdownButton',
  component:DropdownButton,
} satisfies Meta<typeof DropdownButton>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    onPress: () => {},
    collapsed: false,
    background: 'white',
    arrowColor: 'red',
    borderColor: 'red',
    horizontal: false,
    accessibilityLabel: '',
  },
};
