import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  dropdownButtonWrapper: {
    borderRadius: 17,
    height: 34,
    width: 34,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
  },
  styledArrow: {
    height: 10,
    width: 10,
  },
});
