import React from 'react';
import { TouchableOpacity } from 'react-native';

import { BottomArrow } from '~/assets/images/Icons';
import { Props } from '~/components/atoms/dropdownButton/dropdownButtonProps';
import { styles } from '~/components/atoms/dropdownButton/dropdownButtonStyles';
import { theme } from '~/theme/Theme';

const DropdownButton = ({
  onPress,
  collapsed,
  background,
  arrowColor,
  borderColor,
  horizontal,
  accessibilityLabel,
}: Props) => {
  return (
    <TouchableOpacity
      style={[
        styles.dropdownButtonWrapper,
        {
          backgroundColor: background ? background : theme.colors['White'],
          borderColor: borderColor ? borderColor : theme.colors['White'],
        },
      ]}
      onPress={onPress}
      accessibilityLabel={accessibilityLabel}
    >
      <BottomArrow
        horizontal={horizontal}
        collapsed={collapsed}
        style={[
          styles.styledArrow,
          { top: collapsed ? 2 : 0 },
          {
            transform: collapsed
              ? [{ rotate: `${horizontal ? 90 : 0}deg` }]
              : [{ rotate: `${horizontal ? 270 : 180}deg` }],
          },
        ]}
        pathStrokeColor={arrowColor}
      />
    </TouchableOpacity>
  );
};

export default DropdownButton;
