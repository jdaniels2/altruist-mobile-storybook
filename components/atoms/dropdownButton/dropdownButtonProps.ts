import { GestureResponderEvent } from 'react-native';

export type Props = {
  onPress: (event: GestureResponderEvent) => void;
  collapsed: boolean;
  background?: string;
  arrowColor?: string;
  borderColor?: string;
  horizontal?: boolean;
  accessibilityLabel?: string;
};
