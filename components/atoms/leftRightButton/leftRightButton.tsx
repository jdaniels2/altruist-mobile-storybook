import React from 'react';
import { TouchableOpacity } from 'react-native';

import { RightArrow } from '~/assets/images/Icons';
import { styles } from '~/components/atoms/leftRightButton/leftRightButtonStyles';
import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';
import { theme } from '~/theme/Theme';

type Props = {
  right: boolean;
  slide: Function;
  background: string;
  arrowColor: string;
};

const LeftRightButton = ({ right, slide, background, arrowColor }: Props) => {
  return (
    <TouchableOpacity
      hitSlop={TOUCHABLE_HIT_SLOP}
      onPress={() => slide(right)}
      style={[
        styles.buttonWrapper,
        { backgroundColor: background ? background : theme.colors['White'] },
      ]}
    >
      <RightArrow
        style={[
          styles.styledArrow,
          {
            transform: right ? [{ rotate: '0deg' }] : [{ rotate: '180deg' }],
          },
          {
            marginRight: right ? 0 : 2,
            marginLeft: right ? 2 : 0,
          },
        ]}
        pathStrokeColor={arrowColor}
      />
    </TouchableOpacity>
  );
};

export default LeftRightButton;
