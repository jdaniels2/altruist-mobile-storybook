import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  buttonWrapper: {
    borderRadius: 50,
    width: 34,
    height: 34,
    justifyContent: 'center',
    alignItems: 'center',
  },
  styledArrow: {
    height: 20,
    width: 20,
  },
});
