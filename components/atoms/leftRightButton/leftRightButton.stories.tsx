import type {Meta, StoryObj} from '@storybook/react';

import LeftRightButton from './leftRightButton';
import { theme } from '~/theme/Theme';

const meta = {
  title: 'atoms/LeftRightButton',
  component: LeftRightButton,
} satisfies Meta<typeof LeftRightButton>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    right: true, slide: () => {}, background: theme.colors['White'], arrowColor: theme.colors['Red']
  },
};
