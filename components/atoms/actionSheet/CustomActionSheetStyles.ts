import { StyleSheet, Platform, ViewStyle, TextStyle } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

const whitebox = {
  borderRadius: Platform.OS === 'ios' ? 14 : 0,
  backgroundColor: theme.colors['5Black'],
};

const optionText = {
  ...Platform.select({
    ios: {
      color: theme.colors['Blue'],
      marginHorizontal: 12,
      marginVertical: 16,
      textAlign: 'center',
      ...font(20, 25),
    },
    android: {
      ...font(16, 24),
      color: theme.colors['Black'],
      marginHorizontal: 28,
      marginVertical: 15,
      textAlign: 'left',
    },
  }),
};

type Styles = {
  container: ViewStyle;
  separator: ViewStyle;
  whitebox: ViewStyle;
  title: TextStyle;
  titleView: ViewStyle;
  textContainer: ViewStyle;
  text: TextStyle;
  optionText: TextStyle;
  destructiveOptionText: TextStyle;
  cancelButtonContainer: ViewStyle;
  androidCancelButtonContainer: ViewStyle;
  cancelTouchable: ViewStyle;
  cancelButtonTitle: TextStyle;
};

export const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    ...Platform.select({
      ios: {
        marginHorizontal: 10,
        marginBottom: 24,
      },
      android: {
        margin: 0,
      },
    }),
  },
  separator: {
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    height: Platform.OS === 'ios' ? 1 : 0,
  },
  whitebox,
  title: {
    ...font(14, 24, -0.08),
    color: theme.colors['70Black'],
    textAlign: 'center',
    textAlignVertical: 'center',
    marginTop: 16,
    marginBottom: 10,
  },
  titleView: {
    paddingHorizontal: 30,
  },
  textContainer: {
    ...whitebox,
    paddingHorizontal: 24,
    paddingVertical: 32,
  },
  text: {
    color: theme.colors['70Black'],
    ...font(16, 22),
    alignSelf: 'stretch',
  },
  optionText,
  destructiveOptionText: {
    ...optionText,
    color: theme.colors['Orange'],
  },
  cancelButtonContainer: {
    ...whitebox,
    height: 57,
    marginTop: 10,
  },
  androidCancelButtonContainer: {
    ...whitebox,
    height: 57,
    borderTopColor: theme.colors['70Black'],
    borderTopWidth: 1,
  },
  cancelTouchable: {
    flex: 1,
    justifyContent: 'center',
    ...Platform.select({
      ios: {
        alignItems: 'center',
      },
    }),
  },
  cancelButtonTitle: {
    ...Platform.select({
      android: {
        textAlign: 'left',
        paddingHorizontal: 28,
        color: theme.colors['Black'],
        ...font(16, 24),
      },
      ios: {
        ...font(20, 25, 0.38, '600'),
        color: theme.colors['Blue'],
      },
    }),
  },
});
