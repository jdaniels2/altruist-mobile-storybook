import React from 'react';
import { View, Text, TouchableOpacity, Platform } from 'react-native';
import Modal from 'react-native-modal';

import { styles } from '~/components/atoms/actionSheet/CustomActionSheetStyles';

export type Props = {
  title?: string | JSX.Element;
  content?: string | JSX.Element;
  options?: any[];
  visible: boolean;
  onDismiss: (arg0?: any) => any;
  onPress?: (arg0?: any) => any;
  cancelTitle?: string;
};

const renderOption = (
  option:
    | string
    | { text: string; destructive: boolean; onPress: (arg0: number | undefined) => void },
  i: number,
  onDismiss: Props['onDismiss'],
  onPress: Props['onPress'],
) => {
  const optionContent =
    typeof option === 'string' ? (
      <Text style={styles.optionText}>{option}</Text>
    ) : typeof option === 'object' && option.text ? (
      <Text style={option.destructive ? styles.destructiveOptionText : styles.optionText}>
        {option.text}
      </Text>
    ) : (
      option
    );

  return (
    <React.Fragment key={i}>
      {i !== 0 && <View style={styles.separator} />}
      <TouchableOpacity
        onPress={() => {
          onDismiss();

          const onPressImpl = (typeof option === 'object' && option.onPress) || onPress;
          onPressImpl?.(i);
        }}
      >
        {optionContent}
      </TouchableOpacity>
    </React.Fragment>
  );
};

const CustomActionSheet: React.FC<Props> = React.memo(
  ({ title, content, options, visible, onDismiss, onPress, cancelTitle = 'Done' }) => {
    const modal = (
      <Modal
        isVisible={visible}
        style={styles.container}
        backdropTransitionOutTiming={0}
        onBackdropPress={onDismiss}
        onBackButtonPress={onDismiss}
        useNativeDriver={true}
        useNativeDriverForBackdrop={true}
      >
        {content != null &&
          (typeof content === 'string' ? (
            <View style={styles.textContainer}>
              <Text style={styles.text}>{content}</Text>
            </View>
          ) : (
            <View style={styles.whitebox}>{content}</View>
          ))}

        {options != null && (
          <View style={styles.whitebox} accessibilityLabel='customActionSheetOptions'>
            {title !== undefined && title !== null && typeof title === 'string' && (
              <View style={styles.titleView}>
                <Text style={styles.title}>{title}</Text>
              </View>
            )}
            {title !== undefined && title !== null && typeof title === 'object' && title}
            {title !== undefined && title !== null && <View style={styles.separator} />}
            {options.map((option, i) => renderOption(option, i, onDismiss, onPress))}
          </View>
        )}

        {Platform.OS !== 'android' && (
          <View style={styles.cancelButtonContainer}>
            <TouchableOpacity
              style={styles.cancelTouchable}
              onPress={onDismiss}
              accessibilityLabel='cancelButtonContainerIOS'
            >
              <Text style={styles.cancelButtonTitle}>{cancelTitle}</Text>
            </TouchableOpacity>
          </View>
        )}

        {Platform.OS === 'android' && (
          <View style={styles.androidCancelButtonContainer}>
            <TouchableOpacity
              style={styles.cancelTouchable}
              onPress={onDismiss}
              accessibilityLabel='cancelButtonContainerAndroid'
            >
              <Text style={styles.cancelButtonTitle}>{cancelTitle}</Text>
            </TouchableOpacity>
          </View>
        )}
      </Modal>
    );

    return Platform.OS === 'ios' && !visible ? null : modal; // HACK fix not opening Alert on iOS after action sheet item clicked (probably react-native-modal bug)
  },
);

export default CustomActionSheet;
