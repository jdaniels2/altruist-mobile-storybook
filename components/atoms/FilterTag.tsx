import React, { useCallback } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { CloseIcon } from '~/assets/images/Icons';
import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

type Props<T> = {
  type: T;
  text: string | JSX.Element;
  onDelete: (type: T) => void;
};

const FilterTag = <T,>({ type, text, onDelete }: Props<T>) => {
  const onClosePress = useCallback(() => onDelete(type), [onDelete, type]);

  return (
    <View style={styles.tagContainer}>
      <TouchableOpacity hitSlop={TOUCHABLE_HIT_SLOP} onPress={onClosePress}>
        <CloseIcon height={9} width={9} fill={theme.colors['70Black']} />
      </TouchableOpacity>
      <Text style={styles.text}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  tagContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors['20Black'],
    borderRadius: 30,
    height: 38,
    padding: 13,
    marginRight: 8,
  },
  text: {
    ...font(12, 14, undefined, '500'),
    color: theme.colors['70Black'],
    marginLeft: 10,
  },
});

export default FilterTag;
