import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';
import { theme } from '~/theme/Theme';
import { font, responsivePixelValue } from '~/theme/utilities';

type Props = {
  active: 'ScheduledTransactions' | 'Transactions';
  setTab: (active: Props['active']) => null;
};

const NonNavigationTabBar = ({ active, setTab }: Props) => {
  return (
    <View style={styles.tabsContainer}>
      <View style={styles.tabWrapper}>
        <TouchableOpacity
          accessibilityLabel='Tab navigates to current transactions'
          activeOpacity={1.0}
          hitSlop={TOUCHABLE_HIT_SLOP}
          onPress={() => setTab('Transactions')}
          style={[
            styles.tab,
            {
              borderBottomColor: active === 'Transactions' ? theme.colors['Black'] : 'transparent',
            },
          ]}
        >
          <Text style={styles.tabText}>Transactions</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.tabWrapper}>
        <TouchableOpacity
          accessibilityLabel='Tab navigates to scheduled transactions'
          activeOpacity={1.0}
          hitSlop={TOUCHABLE_HIT_SLOP}
          onPress={() => setTab('ScheduledTransactions')}
          style={[
            styles.tab,
            {
              borderBottomColor:
                active === 'ScheduledTransactions' ? theme.colors['Black'] : 'transparent',
            },
          ]}
        >
          <Text style={styles.tabText}>Scheduled</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default NonNavigationTabBar;

const styles = StyleSheet.create({
  tabsContainer: {
    width: '100%',
    height: 53,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: responsivePixelValue(16),
    borderBottomWidth: 1,
    borderBottomColor: theme.colors['20Black'],
    backgroundColor: theme.colors['Light Gray'],
  },
  tabText: {
    paddingTop: 22,
    textAlign: 'center',
    color: theme.colors['Black'],
    ...font(15, 15),
  },
  tabWrapper: {
    width: '50%',
    height: 53,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tab: {
    width: 112,
    alignContent: 'center',
    justifyContent: 'center',
    height: 53,
    borderBottomWidth: 2,
  },
});
