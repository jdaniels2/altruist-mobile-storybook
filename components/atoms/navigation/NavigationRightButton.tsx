import React from 'react';
import { GestureResponderEvent, TouchableOpacity } from 'react-native';

import { styles } from '~/components/atoms/navigation/navigationStyle';
import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';

type Props = {
  onPress: (event: GestureResponderEvent) => void;
  icon: JSX.Element;
  accessibilityLabel?: string;
};

const NavigationRightButton = ({ onPress, icon, accessibilityLabel }: Props) => {
  return (
    <TouchableOpacity
      style={styles.styledRightIcon}
      onPress={onPress}
      hitSlop={TOUCHABLE_HIT_SLOP}
      accessibilityLabel={accessibilityLabel}
    >
      {icon}
    </TouchableOpacity>
  );
};

export default NavigationRightButton;
