import React, { useCallback } from 'react';
import { TouchableOpacity, StyleSheet, ViewStyle } from 'react-native';

import { BackIcon } from '~/assets/images/Icons';
import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';

type Props = {
  onPress?: Function;
};

const NavigationBackButton = (props: Props) => {
  const navigation = () => {}
  const onPress = useCallback(() => {}, [navigation, props]);

  return (
    <TouchableOpacity style={styles.backIcon} hitSlop={TOUCHABLE_HIT_SLOP} onPress={onPress}>
      <BackIcon />
    </TouchableOpacity>
  );
};

type Style = {
  backIcon: ViewStyle;
};

const styles = StyleSheet.create<Style>({
  backIcon: {
    marginLeft: 10,
  },
});

export default NavigationBackButton;
