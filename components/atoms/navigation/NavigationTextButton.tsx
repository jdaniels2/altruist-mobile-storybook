import React from 'react';
import { GestureResponderEvent, Text, TextStyle, TouchableOpacity } from 'react-native';

import { styles } from '~/components/atoms/navigation/navigationStyle';
import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';
import { theme } from '~/theme/Theme';

type Props = {
  onPress: (event: GestureResponderEvent) => void;
  title: string;
  color?: string;
  styleProp?: TextStyle;
};

const NavigationTextButton = ({ onPress, title, color, styleProp }: Props) => (
  <TouchableOpacity hitSlop={TOUCHABLE_HIT_SLOP} onPress={onPress}>
    <Text
      style={[styles.textButton, styleProp, { color: color ? color : theme.colors['Dark Gray'] }]}
    >
      {title}
    </Text>
  </TouchableOpacity>
);

export default NavigationTextButton;
