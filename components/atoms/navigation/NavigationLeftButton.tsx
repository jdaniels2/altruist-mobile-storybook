import React from 'react';
import { GestureResponderEvent, TouchableOpacity } from 'react-native';

import { styles } from '~/components/atoms/navigation/navigationStyle';
import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';

type Props = {
  onPress: ((event: GestureResponderEvent) => void) | undefined;
  icon: JSX.Element;
};

const NavigationLeftButton = ({ onPress, icon }: Props) => {
  return (
    <TouchableOpacity
      style={styles.leftButtonTouchable}
      onPress={onPress}
      hitSlop={TOUCHABLE_HIT_SLOP}
    >
      {icon}
    </TouchableOpacity>
  );
};

export default NavigationLeftButton;
