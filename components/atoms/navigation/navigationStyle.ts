import { Platform, StyleSheet } from 'react-native';

import { font, responsivePixelValue } from '~/theme/utilities';

export const styles = StyleSheet.create({
  leftButtonTouchable: {
    marginLeft: 10,
  },
  styledRightIcon: {
    paddingRight: responsivePixelValue(16),
  },
  textButton: {
    paddingRight: responsivePixelValue(16),
    top: Platform.OS === 'android' ? 1 : 3,
    ...font(17, 22),
  },
});
