import React from 'react';
import { StyleSheet, Switch, SwitchProps } from 'react-native';

const Toggle = ({
  onValueChange,
  value,
  disabled = false,
  accessibilityLabel,
  thumbColor,
  trackColor,
}: SwitchProps) => {
  return (
    <Switch
      onValueChange={onValueChange}
      value={value}
      trackColor={trackColor}
      thumbColor={thumbColor}
      style={styles.switch}
      disabled={disabled}
      accessibilityLabel={accessibilityLabel}
    />
  );
};

const styles = StyleSheet.create({
  switch: {
    height: 30,
    width: 30,
    marginRight: 10,
  },
});

export default Toggle;
