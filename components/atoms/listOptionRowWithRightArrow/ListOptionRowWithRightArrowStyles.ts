import { StyleSheet } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 30,
  },
  subtext: {
    ...font(12),
    paddingTop: 10,
    color: theme.colors['70Black'],
  },
  subtextLarge: {
    ...font(14),
    paddingTop: 14,
    color: theme.colors['70Black'],
  },
  option: {
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 16,
    height: 75,
    borderRadius: 2,
    flexDirection: 'row',
    paddingHorizontal: 18,
    backgroundColor: theme.colors['5Black'],
  },
  optionLarge: {
    justifyContent: 'space-between',
    paddingTop: 32,
    marginTop: 16,
    height: 128,
    borderRadius: 2,
    flexDirection: 'row',
    paddingHorizontal: 18,
    backgroundColor: theme.colors['5Black'],
  },
  optionText: {
    ...font(14, 18, 0.2),
    top: 3,
    color: theme.colors['Dark Gray'],
    paddingRight: 8,
  },
  optionTextLarge: {
    ...font(16, 19, 0.2),
    top: 3,
    color: theme.colors['Black'],
    paddingRight: 8,
  },
  textContainer: {
    width: '85%',
  },
  arrowContainer: {
    borderRadius: 14,
    width: 28,
    height: 28,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors['Light Gray'],
  },
  textIconContainer: {
    flexDirection: 'row',
  },
  infoIcon: {
    top: 3,
  },
  containerSelected: {
    backgroundColor: theme.colors['Dark Gray'],
  },
});
