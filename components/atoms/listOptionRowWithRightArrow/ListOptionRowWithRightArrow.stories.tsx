import type {Meta, StoryObj} from '@storybook/react';

import ListOptionRowWithRightArrow from './ListOptionRowWithRightArrow';

const meta = {
  title: 'atoms/ListOptionRowWithRightArrow',
  component: ListOptionRowWithRightArrow,
} satisfies Meta<typeof ListOptionRowWithRightArrow>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    text: 'text',
    isSelected: true,
    onPress: () => {},
    iconPrefix: undefined
  },
};
