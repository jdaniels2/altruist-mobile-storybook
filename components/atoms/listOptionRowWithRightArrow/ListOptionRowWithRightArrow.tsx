import React, { useState, useMemo, useCallback } from 'react';
import { Pressable, View, Text, TouchableOpacity, ViewStyle } from 'react-native';

import { RightArrow, InfoIcon } from '~/assets/images/Icons';
import { styles } from '~/components/atoms/listOptionRowWithRightArrow/ListOptionRowWithRightArrowStyles';
import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';
import { theme } from '~/theme/Theme';

export type Props = {
  style?: ViewStyle;
  text: string;
  onPress: (arg0: any, arg1?: any) => void;
  value: any;
  field?: string;
  disabled?: boolean;
  subtext?: string;
  grayedOut?: boolean;
  infoIconOnPress?: () => void;
  isLargeSelector?: boolean;
  accessibilityLabel?: string;
  iconPrefix?: JSX.Element;
  isSelected?: boolean;
  accessibilityHint?: string;
  arrowContainerStyle?: ViewStyle;
  optionStyleProp?: ViewStyle;
  optionStylePropLarge?: ViewStyle;
  activeOptionColor?: string;
  selectedOptionColor?: string;
  arrorActiveColor?: string;
};

const ListOptionRowWithRightArrow: React.FC<Props> = ({
  style,
  text,
  onPress,
  value,
  field = null || '',
  disabled,
  subtext,
  grayedOut = disabled,
  infoIconOnPress,
  isLargeSelector = false,
  accessibilityLabel,
  iconPrefix,
  isSelected,
  accessibilityHint,
  arrowContainerStyle,
  optionStyleProp,
  optionStylePropLarge,
  activeOptionColor,
  selectedOptionColor,
  arrorActiveColor,
}) => {
  const [isActive, setIsActive] = useState(false);

  const handlePress = useCallback(() => {
    field ? onPress(field, value) : onPress(value);
    setIsActive(false);
  }, [field, onPress, value]);

  const pathStrokeColor = useMemo(() => {
    if (isActive && !disabled) {
      return theme.colors['White'];
    }

    if (disabled || grayedOut) {
      return theme.colors['40Black'];
    }

    return theme.colors['70Black'];
  }, [disabled, grayedOut, isActive]);

  const optionStyle = useMemo(() => {
    const optionStyles = [];
    if (isLargeSelector) {
      optionStyles.push(!!optionStylePropLarge ? optionStylePropLarge : styles.optionLarge);
    } else {
      optionStyles.push(!!optionStyleProp ? optionStyleProp : styles.option);
    }

    if (isActive) {
      optionStyles.push({ backgroundColor: activeOptionColor ?? theme.colors['Dark Gray'] });
    }

    if ((disabled || grayedOut) && !isActive) {
      optionStyles.push({ backgroundColor: theme.colors['5Black'] });
    }

    if (isSelected) {
      optionStyles.push({ backgroundColor: selectedOptionColor ?? theme.colors['Dark Gray'] });
    }

    return optionStyles;
  }, [
    activeOptionColor,
    disabled,
    grayedOut,
    isActive,
    isLargeSelector,
    isSelected,
    optionStyleProp,
    optionStylePropLarge,
    selectedOptionColor,
  ]);

  const arrowStyle = useMemo(() => {
    const arrowStyles = [styles.arrowContainer, arrowContainerStyle];

    if (isActive && !disabled) {
      arrowStyles.push({ backgroundColor: arrorActiveColor ?? theme.colors['85Black'] });
    }
    return arrowStyles;
  }, [arrorActiveColor, arrowContainerStyle, disabled, isActive]);

  return (
    <Pressable
      style={[styles.container, style]}
      onPressOut={() => {
        !disabled && setIsActive(false);
      }}
      onPressIn={() => {
        !disabled && setIsActive(true);
      }}
      onPress={disabled ? undefined : handlePress}
      disabled={disabled}
      accessibilityLabel={accessibilityLabel}
      accessibilityHint={accessibilityHint}
    >
      <View style={optionStyle}>
        <View style={styles.textContainer}>
          <View style={styles.textIconContainer}>
            <Text
              style={[
                isLargeSelector ? styles.optionTextLarge : styles.optionText,
                isActive && { color: theme.colors['White'] },
                (disabled || grayedOut) && !isActive && { color: theme.colors['40Black'] },
                isSelected && { color: 'white' },
              ]}
              numberOfLines={2}
            >
              {text}
            </Text>

            {!!infoIconOnPress && (
              <TouchableOpacity
                style={styles.infoIcon}
                hitSlop={TOUCHABLE_HIT_SLOP}
                onPress={infoIconOnPress}
              >
                <InfoIcon color={theme.colors['50Black']} />
              </TouchableOpacity>
            )}
          </View>

          {subtext && (
            <Text
              style={[
                isLargeSelector ? styles.subtextLarge : styles.subtext,
                (disabled || grayedOut) && { color: theme.colors['40Black'] },
              ]}
            >
              {subtext}
            </Text>
          )}
        </View>

        <View style={arrowStyle}>
          {iconPrefix ? iconPrefix : <RightArrow pathStrokeColor={pathStrokeColor} />}
        </View>
      </View>
    </Pressable>
  );
};

export default ListOptionRowWithRightArrow;
