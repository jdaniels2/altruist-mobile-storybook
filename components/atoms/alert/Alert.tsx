import React from 'react';
import { Platform, StyleSheet, ViewStyle, TextStyle } from 'react-native';
import Dialog from 'react-native-dialog';

import CustomActionSheet from '~/components/atoms/actionSheet/CustomActionSheet';
import { AktivGroteskMedium } from '~/constants/fonts';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export type Props = {
  visible: boolean;
  description?: string | Function | Element;
  cancelText: string;
  onCancelPress: (arg0?: any) => any;
  title?: string;
  onConfirmPress?: (arg0?: any) => any | (() => void);
  confirmText?: string;
  confirmTextColor?: string;
  iosAsActionSheet?: boolean;
  isAndroidActionSheet?: boolean;
};

const Alert: React.FC<Props> = React.memo(
  ({
    visible,
    description,
    cancelText,
    onCancelPress,
    title = '',
    confirmText = undefined,
    onConfirmPress = undefined,
    iosAsActionSheet = false,
    isAndroidActionSheet = false,
    confirmTextColor,
  }) => {
    if (iosAsActionSheet && Platform.OS === 'ios') {
      return (
        <CustomActionSheet
          visible={visible}
          content={description as string}
          onDismiss={onCancelPress}
          cancelTitle={cancelText}
        />
      );
    }
    if (isAndroidActionSheet && Platform.OS === 'android') {
      return (
        <CustomActionSheet
          visible={visible}
          content={description as string}
          onDismiss={onCancelPress}
          cancelTitle={cancelText}
        />
      );
    }

    return (
      <Dialog.Container
        visible={visible}
        // @ts-expect-error
        backdropOpacity={0.6}
        blurStyle={styles.blurStyle}
        style={styles.container}
        contentStyle={styles.content}
        useNativeDriver={true}
      >
        {title != null && <Dialog.Title style={styles.title}>{title}</Dialog.Title>}

        {typeof description === 'string' ? (
          <Dialog.Description style={styles.description}>{description}</Dialog.Description>
        ) : typeof description === 'function' ? (
          description()
        ) : (
          description
        )}

        {cancelText && (
          <Dialog.Button label={cancelText} onPress={onCancelPress} style={styles.cancelButton} />
        )}

        {confirmText && (
          <Dialog.Button
            label={confirmText}
            onPress={onConfirmPress as () => void}
            style={{ ...styles.button, color: confirmTextColor }}
          />
        )}
      </Dialog.Container>
    );
  },
);

type Styles = {
  container: ViewStyle;
  content?: ViewStyle;
  header?: ViewStyle;
  title: TextStyle;
  description: TextStyle;
  button: ViewStyle;
  cancelButton: ViewStyle;
  blurStyle: ViewStyle;
};
const styles = StyleSheet.create<Styles>({
  container: Platform.select({
    ios: {
      width: '100%',
      marginLeft: 0,
      marginTop: 0,
    },
    default: {
      width: 280,
      marginRight: 'auto',
      marginLeft: 'auto',
      shadowOpacity: 0,
    },
  }),
  content: Platform.select({
    android: {
      paddingTop: 0,
      paddingBottom: 12,
      paddingHorizontal: 18,
      elevation: 0,
      borderRadius: 4,
    },
  }),
  header: Platform.select({
    android: {
      padding: 0,
      margin: 0,
    },
  }),
  title: Platform.select({
    ios: {
      ...font(17, 22, -0.41, '600'),
    },
    default: {
      ...font(16, 24, -0.41, '400'),
      marginTop: 10,
      color: 'black',
    },
  }),
  description: {
    ...font(13, 18),
    color: theme.colors['70Black'],
    ...Platform.select({
      android: {
        letterSpacing: 0.25,
        marginBottom: 10,
      },
    }),
  },
  button: Platform.select({
    ios: {
      color: theme.colors['Blue'],
    },
    default: {
      color: theme.colors['Blue'],
      fontFamily: AktivGroteskMedium,
      letterSpacing: 1.25,
    },
  }),
  cancelButton: Platform.select({
    ios: {
      fontWeight: '600',
      color: theme.colors['Blue'],
      letterSpacing: -0.41,
    },
    default: {
      color: theme.colors['Blue'],
      fontFamily: AktivGroteskMedium,
      letterSpacing: 1.25,
    },
  }),
  blurStyle: {
    backgroundColor: theme.colors['5Black'],
  },
});

export default Alert;
