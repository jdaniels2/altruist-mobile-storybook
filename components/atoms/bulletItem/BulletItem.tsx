import React from 'react';
import { StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export type Props = {
  text: string;
  textStyle?: Maybe<TextStyle>;
  bulletStyle?: Maybe<TextStyle>;
  containerStyle?: Maybe<ViewStyle>;
  listNumber?: string;
};

const BulletItem = ({ text, textStyle, bulletStyle, containerStyle, listNumber }: Props) => {
  return (
    <View style={containerStyle ?? styles.container}>
      <Text style={bulletStyle ?? styles.bullet}>{!!listNumber ? listNumber : '\u2B24'}</Text>
      <Text style={textStyle ?? styles.bulletText}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  bullet: {
    paddingTop: 9,
    paddingRight: 8,
    fontSize: 3,
    color: theme.colors['70Black'],
  },
  bulletText: {
    ...font(12, 22, 0.2, '400'),
    color: theme.colors['70Black'],
  },
});

export default BulletItem;
