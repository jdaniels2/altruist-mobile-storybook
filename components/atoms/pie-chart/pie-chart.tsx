import R from 'ramda';
import React, { useState, useEffect, useMemo } from 'react';
import { View } from 'react-native';
import Svg from 'react-native-svg';
import Victory from '~/victory';

import {
  accountDetailLabelY,
  animate,
  chartWidth,
  colorScale,
  defaultGraphicData,
  innerRadius,
  labelY,
  victoryLabelWidth,
} from '~/components/atoms/pie-chart/PieChartConstants';
import {
  EventCallbackInterface,
  Props,
  StringOrNumberOrList,
} from '~/components/atoms/pie-chart/PieChartProps';
import { styles } from '~/components/atoms/pie-chart/PieChartStyles';
import { theme } from '~/theme/Theme';
import { AllowedAllocation } from '~/types/graphql';
import { getAllocationType, calculatePercentage } from '~/utils/formatter';

const VictoryPie = Victory.VictoryPie;
const VictoryLabel =Victory.VictoryLabel;

const PieChart = ({
  activeAllocation,
  realAllocations,
  allocations,
  sharedSetActiveAllocation,
  accountDetails,
}: Props) => {
  const [externalMutations, setExternalMutations] = useState<
    EventCallbackInterface<string | string[], StringOrNumberOrList>[] | undefined
  >();
  const activeSlice = activeAllocation.name;
  const graphicData = allocations;

  useEffect(() => {
    // To prevent infinite loop, need to set external mutation to undefined after it changes
    setExternalMutations(undefined);
  }, [externalMutations]);

  const labelText = useMemo(
    () =>
      calculatePercentage(
        realAllocations as { value: number }[],
        activeAllocation.valueCopy !== undefined
          ? activeAllocation.valueCopy
          : activeAllocation.value ?? 0,
      ),
    [activeAllocation.value, activeAllocation.valueCopy, realAllocations],
  );

  const allocationType = useMemo(
    () => getAllocationType(activeSlice as AllowedAllocation),
    [activeSlice],
  );

  const chartColorScale = useMemo(() => colorScale(allocations), [allocations]);

  return (
    <View style={styles.container}>
      <Svg width={chartWidth} height={350}>
        <VictoryPie
          standalone={false}
          innerRadius={innerRadius}
          width={chartWidth}
          labels={({ datum }) => ``}
          y={(d) => d.value}
          style={{
            labels: { fontSize: 20 },
            data: {
              fillOpacity: 1,
              stroke: ({ datum, style }: any) => {
                return datum.name === activeSlice ? style.fill : undefined;
              },
              strokeWidth: ({ datum }) => {
                return datum.name === activeSlice ? 15 : 0;
              },
            },
          }}
          data={graphicData}
          animate={animate}
          padAngle={R.equals(graphicData, defaultGraphicData) ? 0 : 5}
          colorScale={chartColorScale}
          events={[
            {
              childName: 'pie',
              target: 'data',
              eventHandlers: {
                onPress: () => {
                  return [
                    {
                      // Add an event to reset all slices to the original color
                      target: 'data',
                      eventKey: 'all',
                      mutation: () => {
                        return null;
                      },
                    },
                    {
                      // Then add an event to set changes. (eventKey will automatically use current target)
                      target: 'data',
                      mutation: (props) => {
                        sharedSetActiveAllocation(props.datum.name);
                        setExternalMutations([
                          {
                            target: ['data'],
                            eventKey: 'all',
                            mutation: () => ({ style: undefined }),
                          },
                        ]);
                        return {
                          style: {
                            fill: props.style.fill,
                            stroke: props.style.fill,
                            strokeWidth: 15,
                            cursor: 'pointer',
                          },
                        };
                      },
                    },
                  ];
                },
              },
            },
          ]}
          externalEventMutations={externalMutations}
        />

        <VictoryLabel
          textAnchor='middle'
          style={{ ...styles.victoryLabelAllocationType, fill: theme.colors['60Black'] }}
          x={victoryLabelWidth}
          y={labelY}
          text={allocationType}
        />

        <VictoryLabel
          textAnchor='middle'
          style={{
            ...styles.victoryLabelPercentage,
            fill: theme.colors[accountDetails ? 'Black' : 'White'],
          }}
          x={victoryLabelWidth}
          y={accountDetailLabelY}
          text={labelText}
        />
      </Svg>
    </View>
  );
};

export default PieChart;
