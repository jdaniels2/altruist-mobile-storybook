import { Dimensions } from 'react-native';

import { ColorScalePropType } from '~/components/atoms/pie-chart/PieChartProps';
import { colorMap } from '~/constants/constants';
import { Allocation } from '~/types/graphql';

export const screenWidth = Dimensions.get('window').width;
export const screenHeight = Dimensions.get('screen').height;
export const defaultGraphicData = [{ name: 'STOCKS', value: 100 }];
export const isSmallPhone = screenHeight < 739;

export const innerRadius =
  screenHeight < 784
    ? screenHeight < 668
      ? screenWidth / 3.2
      : screenWidth / 3.1
    : screenWidth / 3.9;
export const chartWidth = screenWidth / 1.25;
export const labelY = isSmallPhone
  ? screenHeight > 668
    ? screenHeight / 4.5
    : screenHeight / 4
  : screenHeight / 4.75;
export const victoryLabelWidth = screenWidth / 2.5;
export const accountDetailLabelY = isSmallPhone ? screenHeight / 4 + 22 : screenHeight / 4.75 + 22;

export const colorScale = (
  allocations: (Allocation & any)[],
): ColorScalePropType => {
  const newAllocations = allocations.map((obj) => {
    return colorMap[obj.name.toLowerCase() as keyof typeof colorMap];
  });
  return newAllocations;
};
export const animate = { easing: 'linear', duration: 400 } as any;
