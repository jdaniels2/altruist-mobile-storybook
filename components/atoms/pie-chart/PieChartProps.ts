import { Allocation } from '~/types/graphql';

export type Props = {
  allocations: (Allocation & any)[];
  activeAllocation: any;
  realAllocations: any | undefined;
  sharedSetActiveAllocation: (name: string) => void;
  accountDetails?: boolean;
};

// copied from victory-native library as export isn't working
export type StringOrNumberOrList = string | number | (string | number)[];
export interface EventCallbackInterface<TTarget, TEventKey> {
  childName?: string | string[];
  target?: TTarget;
  eventKey?: TEventKey;
  mutation: (props: any) => any;
  callback?: (props: any) => any;
}
export type ColorScalePropType =
  | 'grayscale'
  | 'qualitative'
  | 'heatmap'
  | 'warm'
  | 'cool'
  | 'red'
  | 'green'
  | 'blue'
  | string[];
