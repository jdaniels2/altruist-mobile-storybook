import type {Meta, StoryObj} from '@storybook/react';

import PieChart from './pie-chart';
import { View } from 'react-native';

const meta = {
  title: 'atoms/PieChart',
  component: (props) => <View style={{
    width: '100%',
    height: '100',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
  } as any}><PieChart {...props}/></View>,
} satisfies Meta<typeof PieChart>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    allocations: [{name: 'Cash', valueCopy: 10, value: 10}, {name: 'Bonds', valueCopy: 10, value: 10}],
    realAllocations:  [{name: 'Cash', valueCopy: 10, value: 10}],
    activeAllocation: [{name: 'Cash', valueCopy: 10, value: 10}],
    sharedSetActiveAllocation: () => {}
  },
};
