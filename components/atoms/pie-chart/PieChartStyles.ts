import { StyleSheet } from 'react-native';

import { font } from '~/theme/utilities';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
  },
  victoryLabelAllocationType: {
    ...font(12, 14, 0.2, '400'),
  },
  victoryLabelPercentage: {
    // eslint-disable-next-line no-sparse-arrays
    ...font(20, 20, undefined, '400'),
  },
});
