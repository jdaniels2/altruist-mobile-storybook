import React, { useMemo } from 'react';
import {
  View,
  StyleSheet,
  Platform,
  TouchableOpacity,
  Text,
  ViewStyle,
  TextStyle,
  TouchableOpacityProps,
} from 'react-native';

import { CloseIcon } from '~/assets/images/Icons';
import NavigationBackButton from '~/components/atoms/navigation/NavigationBackButton';
import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

type Props = {
  title?: string;
  showSeparator?: boolean;
  showBack?: boolean;
  onBackPress?: () => void;
  leftElement?: JSX.Element;
  rightElement?: JSX.Element;
  type?: 'default' | 'modal' | 'mma';
};

const NavigationBar = ({
  title,
  onBackPress,
  leftElement: leftItem,
  rightElement,
  showSeparator = true,
  showBack = true,
  type = 'default',
}: Props) => {

  const style = useMemo(() => {
    return type === 'default'
      ? styles.container
      : type === 'modal'
      ? styles.modalContainer
      : styles.mmaContainer;
  }, [type]);

  return (
    <View style={showSeparator ? style : [style, { borderBottomWidth: 0 }]}>
      <View
        style={
          title ? styles.leftButtonContainer : [styles.leftButtonContainer, styles.noTitleContainer]
        }
      >
        {leftItem ??
          (showBack ? <NavigationBackButton onPress={onBackPress} /> : null)}
      </View>
      <Text style={styles.title}>{title ?? ''}</Text>
      <View style={styles.rightButtonContainer}>{rightElement ?? null}</View>
    </View>
  );
};

export const TextButton = ({ text, ...otherProps }: { text: string } & TouchableOpacityProps) => {
  return (
    <TouchableOpacity style={styles.button} hitSlop={TOUCHABLE_HIT_SLOP} {...otherProps}>
      <Text style={otherProps.disabled ? styles.buttonTextDisabled : styles.buttonText}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};

export const CloseButton = ({
  close,
  style,
}: {
  close: () => void;
  style?: ViewStyle | ViewStyle[];
}) => {
  return (
    <TouchableOpacity style={[styles.button, style]} onPress={close} hitSlop={TOUCHABLE_HIT_SLOP}>
      <CloseIcon />
    </TouchableOpacity>
  );
};

type ContainerStyle = {
  container: ViewStyle;
  modalContainer: ViewStyle;
  buttonText: TextStyle;
  buttonTextDisabled: TextStyle;
  mmaContainer: ViewStyle;
};

type Style = {
  title: TextStyle;
  noTitleContainer: ViewStyle;
  leftButtonContainer: ViewStyle;
  rightButtonContainer: ViewStyle;
  button: ViewStyle;
} & ContainerStyle;

const styles = StyleSheet.create<Style>({
  title: {
    ...font(17, 22, -0.41, '500'),
    flex: 1,
    color: theme.colors['Black'],
    textAlign: 'center',
  },
  noTitleContainer: {
    marginLeft: -10,
  },
  leftButtonContainer: {
    alignItems: 'flex-start',
    paddingLeft: 16,
    width: 100,
  },
  rightButtonContainer: {
    alignItems: 'flex-end',
    paddingRight: 16,
    width: 100,
  },
  button: {
    alignItems: 'flex-end',
  },

  ...((): ContainerStyle => {
    const container: ViewStyle = {
      flexDirection: 'row',
      alignItems: 'center',
      borderColor: theme.colors['20Black'],
      backgroundColor: theme.colors['Light Gray'],
      borderBottomWidth: 1,
    };
    const textButton: TextStyle = font(17, 22, -0.41);
    return {
      container: {
        ...container,
        height: Platform.select({ ios: 44, android: 52 }),
      },
      modalContainer: {
        ...container,
        height: 73,
      },
      mmaContainer: {
        ...container,
        backgroundColor: theme.colors['5Black'],
      },
      buttonText: {
        ...textButton,
        color: theme.colors['Blue'],
      },
      buttonTextDisabled: {
        ...textButton,
        color: theme.colors['40Black'],
      },
    };
  })(),
});

export default NavigationBar;
