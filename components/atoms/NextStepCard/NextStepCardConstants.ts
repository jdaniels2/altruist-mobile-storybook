import { Advisor } from '~/types/graphql';

export const finishAccountSetup = {
  title: 'Finish Account Setup',
  description: 'You have in-progress accounts that need your attention.',
  buttonText: 'Start now',
};

export const changesRequiredCSCorp = {
  title: 'Changes Required',
  description:
    'The secretary has returned the account application. We recommend reaching out to the secretary to understand why it was returned.',
  buttonText: 'Start Now',
};

export const approveTransferRequest = (organizationName: Advisor['organaizationName']) => ({
  title: 'Approve Transfer Request',
  description: `${organizationName} has requested a transfer to your account.`,
  buttonText: 'Review Now',
});

export const mmaApproveRequest = (organizationName: Advisor['organaizationName']) => ({
  title: 'Approve Request',
  description: `${organizationName} has requested permission to transfer funds between your accounts.`,
  buttonText: 'Review Now',
});
