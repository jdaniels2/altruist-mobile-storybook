import React, { forwardRef, useCallback, useMemo, ForwardRefRenderFunction } from 'react';
import { TouchableOpacity, Text, StyleSheet, Image, Dimensions, View } from 'react-native';
import Carousel from 'react-native-snap-carousel';

import { cardFactory } from '~/components/atoms/NextStepCard/NextStepCardFactory';
import { CardProps, NextStepCardProps } from '~/components/atoms/NextStepCard/NextStepCardProps';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

const { width, height } = Dimensions.get('window');

const Card = ({ onPress, item, agreementsCount }: CardProps) => {
  const advisorInfo = [{organaizationName: 'org'}]
  const { organaizationName: organizationName } = (advisorInfo && advisorInfo[0]) || {};

  const card = useMemo(() => {
    return cardFactory({ item, organizationName, agreementsCount });
  }, [agreementsCount, item, organizationName]);

  return (
    <TouchableOpacity style={styles.cardContainer} onPress={onPress}>
      <View>
        <Text style={styles.cardTitleText}>{card.title}</Text>

        <Text style={styles.cardDescriptionText}>{card.description}</Text>

        {!!card.SecondaryActionComponent && <card.SecondaryActionComponent />}

        <Image
          style={[styles.image, item.type === 'changesRequiredCSCorp' && styles.declinedAccount]}
          source={card.image}
        />
      </View>

      <View>
        <Text style={styles.startText} onPress={onPress}>
          {card.buttonText}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const NextStepCard: ForwardRefRenderFunction<any, NextStepCardProps> = (
  {
    onPress,
    acatsToAccept,
    needsAttention,
    needsAttentionGrouped,
    mmaToApprove,
    rejectedCSCorpAccounts,
    certifiedCSCorpAccounts,
  },
  ref,
) => {
  const  showFundingAccountNextSteps = false;
  const { showSignatureRequiredCard, showMultipleSignaturesRequiredCard, agreementsCount } = {showSignatureRequiredCard: true, showMultipleSignaturesRequiredCard: true, agreementsCount: 2}

  const cards = useMemo(() => {
    let cards = [] as CardProps['item'][];
    if (needsAttention) {
      cards.push({ type: 'needsAttention' });
    }
    if (needsAttentionGrouped) {
      cards.push({ type: 'needsAttentionGrouped' });
    }
    if (acatsToAccept && acatsToAccept.length > 0) {
      cards.push({ type: 'ACAT' });
    }
    if (mmaToApprove && mmaToApprove?.length > 0) {
      cards.push({ type: 'MMA' });
    }
    if (rejectedCSCorpAccounts && rejectedCSCorpAccounts.length > 0) {
      cards.push({ type: 'changesRequiredCSCorp' });
    }

    if (certifiedCSCorpAccounts && certifiedCSCorpAccounts?.length > 0) {
      cards.push({ type: 'finishCSCorpAccount' });
    }

    if (showFundingAccountNextSteps) {
      cards.push({ type: 'fundingAccount' });
    }

    if (showSignatureRequiredCard) {
      cards.push({ type: 'signClientAgreement' });
    }

    if (showMultipleSignaturesRequiredCard) {
      cards.push({ type: 'signMultipleClientAgreements' });
    }

    return cards;
  }, [
    needsAttention,
    needsAttentionGrouped,
    acatsToAccept,
    mmaToApprove,
    rejectedCSCorpAccounts,
    certifiedCSCorpAccounts,
    showFundingAccountNextSteps,
    showSignatureRequiredCard,
    showMultipleSignaturesRequiredCard,
  ]);

  const renderItem = useCallback(
    ({ item }) => (
      <Card item={item} onPress={() => onPress(item)} agreementsCount={agreementsCount} />
    ),
    [agreementsCount, onPress],
  );

  return (
    <View style={styles.container}>
      <Carousel
        data={cards}
        renderItem={renderItem}
        removeClippedSubviews={false}
        sliderWidth={width}
        itemHeight={height}
        itemWidth={width - 48}
        inactiveSlideScale={0.95}
        ref={ref}
      />
    </View>
  );
};

const titleStyle = {
  ...font(20, 20),
  color: theme.colors['Black'],
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  rowContainer: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 24,
    marginBottom: 48,
    marginTop: 72,
  },
  titleText: {
    ...titleStyle,
    textAlign: 'center',
  },
  cardContainer: {
    backgroundColor: theme.colors['White'],
    borderRadius: 12,
    marginTop: 48,
    paddingHorizontal: '12.5%',
    paddingVertical: 60,
    height: 448,
    justifyContent: 'space-between',
  },
  cardTitleText: {
    ...titleStyle,
  },
  cardDescriptionText: {
    marginTop: 24,
    ...font(16, 26),
    color: theme.colors['Dark Gray'],
  },
  startText: {
    ...font(16, 16, undefined, '500'),
    textDecorationLine: 'underline',
    color: theme.colors['Black'],
  },
  declinedAccount: {
    marginTop: 13,
  },
  image: {
    alignSelf: 'flex-end',
    marginRight: 3,
    marginTop: 15,
  },
});

export default forwardRef(NextStepCard);
