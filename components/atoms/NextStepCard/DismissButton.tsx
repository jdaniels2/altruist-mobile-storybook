import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react';
import { Text, StyleSheet } from 'react-native';

import { StepName } from '~/components/atoms/NextStepCard/redux/nextStepsStorage';
import { AsyncStorageKeys } from '~/constants/asyncStorage';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

type Props = {
  stepName: StepName;
};

export const DismissButton = (props: Props) => {
  const dispatch = () => {}
  return (
    <Text
      style={styles.link}
      onPress={async () => {
        if (props.stepName === 'HeldAway') {}
        await AsyncStorage.setItem(AsyncStorageKeys.hasDismissedLinkCard, 'true');
      }}
    >
      Dismiss
    </Text>
  );
};

export const styles = StyleSheet.create({
  link: {
    ...font(16, 26, 0.2, '400'),
    textDecorationLine: 'underline',
    color: theme.colors['60Black'],
  },
});
