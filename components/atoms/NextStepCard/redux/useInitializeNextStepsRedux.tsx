import { useEffect, useRef } from 'react';

import { getCurrentNextStepsStorage } from '~/components/atoms/NextStepCard/redux/nextStepsStorage';

export function useInitializeNextStepsRedux() {
  const hasInitializedRedux = false
  const syncingRef = useRef(false);

  useEffect(() => {
    if (hasInitializedRedux === false && !syncingRef.current) {
      syncingRef.current = true;
      getCurrentNextStepsStorage().then((nextSteps) => {
        syncingRef.current = false;
      });
    }
  }, [hasInitializedRedux]);
  return { hasInitializedDismissibleNextSteps: hasInitializedRedux };
}
