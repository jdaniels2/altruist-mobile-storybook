import AsyncStorage from '@react-native-async-storage/async-storage';

export const KEY = 'nextStepsStorage';
export type StepName = 'HeldAway' | 'FundingAccounts';
export type NextStepsStorage = Record<StepName, boolean | null>;

/**
 *
 * If a key does not exist in async storage, it will return null.
 */

function safeParse(value: string | null | undefined): NextStepsStorage {
  if (value === null || value === undefined) {
    return {
      HeldAway: null,
      FundingAccounts: null,
    };
  }
  try {
    return JSON.parse(value || '{}');
  } catch (error) {
    return {
      HeldAway: null,
      FundingAccounts: null,
    };
  }
}

export function getCurrentNextStepsStorage(): Promise<NextStepsStorage>;
export function getCurrentNextStepsStorage(raw: true): Promise<string | null | undefined>;
export function getCurrentNextStepsStorage(
  raw?: boolean,
): Promise<NextStepsStorage | string | null | undefined> {
  return new Promise((resolve) => {
    AsyncStorage.getItem(KEY, (_error, result) => {
      if (raw) {
        return resolve(result);
      }
      resolve(safeParse(result));
    });
  });
}

export const setShownNextStep = async (stepName: StepName, shown: boolean): Promise<void> => {
  const rawCurrentNextSteps = await getCurrentNextStepsStorage(true);

  const nextState = JSON.stringify({ [stepName]: shown });

  return new Promise((resolve) => {
    if (rawCurrentNextSteps === null || rawCurrentNextSteps === undefined) {
      // if we have nothing in async storage now, lets store a net new object
      AsyncStorage.setItem(KEY, nextState, () => {
        resolve();
      });
    } else {
      // otherwise the async storage lib can merge the properties for us
      AsyncStorage.mergeItem(KEY, nextState, () => {
        resolve();
      });
    }
  });
};

/**
 * If a key does not exist in async storage, it will return null.
 */
export const getShownNextStep = async (stepName: StepName): Promise<boolean | null> => {
  const currentNextSteps = await getCurrentNextStepsStorage();
  return currentNextSteps[stepName];
};

export const clearShownNextSteps = (): Promise<void> => {
  return new Promise((resolve) => {
    AsyncStorage.removeItem(KEY, () => {
      resolve();
    });
  });
};

// NOTE: Call this in console to clear async storage item.
try {
  /** You will need to clear redux as well. */
  (global as any).clearShowNextSteps = clearShownNextSteps;
} catch (error) {
  // noop
}
