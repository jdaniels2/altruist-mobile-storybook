import {
  setShownNextStep,
  NextStepsStorage,
  StepName,
} from '~/components/atoms/NextStepCard/redux/nextStepsStorage';

type State = NextStepsStorage & { initializedRedux: boolean };

export const nextStepsSlice = () => {}

export const hideNextStep = (stepName: StepName) => async (dispatch) => {
  await setShownNextStep(stepName, false);
};
