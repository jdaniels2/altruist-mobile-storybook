import { GestureResponderEvent } from 'react-native';

import { StateOutput, MMA } from '~/types/graphql';

export type CardType =
  | 'MMA'
  | 'needsAttention'
  | 'changesRequiredCSCorp'
  | 'ACAT'
  | 'heldAway'
  | 'needsAttentionGrouped'
  | 'fundingAccount'
  | 'finishCSCorpAccount'
  | 'signClientAgreement'
  | 'signMultipleClientAgreements';

export type CardProps = {
  onPress: (event: GestureResponderEvent) => void;
  item: { type: CardType; secretaryName?: string };
  agreementsCount: number;
};

export type NextStepCardProps = {
  onPress: (item: CardProps['item']) => void;
  acatsToAccept: any[];
  needsAttention: boolean;
  needsAttentionGrouped: boolean;
  mmaToApprove: MMA[];
  certifiedCSCorpAccounts?: StateOutput[];
  rejectedCSCorpAccounts?: StateOutput[];
};
