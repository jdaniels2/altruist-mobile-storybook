import React, { FunctionComponent } from 'react';

import { DismissButton } from '~/components/atoms/NextStepCard/DismissButton';
import { CardProps } from '~/components/atoms/NextStepCard/NextStepCardProps';
import { Advisor } from '~/types/graphql';

type CardDetails = {
  title: string;
  description: string;
  buttonText: string;
  image: any;
  SecondaryActionComponent?: FunctionComponent;
};

type CardFactoryOptions = {
  item: CardProps['item'];
  organizationName: Advisor['organaizationName'];
  agreementsCount?: number;
};

export const cardFactory = ({
  item,
  organizationName,
  agreementsCount,
}: CardFactoryOptions): CardDetails => {
  if (item.type === 'MMA') {
    return mmaApproveRequest(organizationName);
  }

  if (item.type === 'needsAttention') {
    return finishAccountSetup;
  }

  if (item.type === 'needsAttentionGrouped') {
    return finishAccountSetupGrouped;
  }

  if (item.type === 'changesRequiredCSCorp') {
    return changesRequiredCSCorp;
  }

  if (item.type === 'fundingAccount') {
    return fundingAccountNextStep;
  }

  if (item.type === 'finishCSCorpAccount') {
    return finishCSCorpAccount;
  }

  if (item.type === 'signClientAgreement') {
    return signClientAgreement;
  }

  if (item.type === 'signMultipleClientAgreements') {
    return signMultipleClientAgreements(agreementsCount);
  }

  return approveTransferRequest(organizationName);
};

const emptyPNG = require('~/assets/images/accountDetails/empty.png');
const setupPNG = require('~/assets/images/accountOpeningFlow/setup.png');
const FundingAccountPNG = require('~/assets/images/heldAway/connecting_03.png');

const fundingAccountNextStep = {
  title: 'Connect your bank',
  description: 'Transfer money to and from your Altruist account securely.',
  buttonText: 'Add Funding Accounts',
  image: FundingAccountPNG,
  SecondaryActionComponent: () => <DismissButton stepName='FundingAccounts' />,
};

const finishAccountSetup = {
  title: 'Finish Account Setup',
  description: 'You have in-progress accounts that need your attention.',
  buttonText: 'Start now',
  image: setupPNG,
};

const finishAccountSetupGrouped = {
  title: 'Finish Account Setup',
  description: 'You have in-progress accounts that need your attention.',
  buttonText: 'Start now',
  image: setupPNG,
};

export const changesRequiredCSCorp = {
  title: 'Changes Required',
  description:
    'The secretary has returned the account application. We recommend reaching out to the secretary to understand why it was returned.',
  buttonText: 'Start Now',
  image: setupPNG,
};

const approveTransferRequest = (organizationName: Advisor['organaizationName']) => ({
  title: 'Approve Transfer Request',
  description: `${organizationName} has requested a transfer to your account.`,
  buttonText: 'Review Now',
  image: setupPNG,
});

const mmaApproveRequest = (organizationName: Advisor['organaizationName']) => ({
  title: 'Approve Request',
  description: `${organizationName} has requested permission to transfer funds between your accounts.`,
  buttonText: 'Review Now',
  image: emptyPNG,
});

export const finishCSCorpAccount = {
  title: 'Finish Account Setup',
  description:
    'The secretary has certified the application. You can now submit the account to be opened.',
  buttonText: 'Open now',
  image: setupPNG,
};

const signClientAgreement = {
  title: 'Signature Requested',
  description: 'Your advisor sent a document that requires your review.',
  buttonText: 'Review & Sign',
  image: setupPNG,
};

const signMultipleClientAgreements = (agreementsCount?: number) => ({
  title: 'Signature Requested',
  description: `Your advisor sent ${agreementsCount} documents that require your review.`,
  buttonText: 'Review Documents',
  image: setupPNG,
});
