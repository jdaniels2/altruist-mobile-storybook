import React, { LegacyRef } from 'react';
import { FlatList, FlatListProps, Platform, RefreshControl, StyleSheet } from 'react-native';

import { theme } from '~/theme/Theme';

const isIOS = Platform.OS === 'ios';

type Props<T> = FlatListProps<T> & {
  listRef?: LegacyRef<FlatList<T>>;
};

const FlatListWithRefresh = <T,>({
  onRefresh,
  refreshing = false,
  listRef,
  style,
  ...otherProps
}: Props<T>) => {
  return (
    <FlatList
      ref={listRef}
      style={[styles.baseListStyle, style]}
      initialNumToRender={3}
      progressViewOffset={70}
      contentContainerStyle={styles.contentContainer}
      showsVerticalScrollIndicator={false}
      onRefresh={isIOS ? null : onRefresh}
      refreshing={isIOS ? null : refreshing}
      refreshControl={
        isIOS ? (
          <RefreshControl
            style={styles.refreshControl}
            refreshing={false}
            tintColor={theme.colors['Light Gray']}
            onRefresh={onRefresh ?? undefined}
          />
        ) : undefined
      }
      {...otherProps}
    />
  );
};

const styles = StyleSheet.create({
  refreshControl: {
    opacity: 0,
    height: 0,
  },
  baseListStyle: {
    backgroundColor: theme.colors['Light Gray'],
  },
  contentContainer: {
    paddingTop: Platform.select({ ios: 0, android: 20 }),
    flexGrow: 1,
  },
});

export default FlatListWithRefresh;
