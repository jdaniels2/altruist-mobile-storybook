import type {Meta, StoryObj} from '@storybook/react';

import RadioIcon from './RadioIcon';

const meta = {
  title: 'atoms/RadioIcon',
  component: RadioIcon,
} satisfies Meta<typeof RadioIcon>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    isSelected: true,
    style: {},
  },
};
