import React from 'react';
import { StyleSheet, View, ViewStyle } from 'react-native';

import { theme } from '~/theme/Theme';

const RadioIcon = ({ isSelected, style }: { isSelected?: boolean; style?: ViewStyle }) => {
  const innerStyle = isSelected ? styles.containerSelected : styles.container;
  return (
    <View style={style ? [innerStyle, style] : innerStyle}>
      {isSelected ? <View style={styles.center} /> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  center: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: theme.colors['40Black'],
  },
  ...(() => {
    const container: ViewStyle = {
      width: 20,
      height: 20,
      borderRadius: 10,
      alignItems: 'center',
      justifyContent: 'center',
    };
    return {
      container: {
        ...container,
        backgroundColor: theme.colors['Light Gray'],
      },
      containerSelected: {
        ...container,
        backgroundColor: theme.colors['85Black'],
      },
    };
  })(),
});

export default RadioIcon;
