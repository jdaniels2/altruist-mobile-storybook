import { ViewStyle, ImageStyle, TextStyle, StyleSheet } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

type Styles = {
  outerContainer: ViewStyle;
  innerContainer: ViewStyle;
  image: ImageStyle;
  text: TextStyle;
  textSpace: ViewStyle;
  footerSpace: ViewStyle;
};

export const styles = StyleSheet.create<Styles>({
  outerContainer: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    paddingVertical: 20,
    backgroundColor: theme.colors['White'],
    flexGrow: 1,
  },
  innerContainer: {
    alignItems: 'stretch',
  },
  image: {
    flexShrink: 1,
    alignSelf: 'center',
  },
  text: {
    ...font(16, 24, 0.2),
    textAlign: 'center',
    color: theme.colors['Dark Gray'],
    width: 300,
  },
  textSpace: {
    height: 32, // flex, minHeight and maxHeight are bugged if set here
  },
  footerSpace: {
    flex: 1,
    maxHeight: 56,
    minHeight: 20,
  },
});
