import { theme } from '~/theme/Theme';

export type Props = {
  backgroundColor?: keyof typeof theme.colors;
  imageSource?: string;
  text?: string;
  button?: JSX.Element;
  footer?: JSX.Element;
  accessibilityLabel?: string;
};
