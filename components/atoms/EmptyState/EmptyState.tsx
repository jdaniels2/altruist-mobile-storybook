import React from 'react';
import { Image, Text, View } from 'react-native';

import { Props } from '~/components/atoms/EmptyState/EmptyStateProps';
import { styles } from '~/components/atoms/EmptyState/EmptyStateStyles';
import { theme } from '~/theme/Theme';
const defaultImage = require('~/assets/images/empty-state.png');

const EmptyState = ({
  backgroundColor,
  imageSource,
  text,
  button,
  footer,
  accessibilityLabel,
}: Props) => {
  return (
    <View
      style={[
        styles.outerContainer,
        backgroundColor && { backgroundColor: theme.colors[backgroundColor] },
      ]}
      accessibilityLabel={accessibilityLabel}
    >
      <View style={styles.innerContainer}>
        <Image style={styles.image} resizeMode='contain' source={imageSource || defaultImage} />
        <View style={styles.textSpace} />
        <Text style={styles.text}>{text}</Text>
        <View style={styles.textSpace} />
        {button}
        {footer && <View style={styles.footerSpace} />}
        {footer}
      </View>
    </View>
  );
};

export default EmptyState;
