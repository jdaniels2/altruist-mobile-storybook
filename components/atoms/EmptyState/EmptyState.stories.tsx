import type {Meta, StoryObj} from '@storybook/react';

import EmptyState from './EmptyState';

const meta = {
  title: 'atoms/EmptyState',
  component:EmptyState,
} satisfies Meta<typeof EmptyState>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    text: 'text',
    button: undefined,
    footer: undefined,
    backgroundColor: '10Black',
    imageSource: '',
    accessibilityLabel: '',
  },
};
