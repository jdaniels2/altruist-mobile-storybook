import type {Meta, StoryObj} from '@storybook/react';

import AccountListItem from './AccountListItem';

const meta = {
  title: 'atoms/AccountListItem',
  component: AccountListItem,
} satisfies Meta<typeof AccountListItem>;

export default meta;

type Story = StoryObj<typeof meta>;
const account = {
    realTimeBalance: 100,
    institutionName: 'Altruist',
    financialAccountName: 'Altruist',
    accountType: 'INDIVIDUAL',
    accountNumber: '1111',
    status: 'ACTIVE'
}
export const Basic: Story = {
  args: {
    account,
    apy: .001,
    handleAccountPressed: () => {}
  },
};
