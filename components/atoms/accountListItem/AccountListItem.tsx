import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';

import { InfoIcon, RightArrow } from '~/assets/images/Icons';
import { styles } from '~/components/atoms/accountListItem/AccountListItemStyles';
import { ACCOUNT_STATUS, highYieldSavingAccountTypes } from '~/constants/constants';
import { AccountTypeInfo, ACCOUNT_TYPE_INFOS } from '~/constants/financialAccounts';
import { theme } from '~/theme/Theme';
import { AccountDetails } from '~/types/graphql';
import { getAccountStatusTitle } from '~/utils/financialAccounts';
import { financialInstitution, currency, percent } from '~/utils/formatter';

export type Props = {
  account: AccountDetails;
  handleAccountPressed: Function;
  apy?: number;
};

const AccountListItem = React.memo(
  ({
    account: { realTimeBalance, institutionName, status, accountType, accountNumber },
    account,
    handleAccountPressed,
    apy,
  }: Props) => {
    const isActive = status === ACCOUNT_STATUS.ACTIVE;
    const accountTypeInfo = ACCOUNT_TYPE_INFOS[accountType as AccountTypeInfo]?.displayName ?? '';
    const displayAccountNumber = accountNumber ? `(**${accountNumber?.slice(-5)})` : '';
    return (
      <TouchableOpacity
        style={[
          styles.cardContainer,
          {
            backgroundColor: isActive ? theme.colors['White'] : theme.colors['5Black'],
          },
        ]}
        activeOpacity={0.8}
        onPress={() => {
          handleAccountPressed(account);
        }}
        accessibilityLabel={account?.financialAccountName}
      >
        <View style={styles.descriptionContainer}>
          <Text style={styles.descriptionText}>
            {account?.financialAccountName?.length > 0
              ? account?.financialAccountName
              : `${accountTypeInfo} ${displayAccountNumber}`}
          </Text>
          <View style={styles.arrowContainer}>
            {isActive ? <RightArrow style={styles.styledArrow} /> : <InfoIcon />}
          </View>
        </View>

        <View style={styles.footerContainer}>
          {isActive && <Text style={styles.balanceValue}>{currency(realTimeBalance || 0)}</Text>}
          {!isActive && (
            <Text style={styles.institutionText}>{getAccountStatusTitle(status!)}</Text>
          )}
          {!!institutionName && !highYieldSavingAccountTypes.hasOwnProperty(accountType!) && (
            <Text style={styles.institutionText}>{financialInstitution(institutionName)}</Text>
          )}
          {highYieldSavingAccountTypes.hasOwnProperty(accountType!) && !!apy && (
            <View style={styles.apyContainer}>
              <Text style={styles.apyText}>{percent(apy)} APY</Text>
            </View>
          )}
        </View>
      </TouchableOpacity>
    );
  },
);

export default AccountListItem;
