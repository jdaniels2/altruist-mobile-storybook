import { StyleSheet } from 'react-native';

import { altruistBlue } from '~/constants/constants';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export const styles = StyleSheet.create({
  cardContainer: {
    width: '100%',
    height: 198,
    borderRadius: 12,
    padding: 30,
    overflow: 'hidden',
    backgroundColor: theme.colors['5Black'],
    justifyContent: 'space-between',
  },
  balanceValue: {
    ...font(16, 16, 0.5),
    flex: 1,
  },
  descriptionContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  descriptionText: {
    ...font(16, 26, 0.2),
    width: '100%',
    color: theme.colors['Black'],
    flex: 4,
  },
  footerContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    paddingTop: 23,
  },
  arrowContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
  },
  styledArrow: {
    height: 20,
    width: 20,
  },
  institutionText: {
    color: theme.colors['40Black'],
    ...font(12, 12, 0.2),
  },
  apyText: {
    color: theme.colors['White'],
    ...font(12, 12, 0.2),
  },
  apyContainer: {
    backgroundColor: altruistBlue,
    padding: 10,
    borderRadius: 100,
  },
});
