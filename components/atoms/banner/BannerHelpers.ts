export type BannerScreens = 'dashboard' | 'accounts';

export const mapScreen = {
  dashboard: 'hasSeenDashboardCount',
  accounts: 'hasSeenAccountsTabCount',
};

export const getShouldShowBanner = async (screen: BannerScreens) => {
  const bannerData = {dashboard: 0, accounts: 0 };
  const bannerKeychainProperty = mapScreen[
    screen
  ] as keyof SecureStorage.HighYieldSavingsBannerType;
  if (!!bannerData) {
    if (bannerData[bannerKeychainProperty] >= 3) {
      return false;
    } else {
      return true;
    }
  } else {
    return true;
  }
};

export const updateShowBanner = async (screen: BannerScreens) => {
};
