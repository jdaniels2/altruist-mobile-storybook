import type {Meta, StoryObj} from '@storybook/react';
import { View } from 'react-native';

import HighYieldCashAccountBanner from './HighYieldCashAccountBanner';

const meta = {
  title: 'atoms/HighYieldCashAccountBanner',
  component: (props) =>  <View style={props.storyBookContainer}><HighYieldCashAccountBanner {...props}/></View>,
} satisfies Meta<typeof HighYieldCashAccountBanner>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    navigation: {navigate: () => {}},
    storyBookContainer: { width: '100%'}
  },
};
