import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

import { CloseIcon } from '~/assets/images/Icons';
import { styles } from '~/components/atoms/banner/BannerStyles';
import { useHighYieldCash } from '~/components/atoms/banner/hooks/useHighYieldCash';
import { theme } from '~/theme/Theme';

const altruistCashLogo = require('~/assets/images/AltruistCashLogo.png');

type Props = {
  navigation: any;
  addPaddingBottom?: boolean;
};
const screen = 'dashboard';

const HighYieldCashDashboardBanner = ({ navigation, addPaddingBottom }: Props) => {
  const { showBanner, isHYCEnabled, earnText, onPressGetStarted, onClose } = useHighYieldCash({
    screen,
    navigation,
  });

  if (!showBanner || !isHYCEnabled) {
    return null;
  }

  return (
    <TouchableOpacity
      style={[styles.mainContainerDashboard, { paddingBottom: addPaddingBottom ? 32 : 0 }]}
      accessibilityLabel='High Yield Cash Banner'
      onPress={onPressGetStarted}
    >
      <View style={styles.hysContainer}>
        <View style={styles.hysDescriptionContainer}>
          <Text style={styles.hysDescriptionText}>{earnText}</Text>
          <TouchableOpacity onPress={onClose} accessibilityLabel='Close High Yield Cash Banner'>
            <CloseIcon fill={theme.colors['White']} />
          </TouchableOpacity>
        </View>

        <View style={styles.footerContainer}>
          <Text style={styles.getStartedText}>Learn More</Text>

          <Image style={styles.altruistLogo} source={altruistCashLogo} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default HighYieldCashDashboardBanner;
