import type {Meta, StoryObj} from '@storybook/react';
import { View } from 'react-native';

import HighYieldCashDashboardBanner from './HighYieldCashDashboardBanner';

const meta = {
  title: 'atoms/HighYieldCashDashboardBanner',
  component: (props: any) =>  <View style={props.storyBookContainer}><HighYieldCashDashboardBanner {...props}/></View>,
} satisfies Meta<typeof HighYieldCashDashboardBanner>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    navigation: {navigate: () => {}},
    storyBookContainer: { width: '100%'}
  },
};
