import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { CloseIcon } from '~/assets/images/Icons';
import { styles } from '~/components/atoms/banner/BannerStyles';
import { useHighYieldCash } from '~/components/atoms/banner/hooks/useHighYieldCash';
import { theme } from '~/theme/Theme';

const screen = 'accounts';

const HighYieldCashAccountBanner = ({ navigation }: any) => {
  const { showBanner, isHYCEnabled, earnText, onPressGetStarted, onClose } = useHighYieldCash({
    screen,
    navigation,
  });

  if (!showBanner || !isHYCEnabled) {
    return null;
  }

  return (
    <TouchableOpacity
      style={styles.mainContainerAccount}
      accessibilityLabel='High Yield Cash Banner'
      onPress={onPressGetStarted}
    >
      <View style={styles.accountBanner}>
        <View style={styles.accountDescription}>
          <Text style={styles.accountText}>
            {earnText} <Text style={styles.getStartedTextAccount}>Learn More</Text>
          </Text>

          <TouchableOpacity onPress={onClose} accessibilityLabel='Close High Yield Cash Banner'>
            <CloseIcon fill={theme.colors['White']} />
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default HighYieldCashAccountBanner;
