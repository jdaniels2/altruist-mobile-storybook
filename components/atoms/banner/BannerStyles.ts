import { StyleSheet } from 'react-native';

import { altruistBlue } from '~/constants/constants';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export const styles = StyleSheet.create({
  mainContainerDashboard: {
    paddingTop: 40,
    paddingHorizontal: 24,
  },
  mainContainerAccount: {
    paddingHorizontal: 24,
  },
  hysContainer: {
    width: '100%',
    height: 198,
    borderRadius: 12,
    padding: 30,
    overflow: 'hidden',
    backgroundColor: altruistBlue,
    justifyContent: 'space-between',
  },
  hysDescriptionContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  hysDescriptionText: {
    ...font(16, 26, 0.2),
    color: theme.colors['White'],
    width: '80%',
    bottom: 5,
  },
  footerContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  getStartedText: {
    ...font(16, 26, 0.2),
    color: theme.colors['White'],
    textDecorationLine: 'underline',
    top: 5,
  },
  altruistLogo: {
    resizeMode: 'contain',
  },
  accountBanner: {
    width: '100%',
    borderRadius: 12,
    padding: 24,
    paddingBottom: 14,
    overflow: 'hidden',
    backgroundColor: altruistBlue,
  },
  accountDescription: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
  },
  accountText: {
    ...font(14, 21, 0.2),
    color: theme.colors['White'],
    bottom: 5,
    width: '90%',
  },
  getStartedTextAccount: {
    ...font(14, 21, 0.2),
    color: theme.colors['White'],
    textDecorationLine: 'underline',
    top: 2,
  },
});
