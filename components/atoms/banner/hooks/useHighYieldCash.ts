import { useState, useMemo, useEffect, useCallback, useRef } from 'react';

import {
  BannerScreens,
  getShouldShowBanner,
  updateShowBanner,
} from '~/components/atoms/banner/BannerHelpers';
import { percent } from '~/utils/formatter';

type Props = {
  screen: BannerScreens;
  navigation: any;
};

// hack for lack of session-storage in React-Native
let hasViewedBannerInSesson = {
  dashboard: false,
  accounts: false,
};

export const useHighYieldCash = ({ screen, navigation }: Props) => {
  const apyInfoData = { apyInfo: { apy: .001} }
  const [showBanner, setShowBanner] = useState(false);
  const isHYCEnabled = true
  const apy = apyInfoData?.apyInfo?.apy;
  // prevents logEvent from calling again due to re-render
  const hasLoggedBannerViewed = useRef(false);

  const earnText = useMemo(() => {
    return `Altruist Cash users can now earn ${percent(apy!)} APY.`;
  }, [apy]);

  useEffect(() => {
    (async () => {
      if (!apy) {
        setShowBanner(false);
        return;
      }
      if (!showBanner && isHYCEnabled && !hasViewedBannerInSesson[screen]) {
        const shouldShowBanner = await getShouldShowBanner(screen);
        setShowBanner(shouldShowBanner);
        if (shouldShowBanner && !hasLoggedBannerViewed.current) {
          hasLoggedBannerViewed.current = true;
        }
      }
    })();
  }, [showBanner, isHYCEnabled, screen, apy]);

  const onClose = useCallback(async () => {
    await updateShowBanner(screen);
    hasViewedBannerInSesson[screen] = true;
    setShowBanner(false);
  }, [screen]);

  const onPressGetStarted = useCallback(() => {
    navigation.navigate('AltruistCash');
  }, [navigation, screen]);

  return useMemo(
    () => ({ earnText, showBanner, setShowBanner, onClose, onPressGetStarted, isHYCEnabled }),
    [earnText, isHYCEnabled, onClose, onPressGetStarted, showBanner],
  );
};
