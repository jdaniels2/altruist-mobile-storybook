import type {Meta, StoryObj} from '@storybook/react';

import {MessageBox} from './MessageBox';
import { theme } from '~/theme/Theme';
import { Text } from 'react-native';

const meta = {
  title: 'atoms/MessageBox',
  component: () => (<MessageBox>
  <Text>
    In addition to opening these accounts, we'd like you to approve these account transfers.
    Deselect any transfers you don't want to make.
  </Text>
</MessageBox>),
} satisfies Meta<typeof MessageBox>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    style: undefined,
    color: theme.colors['Black'],
    size: 12,
    trackWidth: 2
  },
};
