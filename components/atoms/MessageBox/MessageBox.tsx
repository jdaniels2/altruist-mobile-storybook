import React, { ReactNode } from 'react';
import { StyleSheet, View } from 'react-native';

import { theme } from '~/theme/Theme';

export const MessageBox = ({ children }: { children: ReactNode }) => {
  return <View style={styles.advisorMessageContainer}>{children}</View>;
};
const styles = StyleSheet.create({
  advisorMessageContainer: {
    borderColor: theme.colors['20Black'],
    borderWidth: 1,
    borderStyle: 'solid',
    padding: 24,
    borderRadius: 12,
    borderTopLeftRadius: 2,
  },
});
