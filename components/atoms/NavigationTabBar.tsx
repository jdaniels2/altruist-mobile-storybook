import { MaterialTopTabBarProps } from '@react-navigation/material-top-tabs';
import React from 'react';
import { FlexAlignType, StyleSheet, Text, View, TouchableOpacity } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export const renderTabBar = ({
  state,
  descriptors,
  navigation,
  position,
}: MaterialTopTabBarProps) => {
  return (
    <View style={styles.tabBar}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label = options.tabBarLabel ?? options.title ?? route.name;
        const isSelected = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isSelected && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole='button'
            accessibilityState={isSelected ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={[index ? styles.tabBarRightButton : styles.tabBarLeftButton]}
          >
            <Text style={styles.tabBarButtonText}>{label}</Text>
            {isSelected && <View style={styles.underline} />}
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listHeaderText: {
    ...font(34, 34, undefined, '500'),
    paddingHorizontal: 24,
    paddingBottom: 30,
  },
  tabBar: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: theme.colors['20Black'],
  },
  underline: {
    marginTop: 9,
    height: 2,
    width: 112,
    backgroundColor: theme.colors['Black'],
    marginBottom: -1,
  },
  tabBarButtonText: {
    ...font(15, 15, 0.2),
    color: theme.colors['Black'],
  },
  ...(() => {
    const padding = 23;
    const tabBarButton = {
      alignItems: 'center' as FlexAlignType,
      flex: 1,
      paddingTop: 18,
    };
    return {
      tabBarLeftButton: {
        ...tabBarButton,
        paddingLeft: padding,
      },
      tabBarRightButton: {
        ...tabBarButton,
        paddingRight: padding,
      },
    };
  })(),
});
