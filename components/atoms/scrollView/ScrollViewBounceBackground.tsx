// https://stackoverflow.com/questions/40366080/2-different-background-colours-for-scrollview-bounce
import React from 'react';
import { View, StyleSheet, Platform } from 'react-native';

import { theme } from '~/theme/Theme';

const SPACER_SIZE = 1000; // makes top of scroll view background color different

const ScrollViewBounceBackground: React.FC<{ backgroundColor?: keyof typeof theme.colors }> = ({
  backgroundColor,
}) => {
  return Platform.OS === 'ios' ? (
    <View
      style={[styles.scrollViewTop, { backgroundColor: theme.colors[backgroundColor ?? 'White'] }]}
    />
  ) : null;
};

const styles = StyleSheet.create({
  scrollViewTop: {
    height: SPACER_SIZE,
    position: 'absolute',
    top: -SPACER_SIZE,
    left: 0,
    right: 0,
  },
});

export default ScrollViewBounceBackground;
