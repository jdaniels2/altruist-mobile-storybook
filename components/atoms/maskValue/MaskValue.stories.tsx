import type {Meta, StoryObj} from '@storybook/react';

import MaskValue from './MaskValue';
import { TIN_MASK } from '~/constants/constants';

const meta = {
  title: 'atoms/MaskValue',
  component: MaskValue,
} satisfies Meta<typeof MaskValue>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    value: 'value',
    mask: TIN_MASK as string[]
  },
};
