import React, { useState } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Platform } from 'react-native';

import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';
import theme from '~/theme/figma/theme.json';
import { font } from '~/theme/utilities';
import { formatMask } from '~/utils/formatter';

export type MaskedValueProps = {
  mask: string[];
  value: string | undefined;
};

const MaskValue = ({ value, mask }: MaskedValueProps) => {
  const [maskedVisible, setMaskedVisible] = useState(false);

  const text = maskedVisible ? (
    <Text style={[styles.textValue, styles.maskNumbers]}>
      {formatMask(value?.toString(), mask)}
    </Text>
  ) : (
    <>
      <Text style={styles.bullets}>•••••</Text>
      <Text style={[styles.textValue, styles.maskNumbers]}>
        {'  ' + value?.toString().substring(5)}
      </Text>
    </>
  );

  return (
    <View style={styles.maskContainer}>
      {text}
      <TouchableOpacity
        hitSlop={TOUCHABLE_HIT_SLOP}
        onPress={() => setMaskedVisible(!maskedVisible)}
      >
        <Text style={styles.maskVisibilityButton}>{maskedVisible ? 'Hide' : 'Show'}</Text>
      </TouchableOpacity>
    </View>
  );
};

const MaskedValueComponent = React.memo(MaskValue);

export default MaskedValueComponent;

const styles = StyleSheet.create({
  textValue: {
    flex: 1,
    ...font(16, 16, 0.2),
    color: theme.colors['Black'],
  },

  maskNumbers: {
    flex: 0,
  },
  bullets: {
    ...Platform.select({ ios: { marginTop: -10 } }),
    ...font(32, 32, 1.5),
  },
  maskContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 28,
  },
  maskVisibilityButton: {
    ...font(12, 12, 0.2),
    color: theme.colors['Black'],
    marginLeft: 32,
    ...Platform.select({ ios: { marginBottom: 2 }, android: { marginTop: 3 } }),
  },
});
