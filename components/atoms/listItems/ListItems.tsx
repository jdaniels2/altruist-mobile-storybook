import React from 'react';
import { StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export type ListHeaderFooterProps = {
  text: string;
  headerCustomStyle?: ViewStyle;
  textCustomStyle?: TextStyle;
};

export const ListHeaderFooter = ({
  text,
  headerCustomStyle,
  textCustomStyle,
}: ListHeaderFooterProps) => {
  return (
    <View style={[styles.headerContainer, headerCustomStyle]}>
      <Text style={[styles.headerText, textCustomStyle]}>{text}</Text>
    </View>
  );
};

export const CaughtUpFooter = () => (
  <View style={styles.footerContainer}>
    <Text style={styles.footerText}>You're all caught up</Text>
  </View>
);

const styles = StyleSheet.create({
  headerContainer: {
    height: 98,
    justifyContent: 'center',
  },
  headerText: {
    ...font(15, 20, 0.2),
    color: theme.colors['40Black'],
  },
  footerContainer: {
    paddingHorizontal: 24,
    height: 98,
    justifyContent: 'center',
  },
  footerText: {
    ...font(14, 14),
    color: theme.colors['40Black'],
    textAlign: 'center',
  },
});
