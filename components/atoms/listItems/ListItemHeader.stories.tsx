import type {Meta, StoryObj} from '@storybook/react';

import { ListHeaderFooter } from './ListItems';

const meta = {
  title: 'atoms/ListHeaderFooter',
  component: ListHeaderFooter,
} satisfies Meta<typeof ListHeaderFooter>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    text: 'text'
  },
};
