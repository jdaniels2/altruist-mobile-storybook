import type {Meta, StoryObj} from '@storybook/react';

import { CaughtUpFooter } from './ListItems';

const meta = {
  title: 'atoms/CaughtUpFooter',
  component: CaughtUpFooter,
} satisfies Meta<typeof CaughtUpFooter>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    text: 'text'
  },
};
