import { ViewStyle, PickerProps } from 'react-native';


export type Props = {
  style?: ViewStyle;
  name: string;
  placeholder: string;
  value: string | undefined;
  items: any[] | undefined;
  isFocused: boolean;
  setFocused: (arg1: boolean) => void;
  onValueChange: PickerProps['onValueChange'];
  editable?: boolean;
};
