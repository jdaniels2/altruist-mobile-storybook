import { StyleSheet, Platform } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export const styles = StyleSheet.create({
  iosPicker: {
    marginHorizontal: -30,
    marginBottom: -20,
  },
  placeholder: {
    ...font(16),
    color: theme.colors['70Black'],
  },
  focused: {
    borderColor: theme.colors['Black'],
  },
  text: {
    ...font(18),
    color: theme.colors['Black'],
  },
  textDisabled: {
    color: theme.colors['70Black'],
  },
  input: {
    borderBottomWidth: 1,
    paddingBottom: 7,
    marginTop: 8,
    paddingLeft: Platform.select({ ios: 0, android: 5 }),
    backgroundColor: 'transparent',
    borderColor: theme.colors['10Black'],
  },
});
