import React, { useState, useMemo, useCallback } from 'react';
import { Platform, Pressable, Text, View } from 'react-native';
import Collapsible from 'react-native-collapsible';

import { Props } from '~/components/atoms/PickerField/PickerFieldProps';
import { styles } from '~/components/atoms/PickerField/PickerFieldStyles';
// import CustomPicker from '~/components/molecules/CustomPicker';
import { TOUCHABLE_HIT_SLOP } from '~/constants/constants';

const isAndroid = Platform.OS === 'android';

const PickerField = ({
  style,
  name,
  placeholder,
  value,
  items,
  isFocused,
  setFocused,
  onValueChange,
  editable = true,
}: Props) => {
  const [androidPickerVisible, setAndroidPickerVisible] = useState(false);
  const selectedItem = useMemo(() => items && items.find((x) => x.value === value), [items, value]);

  const onFocus = useCallback(() => {
    setFocused(true);
    isAndroid && setAndroidPickerVisible(true);
  }, [setFocused]);

  return (
    <>
      <Pressable onPress={onFocus} disabled={!editable} hitSlop={TOUCHABLE_HIT_SLOP}>
        {() => {
          return (
            <View style={[styles.input, isFocused && styles.focused, style]}>
              {selectedItem ? (
                <Text style={editable ? styles.text : styles.textDisabled}>
                  {selectedItem && selectedItem.value}
                </Text>
              ) : (
                <Text style={styles.placeholder}>{placeholder}</Text>
              )}
            </View>
          );
        }}
      </Pressable>

      <Collapsible collapsed={!isFocused}>
        {/* <CustomPicker
          title={name}
          selectedValue={value}
          items={items}
          onValueChange={onValueChange}
          androidIsVisible={androidPickerVisible}
          setAndroidIsVisible={setAndroidPickerVisible}
          iosStyle={styles.iosPicker}
        /> */}
        <View>
          <Text>Collapse</Text>
        </View>
      </Collapsible>
    </>
  );
};

export default PickerField;
