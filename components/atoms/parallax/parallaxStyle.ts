import { StyleSheet } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export const parallaxStyles = StyleSheet.create({
  footerContainer: {
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors['Light Gray'],
  },
  footerText: {
    color: theme.colors['40Black'],
    ...font(14, 14),
  },
});
