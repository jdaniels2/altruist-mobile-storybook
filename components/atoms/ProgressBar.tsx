import React from 'react';
import { View, StyleSheet } from 'react-native';

import { theme } from '~/theme/Theme';

type ProgressType = {
  progress?: number;
};
const ProgressBar = ({ progress = 0 }: ProgressType) => {
  return (
    <View style={styles.container}>
      <View style={[styles.progressBar, { flex: progress }]} />
      <View style={{ flex: 1 - progress }} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 4,
    width: '100%',
    flexDirection: 'row',
    borderColor: theme.colors['20Black'],
    borderTopWidth: 1,
    backgroundColor: theme.colors['Light Gray'],
  },
  progressBar: {
    backgroundColor: theme.colors['Green Highlight'],
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
  },
});

export default ProgressBar;
