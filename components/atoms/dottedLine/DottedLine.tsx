import React from 'react';
import { StyleSheet, View } from 'react-native';

import { theme } from '~/theme/Theme';

export type Props = { dashColor?: keyof typeof theme.colors };

const DottedLine = ({ dashColor }: Props) => (
  <View
    style={[styles.dash, { borderColor: dashColor ? theme.colors[dashColor] : undefined }]}
  ></View>
);

const styles = StyleSheet.create({
  dash: {
    borderWidth: 0.5,
    borderStyle: 'dashed',
    borderRadius: 1,
    marginTop: '5%',
  },
});

export default DottedLine;
