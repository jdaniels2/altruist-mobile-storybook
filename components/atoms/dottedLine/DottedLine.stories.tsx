import type {Meta, StoryObj} from '@storybook/react';
import { View } from 'react-native';

import DottedLine from './DottedLine';

const meta = {
  title: 'atoms/DottedLine',
  component: (props: any) => <View style={props.storyBookContainer}><DottedLine {...props}/></View>,
} satisfies Meta<typeof DottedLine>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    dashColor: 'black',
    storyBookContainer: {
      width: '100%'
    }
  },
};
