import { RefObject } from 'react';
import { TextInputProps } from 'react-native';

// used in settings forms
export const renderTitle = (
  defaultValue: string,
  value: string,
  touched: boolean,
  error?: string,
  isValid?: boolean,
  optional?: boolean,
) => {
  if (!value && !touched) return '';

  return optional || isValid ? defaultValue : error;
};

type InputProps = {
  field: string;
  placeholder?: string;
  value?: string;
  touched: boolean;
  error?: boolean;
  formattedValue?: string;
  isValid?: boolean;
};

export const getInputProps = ({
  field,
  placeholder,
  value,
  touched,
  error,
  formattedValue,
  isValid,
}: InputProps) => {
  return {
    name: field,
    value: formattedValue || value,
    error: (touched || !!value) && !isValid && error,
    placeholder: placeholder && !touched && isValid ? placeholder : '',
  };
};

export type AddressItemType = {
  title: string;
  key: string;
  setValues?: (val: string) => string | undefined;
  inputProps: Omit<TextInputProps, 'style'>;
  ref?: RefObject<any>;
};
