import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import InputBox from '~/components/atoms/input/InputBox/InputBox';
import { Props } from '~/components/atoms/input/PercentageInput/PercentageInputProps';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

const PercentageInput = ({
  name,
  percentage,
  onChangeText,
  maxLength = 4,
  accessibilityLabel,
}: Props) => (
  <View style={styles.layout}>
    <View style={styles.container}>
      <Text style={styles.text} numberOfLines={1}>
        {name}
      </Text>

      <InputBox
        accessibilityLabel={accessibilityLabel}
        onChangeText={onChangeText}
        value={percentage}
        keyboardType='numeric'
        placeholder='0'
        placeholderTextColor={theme.colors['Black']}
        suffix='%'
        maxLength={maxLength}
      />
    </View>
  </View>
);

const styles = StyleSheet.create({
  layout: {
    paddingBottom: 32,
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    ...font(16, 16, 0.2),
    flex: 1,
    paddingRight: 16,
    color: theme.colors['Black'],
  },
});

export default PercentageInput;
