import type {Meta, StoryObj} from '@storybook/react';

import PercentageInput from './PercentageInput';

const meta = {
  title: 'atoms/PercentageInput',
  component:PercentageInput,
} satisfies Meta<typeof PercentageInput>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    name: 'name',
    onChangeText: () => {},
    percentage: "1",
    accessibilityLabel: '',
  },
};
