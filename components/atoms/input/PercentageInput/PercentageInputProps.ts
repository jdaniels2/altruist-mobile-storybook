export type Props = {
  name: string;
  percentage: string;
  maxLength?: number;
  onChangeText?: ((text: string) => void) | undefined;
  accessibilityLabel?: string;
};
