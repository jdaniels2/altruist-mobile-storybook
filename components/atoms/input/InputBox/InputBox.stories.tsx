import type {Meta, StoryObj} from '@storybook/react';

import InputBox from './InputBox';

const meta = {
  title: 'atoms/InputBox',
  component:InputBox,
} satisfies Meta<typeof InputBox>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    value: '',
    onChangeText: () => {},
    inputStyle: {},
    error: false,
    accessibilityLabel: '',
  },
};
