import { StyleSheet } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export const styles = StyleSheet.create({
  textInput: {
    color: theme.colors['Black'],
  },
  inputBox: {
    ...font(16, 16, 0.2, '400'),
    color: theme.colors['Black'],
    width: 64,
    height: 40,
    textAlign: 'center',
    borderWidth: 1,
    borderRadius: 3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
