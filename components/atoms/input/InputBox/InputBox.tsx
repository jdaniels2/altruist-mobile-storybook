import React, { useState, useRef, useCallback, LegacyRef } from 'react';
import { Text, TextInput, TouchableOpacity } from 'react-native';

import { Props } from '~/components/atoms/input/InputBorderBottom/InputBorderBottomProps';
import { styles } from '~/components/atoms/input/InputBox/InputBoxStyles';
import { theme } from '~/theme/Theme';

type InputBoxProps = Props & {
  handleBlur?: Function;
  handleFocus?: Function;
  borderColor?: string;
  backgroundColor?: string;
  suffix?: string;
};

const InputBox = ({
  placeholder,
  onChangeText,
  value,
  secureTextEntry,
  autoCapitalize,
  multiline,
  keyboardType,
  numberOfLines,
  maxLength,
  handleBlur,
  returnKeyType,
  handleFocus,
  autoFocus,
  editable = true,
  backgroundColor = '',
  borderColor: customBorderColor,
  placeholderTextColor = theme.colors['40Black'],
  suffix,
  accessibilityLabel,
}: InputBoxProps) => {
  const ref = useRef<{
    focus: Function;
  }>();
  const [borderColor, setBorderColor] = useState(customBorderColor || theme.colors['10Black']);

  const onFocus = useCallback(() => {
    setBorderColor(theme.colors['Black']);
    handleFocus && handleFocus();
  }, [handleFocus]);

  const onBlur = useCallback(() => {
    setBorderColor(customBorderColor || theme.colors['10Black']);
    handleBlur && handleBlur();
  }, [customBorderColor, handleBlur]);

  return (
    <TouchableOpacity
      style={[
        styles.inputBox,
        {
          backgroundColor: backgroundColor ? backgroundColor : theme.colors['White'],
          borderColor: borderColor ? borderColor : theme.colors['White'],
        },
      ]}
      onPress={() => ref?.current?.focus()}
    >
      <TextInput
        style={styles.textInput}
        ref={ref as LegacyRef<TextInput>}
        accessibilityLabel={accessibilityLabel}
        editable={editable}
        onFocus={onFocus}
        onBlur={onBlur}
        value={value}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
        multiline={multiline}
        numberOfLines={numberOfLines}
        autoCapitalize={autoCapitalize}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        keyboardType={keyboardType}
        maxLength={maxLength}
        returnKeyType={returnKeyType}
        autoFocus={autoFocus}
        textContentType='none'
        // in react-native 0.61.5, there is a bug with selectionColor in android.
        // https://github.com/facebook/react-native/issues/22762
        // please follow the link if the cursor color needs to change.
        selectionColor={theme.colors['Blue']}
      />
      {suffix && <Text>{suffix}</Text>}
    </TouchableOpacity>
  );
};

export default React.forwardRef(InputBox);
