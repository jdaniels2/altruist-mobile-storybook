import { Platform, StyleSheet } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export const styles = StyleSheet.create({
  textInput: {
    color: theme.colors['Black'],
  },
  inputContainer: {
    ...font(16),
    borderRadius: 3,
    height: 60,
    justifyContent: 'center',
    borderWidth: 1,
    ...Platform.select({
      ios: {
        paddingHorizontal: 22,
        paddingVertical: 20,
      },
      android: {
        paddingHorizontal: 24,
        paddingVertical: 10,
      },
    }),
    backgroundColor: theme.colors['White'],
  },
  input: {
    ...font(16),
    top: 2,
    color: theme.colors['Black'],
    paddingLeft: 0,
  },
  passwordHideInput: {
    paddingRight: 35,
  },
});
