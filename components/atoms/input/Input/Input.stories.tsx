import type {Meta, StoryObj} from '@storybook/react';

import Input from './input';

const meta = {
  title: 'atoms/Input',
  component:Input,
} satisfies Meta<typeof Input>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    value: '',
    onChangeText: () => {},
    error: false,
    passwordHide: false,
    accessibilityLabel: '',
    defaultValue: 'default'
  },
};
