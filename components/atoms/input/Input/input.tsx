import React, { useCallback, useState } from 'react';
import { StatusBar, TextInput, View } from 'react-native';

import { styles } from '~/components/atoms/input/Input/input-styles';
import { Props } from '~/components/atoms/input/Input/inputProps';
import { TOUCHABLE_HIT_SLOP, STATUS_BAR_STYLE } from '~/constants/constants';
import { theme } from '~/theme/Theme';
import { filterEmojis } from '~/utils/formatter';

const Input = ({ onChangeText, error, passwordHide, ...props }: Props) => {
  const [borderColor, setBorderColor] = useState(theme.colors['White']);

  const onFocus = useCallback(() => {
    StatusBar.setBarStyle(STATUS_BAR_STYLE);
    setBorderColor(theme.colors['Black']);
  }, []);

  const onBlur = useCallback(() => {
    setBorderColor(theme.colors['White']);
  }, []);

  return (
    <View
      style={[
        styles.inputContainer,
        {
          borderColor: error
            ? theme.colors['Orange']
            : borderColor
            ? borderColor
            : theme.colors['White'],
        },
      ]}
    >
      <TextInput
        style={[styles.input, { paddingRight: passwordHide ? 35 : 0 }]}
        hitSlop={TOUCHABLE_HIT_SLOP}
        onChangeText={(value) => onChangeText?.(filterEmojis(value.trim())!)}
        placeholderTextColor={theme.colors['40Black']}
        selectionColor={theme.colors['Blue']}
        onBlur={onBlur}
        onFocus={onFocus}
        {...props}
      />
    </View>
  );
};

export default Input;
