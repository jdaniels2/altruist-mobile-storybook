import React, { useState, useMemo, ForwardedRef, useCallback } from 'react';
import { StatusBar, TextInput, View, Text } from 'react-native';

import { Props } from '~/components/atoms/input/InputBorderBottom/InputBorderBottomProps';
import { styles } from '~/components/atoms/input/InputBorderBottom/InputBorderBottomStyle';
import { STATUS_BAR_STYLE } from '~/constants/constants';
import { theme } from '~/theme/Theme';
import { formatMask } from '~/utils/formatter';

const InputBorderBottom = (
  {
    style,
    inputStyle,
    error,
    passwordHide,
    mask,
    onFocus: onFocusImpl,
    onBlur: onBlurImpl,
    secureTextEntry,
    value,
    maxLength,
    initialIsFocused = false,
    editable = true,
    suffix,
    ...otherProps
  }: Props,
  ref: ForwardedRef<TextInput>,
) => {
  const [isFocused, setIsFocused] = useState(initialIsFocused);

  const onFocus: typeof onFocusImpl = useCallback(
    (event:any) => {
      StatusBar.setBarStyle(STATUS_BAR_STYLE);
      setIsFocused(true);
      onFocusImpl?.(event);
    },
    [onFocusImpl],
  );

  const onBlur: typeof onBlurImpl = useCallback(
    (event:any) => {
      setIsFocused(false);
      onBlurImpl?.(event);
    },
    [onBlurImpl],
  );

  const formattedValue = useMemo(
    () => (secureTextEntry ? value : formatMask(value, mask)),
    [mask, secureTextEntry, value],
  );

  const maskLength = useMemo(
    () => mask?.reduce((r: number, x) => r + (typeof x === 'string' ? x.length : x), 0),
    [mask],
  );
  const inputMaxLength = mask && !secureTextEntry ? maskLength : maxLength;

  const containerStyle = useMemo(() => {
    const result = [suffix && !!value ? styles.suffixContainer : styles.container, style];
    if (error) {
      result.push({ borderColor: theme.colors['Orange'] });
    } else if (isFocused) {
      result.push({ borderColor: theme.colors['Black'] });
    }
    return result;
  }, [error, isFocused, style, suffix, value]);

  const editableInputStyle = editable ? styles.input : styles.inputDisabled;
  const textInputStyle = [
    editableInputStyle,
    passwordHide ? { paddingRight: 35 } : null,
    inputStyle,
  ];
  return (
    <View style={containerStyle}>
      <TextInput
        ref={ref}
        style={textInputStyle}
        textContentType='none'
        onFocus={onFocus}
        onBlur={onBlur}
        value={formattedValue}
        placeholderTextColor={theme.colors['70Black']}
        secureTextEntry={secureTextEntry}
        editable={editable}
        // in react-native 0.61.5, there is a bug with selectionColor in android.
        // https://github.com/facebook/react-native/issues/22762
        // please follow the link if the cursor color needs to change.
        selectionColor={theme.colors['Blue']}
        maxLength={inputMaxLength}
        {...otherProps}
      />
      {!!formattedValue && !!suffix ? <Text style={styles.suffixText}>{suffix}</Text> : null}
    </View>
  );
};

export default React.forwardRef(InputBorderBottom);
