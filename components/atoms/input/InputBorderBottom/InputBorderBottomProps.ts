import { TextInputProps, ViewStyle, StyleProp, TextStyle } from 'react-native';

export type Props = Omit<TextInputProps, 'style'> & {
  style?: ViewStyle;
  inputStyle?: StyleProp<TextStyle>;
  error?: boolean;
  passwordHide?: boolean;
  mask?: Array<string | number>;
  initialIsFocused?: boolean;
  editable?: boolean;
  suffix?: string;
};
