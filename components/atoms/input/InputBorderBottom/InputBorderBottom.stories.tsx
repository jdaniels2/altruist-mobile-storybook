import type {Meta, StoryObj} from '@storybook/react';

import InputBorderBottom from './InputBorderBottom';

const meta = {
  title: 'atoms/InputBorderBottom',
  component:InputBorderBottom,
} satisfies Meta<typeof InputBorderBottom>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    value: '',
    onChangeText: () => {},
    inputStyle: {},
    error: false,
    accessibilityLabel: '',
  },
};
