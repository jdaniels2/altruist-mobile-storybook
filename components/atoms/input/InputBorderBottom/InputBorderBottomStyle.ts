import { StyleSheet, Platform } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

const containerStyle = {
  height: 44,
  paddingRight: 8,
  borderColor: theme.colors['40Black'],
  borderBottomWidth: 1,
};

export const styles = StyleSheet.create({
  container: {
    ...containerStyle,
    justifyContent: 'center',
  },
  input: {
    ...font(16),
    top: 2,
    color: theme.colors['Black'],
    ...Platform.select({
      android: {
        paddingHorizontal: -4,
      },
    }),
    flex: 1,
  },
  inputDisabled: {
    ...font(16),
    top: 2,
    color: theme.colors['70Black'],
  },
  suffixContainer: {
    ...containerStyle,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  suffixText: {
    top: 2,
  },
});
