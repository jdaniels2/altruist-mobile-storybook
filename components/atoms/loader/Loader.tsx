import React from 'react';
import { ViewStyle } from 'react-native';
// @ts-expect-error
import { MaterialIndicator } from 'react-native-indicators';

import { theme } from '~/theme/Theme';

type Props = {
  style?: ViewStyle;
  color?: keyof typeof theme.colors | string;
  size?: number;
  trackWidth?: number;
};

const Loader = ({
  style = undefined,
  color = theme.colors['40Black'],
  size = 50,
  trackWidth = 5,
}: Props) => {
  return (
    <MaterialIndicator
      style={style}
      color={color}
      size={size}
      trackWidth={trackWidth}
      accessibilityLabel='Loader Icon'
    />
  );
};

export default Loader;
