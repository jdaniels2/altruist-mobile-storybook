import type {Meta, StoryObj} from '@storybook/react';

import Loader from './Loader';
import { theme } from '~/theme/Theme';

const meta = {
  title: 'atoms/Loader',
  component: Loader,
} satisfies Meta<typeof Loader>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    style: undefined,
    color: theme.colors['Black'],
    size: 12,
    trackWidth: 2
  },
};
