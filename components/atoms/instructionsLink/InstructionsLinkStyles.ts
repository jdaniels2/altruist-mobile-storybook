import { StyleSheet } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export const styles = StyleSheet.create({
  instructionsContainer: {
    marginTop: 48,
    marginLeft: 32,
    flex: 1,
  },
  instructionsText: {
    ...font(14, 20),
    color: theme.colors['70Black'],
  },
  underlinedText: {
    textDecorationLine: 'underline',
  },
});
