import type {Meta, StoryObj} from '@storybook/react';

import InstructionsLink from './InstructionsLink';

const meta = {
  title: 'atoms/InstructionsLink',
  component: InstructionsLink,
} satisfies Meta<typeof InstructionsLink>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {},
};
