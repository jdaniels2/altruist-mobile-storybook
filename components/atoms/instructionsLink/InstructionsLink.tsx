import React from 'react';
import { Text, View } from 'react-native';

import { styles } from '~/components/atoms/instructionsLink/InstructionsLinkStyles';

const InstructionsLink = () => {

  const handleCheckPress = () => {};

  const handleWirePress = () => {};

  return (
    <View style={styles.instructionsContainer}>
      <Text style={styles.instructionsText}>
        Want to make a deposit by{'\n'}
        <Text
          style={styles.underlinedText}
          onPress={handleCheckPress}
          accessibilityLabel={'View Check Info'}
        >
          check&nbsp;
        </Text>
        or&nbsp;
        <Text
          style={styles.underlinedText}
          onPress={handleWirePress}
          accessibilityLabel={'View Wire Transfer Info'}
        >
          wire transfer
        </Text>
        ?
      </Text>
    </View>
  );
};

export default InstructionsLink;
