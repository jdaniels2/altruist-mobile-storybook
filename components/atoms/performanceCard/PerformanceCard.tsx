import React, { memo } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import DottedLine from '~/components/atoms/dottedLine/DottedLine';
import { rowValueColor } from '~/components/atoms/performanceCard/performanceCardConstants';
import { usePerformanceCard } from '~/components/atoms/performanceCard/usePerformanceCard';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';
import { Summary } from '~/types/graphql';

export type Props = {
  performance: Summary;
  endDate: string | undefined;
  isDark?: boolean;
  isHighYieldSavings?: boolean;
  hasChartData: boolean | undefined;
};

const PerformanceCard = memo(
  ({ performance = {} as Summary, endDate, isDark, isHighYieldSavings, hasChartData }: Props) => {
    const { data, earningSheet, netDepositSheet, apySheet, fdicSheet } = usePerformanceCard({
      performance,
      endDate,
      isHighYieldSavings,
      hasChartData,
    });

    return (
      <View style={styles.container}>
        {earningSheet}
        {netDepositSheet}
        {apySheet}
        {fdicSheet}

        {data?.map((row, i) => {
          if (row.hide) {
            return null;
          }
          return (
            <View
              style={[styles.row, isDark && { borderBottomColor: theme.colors['90Black'] }]}
              key={i}
            >
              <Text style={styles.rowTitle}>{row.title}</Text>
              {/* key={row.value} is a hack to force onLayout for DottedLine */}
              <View key={row.value}>
                <Text
                  style={[
                    styles.rowValue,
                    isDark && { color: theme.colors['White'] },
                    row.rowValueColor && rowValueColor(row.value, isDark),
                  ]}
                  onPress={row.onPress}
                >
                  {typeof row?.value === 'number' ? row.formatValue?.(row?.value) : 'N/A'}
                </Text>

                {row.dottedLine && (
                  <DottedLine
                    dashColor={isDark ? ('40Black' as keyof typeof theme['colors']) : undefined}
                  />
                )}
              </View>
            </View>
          );
        })}
      </View>
    );
  },
);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 32,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 108,
    borderBottomColor: theme.colors['20Black'],
    borderBottomWidth: 1,
  },
  rowValue: {
    ...font(16, 16),
    color: theme.colors['Black'],
    alignSelf: 'flex-end',
  },
  rowTitle: {
    ...font(16, 16, 0.2),
    color: theme.colors['70Black'],
    flex: 1,
  },
});

export default PerformanceCard;
