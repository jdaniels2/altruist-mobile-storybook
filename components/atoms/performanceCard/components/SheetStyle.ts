import { Platform, StyleSheet } from 'react-native';

import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export const styles = StyleSheet.create({
  sheetContainer: {
    backgroundColor: theme.colors['5Black'],
    borderRadius: Platform.OS === 'ios' ? 14 : 0,
    ...Platform.select({
      ios: {
        paddingTop: 32,
        paddingHorizontal: 24,
        paddingBottom: 8,
      },
      android: {
        paddingTop: 10,
        paddingHorizontal: 10,
        paddingBottom: 0,
      },
    }),
  },
  sheetRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 24,
  },
  sheetRowTitle: {
    color: theme.colors['70Black'],
    ...Platform.select({
      ios: {
        ...font(16, 16),
      },
      android: {
        ...font(14, 14),
      },
    }),
  },
  sheetAmount: {
    color: theme.colors['Black'],
    ...Platform.select({
      ios: {
        ...font(16, 16),
      },
      android: {
        ...font(14, 14),
      },
    }),
  },
  fdicText: { color: theme.colors['Black'], ...font(16, 24) },
  fdicLearnMore: {
    color: theme.colors['Black'],
    ...font(16, 24),
    textDecorationLine: 'underline',
    top: Platform.OS === 'android' ? 6 : 2,
  },
});
