import React from 'react';
import { View, Text } from 'react-native';

import { styles } from '~/components/atoms/performanceCard/components/SheetStyle';
import { Summary } from '~/types/graphql';
import { currency } from '~/utils/formatter';

type Props = {
  performance: Summary;
  actionSheetType: 'earnings' | 'netDeposits';
  isHighYieldSavings?: boolean;
};
const PerformanceActionSheet = ({ performance, actionSheetType, isHighYieldSavings }: Props) => (
  <View style={styles.sheetContainer}>
    {actionSheetType === 'earnings' && (
      <>
        {!isHighYieldSavings && (
          <View style={styles.sheetRow}>
            <Text style={styles.sheetRowTitle}>Gains/Losses</Text>

            <Text style={styles.sheetAmount}>{currency(performance.gainLoss)}</Text>
          </View>
        )}

        {!isHighYieldSavings && (
          <View style={styles.sheetRow}>
            <Text style={styles.sheetRowTitle}>Dividends</Text>

            <Text style={styles.sheetAmount}>{currency(performance.dividend)}</Text>
          </View>
        )}

        <View style={styles.sheetRow}>
          <Text style={styles.sheetRowTitle}>Interest</Text>

          <Text style={styles.sheetAmount}>{currency(performance.interest)}</Text>
        </View>

        <View style={styles.sheetRow}>
          <Text style={styles.sheetRowTitle}>Fees</Text>

          <Text style={styles.sheetAmount}>{currency(performance.fee)}</Text>
        </View>

        <View style={styles.sheetRow}>
          <Text style={styles.sheetRowTitle}>Withholdings</Text>

          <Text style={styles.sheetAmount}>{currency(performance.withhold)}</Text>
        </View>
      </>
    )}

    {actionSheetType === 'netDeposits' && (
      <>
        <View style={styles.sheetRow}>
          <Text style={styles.sheetRowTitle}>Deposits</Text>

          <Text style={styles.sheetAmount}>{currency(performance.deposit)}</Text>
        </View>

        <View style={styles.sheetRow}>
          <Text style={styles.sheetRowTitle}>Withdrawals</Text>

          <Text style={styles.sheetAmount}>{currency(performance.withdrawal)}</Text>
        </View>

        <View style={styles.sheetRow}>
          <Text style={styles.sheetRowTitle}>Transfers In</Text>

          <Text style={styles.sheetAmount}>{currency(performance.transferIn)}</Text>
        </View>

        <View style={styles.sheetRow}>
          <Text style={styles.sheetRowTitle}>Transfers Out</Text>

          <Text style={styles.sheetAmount}>{currency(performance.transferOut)}</Text>
        </View>
      </>
    )}
  </View>
);

export default PerformanceActionSheet;
