import { theme } from '~/theme/Theme';
import { round } from '~/utils/ThirdParty/DecimalLight';
import { percent } from '~/utils/formatter';

export const rowValueColor = (value?:number, isDark?: boolean) => {
  const roundedValue = round(value ?? 0, 3);
  if (roundedValue > 0) {
    return { color: theme.colors['Green Highlight'] };
  }
  if (roundedValue < 0) {
    return { color: theme.colors['Orange'] };
  }

  return { color: theme.colors[isDark ? 'White' : 'Black'] };
};

export const formatTimeWeightedReturn = (value: number) => {
  const roundedValue = round(value, 3);

  if (roundedValue === 0) {
    return percent(roundedValue);
  }

  return percent(value);
};
