import moment from 'moment';
import React, { useState, useMemo } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import Alert from '~/components/atoms/alert/Alert';
import { Props } from '~/components/atoms/performanceCard/PerformanceCard';
import PerformanceActionSheet from '~/components/atoms/performanceCard/components/PerformanceActionSheet';
import { styles } from '~/components/atoms/performanceCard/components/SheetStyle';
import { formatTimeWeightedReturn } from '~/components/atoms/performanceCard/performanceCardConstants';
import { DISCLOSURE_LINK } from '~/constants/constants';
import { Summary } from '~/types/graphql';
import { currency, percent } from '~/utils/formatter';
import { openLink } from '~/utils/methods';

export const usePerformanceCard = ({
  performance = {} as Summary,
  endDate,
  isHighYieldSavings,
  hasChartData,
}: Props) => {
  const { timeWeightedReturn, startDate, startBalance, netDeposits, earnings, balance } =
    performance;

  const [earningSheetVisible, setEarningSheetVisible] = useState(false);
  const [netDepositSheetVisible, setNetDepositSheetVisible] = useState(false);
  const [fdicSheetVisible, setFdicSheetVisible] = useState(false);
  const [apySheetVisible, setApySheetVisible] = useState(false);
  const apyInfoData = { apyInfo: .001}

  const earningSheet = useMemo(
    () => (
      <Alert
        visible={earningSheetVisible}
        description={
          <PerformanceActionSheet
            performance={performance}
            actionSheetType={'earnings'}
            isHighYieldSavings={isHighYieldSavings}
          />
        }
        cancelText='Done'
        onCancelPress={() => setEarningSheetVisible(false)}
        iosAsActionSheet={true}
      />
    ),
    [earningSheetVisible, isHighYieldSavings, performance],
  );

  const netDepositSheet = useMemo(
    () => (
      <Alert
        visible={netDepositSheetVisible}
        description={
          <PerformanceActionSheet performance={performance} actionSheetType={'netDeposits'} />
        }
        cancelText='Done'
        onCancelPress={() => setNetDepositSheetVisible(false)}
        iosAsActionSheet={true}
      />
    ),
    [netDepositSheetVisible, performance],
  );

  const fdicSheet = useMemo(
    () => (
      <Alert
        visible={fdicSheetVisible}
        description={
          <View style={styles.sheetContainer}>
            <View style={styles.sheetRow}>
              <Text style={styles.fdicText}>
                FDIC insurance is limited to $250,000 per depositor, per FDIC-insured bank, per
                ownership category and is subject to conditions. Neither Altruist Financial LLC nor
                any of its affiliates are banks.{'\n'} {'\n'}Cash must be deposited with Partner
                Banks to become eligible for FDIC insurance up to $1 million for individual accounts
                / up to $2 million for joint accounts. If cash is held at Partner Banks outside of
                the Altruist Cash program, it can impact the total FDIC coverage amounts.{' '}
                <TouchableOpacity onPress={() => openLink(DISCLOSURE_LINK)}>
                  <Text style={styles.fdicLearnMore}>Learn more</Text>
                </TouchableOpacity>
              </Text>
            </View>
          </View>
        }
        cancelText='Done'
        onCancelPress={() => setFdicSheetVisible(false)}
        iosAsActionSheet={true}
      />
    ),
    [fdicSheetVisible],
  );

  const apySheet = useMemo(
    () => (
      <Alert
        visible={apySheetVisible}
        description={
          <View style={styles.sheetContainer}>
            <View style={styles.sheetRow}>
              <Text style={styles.fdicText}>
                APY is variable and can change. Cash must be deposited with Partner Banks to earn
                interest. Interest is paid into the Altruist Cash account on the last business day
                of the month.
              </Text>
            </View>
          </View>
        }
        cancelText='Done'
        onCancelPress={() => setApySheetVisible(false)}
        iosAsActionSheet={true}
      />
    ),
    [apySheetVisible],
  );

  const data = useMemo(
    () => [
      {
        title: 'Current APY',
        value: apyInfoData?.apyInfo?.apy,
        rowValueColor: true,
        dottedLine: true,
        formatValue: (val: number) => (val ? percent(val, 2) : 'N/A'),
        onPress: () => setApySheetVisible(true),
        hide: !isHighYieldSavings,
      },
      {
        title: 'Return',
        value: timeWeightedReturn ? timeWeightedReturn : undefined,
        formatValue: formatTimeWeightedReturn,
        rowValueColor: true,
        dottedLine: false,
        hide: !!isHighYieldSavings || !hasChartData,
      },
      {
        title: `Start (${moment(startDate).format('MM/DD/YY')})`,
        value: startBalance ? startBalance : 0,
        formatValue: currency,
        hide: !hasChartData,
      },
      {
        title: 'Net Flows',
        value: netDeposits ? netDeposits : 0,
        formatValue: currency,
        onPress: () => setNetDepositSheetVisible(true),
        dottedLine: true,
        hide: !hasChartData,
      },
      {
        title: 'Earnings',
        value: earnings ? earnings : 0,
        formatValue: currency,
        onPress: () => setEarningSheetVisible(true),
        dottedLine: true,
        hide: !hasChartData,
      },
      {
        title: `End (${moment(endDate).format('MM/DD/YY')})`,
        value: balance ? balance : 0,
        formatValue: currency,
        hide: !hasChartData,
      },
    ],
    [
      apyInfoData?.apyInfo?.apy,
      isHighYieldSavings,
      timeWeightedReturn,
      hasChartData,
      startDate,
      startBalance,
      netDeposits,
      earnings,
      endDate,
      balance,
    ],
  );

  return useMemo(
    () => ({
      data,
      apySheet,
      earningSheet,
      netDepositSheet,
      fdicSheet,
      earningSheetVisible,
      netDepositSheetVisible,
      setEarningSheetVisible,
      fdicSheetVisible,
      setFdicSheetVisible,
      apySheetVisible,
      setApySheetVisible,
    }),
    [
      apySheet,
      data,
      earningSheet,
      earningSheetVisible,
      fdicSheet,
      netDepositSheet,
      netDepositSheetVisible,
      setEarningSheetVisible,
      fdicSheetVisible,
      apySheetVisible,
      setApySheetVisible,
      setFdicSheetVisible,
    ],
  );
};
