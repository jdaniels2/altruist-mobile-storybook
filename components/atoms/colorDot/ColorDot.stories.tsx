import type {Meta, StoryObj} from '@storybook/react';

import ColorDot from './ColorDot';
import { theme } from '~/theme/Theme';

const meta = {
  title: 'atoms/ColorDot',
  component: ColorDot,
} satisfies Meta<typeof ColorDot>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    style: {
        backgroundColor: theme.colors['Red']
    }
  },
};
