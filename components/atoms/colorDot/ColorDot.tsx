import React from 'react';
import { StyleSheet, View, ViewStyle } from 'react-native';

import { theme } from '~/theme/Theme';

export type Props = {
  style?: ViewStyle | ViewStyle[];
};

const ColorDot = ({ style }: Props) => {
  return <View style={[styles.circle, style]} />;
};

const styles = StyleSheet.create({
  circle: {
    width: 8,
    height: 8,
    borderRadius: 6,
    backgroundColor: theme.colors['Black'],
  },
});

export default ColorDot;
