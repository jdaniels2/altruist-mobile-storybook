import type {Meta, StoryObj} from '@storybook/react';

import Button, { styles } from './button';

const meta = {
  title: 'atoms/Button',
  component: Button,
} satisfies Meta<typeof Button>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    text: 'Hello World',
    loading: false,
    loadingText: 'loading',
    disabled: false,
    textStyle: styles.text,
    style: styles.container,
    disabledStyle: styles.disabledContainer,
  },
};
