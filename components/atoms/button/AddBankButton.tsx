import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  GestureResponderEvent,
  ViewStyle,
  TextStyle,
} from 'react-native';

import Button from '~/components/atoms/button';
import Loader from '~/components/atoms/loader/Loader';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export type Props = {
  onPress: (event: GestureResponderEvent) => void;
  isTextButton?: boolean;
  text?: string;
  loading?: boolean;
  accessibilityLabel?: string;
  textStyle?: TextStyle;
};

const AddBankButton = ({
  onPress,
  loading,
  text,
  isTextButton = false,
  accessibilityLabel = 'Add Funding Account',
  textStyle,
}: Props) => {
  return isTextButton ? (
    <TouchableOpacity onPress={onPress} accessibilityLabel={accessibilityLabel}>
      {loading ? (
        <Loader size={16} trackWidth={2} style={styles.smallLoader} />
      ) : (
        <Text style={textStyle ?? styles.textButton}>{text ?? 'Add'}</Text>
      )}
    </TouchableOpacity>
  ) : (
    <Button
      text={text ?? 'Add Funding Account'}
      loading={loading}
      style={styles.button}
      onPress={onPress}
      accessibilityLabel={accessibilityLabel}
    />
  );
};

type Styles = {
  button: ViewStyle;
  textButton: TextStyle;
  smallLoader: ViewStyle;
};
const styles = StyleSheet.create<Styles>({
  button: {
    backgroundColor: theme.colors['Blue'],
  },
  textButton: {
    ...font(17, 22),
    paddingRight: 16,
    color: theme.colors['Blue'],
  },
  smallLoader: {
    paddingRight: 20,
  },
});

export default AddBankButton;
