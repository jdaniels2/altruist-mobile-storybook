import React from 'react';
import {
  StyleSheet,
  Text,
  TextStyle,
  ViewStyle,
  TouchableOpacity,
  GestureResponderEvent,
} from 'react-native';

import Loader from '~/components/atoms/loader/Loader';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

export type Props = {
  text: string;
  onPress?: (event: GestureResponderEvent) => void;
  onPressIn?: (event: GestureResponderEvent) => void;
  loading?: boolean;
  loadingText?: string;
  loadingStyle?: ViewStyle;
  disabled?: boolean;
  style?: ViewStyle;
  textStyle?: TextStyle;
  accessibilityLabel?: string;
  disabledStyle?: ViewStyle;
};

const Button = React.memo(
  ({
    text,
    onPress,
    loading,
    loadingText,
    loadingStyle,
    disabled,
    style,
    textStyle,
    accessibilityLabel,
    onPressIn,
    disabledStyle,
  }: Props) => {
    const touchableDisabled = disabled || loading || undefined;

    const loader = () => {
      if (!!loadingText) {
        return (
          <>
            <Text style={[styles.text, textStyle]}>{loadingText}</Text>
            <Loader
              style={styles.loaderNextToText}
              size={32}
              trackWidth={1.8}
              color={theme.colors['White']}
            />
          </>
        );
      } else {
        return <Loader size={24} trackWidth={3.2} color={theme.colors['White']} />;
      }
    };

    return (
      <TouchableOpacity
        style={[
          styles.container,
          style,
          touchableDisabled ? disabledStyle ?? styles.disabledContainer : null,
          loading && loadingStyle,
        ]}
        disabled={touchableDisabled}
        onPress={onPress}
        onPressIn={onPressIn}
        accessibilityLabel={accessibilityLabel}
      >
        {loading ? (
          loader()
        ) : (
          <Text style={[styles.text, textStyle, disabled ? styles.disabledText : null]}>
            {text}
          </Text>
        )}
      </TouchableOpacity>
    );
  },
);

export const styles = StyleSheet.create({
  container: {
    height: 72,
    borderRadius: 2,
    justifyContent: 'center',
    backgroundColor: theme.colors['Green'],
  },
  disabledContainer: {
    backgroundColor: theme.colors['10Black'],
  },
  text: {
    marginTop: 4,
    color: theme.colors['White'],
    textAlign: 'center',
    textDecorationLine: 'underline',
    ...font(16, 16, -0.2, '500'),
  },
  disabledText: {
    color: theme.colors['30Black'],
  },
  loaderNextToText: {
    position: 'absolute',
    alignSelf: 'flex-end',
    right: '10%',
    maxWidth: 32,
  },
});

export default Button;
