import type {Meta, StoryObj} from '@storybook/react';

import ListOptionRowWithCheckBox from './ListOptionRowWithCheckBox';

const meta = {
  title: 'atoms/ListOptionRowWithCheckBox',
  component: ListOptionRowWithCheckBox,
} satisfies Meta<typeof ListOptionRowWithCheckBox>;

export default meta;

type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    text: 'text',
    isSelected: true,
    onPress: () => {},
    iconPrefix: undefined
  },
};
