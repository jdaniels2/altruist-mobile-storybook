import React from 'react';
import { Pressable, StyleSheet, Text, View } from 'react-native';

import CheckBox from '~/components/atoms/checkbox/CheckBox';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

type Props = {
  text: string;
  field?: string;
  isSelected?: boolean;
  onPress: () => void;
  iconPrefix?: () => JSX.Element;
};

const ListOptionRowWithCheckBox = ({ text, isSelected, onPress, iconPrefix }: Props) => {
  return (
    <Pressable
      style={[styles.container, isSelected && styles.containerSelected]}
      disabled={!onPress}
      onPress={onPress}
    >
      <View style={styles.textContainer}>
        <View>
          <Text style={[styles.text, isSelected && styles.textSelected]} numberOfLines={2}>
            {text}
          </Text>
        </View>
        {iconPrefix?.()}
      </View>

      <CheckBox
        style={isSelected ? styles.checkBoxSelected : styles.checkBox}
        value={isSelected}
        onPress={onPress}
        checkColor={'lightGrey'}
      />
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 72,
    paddingHorizontal: 20,
    marginVertical: 8,
    borderRadius: 2,
    backgroundColor: theme.colors['White'],
  },
  textContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '85%',
  },
  text: {
    marginRight: 8,
    ...font(14, 18),
    color: theme.colors['Dark Gray'],
  },
  containerSelected: {
    backgroundColor: theme.colors['Dark Gray'],
  },
  textSelected: {
    ...font(14, 18, undefined, '500'),
    color: theme.colors['White'],
  },
  checkBox: {
    backgroundColor: theme.colors['Light Gray'],
  },
  checkBoxSelected: {
    backgroundColor: theme.colors['85Black'],
  },
});

export default ListOptionRowWithCheckBox;
