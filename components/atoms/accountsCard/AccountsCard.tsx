import React, { useCallback } from 'react';
import { TouchableOpacity, View, Text, Image } from 'react-native';

import { InfoIcon } from '~/assets/images/Icons';
import { styles } from '~/components/atoms/accountsCard/AccountsCardStyles';
import { Institutions } from '~/constants/brokerage';
import { ACCOUNT_STATUS } from '~/constants/constants';
import { ACCOUNT_TYPE_INFOS, AccountTypeInfo } from '~/constants/financialAccounts';
import { theme } from '~/theme/Theme';
import {
  AccountDetails,
  AccountStatusOutput,
  CorpApplicationStatus,
  StateOutput,
} from '~/types/graphql';
import { getAccountCardStatusDescription, getAccountStatusTitle } from '~/utils/financialAccounts';
import { currency } from '~/utils/formatter';

const altruistGreyLogo = require('~/assets/images/accountsProviders/provider-altruist-grey.png');
const altruistLogo = require('~/assets/images/accountsProviders/provider-altruist.png');
const ameritradeLogo = require('~/assets/images/accountsProviders/provider-ameritrade.png');

const LOGO_TO_INSTITUTION = {
  [Institutions.DriveWealth]: altruistLogo,
  [Institutions.ApexCustodian]: altruistLogo,
  [Institutions.TdAmeritrade]: ameritradeLogo,
  [Institutions.Ameritrade]: ameritradeLogo,
};

export type Props = {
  account: AccountDetails & AccountStatusOutput & StateOutput;
  handleAccountPressed: Function;
};

const AccountsCard = React.memo(
  ({ account = {} as Props['account'], handleAccountPressed }: Props) => {
    const status = account?.status || account.state;
    const isActive = account?.status === ACCOUNT_STATUS.ACTIVE;
    const isRestricted = account?.status === ACCOUNT_STATUS.RESTRICTED;
    const companyName = account?.account?.corporationDetails?.corporation?.profile?.companyName;
    const applicationStatus = account?.account?.corporationDetails?.corporation?.applicationStatus;
    const accountType = account.accountType ?? account.account?.accountType ?? '';
    const accountTypeInfo = ACCOUNT_TYPE_INFOS[accountType as AccountTypeInfo]?.displayName ?? '';
    const accountNumber = account?.accountNumber;
    const displayAccountNumber = accountNumber ? `(${accountNumber.slice(-4)})` : '';

    const onPress = useCallback(() => {
      handleAccountPressed(
        account?.financialAccountId,
        account?.accountType,
        status,
        applicationStatus,
        account?.account?.corporationDetails?.corporationId,
      );
    }, [
      account?.account?.corporationDetails?.corporationId,
      account?.accountType,
      account?.financialAccountId,
      applicationStatus,
      handleAccountPressed,
      status,
    ]);

    return (
      <TouchableOpacity
        style={[
          styles.cardContainer,
          {
            backgroundColor:
              isActive || isRestricted ? theme.colors['White'] : theme.colors['5Black'],
          },
        ]}
        activeOpacity={0.8}
        onPress={onPress}
        accessibilityLabel='accountsCardContainer'
      >
        <View style={styles.headerContainer}>
          <Text style={styles.balanceText} accessibilityLabel='accountsCardBalanceText'>
            {getAccountStatusTitle(status, applicationStatus!)}
          </Text>
        </View>

        {isActive && (
          <Text style={styles.balanceValue}>{currency(account?.realTimeBalance || 0)}</Text>
        )}

        <View
          style={isActive ? styles.activeDescriptionContainer : styles.inactiveDescriptionContainer}
        >
          <Text style={styles.descriptionText} numberOfLines={5}>
            {account?.financialAccountName && account?.financialAccountName?.length > 0
              ? account?.financialAccountName
              : companyName
              ? `${companyName} Corporation`
              : `${accountTypeInfo} ${displayAccountNumber}`}
          </Text>
        </View>
        {!isActive && (
          <View style={styles.additionalInfoContainer}>
            <Text style={styles.additionalInfo}>
              {getAccountCardStatusDescription(status, applicationStatus!)}
            </Text>
            {isRestricted && <InfoIcon />}
          </View>
        )}

        <View style={styles.footerContainer}>
          <Image
            style={styles.providerLogo}
            source={isActive ? LOGO_TO_INSTITUTION[account?.institutionName!] : altruistGreyLogo}
          />

          {(applicationStatus === CorpApplicationStatus.UNDER_SECRETARY_REVIEW ||
            applicationStatus === CorpApplicationStatus.PENDING_SECRETARY_REVIEW) && (
            <Text style={styles.resendText}>Resend</Text>
          )}
        </View>
      </TouchableOpacity>
    );
  },
);

export default AccountsCard;
