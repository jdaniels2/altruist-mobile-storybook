import { Dimensions, StyleSheet } from 'react-native';

import { theme } from '~/theme/Theme';
import { font, responsivePixelValue } from '~/theme/utilities';

const screenHeight =
  Dimensions.get('screen').height > 568 ? 448 : Dimensions.get('screen').height / 1.5;

export const styles = StyleSheet.create({
  cardContainer: {
    width: '100%',
    height: screenHeight,
    borderRadius: 12,
    paddingTop: 56,
    paddingHorizontal: responsivePixelValue(40),
    paddingBottom: 53,
    overflow: 'hidden',
  },
  headerContainer: {
    flexDirection: 'row',
    paddingBottom: 24,
    justifyContent: 'space-between',
  },
  balanceText: {
    color: theme.colors['70Black'],
    ...font(14, 14, 0.2),
  },
  balanceValue: {
    ...font(26, 26, 0.5),
    flex: 1,
  },
  activeDescriptionContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    paddingBottom: 56,
  },
  inactiveDescriptionContainer: {
    flex: 1,
  },
  descriptionText: {
    ...font(16, 18, 0.2),
    width: '100%',
    flex: 1,
  },
  additionalInfoContainer: {
    flexDirection: 'row',
  },
  additionalInfo: {
    ...font(12, 22, 0.2, '400'),
    color: theme.colors['70Black'],
    paddingBottom: 20,
    paddingRight: 5,
    flex: 1,
  },
  footerContainer: {
    ...font(16, 26, 0.2),
    borderTopWidth: 1,
    borderTopColor: theme.colors['Light Gray'],
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 23,
  },
  providerLogo: {
    resizeMode: 'contain',
  },
  institutionText: {
    color: theme.colors['40Black'],
    ...font(12, 12, 0.2),
  },
  infoIconContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  resendText: {
    ...font(16, 16, 0.2, '500'),
    color: theme.colors['40Black'],
    textDecorationLine: 'underline',
  },
});
