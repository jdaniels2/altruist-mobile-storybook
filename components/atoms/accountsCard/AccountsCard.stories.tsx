import type {Meta, StoryObj} from '@storybook/react';
import { View } from 'react-native';

import AccountsCard from './AccountsCard';

const meta = {
  title: 'atoms/AccountsCard',
  component: (props: any) =>  <View style={props.storyBookContainer}><AccountsCard {...props}/></View>,
} satisfies Meta<typeof AccountsCard>;

export default meta;

type Story = StoryObj<typeof meta>;
const account:any = {
    realTimeBalance: 100,
    institutionName: 'Altruist',
    financialAccountName: 'Altruist',
    accountType: 'INDIVIDUAL',
    accountNumber: '1111',
    status: 'ACTIVE',
    financialAccountId: '1',
    owners: [],
}
export const Basic: Story = {
  args: {
    account,
    handleAccountPressed: () => {},
    storyBookContainer: { width: '100%'}
  },
};
