import React, { useEffect, useState } from 'react';
import { StyleSheet, Animated, Platform, View } from 'react-native';

import { AktivGroteskMedium } from '~/constants/fonts';
import { theme } from '~/theme/Theme';
import { font } from '~/theme/utilities';

const isIOS = Platform.OS === 'ios';

type Props = {
  showHeader: boolean;
  rightStickyIcon?: JSX.Element;
  title: string;
  duration: number;
  isHome?: boolean;
};
const ScrollHeader: React.FC<Props> = ({
  showHeader,
  rightStickyIcon,
  title,
  duration = 400,
  isHome,
}) => {
  const [headerTitleAnimatedValue] = useState(new Animated.Value(0));
  useEffect(() => {
    const fadeIn = () => {
      headerTitleAnimatedValue.setValue(0);
      Animated.timing(headerTitleAnimatedValue, {
        useNativeDriver: true,
        toValue: 1,
        duration: duration,
      }).start();
    };
    fadeIn();
  }, [duration, headerTitleAnimatedValue]);

  return (
    <View>
      {(showHeader || (!isIOS && !isHome)) && (
        <Animated.Text style={[styles.headerText, { opacity: headerTitleAnimatedValue }]}>
          {title}
        </Animated.Text>
      )}
      {rightStickyIcon}
    </View>
  );
};

const styles = StyleSheet.create({
  headerText: {
    ...Platform.select({
      ios: {
        ...font(17, 22, undefined, undefined, AktivGroteskMedium),
      },
      android: {
        ...font(20, 24, undefined, undefined, AktivGroteskMedium),
      },
    }),
    color: theme.colors['Black'],
  },
});

export default ScrollHeader;
