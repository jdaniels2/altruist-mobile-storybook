module.exports = {
  extends: ['react-app', 'plugin:prettier/recommended'],
  plugins: ['prettier'],
  parser: '@typescript-eslint/parser',
  rules: {
    'no-alert': 'error',
    'no-use-before-define': 'off', // FIX https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-use-before-define.md#how-to-use
    'prettier/prettier': 'error',
    'no-duplicate-imports': 'error',
    'no-console': 'error',
    'no-restricted-imports': [
      'error',
      {
        patterns: ['./*', '../*'],
      },
    ],
    'import/order': [
      'error',
      {
        alphabetize: { order: 'asc' },
        'newlines-between': 'always',
        groups: ['external', 'object'],
        pathGroups: [
          {
            pattern: '~/**',
            group: 'object',
          },
        ],
      },
    ],
  },
  ignorePatterns: ['node_modules/'],
  globals: {
    __DEV__: true,
  },
};
