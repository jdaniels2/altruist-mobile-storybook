import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import OnDevice from './OnDeviceindex'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

function App() {
  return (
    <View style={styles.container}>
      <Text>Open up App.tsx to start working on your app!</Text>
    </View>
  );
}

let AppEntryPoint = App;
// cant get env to load properly, can hardcode to true for now
// const storyBookEnabled = process.env.STORYBOOK_ENABLED
const storyBookEnabled = true;
if (storyBookEnabled) {
  AppEntryPoint = OnDevice
}

export default AppEntryPoint;
