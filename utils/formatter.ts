import moment, { Moment } from 'moment';
import { Platform } from 'react-native';

import { whitelist } from '~/constants/constants';
import { ACCOUNT_TYPE_INFOS } from '~/constants/financialAccounts';
import {
  Institution,
  AllowedAllocation,
  AccountDetails,
  UserProfiles,
  UserInfoOutputQuery,
  AccountClassification,
  PlaidAuthenticationStatus,
} from '~/types/graphql';

const regularCharactersRegex = /[^a-zA-Z0-9 ,-]/g;
const emojiRegex =
  /[\u{1f300}-\u{1f5ff}\u{1f900}-\u{1f9ff}\u{1f600}-\u{1f64f}\u{1f680}-\u{1f6ff}\u{2600}-\u{26ff}\u{2700}-\u{27bf}\u{1f1e6}-\u{1f1ff}\u{1f191}-\u{1f251}\u{1f004}\u{1f0cf}\u{1f170}-\u{1f171}\u{1f17e}-\u{1f17f}\u{1f18e}\u{3030}\u{2b50}\u{2b55}\u{2934}-\u{2935}\u{2b05}-\u{2b07}\u{2b1b}-\u{2b1c}\u{3297}\u{3299}\u{303d}\u{00a9}\u{00ae}\u{2122}\u{23f3}\u{24c2}\u{23e9}-\u{23ef}\u{25b6}\u{23f8}-\u{23fa}]/gu;
// eslint-disable-next-line no-control-regex
const asciiRegex = /[^\x00-\x7F]/g;

export const replaceNotDigitsRegex = /[^0-9]/g;

export const UI_DATE_FORMAT = 'MM/DD/YYYY';

export const formatPhoneNumber = (value?: string) => {
  if (!value) return '';

  // only allows 0-9 inputs
  const currentValue = value.trim().replace(replaceNotDigitsRegex, '');
  const cvLength = currentValue.length;

  // returns: "x", "xx", "xxx"
  if (cvLength < 4) return currentValue;

  // returns: "(xxx)", "(xxx) x", "(xxx) xx", "(xxx) xxx",
  if (cvLength < 7) return `(${currentValue.slice(0, 3)}) ${currentValue.slice(3)}`;

  // returns: "(xxx) xxx-", (xxx) xxx-x", "(xxx) xxx-xx", "(xxx) xxx-xxx", "(xxx) xxx-xxxx"
  return `(${currentValue.slice(0, 3)}) ${currentValue.slice(3, 6)}-${currentValue.slice(6, 10)}`;
};

export const lastFour = (numberString?: string) =>
  numberString ? numberString.substring(numberString.length - 4) : numberString;

export const lastFive = (numberString?: string) =>
  numberString ? numberString.substring(numberString.length - 5) : numberString;

export const capitalize = (words?: any) => {
  if (!words) return '';
  return typeof words === 'string'
    ? words
        .toLowerCase()
        .split(' ')
        .map((w) => {
          if (whitelist[w as keyof typeof whitelist]) {
            return w.toUpperCase();
          }

          return w.substring(0, 1).toUpperCase() + w.substring(1);
        })
        .join(' ')
    : '';
};

export const capitalizeExceptArticles = (words?: any) => {
  if (!words) return '';
  return typeof words === 'string'
    ? words
        .toLowerCase()
        .split(' ')
        .map((w) => {
          if (w === 'a' || w === 'an' || w === 'the' || w === 'or' || w === 'to') return w;
          if (whitelist[w as keyof typeof whitelist]) {
            return w.toUpperCase();
          }

          return w.substring(0, 1).toUpperCase() + w.substring(1);
        })
        .join(' ')
    : '';
};

export const capitalizeNoFormat = (words?: string) => {
  if (!words) return '';
  return typeof words === 'string'
    ? words
        .split(' ')
        .map((w) => {
          return w.substring(0, 1).toUpperCase() + w.substring(1);
        })
        .join(' ')
    : '';
};

export const capitalizeWithWhiteList = (words?: string | null) => {
  if (!words) return '';
  return words
    .replace(/_/g, ' ')
    .split(' ')
    .map((w) => {
      if (whitelist[w.toLowerCase() as keyof typeof whitelist]) {
        return w.toUpperCase();
      }

      return capitalize(w);
    })
    .join(' ');
};

export const getUserInitials = (firstName?: string, lastName?: string) => {
  return `${firstName && firstName.toUpperCase().charAt(0)}${
    lastName && lastName.toUpperCase().charAt(0)
  }`;
};

export const capitalizeFirstLetter = (string?: string | null) =>
  string && string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();

const localization = {
  languageCode: 'en-US',
  countryCode: 'USD',
};

export const currency = (
  number: number,
  minimumFractionDigits: number = 2,
  maximumFractionDigits: number = 2,
) =>
  new Intl.NumberFormat(localization.languageCode, {
    style: 'currency',
    currency: localization.countryCode,
    minimumFractionDigits,
    maximumFractionDigits,
  }).format(number);

export const percent = (number: number, minimumFractionDigits = 2, maximumFractionDigits = 2) =>
  new Intl.NumberFormat(localization.languageCode, {
    style: 'percent',
    minimumFractionDigits,
    maximumFractionDigits,
  }).format(number);

export const decimal = (number: number, minimumFractionDigits = 0, maximumFractionDigits = 2) =>
  new Intl.NumberFormat(localization.languageCode, {
    style: 'decimal',
    minimumFractionDigits,
    maximumFractionDigits,
  }).format(number);

export const getAllocationType = (type: AllowedAllocation) => {
  switch (type) {
    case 'STOCKS':
      return 'Equity';

    case 'BONDS':
      return 'Fixed income';

    case 'CASH':
      return 'Cash held directly';

    case 'FUNDS_CASH':
      return 'Cash within funds';

    case 'FixedIncome':
      return 'Fixed income';

    case 'OTHER':
      return 'Other';

    default:
      return '';
  }
};

export const calculatePercentage = (
  allocations: { value: number }[],
  val: number,
  type: 'string' | 'number' = 'string',
) => {
  const total = sumAllocations(allocations);
  if (type === 'string') {
    return percent(val / total);
  } else {
    return val / total;
  }
};

export const sumAllocations = (allocations: { value: number }[]) =>
  allocations.reduce((acc, val) => {
    return val.value + acc;
  }, 0);

export const financialInstitution = (
  type: Institution | AccountClassification | 'DRIVEWEALTH' | 'APEX_CUSTODIAN' | string,
) => {
  switch (type) {
    case 'DRIVEWEALTH':
    case 'BROKERAGE':
    case 'APEX_CUSTODIAN':
    case 'ALTRUIST_CLEARING':
      return 'Altruist';
    case 'AMERITRADE':
    case 'CUSTODIAN':
      return 'TD Ameritrade';
    case 'SCHWAB':
      return 'Schwab';

    default:
      return type;
  }
};

export const getAccountTypeName = <T extends { accountType?: any }>(account: T) =>
  account?.accountType
    ? ACCOUNT_TYPE_INFOS[
        account?.accountType?.replace(/_/g, ' ') as keyof typeof ACCOUNT_TYPE_INFOS
      ]?.displayName
    : '';

export const getAccountHolderName = (userProfile?: UserProfiles | UserInfoOutputQuery) => {
  const firstName = userProfile?.firstName;
  const lastName = userProfile?.lastName;
  return firstName && lastName ? `${firstName} ${lastName}` : '[Account Holder Name]';
};

export const splitByCapitalLetter = (word: string) => word.split(/(?=[A-Z])/).join(' ');

export const formatAddress = ({
  street,
  state,
  city,
  zipCode,
}: {
  street?: string;
  state?: string;
  city?: string;
  zipCode?: string;
}) => {
  const args = [street, city, state, zipCode];
  const fullAddress = args.reduce((acc, arg) => {
    if (arg) {
      if (!acc) {
        acc = arg;
      } else {
        acc += `, ${arg}`;
      }
    }
    return acc;
  }, '');

  return fullAddress;
};

export const formatMask = (value?: string, mask?: (string | number)[]) => {
  if (!mask || !value) return value;

  const rawValue = value.replace(replaceNotDigitsRegex, '');

  let result = '';
  let symbolsAdded = 0;
  for (const section of mask) {
    if (symbolsAdded >= rawValue.length) return result;

    if (typeof section === 'string') {
      result += section;
    } else {
      const substringToAdd = rawValue.substring(symbolsAdded, symbolsAdded + section);
      result += substringToAdd;
      symbolsAdded += substringToAdd.length;
    }
  }

  return result;
};

export const filterDoubleSpace = (value?: string) => value?.replace(/\s{2,}/g, ' ');
export const filterEmojis = (value?: string) => value?.replace(emojiRegex, '');
export const filterSpecialCharacters = (value?: string, replacement?: string) =>
  filterDoubleSpace(value?.replace(regularCharactersRegex, replacement ?? ''));
export const filterNonNumbers = (value?: string) => value?.replace(replaceNotDigitsRegex, '');
export const filterNonFloatNumbers = (value?: string) => value?.replace(/[^0-9.]/g, '');
export const filterNonAscii = (value?: string) => filterDoubleSpace(value?.replace(asciiRegex, ''));

export const formatName = (name?: string) => {
  if (!name) return '';

  return capitalize(name.toLowerCase().split('_').join(' '));
};

export const isUnknown = (string?: string) => string === 'UNKNOWN' || string == null; // TO DO: update once BE clarifies what indicates a missing field

export const formatAccountNumber = (accountNumber?: string, mask?: string) => {
  if (!accountNumber) return '';
  const isAltruistAccount = accountNumber?.match(/^[A-Z]{2,}.*$/);

  if (isUnknown(accountNumber)) {
    if (mask == null) {
      return '';
    } else {
      return mask;
    }
  }

  return isAltruistAccount
    ? `${accountNumber.substring(2, 4).toUpperCase()}${lastFour(accountNumber)}`
    : lastFour(accountNumber);
};

export const masked = (data: object | string) => {
  if (!__DEV__ && data != null) return '***';

  return typeof data === 'object' ? JSON.stringify(data) : data;
};

export const formatFundingAccountName = ({
  institutionName,
  accountNumber,
  accountType,
  isAbbreviated,
  authenticationStatus,
}: {
  institutionName?: string;
  accountNumber?: string;
  accountType?: string | null;
  isAbbreviated?: boolean;
  authenticationStatus?: PlaidAuthenticationStatus;
}) => {
  let fundingPrefix = institutionName;
  switch (institutionName) {
    case 'DRIVEWEALTH':
    case 'APEX_CUSTODIAN':
      fundingPrefix = '';
      break;
    case 'UNKNOWN':
    case undefined:
      fundingPrefix = 'Bank';
      break;
    default:
      break;
  }

  const isAuthenticationPending =
    authenticationStatus &&
    ['PENDING', 'PENDING_MANUAL', 'PENDING_VERIFICATION'].includes(authenticationStatus);

  const type = accountType ? `${capitalize(accountType)}` : '';
  const unknownBankName = fundingPrefix === 'Bank Account';
  const bankDisplayNumber =
    isAuthenticationPending || authenticationStatus === 'FAILED' || accountNumber == null
      ? ''
      : `(${formatAccountNumber(accountNumber)})`;

  return unknownBankName
    ? `${type} ${fundingPrefix} ${bankDisplayNumber}`
    : `${fundingPrefix}${isAbbreviated ? '' : ' '}${type} ${bankDisplayNumber}`;
};

export const formatRowTitle = (account: AccountDetails) => {
  const platformTruncate = Platform.select({
    default: 24,
    android: 20,
  });
  const truncate = (string: string) =>
    string.length > platformTruncate ? string.substring(0, platformTruncate - 1) + '...' : string;
  const name = account?.financialAccountName;

  return name ? truncate(name) : '';
};

const PHONE_NUMBER_LENGTH = 10;
export function cleanPhoneNumber(
  phoneNumber: string | undefined = '',
  includeCountryCode = false,
  definedCountryCode = '',
) {
  if (!phoneNumber) return undefined;
  const cleanOriginalNumber = phoneNumber.replace(/\D+/g, '');
  const cleanPhoneNumber = cleanOriginalNumber.substring(
    cleanOriginalNumber.length - PHONE_NUMBER_LENGTH,
  );

  if (includeCountryCode && definedCountryCode) {
    return `${definedCountryCode}${cleanPhoneNumber}`;
  } else if (includeCountryCode) {
    const countryCodeFromPhoneNumber = cleanOriginalNumber.substring(
      0,
      cleanOriginalNumber.length - PHONE_NUMBER_LENGTH,
    );
    const countryCode = countryCodeFromPhoneNumber ? `+${countryCodeFromPhoneNumber}` : '+1';
    return `${countryCode}${cleanPhoneNumber}`;
  } else {
    return cleanPhoneNumber;
  }
}

export const formatAccountName = ({
  ownerFirstName,
  ownerLastName,
  accountType,
  accountNumber,
}: AccountDetails) =>
  `${ownerFirstName} ${ownerLastName} ${capitalize(accountType)} (${lastFive(accountNumber!)})`;

type FormatAddressType = {
  addr1?: string;
  state?: string;
  city?: string;
  zip?: string;
};

export const formatMailingAndBusinessAddress = (address?: FormatAddressType) => {
  if (!address) return;
  const { addr1, state, city, zip } = address;

  const args = [addr1, city, state, zip];
  const fullAddress = args.reduce((acc, arg) => {
    if (arg) {
      if (!acc) {
        acc = arg;
      } else {
        acc += `, ${arg}`;
      }
    }
    return acc;
  }, '');

  return fullAddress;
};

type FormatEmployerAddress = {
  streetAddress1?: string;
  streetAddress2?: string;
  state?: string;
  city?: string;
  postalCode?: string;
};

export const formatEmployerEnrolledMailingAddress = (address?: FormatEmployerAddress) => {
  if (!address) return;
  const { streetAddress1, streetAddress2, state, city, postalCode } = address;
  const args = [streetAddress1, streetAddress2, city, state, postalCode];
  const fullAddress = args.reduce((acc, arg) => {
    if (arg) {
      if (!acc) {
        acc = arg;
      } else {
        acc += `, ${arg}`;
      }
    }
    return acc;
  }, '');

  return fullAddress;
};

export const formatOrderStatus = (status: String | undefined, type: String | undefined) => {
  const isOrder = type === 'buy' || type === 'sell';
  if (isOrder && status === 'Complete') {
    return 'Filled';
  }
  if (isOrder && status === 'Failed') {
    return 'Rejected';
  }
  if (isOrder && status === 'PENDING_REVIEW') {
    return 'In Review';
  }
  if ((isOrder && status === 'Submitted') || status === 'Pending Complete') {
    return 'Open';
  }

  return status;
};

export const formatTimestampActivityCard = (
  timestamp: Moment | undefined,
  currentTime: Moment | undefined,
) => {
  if (!timestamp || !currentTime) return '';
  const timeSince = moment(currentTime).diff(timestamp, 'minutes');

  if (timeSince < 1) {
    return 'Now';
  } else if (timeSince < 60) {
    return `${timeSince} min ago`;
  } else if (timeSince < 1440) {
    return `${Math.round(timeSince / 60)} hr ago`;
  } else if (timeSince > 1440 && timestamp.year() === currentTime.year()) {
    return `${timestamp.format('MMM D')}`;
  } else return timestamp.format('ll');
};
