import { Maybe } from 'graphql/jsutils/Maybe';

export const getUniqueValues = <T extends unknown[]>(iterable: T): T => {
  return iterable.filter(onlyUnique) as T;
};

const onlyUnique = (value: unknown, index: number, array: unknown[]) => {
  return array.indexOf(value) === index;
};

export const hasItems = (arr: Readonly<Maybe<unknown[]>>): arr is NonNullable<typeof arr> =>
  !!arr && arr.length > 0;

export const isEmpty = (arr: Readonly<Maybe<unknown[]>>): boolean => !hasItems(arr);
