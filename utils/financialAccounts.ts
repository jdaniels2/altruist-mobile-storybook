import { Institutions } from '~/constants/brokerage';
import { ACCOUNT_STATUS, corpAccounts } from '~/constants/constants';
import { ACCOUNT_TYPE_INFOS, RETIREMENT_ACCOUNT_TYPES } from '~/constants/financialAccounts';
import { AccountDetails, AccountType, CorpApplicationStatus } from '~/types/graphql';

export const getAccountCardStatusDescription = (accountStatus: string, corpStatus?: string) => {
  const isActive = accountStatus === ACCOUNT_STATUS.ACTIVE;
  const isClosed = accountStatus === ACCOUNT_STATUS.CLOSED;
  const needsAction = [ACCOUNT_STATUS.NEEDS_ACTION, null].includes(accountStatus);
  const isCorpPending =
    corpStatus === CorpApplicationStatus.PENDING_SECRETARY_REVIEW ||
    corpStatus === CorpApplicationStatus.UNDER_SECRETARY_REVIEW;

  if (needsAction)
    return 'The account couldn’t open due to technical difficulties. Contact your advisor for details.';
  if (isCorpPending)
    return 'The account is pending certification by the secretary. Once the secretary certifies the application, you’ll be able to open the account.';
  else if (!isActive && !isClosed)
    return 'Your account is being opened and you’ll receive an email when it’s approved and ready to be funded.';
  else if (isClosed) return 'Your account has been closed.';
  else if (!isActive && !isClosed)
    return 'Your account is being opened and you’ll receive an email when it’s approved and ready to be funded.';
};

export const getAccountStatusTitle = (
  accountStatus?: string,
  corpStatus?: CorpApplicationStatus,
) => {
  const status = corpStatus || accountStatus;
  switch (status) {
    case ACCOUNT_STATUS.NEEDS_ACTION:
      return 'Failed';
    case ACCOUNT_STATUS.CLOSED:
      return 'Closed';
    case ACCOUNT_STATUS.ACTIVE:
      return 'Balance';
    case ACCOUNT_STATUS.RESTRICTED:
      return 'Restricted';
    case ACCOUNT_STATUS.REJECTED:
      return 'Rejected';
    case CorpApplicationStatus.PENDING_SECRETARY_REVIEW:
      return 'Pending Review';
    case CorpApplicationStatus.UNDER_SECRETARY_REVIEW:
      return 'Under Secretary Review';
    default: {
      return 'Submitted';
    }
  }
};

export const isAltruist = <
  T extends { institutionName?: AccountDetails['institutionName'] | string | undefined },
>(
  { institutionName }: T = {} as T,
) =>
  institutionName === Institutions.DriveWealth ||
  institutionName === Institutions.ApexCustodian ||
  institutionName === 'ALTRUIST_CLEARING';

export const getAccountListStatusText = (
  isMinorMajorityRestricted: boolean | undefined,
  accountType?: keyof typeof ACCOUNT_TYPE_INFOS,
  accountStatus?: string,
  account?: AccountType,
  hideClosedAccounts?: boolean,
  showDefaultMessage?: boolean,
) => {
  const defaultMessage =
    'You can now add funds to the opened accounts and track your progress from the dashboard. \n \nFor accounts that aren’t opened, contact your advisor for details.';
  if (showDefaultMessage) {
    return defaultMessage;
  }
  if ([ACCOUNT_STATUS.NEEDS_ACTION, null].includes(accountStatus || '')) {
    return 'The account couldn’t open due to technical difficulties. Contact your advisor for details.';
  }
  if (
    RETIREMENT_ACCOUNT_TYPES.has(accountType as any) &&
    account?.beneficiaries &&
    account?.beneficiaries.length > 0
  ) {
    return 'Your beneficiary has been added and your account is being opened. This usually takes a minute or two if no additional information is needed. If submitted after 7 PM ET, the request will process tomorrow. \n \nYou’ll receive an email when it’s approved and ready to be funded.';
  } else if (!hideClosedAccounts && accountStatus === ACCOUNT_STATUS.CLOSED) {
    return 'Your account has been closed';
  } else if (
    accountType &&
    corpAccounts.includes(accountType) &&
    (accountStatus === ACCOUNT_STATUS.PENDING || accountStatus === ACCOUNT_STATUS.REJECTED)
  ) {
    return;
  } else if ((accountType === 'UTMA' || accountType === 'UGMA') && isMinorMajorityRestricted) {
    return 'Transfers with this account have been disabled due to the age of the minor on this account.';
  } else if (accountType && (accountType === 'TRUST' || corpAccounts.includes(accountType))) {
    return 'Your account has been submitted and is under further review. \n \nYou will receive an email once the account is approved and ready to fund.';
  } else if (
    accountType &&
    !corpAccounts.includes(accountType) &&
    accountType !== ('TRUST' as keyof typeof ACCOUNT_TYPE_INFOS) &&
    accountStatus === ACCOUNT_STATUS.RESTRICTED
  ) {
    return 'Your account has been submitted and is under further review. \n \nYou will receive an email once the account is approved and ready to fund.';
  } else {
    return defaultMessage;
  }
};
