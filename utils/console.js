const END_TAG = '\x1b[0m';

export const color = {
  red: '\x1b[31m',
  blue: '\x1b[34m',
  black: '\x1b[30m',
  green: '\x1b[32m',
  yellow: '\x1b[33m',
  magenta: '\x1b[35m',
  cyan: '\x1b[36m',
};

export const colored = (color, string) => (__DEV__ ? `${color}${string}${END_TAG}` : string);
