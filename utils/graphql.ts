type Item = {
  __typename?: string;
  [key: string]: any;
};

export const removeAllTypenames = (source?: Item | [Item]) => {
  if (!source) return;

  if (Array.isArray(source)) {
    for (const item of source) {
      removeAllTypenames(item);
    }
  } else if (typeof source === 'object') {
    for (const key in source) {
      if (key === '__typename') {
        delete source.__typename;
      } else {
        removeAllTypenames(source[key]);
      }
    }
  }
};

export const removeAllTypenamesNoMutate = (item?: Item) => {
  if (!item) return;

  const recurse = (source: Item, obj: Item) => {
    if (!source) return;

    if (Array.isArray(source)) {
      for (let i = 0; i < source.length; i++) {
        const item = source[i];
        if (item !== undefined && item !== null && item !== '') {
          source[i] = recurse(item, item);
        }
      }
      return obj;
    } else if (typeof source === 'object') {
      for (const key in source) {
        if (key === '__typename') continue;
        const property = source[key];
        if (Array.isArray(property)) {
          obj[key] = recurse(property, property);
        } else if (!!property && typeof property === 'object') {
          const { __typename, ...rest } = property;
          obj[key] = recurse(rest, rest);
        } else {
          obj[key] = property;
        }
      }
      const { __typename, ...rest } = obj;

      return rest;
    } else {
      return obj;
    }
  };

  return recurse(JSON.parse(JSON.stringify(item)), {});
};
