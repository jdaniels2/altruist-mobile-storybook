import { FundingAccount } from '~/types/graphql';

export const showRelink = (fundingAccount: FundingAccount) =>
  fundingAccount?.verificationStatus === 'CANCELED' ||
  fundingAccount?.verificationStatus === 'REJECTED' ||
  fundingAccount?.authenticationStatus === 'FAILED';
