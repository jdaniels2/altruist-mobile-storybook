import { getDeviceId } from 'react-native-device-info';

const deviceId = getDeviceId();

const iphonesWithNotch = [
  'iPhone10,3',
  'iPhone11,2',
  'iPhone11,4',
  'iPhone11,6',
  'iPhone11,8',
  'iPhone12,1',
  'iPhone12,3',
  'iPhone12,5',
  'iPhone12,8',
  'iPhone13,1',
  'iPhone13,2',
  'iPhone13,3',
  'iPhone13,4',
  'iPhone14,2',
  'iPhone14,3',
  'iPhone14,4',
  'iPhone14,5',
  'iPhone14,6',
  'iPhone14,7',
  'iPhone14,8',
  'iPhone15,1',
  'iPhone15,2',
  'iPhone15,3',
  'iPhone15,4',
  'iPhone15,5',
  'iPhone15,6',
  'iPhone15,7',
  'iPhone15,8',
  'iPhone15,9',
];

export const isIphoneWithNotch = () => {
  return iphonesWithNotch.includes(deviceId);
};
