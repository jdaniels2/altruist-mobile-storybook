import { useCallback, useEffect, useReducer, useRef, useState } from 'react';

export const DEBOUNCE_TIMEOUT_MS = 200;

const forceUpdateReducer = (x) => x + 1;

export const useForceUpdate = () => {
  const [, forceUpdate] = useReducer(forceUpdateReducer, 0);
  return forceUpdate;
};

export const useLastNotNull = (value) => {
  const lastNotNull = useRef(value);

  if (value == null) {
    return lastNotNull.current;
  }

  lastNotNull.current = value;
  return value;
};

export const useDebouncedByKey = (fn) => {
  const [timeouts] = useState({});

  const fnDebounced = useCallback(
    (key, ...args) => {
      if (__DEV__ && typeof key !== 'string' && typeof key !== 'number')
        throw new Error(`key must be a string or a number, but got ${key}`);

      clearTimeout(timeouts[key]);
      timeouts[key] = setTimeout(fn, DEBOUNCE_TIMEOUT_MS, ...args);
    },
    [fn, timeouts],
  );

  return fnDebounced;
};

/**
 * Calls callback on the next render after returned function was called.
 * Used to fix issue when callbacks are invalidated after state was mutated.
 */
export const useNextUpdateCallback = (callback) => {
  // eslint-disable-next-line no-unused-vars
  const forceUpdate = useForceUpdate();
  const shouldCallRef = useRef(false); // ref is used not to rerender after callback is called

  useEffect(() => {
    if (!shouldCallRef.current) return;

    callback();
    shouldCallRef.current = false;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [shouldCallRef.current]);

  return useCallback(() => {
    shouldCallRef.current = true;
    forceUpdate();
  }, [forceUpdate]);
};

export const usePrevious = (value) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref.current;
};
