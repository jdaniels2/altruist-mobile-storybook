import React, { useContext } from 'react';

import { flowContexts } from '~/constants/context/flows';

export const switchDraftValue = (draft, fieldInfo, enabled) => {
  const hasField = draft.hasOwnProperty(fieldInfo.key);
  if (enabled) {
    if (!hasField) {
      draft[fieldInfo.key] = fieldInfo.initialValue;
    }
  } else {
    if (hasField) {
      delete draft[fieldInfo.key];
    }
  }
};

export const connectToContext = (Component) => {
  return ({ navigation, route }) => {
    const { field, contextName, fromSettings, ...otherParams } = route?.params ?? {};
    const contextData = useContext(flowContexts[contextName]);
    const { draft, validFields, changeDraftValue: changeDraftValueImpl, fieldInfos } = contextData;
    const isBeneficiary = contextName === 'BeneficiaryContext';
    const changeDraftValue = isBeneficiary
      ? (key, value) => changeDraftValueImpl({ [key]: value }) // TODO make the same for beneficiary flow
      : changeDraftValueImpl;

    return (
      <Component
        {...contextData}
        {...otherParams}
        {...{
          navigation,
          contextName,
          fromSettings: fromSettings || contextData.fromSettings,
          isBeneficiary,
          changeDraftValue,
        }}
        {...(field && {
          value: draft[field],
          fieldInfo: fieldInfos[field],
          isValid: validFields
            ? validFields.has(field)
            : fieldInfos[field].validate(draft[field], draft),
        })}
      />
    );
  };
};

export const connectToAccountOpeningContext = (Component) => {
  return ({ navigation, route }) => {
    const contextData = useContext(flowContexts.AccountOpeningContext);
    return (
      <Component
        {...contextData}
        {...{
          navigation,
          route,
        }}
      />
    );
  };
};

export const connectToAccountTypeMultiAccountContext = (Component) => {
  return ({ navigation, route }) => {
    const { contextName } = route?.params ?? {};

    const contextData = useContext(flowContexts[contextName]);

    return (
      <Component
        {...contextData}
        {...{
          navigation,
          route,
          isMultiAccountOpening: true,
        }}
      />
    );
  };
};
