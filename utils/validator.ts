import { CITY_MAX_CHARACTERS } from '~/constants/constants';
import { AccountDetails } from '~/types/graphql';

export const emailRegex = /^[^@\s]+@[^@\s.]+\.[^@\s.]+$/g;
export const oneCapitalLetter = /^(?=.*[A-Z])/;
export const oneLowerCaseLetter = /^(?=.*[a-z])/;
export const oneNumber = /^(?=.*[0-9])/;
export const oneSpecialCharacter = /^(?=.*[!@#$%^&*+=_()-])/;

export const validateEmail = (email?: string) => {
  return email?.match(emailRegex);
};

export const addressValidation = {
  addr1: {
    validation: (val?: string, maxLength = 30) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 2) return false;
      if (val.length > maxLength) return false;
      return true;
    },
    error: (val?: string) => {
      if (!val) return 'Street required';
      if (val.length < 2) return 'At least 2 characters';
      return 'No more than 30 characters';
    },
  },
  city: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 2) return false;
      if (val.length > CITY_MAX_CHARACTERS) return false;
      return true;
    },
    error: (val?: string) => {
      if (!val) return 'City required';
      if (val.length < 2) return 'At least 2 characters';
      return `No more than ${CITY_MAX_CHARACTERS} characters`;
    },
  },
  state: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 2) return false;
      return true;
    },
    error: (val?: string) => {
      if (!val) return 'State required';
      return 'At least 2 characters';
    },
  },
  country: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 2) return false;
      return true;
    },
    error: (val?: string) => {
      if (!val) return 'Country required';
      return 'At least 2 characters';
    },
  },
  zipCode: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      return val.length === 5;
    },
    error: (val?: string) => {
      if (!val) return 'Zip code required';
      return 'Invalid Zip';
    },
  },
};

export const personalFormValidation: {
  [field: string]: {
    validation: (value?: string) => boolean | RegExpMatchArray | null | undefined;
    error: (value?: string) => string | undefined;
  };
} = {
  firstName: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 2) return false;
      return true;
    },
    error: (val?: string) => {
      if (!val) return 'First name required';
      return 'At least 2 characters';
    },
  },
  lastName: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 2) return false;
      return true;
    },
    error: (val?: string) => {
      if (!val) return 'Last name required';
      return 'At least 2 characters';
    },
  },
  phone: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 10) return false;
      if (val.length > 10) return false;
      return true;
    },
    error: (val?: string) => {
      if (!val || val.length > 10) return 'Invalid phone number';
      return 'At least 10 characters';
    },
  },
  email: {
    validation: (val?: string) => validateEmail(val),
    error: (val?: string) => {
      if (!val) return 'Invalid email';
      return 'Email required';
    },
  },
  username: {
    validation: (val?: string) => validateEmail(val),
    error: (val?: string) => {
      if (!val) return 'Invalid email';
      return 'Email required';
    },
  },
};

export const passwordValidation = {
  currentPassword: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 8) return false;
      return true;
    },
    error: (val?: string) => {
      if (!val) return 'required';
      if (val.length < 8) return 'Min 8 characters';
    },
  },
  newPassword: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 8) return false;
      if (
        val.match(oneCapitalLetter) &&
        val.match(oneLowerCaseLetter) &&
        val.match(oneNumber) &&
        val.match(oneSpecialCharacter)
      ) {
        return true;
      }
    },
    error: (val?: string) => {
      if (!val) return 'required';
      if (!val.match(oneCapitalLetter)) return 'One capital letter';
      if (!val.match(oneLowerCaseLetter)) return 'One lowercase character';
      if (!val.match(oneNumber)) return 'One number';
      if (!val.match(oneSpecialCharacter)) return 'One special character';
      if (val.length < 8) return 'Min 8 characters';
      return 'required';
    },
  },
  confirmPassword: {
    validation: (confirmPassword?: string, newPassword?: string) => {
      if (!confirmPassword) return false;
      if (confirmPassword?.trim().length === 0) return false;
      if (confirmPassword.charAt(0) === ' ') return false;
      if (confirmPassword.length < 8) return false;
      if (confirmPassword === newPassword) {
        return true;
      }
    },
    error: (confirmPassword?: string, newPassword?: string) => {
      if (!confirmPassword) return 'required';
      if (!confirmPassword?.match(oneCapitalLetter)) return 'One capital letter';
      if (!confirmPassword?.match(oneLowerCaseLetter)) return 'One lowercase character';
      if (!confirmPassword?.match(oneNumber)) return 'One number';
      if (!confirmPassword?.match(oneSpecialCharacter)) return 'One special character';
      if (confirmPassword?.length < 8) return 'Min 8 characters';
      if (confirmPassword !== newPassword) return 'Both passwords must match';
    },
    errors: (confirmPassword?: string, newPassword?: string) => {
      const errors = [];
      if (!newPassword && !confirmPassword)
        return [
          'One capital letter',
          'One lowercase character',
          'One number',
          'One special character',
          'Min 8 characters',
          'Both passwords must match',
        ];
      if (!newPassword?.match(oneCapitalLetter)) {
        errors.push('One capital letter');
      }
      if (!newPassword?.match(oneLowerCaseLetter)) {
        errors.push('One lowercase character');
      }
      if (!newPassword?.match(oneNumber)) {
        errors.push('One number');
      }
      if (!newPassword?.match(oneSpecialCharacter)) {
        errors.push('One special character');
      }
      if (newPassword && newPassword?.length < 8) {
        errors.push('Min 8 characters');
      }
      if (confirmPassword !== newPassword) {
        errors.push('Both passwords must match');
      }

      return errors;
    },
  },
};

export const businessAndMailingAddressValidation = {
  addr1: {
    validation: (val?: string, maxLength = 30) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 2) return false;
      if (val.length > maxLength) return false;
      return true;
    },
    error: (val?: string) => {
      if (!val) return 'Street required';
      if (val.length < 2) return 'At least 2 characters';
      return 'No more than 30 characters';
    },
  },
  city: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 2) return false;
      if (val.length > CITY_MAX_CHARACTERS) return false;
      return true;
    },
    error: (val?: string) => {
      if (!val) return 'City required';
      if (val.length < 2) return 'At least 2 characters';
      return `No more than ${CITY_MAX_CHARACTERS} characters`;
    },
  },
  state: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      if (val.length < 2) return false;
      return true;
    },
    error: (val?: string) => {
      if (!val) return 'State required';
      return 'At least 2 characters';
    },
  },
  zipCode: {
    validation: (val?: string) => {
      if (!val) return false;
      if (val?.trim().length === 0) return false;
      if (val.charAt(0) === ' ') return false;
      return val.length === 5;
    },
    error: (val?: string) => {
      if (!val) return 'Zip code required';
      return 'Invalid Zip';
    },
  },
};

export const isValidAddress = (draft?: any, addressMaxLengths = {} as Record<string, number>) => {
  if (!draft) return false;
  return ['street', 'city', 'state', 'zipCode'].every((key) => {
    if (key === 'street') {
      return addressValidation.addr1.validation(draft[key], addressMaxLengths['address']);
    }
    return addressValidation[key as keyof typeof addressValidation].validation(draft[key]);
  });
};

export const isBusinesOrMailingAddressValid = (
  draft?: any,
  addressMaxLengths = {} as Record<string, number>,
) => {
  if (!draft) return false;
  return ['addr1', 'city', 'state', 'zipCode'].every((key) => {
    let addressKey = key;
    if (key === 'addr1') {
      addressKey = 'address';
    }
    return businessAndMailingAddressValidation[
      key as keyof typeof businessAndMailingAddressValidation
    ].validation(draft[key], addressMaxLengths[addressKey]);
  });
};

export const isValidPersonalForm = (values?: any) => {
  return Object.keys(personalFormValidation).every((key) =>
    personalFormValidation[key as keyof typeof personalFormValidation].validation(values[key]),
  );
};

export const isValidPasswordForm = (values?: any) => {
  return Object.keys(passwordValidation).every((key) =>
    passwordValidation[key as keyof typeof passwordValidation].validation(
      values[key],
      values.newPassword,
    ),
  );
};

export const hasValidAccounts = (accounts: AccountDetails[] | undefined) =>
  accounts?.some(isValidAccount);

export const isValidAccount = (account: AccountDetails | undefined) =>
  !!account?.financialAccountId;
