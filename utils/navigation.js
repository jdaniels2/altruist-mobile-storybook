export const getLastRoute = (state) => {
  const lastRoute =
    state.type === 'tab' ? state.routes[state.index] : state.routes[state.routes.length - 1];
  if (lastRoute.state) {
    return getLastRoute(lastRoute.state);
  }
  return lastRoute;
};
