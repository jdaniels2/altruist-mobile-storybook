import { PermissionsAndroid } from 'react-native';

export const requestPermissions = async () => {
  try {
    const granted = await PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
    ]);

    return Object.values(granted).every((val) => val === PermissionsAndroid.RESULTS.GRANTED);
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err);
  }
};
