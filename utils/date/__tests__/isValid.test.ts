import moment from 'moment';

import { JAN_01_2021_UTC_ISO } from '~/utils/date/dateFormats';
import { isValid } from '~/utils/date/isValid';

describe('isValid', () => {
  it('should default to false', () => {
    expect(isValid('')).toBe(false);
  });

  it('should allow strings', () => {
    expect(isValid(JAN_01_2021_UTC_ISO)).toBe(true);
  });

  it('should allow objects', () => {
    expect(isValid([2021, 0, 1])).toBe(true);
    expect(isValid(moment([2021, 0, 1]))).toBe(true);
    expect(isValid(new Date())).toBe(true);
  });
});
