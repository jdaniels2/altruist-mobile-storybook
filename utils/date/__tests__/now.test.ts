import { now } from '~/utils/date';

jest.useFakeTimers('modern'); // TODO: This becomes default in jest v27 and higher.

describe(now, () => {
  it('returns the local date time in ISO format', () => {
    expect(now()).toBe('2023-01-01T08:00:00.000Z');
  });
});

const MOCK_TIME = new Date(2023, 0, 1, 0, 0, 0, 0);
jest.setSystemTime(MOCK_TIME);
