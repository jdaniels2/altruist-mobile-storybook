import { format, now } from '~/utils/date';

jest.useFakeTimers('modern'); // TODO: This becomes default in jest v27 and higher.

describe(format, () => {
  it('returns the specified format', () => {
    expect(format(now(), DEFAULT_FORMAT)).toBe('2023-01-01');
  });

  it.each([undefined, null, '', 'invalid date format'])(
    'when called with [%s], returns the default value',
    (value) => {
      // NOTE: Warning is expected.
      expect(format(value, DEFAULT_FORMAT, 12345)).toBe(12345);
    },
  );
});

const DEFAULT_FORMAT = 'YYYY-MM-DD';

const MOCK_TIME = new Date(2023, 0, 1, 0, 0, 0, 0);
jest.setSystemTime(MOCK_TIME);
