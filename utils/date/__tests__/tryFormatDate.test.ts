import moment from 'moment';

import {
  ISO_DATE_FORMAT,
  DEFAULT_DATE_FORMAT,
  JAN_01_2021_ARRAY,
  JAN_01_2021_UTC_ISO,
  NOT_APPLICABLE,
  JAN_01_2021_ISO,
  tryFormatDate,
  UNIT_OF_TIME,
  formatDate,
} from '~/utils/date';

const timeOffsetGranularity = UNIT_OF_TIME.SECONDS;
const timeOffset = moment(JAN_01_2021_UTC_ISO).diff(JAN_01_2021_ISO, timeOffsetGranularity);

describe('tryFormatDate', () => {
  it(`should default to "${NOT_APPLICABLE}"`, () => {
    expect(tryFormatDate('')).toBe(NOT_APPLICABLE);
  });

  it(`should default format to ${DEFAULT_DATE_FORMAT}`, () => {
    expect(tryFormatDate(JAN_01_2021_ARRAY)).toBe('01/01/2021');
  });

  it('should convert UTC string to Local in local mode', () => {
    const date = JAN_01_2021_UTC_ISO;
    const dateFormat = 'MM/DD/YYYY HH:mm:ss.SSS';

    const expected = moment.utc(date).add(timeOffset, timeOffsetGranularity).format(dateFormat);

    const actual = tryFormatDate(date, dateFormat);

    expect(actual).toBe(expected);
  });

  it('should print out the utc iso date format', () => {
    const utcDate = moment.utc(JAN_01_2021_ARRAY);
    const actual = tryFormatDate(utcDate, ISO_DATE_FORMAT);
    expect(actual).toBe(JAN_01_2021_UTC_ISO);
  });

  it('should print out the local iso date format', () => {
    const localDate = moment(JAN_01_2021_ARRAY);
    const actual = tryFormatDate(localDate, ISO_DATE_FORMAT);
    expect(actual).toBe(JAN_01_2021_ISO);
  });

  it(`should throw an error if formatting a UTC date without ${ISO_DATE_FORMAT}`, () => {
    const utcDate = moment.utc(JAN_01_2021_UTC_ISO);
    const action = () => formatDate(utcDate);
    expect(action).toThrowError();
  });
});
