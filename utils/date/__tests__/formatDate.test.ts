import { formatDate, tryFormatDate, JAN_01_2021_ARRAY } from '~/utils/date';

describe('formatDate', () => {
  it('should default to an empty string', () => {
    expect(formatDate('')).toBe('');
  });

  it(`should match tryFormat`, () => {
    expect(formatDate(JAN_01_2021_ARRAY)).toBe('01/01/2021');
    expect(tryFormatDate(JAN_01_2021_ARRAY)).toBe('01/01/2021');
  });
});
