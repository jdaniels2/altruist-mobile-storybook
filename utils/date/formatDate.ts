import { Moment } from 'moment';

import { DEFAULT_DATE_FORMAT, DateType, tryFormatDate } from '~/utils/date';

export const formatDate = (date: DateType | Moment | number[], format = DEFAULT_DATE_FORMAT) =>
  tryFormatDate(date, format, '');
