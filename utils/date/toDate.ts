import moment from 'moment';

import { DateString, DateType } from '~/utils/date/DateType';

/** WARNING: DateString is not meant to be used as display text. If you want to format the date, use the `format` function. */
export const toDate = (date?: DateType): DateString => moment(date).toISOString();
