import moment, { Moment } from 'moment';

import { ISO_DATE_FORMAT, DEFAULT_DATE_FORMAT, DateType, isValid } from '~/utils/date';

export const tryFormatDate = (
  date: DateType | Moment | number[],
  format = DEFAULT_DATE_FORMAT,
  defaultValue = 'N/A',
) => {
  if (!isValid(date)) {
    return defaultValue;
  }

  const localDate = moment(date);
  const isUTCMode = localDate.isUTC();
  if (format === ISO_DATE_FORMAT) {
    return isUTCMode ? localDate.toISOString() : localDate.toISOString(true).substring(0, 23);
  } else if (isUTCMode) {
    throw new Error(`You must use ${ISO_DATE_FORMAT} when the date is in UTC mode.`);
  }

  return localDate.format(format);
};
