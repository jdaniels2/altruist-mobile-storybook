import moment, { Moment } from 'moment';

import { DateType } from '~/utils/date';

export const isValid = (date: DateType | Moment | number[]) => !!date && moment(date).isValid();
