import moment from 'moment';

import { DateType, UNIT_OF_TIME } from '~/utils/date';

export const add = (date: DateType, amount: number, unit = UNIT_OF_TIME.SECONDS) => {
  return moment(date).add(amount, unit);
};
