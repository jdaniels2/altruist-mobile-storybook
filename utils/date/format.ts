import moment from 'moment';

import { DateType } from '~/utils/date';

export const format = <R = undefined>(
  date: Maybe<DateType>,
  format?: string,
  defaultValue?: R,
): string | R => {
  if (!date) {
    return defaultValue as R;
  }

  const momentDate = moment(date);
  if (!momentDate.isValid()) {
    return defaultValue as R;
  }

  return momentDate.format(format);
};
