import { toDate } from '~/utils/date';
import { DateString } from '~/utils/date/DateType';

/** WARNING: DateString is not meant to be used as display text. If you want to format the date, use the `format` function. */
export const now = (): DateString => toDate();
