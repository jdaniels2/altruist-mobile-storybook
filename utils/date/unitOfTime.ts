export const UNIT_OF_TIME: Record<string, TimeUnit> = {
  YEARS: 'years',
  QUARTER: 'quarter',
  MONTHS: 'months',
  WEEK: 'week',
  DAYS: 'days',
  HOURS: 'hours',
  MINUTES: 'minutes',
  SECONDS: 'seconds',
  MILLISECONDS: 'milliseconds',
};

export type TimeUnit =
  | 'years'
  | 'quarter'
  | 'months'
  | 'week'
  | 'days'
  | 'hours'
  | 'minutes'
  | 'seconds'
  | 'milliseconds';
