/**
 * DateString is meant to represent a date.
 * String will be used to represent a formatted date.
 *
 * @example
 * ```
 * const toDate = (...): DateString => {...}
 * const format = (...): string => {...}
 * ```
 *
 * TypeScript will see it all as a string. But for the developers, it's a cue to treat one as a date and the other as a string.
 */
export type DateString = string;

/** DateType is meant to represent a date. */
export type DateType = Date | DateString;
