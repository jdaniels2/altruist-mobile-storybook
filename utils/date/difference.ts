import moment from 'moment';

import { TimeUnit } from '~/utils/date/unitOfTime';

export const difference = (from: Date, to: Date, unit: TimeUnit) => moment(to).diff(from, unit);
