export const ISO_DATE_FORMAT = 'ISO_DATE_FORMAT';
export const DEFAULT_DATE_FORMAT = 'MM/DD/YYYY';
export const JAN_01_2021_ARRAY = [2021, 0, 1, 0, 0, 0, 0];
export const JAN_01_2021_UTC_ISO = '2021-01-01T00:00:00.000Z';
export const NOT_APPLICABLE = 'N/A';
export const JAN_01_2021_ISO = '2021-01-01T00:00:00.000';
export const DATE_ONLY_FORMAT = 'YYYY-MM-DD';
