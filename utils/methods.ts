import { Alert, Linking } from 'react-native';

import { Allocation, AllowedAllocation, DataPoints } from '~/types/graphql';
import { getAllocationType } from '~/utils/formatter';

export const logAndAwait = async (name: string, promise: Promise<any>) => {
  // eslint-disable-next-line no-console
  console.log((__DEV__ ? '>>> ' : '') + name);
  const result = await promise;
  // eslint-disable-next-line no-console
  __DEV__ && console.log('<<< ' + name, result);
  return result;
};

export const tryCatch = async (
  tryFuncOrPromise: Function | Promise<any>,
  catchFunc?: Function,
  finallyFunc?: Function,
) => {
  try {
    await (typeof tryFuncOrPromise === 'function'
      ? Promise.resolve(tryFuncOrPromise())
      : tryFuncOrPromise);
    return true;
  } catch (error) {
    catchFunc && catchFunc(error);
    return false;
  } finally {
    finallyFunc && finallyFunc();
  }
};

export const minMaxCalculate = (data: DataPoints[], key: keyof DataPoints) => {
  return data.reduce(
    (accumulator, item) => {
      let val = item[key] as number;
      if (val > accumulator.max) {
        accumulator.max = val;
      }
      if (val < accumulator.min) {
        accumulator.min = val;
      }
      return accumulator;
    },
    { min: Infinity, max: -Infinity },
  );
};

const allocationsOrder = [
  'stocks',
  'bonds',
  'cash',
  'cash (within funds)',
  'cash (held directly)',
  'funds_cash',
  'other',
];

// sort items just for color matching
export const sortAllocations = (
  allocations: Allocation[] | readonly Allocation[],
  populateZeroValues?: boolean,
) => {
  const sortedAllocations: any = [];
  const sortedAllocationsWithEmptyValues: any = [];

  const allocationSearch = (term: string) =>
    allocations.find((element) => element.name.toLowerCase() === term);

  // reorder the incoming data so they match this order

  allocationsOrder.forEach((item) => {
    const foundItem = allocationSearch(item);
    if (foundItem) {
      sortedAllocations.push(foundItem);
      sortedAllocationsWithEmptyValues.push(foundItem);
    } else if (populateZeroValues && getAllocationType(item.toUpperCase() as AllowedAllocation)) {
      // to show categories with no amount
      sortedAllocationsWithEmptyValues.push({ name: item.toUpperCase(), value: 0 } as Allocation);
    }
  });
  return {
    sortedAllocations,
    sortedAllocationsWithEmptyValues,
    showHoldings: sortedAllocations.some((x) => x.value),
  };
};

export const isNonNullable = <T>(value: any): value is NonNullable<T> => value != null;

export const openLink = async (link: string, showError?: boolean, error?: Function) => {
  try {
    const supported = await Linking.canOpenURL(link);
    if (supported) {
      await Linking.openURL(link);
    }
  } catch (err) {
    const currentRoute = ''
    if (showError) {
      if (!!error) {
        error();
      } else {
        Alert.alert(`Your device cannot open this URL: ${link}`, undefined, [{ text: 'OK' }]);
      }
    }
  }
};

export const noop = () => void 0;
