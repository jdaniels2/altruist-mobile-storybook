import * as validator from '~/utils/validator';

describe('validator', () => {
  it('validatesEmail', () => {
    const emptyEmail = '';
    const invalidEmail = 'asdf.com';
    const validEmail = 'asdf@mail.com';
    expect(validator.validateEmail(emptyEmail)).toBeFalsy();
    expect(validator.validateEmail(invalidEmail)).toBeFalsy();
    expect(validator.validateEmail(validEmail)).toStrictEqual([validEmail]);
  });

  it('validates address', () => {
    const emptyAddress = { description: '', street: '', state: '', city: '', zipCode: '' };
    const partialAddress = { description: '', street: 'asdf', state: '', city: '', zipCode: '' };
    const completeAddress = {
      description: '11111 onzo ave',
      street: '11111 onzo ave',
      city: 'Toronto',
      state: 'CA',
      zipCode: '99999',
    };
    expect(validator.isValidAddress(emptyAddress)).toBe(false);
    expect(validator.isValidAddress(partialAddress)).toBe(false);
    expect(validator.isValidAddress(completeAddress)).toBe(true);
  });

  it('validates address max length', () => {
    const addressMaxLengths = {
      address: 60,
    };
    const completeAddress = {
      description: '11111 onzo ave',
      street: '11111 asdfasdfasdfasdfasdfasdfasdfadsfasd onzo ave',
      city: 'Toronto',
      state: 'CA',
      zipCode: '99999',
    };
    const invalidLengthAddress = {
      description: '11111 onzo ave',
      street: '11111 asdfasdfasdfasdfasdfasdfasdfadsfasdfasdfasdfasdfasdfasdfasdfasdf onzo ave',
      city: 'Toronto',
      state: 'CA',
      zipCode: '99999',
    };
    expect(validator.isValidAddress(invalidLengthAddress, addressMaxLengths)).toBe(false);
    expect(validator.isValidAddress(completeAddress, addressMaxLengths)).toBe(true);
  });

  it('validates business address', () => {
    const emptyAddress = { description: '', addr1: '', state: '', city: '', zipCode: '' };
    const partialAddress = { description: '', addr1: 'asdf', state: '', city: '', zipCode: '' };
    const completeAddress = {
      description: '11111 onzo ave',
      addr1: '11111 onzo ave',
      city: 'Toronto',
      state: 'CA',
      zipCode: '99999',
    };
    expect(validator.isBusinesOrMailingAddressValid(emptyAddress)).toBe(false);
    expect(validator.isBusinesOrMailingAddressValid(partialAddress)).toBe(false);
    expect(validator.isBusinesOrMailingAddressValid(completeAddress)).toBe(true);
  });

  it('validates business address max length', () => {
    const emptyAddress = { description: '', addr1: '', state: '', city: '', zipCode: '' };
    const partialAddress = { description: '', addr1: 'asdf', state: '', city: '', zipCode: '' };
    const completeAddress = {
      description: '11111 onzo ave',
      addr1: '11111 onzo ave',
      city: 'Toronto',
      state: 'CA',
      zipCode: '99999',
    };
    expect(validator.isBusinesOrMailingAddressValid(emptyAddress)).toBe(false);
    expect(validator.isBusinesOrMailingAddressValid(partialAddress)).toBe(false);
    expect(validator.isBusinesOrMailingAddressValid(completeAddress)).toBe(true);
  });

  it('validates business address max length', () => {
    const addressMaxLengths = {
      address: 60,
    };
    const completeAddress = {
      description: '11111 onzo ave',
      addr1: '11111 asdfasdfasdfasdfasdfasdfasdfadsfasd onzo ave',
      city: 'Toronto',
      state: 'CA',
      zipCode: '99999',
    };
    const invalidLengthAddress = {
      description: '11111 onzo ave',
      addr1: '11111 asdfasdfasdfasdfasdfasdfasdfadsfasdfasdfasdfasdfasdfasdfasdfasdf onzo ave',
      city: 'Toronto',
      state: 'CA',
      zipCode: '99999',
    };
    expect(validator.isBusinesOrMailingAddressValid(invalidLengthAddress, addressMaxLengths)).toBe(
      false,
    );
    expect(validator.isBusinesOrMailingAddressValid(completeAddress, addressMaxLengths)).toBe(true);
  });

  it('validates personal Form', () => {
    const emptyForm = { firstName: '', lastName: '', phone: '', email: '', username: '' };
    const partialForm = {
      firstName: 'first',
      lastName: 'last',
      phone: '112',
      email: '',
      username: '',
    };
    const completeForm = {
      email: 'first.last@email.com',
      username: 'first.last@email.com',
      firstName: 'first',
      lastName: 'last',
      phone: '1112223333',
    };

    expect(validator.isValidPersonalForm(emptyForm)).toBe(false);
    expect(validator.isValidPersonalForm(partialForm)).toBe(false);
    expect(validator.isValidPersonalForm(completeForm)).toBe(true);
  });

  it('validates password Form', () => {
    const emptyForm = { currentPassword: '', newPassword: '', confirmPassword: '' };
    const partialForm = {
      currentPassword: 'Password1!',
      newPassword: 'Password1!',
      confirmPassword: 'Password1',
    };
    const completeForm = {
      currentPassword: 'Password1!',
      newPassword: 'Password1!',
      confirmPassword: 'Password1!',
    };

    expect(validator.isValidPasswordForm(emptyForm)).toBe(false);
    expect(validator.isValidPasswordForm(partialForm)).toBe(false);
    expect(validator.isValidPasswordForm(completeForm)).toBe(true);
  });
});
