import { PHONE_MASK, SSN_MASK, TIN_MASK } from '~/constants/constants';
import * as formatter from '~/utils/formatter';

describe('formatter', () => {
  it('formats phone number correctly', () => {
    const emptyPhone = '';
    const completePhoneOne = '111-222-3333';
    const completePhoneTwo = '1112223333';
    const incompletePhoneOne = '111';
    const incompletePhoneTwo = '1111';
    const incompletePhoneThree = '1111111';
    expect(formatter.formatPhoneNumber(emptyPhone)).toBe('');
    expect(formatter.formatPhoneNumber(completePhoneOne)).toBe('(111) 222-3333');
    expect(formatter.formatPhoneNumber(completePhoneTwo)).toBe('(111) 222-3333');
    expect(formatter.formatPhoneNumber(incompletePhoneOne)).toBe('111');
    expect(formatter.formatPhoneNumber(incompletePhoneTwo)).toBe('(111) 1');
    expect(formatter.formatPhoneNumber(incompletePhoneThree)).toBe('(111) 111-1');
  });

  it('returns last four of string', () => {
    const emptyString = '';
    const smallString = 'asdf';
    const largeString = 'aaaaaaaaaaaasdf';
    expect(formatter.lastFour(emptyString)).toBe('');
    expect(formatter.lastFour(smallString)).toBe('asdf');
    expect(formatter.lastFour(largeString)).toBe('asdf');
  });

  it('capitalizes correctly', () => {
    const emptyString = '';
    const smallString = 'asdf';
    const largeString = 'asdf asdf asdf';
    expect(formatter.capitalize(emptyString)).toBe('');
    expect(formatter.capitalize(smallString)).toBe('Asdf');
    expect(formatter.capitalize(largeString)).toBe('Asdf Asdf Asdf');
  });

  it('capitalizes with whitelist correctly', () => {
    const emptyString = '';
    const smallString = 'ira';
    const test0 = 'joe schmoe ira';
    const test1 = 'joe schmoe jtwros';
    expect(formatter.capitalize(emptyString)).toBe('');
    expect(formatter.capitalize(smallString)).toBe('IRA');
    expect(formatter.capitalize(test0)).toBe('Joe Schmoe IRA');
    expect(formatter.capitalize(test1)).toBe('Joe Schmoe JTWROS');
  });

  it('gets user initials', () => {
    const testOne = { firstName: 'joe', lastName: 'schmoe' };

    expect(formatter.getUserInitials(testOne.firstName, testOne.lastName)).toBe('JS');
  });

  it('capitalizeFirstLetter', () => {
    const testOne = 'schedule';

    expect(formatter.capitalizeFirstLetter(testOne)).toBe('Schedule');
  });

  it('formats currency', () => {
    const testOne = 0;
    const testTwo = 1;
    const testThree = 1.1;
    const testFour = 120.123;

    expect(formatter.currency(testOne)).toBe('$0.00');
    expect(formatter.currency(testTwo)).toBe('$1.00');
    expect(formatter.currency(testThree)).toBe('$1.10');
    expect(formatter.currency(testFour)).toBe('$120.12');
  });

  it('formats percents', () => {
    const testOne = 0;
    const testTwo = 1;
    const testThree = 1.1;
    const testFour = 0.123;
    const testFive = 0.12345;
    const testSix = 0.0012345;

    expect(formatter.percent(testOne)).toBe('0.00%');
    expect(formatter.percent(testTwo)).toBe('100.00%');
    expect(formatter.percent(testThree)).toBe('110.00%');
    expect(formatter.percent(testFour)).toBe('12.30%');
    expect(formatter.percent(testFive)).toBe('12.35%');
    expect(formatter.percent(testFour, 2, 5)).toBe('12.30%');
    expect(formatter.percent(testFive, 2, 5)).toBe('12.345%');
    expect(formatter.percent(testSix, 2, 5)).toBe('0.12345%');
  });

  it('decimal', () => {
    const testOne = 0;
    const testTwo = 1;
    const testThree = 1.1;
    const testFour = 0.123;
    const testFive = 0.12345;
    const testSix = 0.0012345;

    expect(formatter.decimal(testOne)).toBe('0');
    expect(formatter.decimal(testTwo)).toBe('1');
    expect(formatter.decimal(testThree)).toBe('1.1');
    expect(formatter.decimal(testFour)).toBe('0.12');
    expect(formatter.decimal(testFive)).toBe('0.12');
    expect(formatter.decimal(testFour, 2, 5)).toBe('0.123');
    expect(formatter.decimal(testFive, 2, 5)).toBe('0.12345');
    expect(formatter.decimal(testSix, 2, 5)).toBe('0.00123');
  });

  it('getAllocationType', () => {
    const testOne = 'STOCKS';
    const testTwo = 'BONDS';
    const testThree = 'CASH';
    const testFour = 'FUNDS_CASH';
    const testFive = 'FixedIncome';
    const testSix = 'OTHER';
    const testSeven = 'asdf';

    expect(formatter.getAllocationType(testOne)).toBe('Equity');
    expect(formatter.getAllocationType(testTwo)).toBe('Fixed income');
    expect(formatter.getAllocationType(testThree)).toBe('Cash held directly');
    expect(formatter.getAllocationType(testFour)).toBe('Cash within funds');
    expect(formatter.getAllocationType(testFive)).toBe('Fixed income');
    expect(formatter.getAllocationType(testSix)).toBe('Other');
    expect(formatter.getAllocationType(testSeven)).toBe('');
  });

  it('calculates percentage of allocation', () => {
    const testOne = { allocations: [{ value: 10 }], val: 1, type: 'string' };
    const testTwo = { allocations: [{ value: 10 }], val: 0, type: 'string' };
    const testThree = { allocations: [{ value: 10 }], val: 1, type: 'number' };
    const testFour = { allocations: [{ value: 5 }, { value: 5 }], val: 1, type: 'number' };

    expect(formatter.calculatePercentage(testOne.allocations, testOne.val, testOne.type)).toBe(
      '10.00%',
    );
    expect(formatter.calculatePercentage(testTwo.allocations, testTwo.val, testTwo.type)).toBe(
      '0.00%',
    );
    expect(
      formatter.calculatePercentage(testThree.allocations, testThree.val, testThree.type),
    ).toBe(0.1);
    expect(formatter.calculatePercentage(testFour.allocations, testFour.val, testFour.type)).toBe(
      0.1,
    );
  });

  it('calculates sum of allocations', () => {
    const testOne = { allocations: [{ value: 10 }] };
    const testTwo = { allocations: [{ value: 5 }, { value: 5 }] };

    expect(formatter.sumAllocations(testOne.allocations)).toBe(10);
    expect(formatter.sumAllocations(testTwo.allocations)).toBe(10);
  });

  it('formats financial institution', () => {
    const testOne = 'APEX_CUSTODIAN';
    const testTwo = 'AMERITRADE';
    const testThree = 'asdf';

    expect(formatter.financialInstitution(testOne)).toBe('Altruist');
    expect(formatter.financialInstitution(testTwo)).toBe('TD Ameritrade');
    expect(formatter.financialInstitution(testThree)).toBe('asdf');
  });

  it('formats account type name', () => {
    const testOne = 'TRADITIONAL BENEFICIARY IRA';
    const testTwo = 'SEP IRA';

    expect(formatter.getAccountTypeName({ accountType: testOne })).toBe('Beneficiary IRA');
    expect(formatter.getAccountTypeName({ accountType: testTwo })).toBe('SEP IRA');
  });

  it('getAccountHolderName', () => {
    const testOne = { firstName: 'Joe', lastName: 'Schmoe' };
    expect(formatter.getAccountHolderName(testOne)).toBe('Joe Schmoe');
    expect(formatter.getAccountHolderName()).toBe('[Account Holder Name]');
  });

  it('splits by capital letter', () => {
    const testOne = 'AsdfAsdfAsdfAsdf';
    expect(formatter.splitByCapitalLetter(testOne)).toBe('Asdf Asdf Asdf Asdf');
  });

  it('formatAddress', () => {
    const address = {
      street: '2000 Onzo ave',
      city: 'Torrance',
      state: 'CA',
      zipCode: '99999',
    };
    expect(formatter.formatAddress(address)).toBe('2000 Onzo ave, Torrance, CA, 99999');
  });

  it('formatMask', () => {
    const testOne = { value: '1234567890', mask: PHONE_MASK };
    const testTwo = { value: '1234567890', mask: SSN_MASK };
    const testThree = { value: '1234567890', mask: TIN_MASK };

    expect(formatter.formatMask(testOne.value, testOne.mask)).toBe('(123) 456-7890');
    expect(formatter.formatMask(testTwo.value, testTwo.mask)).toBe('123-45-6789');
    expect(formatter.formatMask(testThree.value, testThree.mask)).toBe('12 - 3456789');
  });

  it('filterDoubleSpace', () => {
    const testOne = 'asdf  asdf';
    expect(formatter.filterDoubleSpace(testOne)).toBe('asdf asdf');
  });

  it('filterEmojis', () => {
    const testOne = '😊asdf😊';
    expect(formatter.filterEmojis(testOne)).toBe('asdf');
  });

  it('filterSpecialCharacters', () => {
    const testOne = '!@#asdf$%^';
    expect(formatter.filterSpecialCharacters(testOne)).toBe('asdf');
  });

  it('filterNonNumbers', () => {
    const testOne = '123asdf456';
    expect(formatter.filterNonNumbers(testOne)).toBe('123456');
  });

  it('filterNonFloatNumbers', () => {
    const testOne = '123asdf456';
    expect(formatter.filterNonNumbers(testOne)).toBe('123456');
  });

  it('filterNonAscii', () => {
    const testOne = 'קוםasdfקום';
    expect(formatter.filterNonAscii(testOne)).toBe('asdf');
  });

  it('it formats name', () => {
    const testOne = 'joe_schmoe';
    expect(formatter.formatName(testOne)).toBe('Joe Schmoe');
  });

  it('isUnknown', () => {
    const testOne = 'UNKNOWN';
    const testTwo = 'asdf';
    expect(formatter.isUnknown(testOne)).toBe(true);
    expect(formatter.isUnknown()).toBe(true);
    expect(formatter.isUnknown(testTwo)).toBe(false);
  });

  it('formatAccountNumber', () => {
    const testOne = 'UNKNOWN';
    const testTwo = 'asdf1234';
    expect(formatter.formatAccountNumber(testOne)).toBe('');
    expect(formatter.formatAccountNumber()).toBe('');
    expect(formatter.formatAccountNumber(testTwo)).toBe('1234');
  });

  it('masked', () => {
    const testOne = 'joe_schmoe';
    expect(formatter.masked(testOne)).toBe(__DEV__ ? 'joe_schmoe' : '***');
  });

  it('formatFundingAccountName', () => {
    const testOne = { institutionName: 'Altruist', accountNumber: '1234', accountType: 'IRA' };
    const testTwo = { institutionName: undefined, accountNumber: '1234', accountType: 'IRA' };
    const testThree = {
      institutionName: undefined,
      accountNumber: '1234',
      accountType: 'IRA',
      authenticationStatus: 'PENDING',
    };
    const testFour = {
      institutionName: 'Altruist',
      accountNumber: '1234',
      accountType: 'IRA',
      authenticationStatus: 'PENDING_MANUAL',
    };
    expect(formatter.formatFundingAccountName(testOne)).toBe('Altruist IRA (1234)');
    expect(formatter.formatFundingAccountName(testTwo)).toBe('Bank IRA (1234)');
    expect(formatter.formatFundingAccountName(testThree)).toBe('Bank IRA ');
    expect(formatter.formatFundingAccountName(testFour)).toBe('Altruist IRA ');
  });

  it('formatRowTitle', () => {
    const testOne = { financialAccountName: 'asdfasdfasdfasdfasdfasdfasdfasdf' };
    expect(formatter.formatRowTitle(testOne)).toBe('asdfasdfasdfasdfasdfasd...');
    expect(formatter.formatRowTitle()).toBe('');
  });
});
