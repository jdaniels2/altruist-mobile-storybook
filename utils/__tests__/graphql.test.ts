import { removeAllTypenames, removeAllTypenamesNoMutate } from '~/utils/graphql';

describe('removeAllTypenames', () => {
  it('removes all typenames', () => {
    const values = {
      __typename: '1',
      inner: {
        __typename: '2',
        name: 'aa',
        address: {
          __typename: '3',
          value: 'some',
        },
        innerArray: [
          {
            __typename: 123,
            name: 'test',
          },
        ],
      },
      array: ['1', { __typename: '4', test: null }],
    };

    removeAllTypenames(values);

    expect(values).toEqual({
      inner: {
        name: 'aa',
        address: {
          value: 'some',
        },
        innerArray: [
          {
            name: 'test',
          },
        ],
      },
      array: ['1', { test: null }],
    });
  });
});

describe('removeAllTypenamesNoMutate', () => {
  it('removes all typenames', () => {
    const values = {
      __typename: '1',
      inner: {
        __typename: '2',
        name: 'aa',
        address: {
          __typename: '3',
          value: 'some',
        },
        innerArray: [
          {
            __typename: 123,
            name: 'test',
          },
        ],
      },
      array: ['1', { __typename: '4', test: null }],
    };
    const valueCopy = JSON.parse(JSON.stringify(values));

    const updatedValues = removeAllTypenamesNoMutate(valueCopy);

    expect(updatedValues).toEqual({
      inner: {
        name: 'aa',
        address: {
          value: 'some',
        },
        innerArray: [
          {
            name: 'test',
          },
        ],
      },
      array: ['1', { test: null }],
    });
    // make sure values isn't mutated
    expect(valueCopy).toMatchObject(values);
  });
});
