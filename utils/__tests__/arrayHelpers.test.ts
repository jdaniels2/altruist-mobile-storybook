import { getUniqueValues } from '~/utils/arrayHelpers';

describe('arrayHelpers', () => {
  it('should return unique values', () => {
    const array = ['a', 'b', 'c', 'a', 'b', 'c'];
    const expected = ['a', 'b', 'c'];
    expect(getUniqueValues(array)).toEqual(expected);
  });
});
