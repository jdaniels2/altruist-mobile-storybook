import ReactNativeBiometrics, { BiometryType as RNBiometryType } from 'react-native-biometrics';

export const rnBiometrics = new ReactNativeBiometrics();
export type BiometryType = RNBiometryType;
export default ReactNativeBiometrics;
