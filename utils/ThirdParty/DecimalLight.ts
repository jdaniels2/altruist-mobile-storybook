/**
 * This file is meant to act as a wrapper for a third party decimal library.
 * We're currently using `decimal.js-light`, but we may want to change that in the future.
 * All exported functions should accept `number` primitives as inputs and return `number` primitives.
 * The `Decimal` type or anything specific to the 3rd party library should only be used in this file.
 */

import Decimal from 'decimal.js-light';
// docs - http://mikemcl.github.io/decimal.js-light/

/** Adds two numbers together using decimal math (not floating point)
 * @example
 *   const exact = add(0.01, 0.05); // 0.06
 *
 *   //instead of
 *   const notExact = 0.01 + 0.05; // 0.060000000000000005
 */
export const add = (a: number, b: number) => {
  const A = new Decimal(a);
  const B = new Decimal(b);
  const result = A.plus(B);
  return result.toNumber();
};

/** Rounds a number to the default Decimal rounding = ROUND_HALF_UP
 * @example
 *  const rounded = roundToInteger(32.04); // 32
 *  const rounded2 = roundToInteger(5.5); // 6
 */
export const roundToInteger = (numberToRound: number) => {
  return round(numberToRound, 0);
};

/** Rounds a number to the default Decimal rounding = ROUND_HALF_UP
 * @example
 *  const rounded = round(32.043, 2); // 32.04
 *  const rounded2 = round(5.57, 1); // 5.6
 */
export const round = (numberToRound: number, decimalPlaces: number) => {
  const decimalNumber = new Decimal(numberToRound);
  return decimalNumber.toDecimalPlaces(decimalPlaces).toNumber();
};

/** Subtracts the second parameter from the first using decimal math (not floating point)
 * @example
 *   const exact = subtract(0.94, 0.01); // 0.93
 *
 *   //instead of
 *   const notExact = 0.94 - 0.01; // 0.9299999999999999
 */
export const subtract = (a: number, b: number) => {
  const A = new Decimal(a);
  const B = new Decimal(b);
  const result = A.minus(B);
  return result.toNumber();
};

/** Multiplies two numbers together using decimal math (not floating point)
 * @example
 *   const exact = multiply(0.07, 100); // 7
 *
 *   //instead of
 *   const notExact = 0.07 * 100; // 7.000000000000001
 */
export const multiply = (a: number, b: number) => {
  const A = new Decimal(a);
  const B = new Decimal(b);
  const result = A.times(B);
  return result.toNumber();
};

/** Divides the first parameter by the second parameter using decimal math (not floating point)
 * @example
 *   const exact = divide(0.45, 100); // 0.0045
 *
 *   //instead of
 *   const notExact = 0.45 / 100; // 0.0045000000000000005
 */
export const divide = (a: number, b: number) => {
  const A = new Decimal(a);
  const B = new Decimal(b);
  const result = A.dividedBy(B);
  return result.toNumber();
};
