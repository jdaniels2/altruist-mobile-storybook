import { Dimensions, TextStyle } from 'react-native';

import { AktivGroteskMedium, AktivGroteskRegular } from '~/constants/fonts';

const { height } = Dimensions.get('window');

// height of phone in figma design
const defaultHeight = 812;

export const responsivePixelValue = (pixels: number) =>
  Math.round((pixels * height) / defaultHeight);

export const removeSpecialCharacters = (str?: string) => (str ? str.replace(/[^\w\s]/gi, '') : '');

export const removeDigitsFromString = (str?: string) => (str ? str.replace(/\d+/g, '') : '');

export const easing = {
  standard: 'bezier(0.4, 0, 0.2, 1)',
  decelerate: 'bezier(0, 0, 0.2, 1)',
  accelerate: 'bezier(0.4, 0, 1, 1)',
  loop: 'bezier(0.86, 0.38, 0.24, 0.61)',
};

// bug with react native where fontWeight doesn't work with android
// https://github.com/facebook/react-native/issues/26193
export const font = (
  fontSize: TextStyle['fontSize'] = 16,
  lineHeight?: TextStyle['lineHeight'],
  letterSpacing?: TextStyle['letterSpacing'],
  fontWeight?: TextStyle['fontWeight'],
  fontFamily: TextStyle['fontFamily'] = AktivGroteskRegular,
) => {
  const style = {
    fontSize,
    lineHeight,
    letterSpacing,
    fontWeight,
    fontFamily:
      fontWeight &&
      fontWeight !== 'normal' &&
      (fontWeight === 'bold' || +fontWeight > 400) &&
      fontFamily === AktivGroteskRegular
        ? AktivGroteskMedium
        : fontFamily,
  };
  return style;
};
