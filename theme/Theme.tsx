import { AktivGroteskRegular, AktivGroteskMedium } from '~/constants/fonts';
import figmaTheme from '~/theme/figma/theme.json';

const mixins = {
  font: (
    sizeValue = 16,
    lineHeight?: number,
    letterSpacing?: number,
    fontWeight = 400,
    fontFamily = AktivGroteskRegular,
  ) => {
    let style = ``;
    if (sizeValue) {
      style = `font-size: ${sizeValue}px;`;
    }
    if (lineHeight) {
      style = `${style} line-height: ${lineHeight}px;`;
    }
    if (letterSpacing) {
      style = `${style} letter-spacing: ${letterSpacing}px;`;
    }
    if (fontWeight) {
      style = `${style} font-weight: ${fontWeight};`;
    }
    if (fontFamily) {
      // adds bold font for android
      if (fontWeight >= 500 && fontFamily === AktivGroteskRegular) {
        style = `${style} font-family: ${AktivGroteskMedium};`;
      } else {
        style = `${style} font-family: ${fontFamily};`;
      }
    }

    return style;
  },
};
export const theme = Object.assign(figmaTheme, mixins);

export const HelloSignTheme =
  'white_labeling_options={"header_background_color":"#F6F6F6","text_color1":"#222729","text_color2":"#474E51","link_color":"#3190E9","primary_button_color":"#00946A","primary_button_text_color":"#FFFFFF","primary_button_color_hover":"#008D65","primary_button_text_color_hover":"#FFFFFF","secondary_button_color":"#454A4B","secondary_button_text_color":"#FFFFFF","secondary_button_color_hover":"#3E4344","secondary_button_text_color_hover":"#FFFFFF","legal_version":"terms2"}';
