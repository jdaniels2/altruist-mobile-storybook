Altruist atoms/molecules in StoryBook

# getting started

First run

```
nvm use
```

We must be on Node 18 to install Storybook.

To get all the dependencies run

```
yarn install
```

To run on the web use

```
yarn storybook: web
```

To run android

```
yarn start
```

To run on ios

```
cd ios
pod install
```

Then open ios -> react_native_storybook_starter.xcworkspace in xcode, and run build.

Then, in the terminal run
```
yarn start
```

in another terminal run

```
yarn ios
```

or

```
yarn android
```

If you add new stories on the native (ondevice version) you either need to have the watcher running or run the stories loader

To update the stories one time

```
yarn update-stories
```
