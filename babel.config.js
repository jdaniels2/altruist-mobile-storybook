module.exports = {
  presets: ['module:@react-native/babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        alias: {
          '~': './',
        },
      },
    ],
    'transform-inline-environment-variables',
    ['babel-plugin-react-docgen-typescript', {exclude: 'node_modules'}],
  ],
};
